package id.co.iconpln.ecatalog.db.service;

import id.co.iconpln.ecatalog.db.domain.AuthUser;

/**
 * Created by LATIF on 12/19/2014.
 */
public interface AuthUserService {
    void save(AuthUser user);
}
