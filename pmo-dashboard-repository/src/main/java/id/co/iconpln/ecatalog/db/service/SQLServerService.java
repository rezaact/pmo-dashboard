package id.co.iconpln.ecatalog.db.service;

import java.util.List;
import java.util.Map;

/**
 * Created by rezafit on 02/12/2016.
 */
public interface SQLServerService {

    public String[] testConnection(Map<String, Object> pTableName);
}
