package id.co.iconpln.ecatalog.db.service.impl;

import id.co.iconpln.ecatalog.db.service.SQLServerService;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by rezafit on 02/12/2016.
 */
@Repository
public class SQLServerServiceImpl implements SQLServerService{

    @Override
    public String[] testConnection(Map<String, Object> pTableName) {
        //System.out.println("------test connection --------");

        String[] sqlInsert = null;
        String selectQuery = createSQLServerSelect(pTableName);
        String insertQuery = createOracleInsert(pTableName);
        BasicDataSource dataSource = createDataSource();
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        List<Map<String, Object>> listMap = new ArrayList<>();
        try{
            Map<String, String> param = new HashedMap();
            listMap = jdbcTemplate.queryForList(selectQuery,param);
            System.out.println("size = " + listMap.size());
            if(listMap.size()>0){
                sqlInsert  = createOracleInsertData(insertQuery,  listMap);
            }

            //System.out.println("success");
        }catch (Exception e){
            e.printStackTrace();
        }

        return sqlInsert;
    }

    private BasicDataSource createDataSource(){
        final String DB_USERNAME = "pmosqladmin";
        final String DB_PASSWORD = "W0rk@PLNPUSAT";
        String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        /*String DB_CONNECTION = "jdbc:sqlserver://10.1.8.201:1433;databaseName=PMODB";*/
        String DB_CONNECTION = "jdbc:sqlserver://10.1.8.162:1433;databaseName=PMODB";

        /*final String DB_USERNAME = "Pusat";
        final String DB_PASSWORD = "P@ssw0rd";
        String DB_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        String DB_CONNECTION = "jdbc:sqlserver://10.1.8.153:1433;databaseName=icon.bod.dashboard";*/

        BasicDataSource datasource = new BasicDataSource();
        datasource.setUsername(DB_USERNAME);
        datasource.setPassword(DB_PASSWORD);
        datasource.setDriverClassName(DB_DRIVER);
        datasource.setUrl(DB_CONNECTION);
        datasource.setInitialSize(1);
        datasource.setMinIdle(1);
        datasource.setMaxIdle(2);
        datasource.setMaxActive(2);
        datasource.setMaxWait(40000);
        datasource.setTestOnBorrow(true);
        datasource.setValidationQuery("SELECT 1");
        datasource.setDefaultAutoCommit(true);

        return datasource;
    }

    private JdbcTemplate getJdbcTemplate(BasicDataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    private String createSQLServerSelect(Map<String, Object> mapTable){
        String tableName = mapTable.get("SQLSERV_TBL_NAME").toString();
        List<Map<String, Object>> listColumn = (List<Map<String, Object>>) mapTable.get("listColumn");
        //String retValue = "select TOP 10 ";
        String retValue = "select ";
        for(Map mapColumn : listColumn ){
            String colType = mapColumn.get("SQLSERV_COL_TYPE").toString();
            if(colType.equals("datetime")){
                retValue +=   " convert(NVARCHAR, " + mapColumn.get("SQLSERV_COL_NAME") + " , 20) " + mapColumn.get("SQLSERV_COL_NAME") + "_date,";
            }else if(colType.equals("bit")){
                //retValue +=  mapColumn.get("SQLSERV_COL_NAME") + ",";
                retValue +=  "\"" + mapColumn.get("SQLSERV_COL_NAME") + "\" \"" + mapColumn.get("SQLSERV_COL_NAME") + "_bit\" ,";
            }else if(colType.equals("float")){
                //retValue +=  mapColumn.get("SQLSERV_COL_NAME") + ",";
                retValue +=  "\"" + mapColumn.get("SQLSERV_COL_NAME") + "\" \"" + mapColumn.get("SQLSERV_COL_NAME") + "_flt\" ,";
            }else if(colType.equals("decimal")){
                //retValue +=  mapColumn.get("SQLSERV_COL_NAME") + ",";
                retValue +=  "\"" + mapColumn.get("SQLSERV_COL_NAME") + "\" \"" + mapColumn.get("SQLSERV_COL_NAME") + "_dcml\" ,";
            }else{
                retValue +=  "\"" + mapColumn.get("SQLSERV_COL_NAME") + "\",";
            }

        }
        retValue = retValue.substring(0, retValue.length()-1);
        retValue = retValue + " from " + tableName;
        //retValue = retValue + " OFFSET 10 ROWS FETCH NEXT 10 ROWS ONLY";
        return retValue;
    }

    private String createOracleInsert(Map<String, Object> mapTable){
        String tableName = mapTable.get("TMP_ORCL_TBL_NAME").toString();
        List<Map<String, Object>> listColumn = (List<Map<String, Object>>) mapTable.get("listColumn");
        String retValue = "Insert into " + tableName + " (";
        for(Map mapColumn : listColumn ){
            retValue += mapColumn.get("ORACLE_COL_NAME") + ",";
        }
        retValue = retValue.substring(0, retValue.length()-1) + ")";
        return retValue;
    }

    private String[] createOracleInsertData(String sqlInsert,  List<Map<String, Object>> listData ){
        String[] retValue = new String[listData.size()];
        Set<String> keysets = listData.get(0).keySet();

        int idx = 0;
        for(Map<String, Object> mapData : listData ){
            String sqlInsertData = sqlInsert + " values ";
            sqlInsertData += "(";
            for(String keyset : keysets){
                String dataVal = null;
                try {
                    if (keyset.contains("_date")) {
                        dataVal = "to_date('" + mapData.get(keyset).toString() + "', 'yyyy-mm-dd hh24:mi:ss'),";
                    } else if (keyset.contains("_bit")) {
                        String tmpBit = mapData.get(keyset).toString();
                        if (tmpBit.equals("false")) {
                            dataVal = "'N',";
                        } else {
                            dataVal = "'Y',";
                        }
                    }else if(keyset.contains("_flt") || keyset.contains("_dcml")){
                        String tmpBit = mapData.get(keyset).toString();
                        dataVal = "'" + tmpBit.replace(".",",") + "',";
                    }else{
                        dataVal = "'" +  mapData.get(keyset).toString() + "',";
                    }
                }catch (Exception e){
                    dataVal = "null,";
                }

                sqlInsertData += dataVal ;
            }
            sqlInsertData = sqlInsertData.substring(0, sqlInsertData.length()-1);
            sqlInsertData += ")";

            retValue[idx] = sqlInsertData;
            idx++;
        }

        return retValue;
    }
}
