package id.co.iconpln.ecatalog.db.service.impl;

import id.co.iconpln.ecatalog.db.domain.AuthRole;
import id.co.iconpln.ecatalog.db.domain.AuthUser;
import id.co.iconpln.ecatalog.db.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class SecurityServiceImpl implements SecurityService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Map<String, Object> map = jdbcTemplate.queryForMap("" +
                        "SELECT \n" +
                        "u.ID_USER, u.USERNAME, u.NAMA_LENGKAP, u.NO_TELPON, u.ALAMAT_EMAIL, u.PASS, u.BAHASA, u.AKTIF, u.ID_GAMBAR, u.NIP, u.UNIT, \n" +
                        "r.ID_ROLE, r.AUTHORITY, r.NAMA_ROLE, r.DESKRIPSI, r.STATUS_AKTIF\n" +
                        "FROM TB_MASTER_USER u LEFT JOIN TB_MASTER_ROLE r ON u.ID_ROLE = r.ID_ROLE \n" +
                        "WHERE u.USERNAME = ?",
                s);
        if (map == null) {
            throw new UsernameNotFoundException("User dengan username: '" + s + "' tidak ditemukan");
        }

        AuthRole role = new AuthRole();
        role.setId(((Number) map.get("ID_ROLE")).longValue());
        role.setNama((String) map.get("NAMA_ROLE"));
        role.setAuthority((String) map.get("AUTHORITY"));
        role.setStatusAktif(((Number) map.get("STATUS_AKTIF")).longValue() == 1l ? true : false);

        AuthUser user = new AuthUser();
        user.setId(((Number) map.get("ID_USER")).longValue());
        user.setUsername((String) map.get("USERNAME"));
        user.setNamaLengkap((String) map.get("NAMA_LENGKAP"));
        user.setNip((String) map.get("NIP"));
        user.setUnit((String) map.get("UNIT"));
        user.setNomorTelp((String) map.get("NO_TELPON"));
        user.setEmail((String) map.get("ALAMAT_EMAIL"));
        user.setPassword((String) map.get("PASS"));
        user.setBahasa((String) map.get("BAHASA"));
        //user.setGambar(map.get("ID_GAMBAR") != null ? ((Number) map.get("ID_GAMBAR")).longValue() : null);
        user.setAktif(((Number) map.get("AKTIF")).longValue() == 1l ? true : false);
        user.setRole(role);

        return user;
    }
}
