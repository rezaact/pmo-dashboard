package id.iconpln.ecatalog.service;

/**
 * Created by rezafit on 02/12/2016.
 */
public interface SchedulerService {

    public void start() throws Exception;

    public void stop() throws Exception;
}
