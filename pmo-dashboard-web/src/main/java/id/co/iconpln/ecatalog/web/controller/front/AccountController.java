package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.service.impl.GmailService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.eclipse.jdt.internal.core.SourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import sun.text.CollatorUtilities;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/account")
public class AccountController {


    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    PlsqlService plsqlService;

    @Autowired
    private GmailService gmailService;

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(){
        //System.out.println("disini logoutnya");
        return "app.front.account.login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(ModelMap modelMap){
        modelMap.put("label", "create");
        return "app.front.account.register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String createUser(ModelMap modelMap, HttpServletRequest request){

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

//        for (String keyName : paramRequest.keySet()) {a
//            params.put(keyName, paramRequest.get(keyName)[0]);
//        }
        params.put("prm_nama_lengkap",  paramRequest.get("prm_nama_lengkap")[0]);
        params.put("prm_nip",  paramRequest.get("prm_nip")[0]);
        params.put("prm_email",  paramRequest.get("prm_email")[0]);
        params.put("prm_no_telp",  paramRequest.get("prm_no_telp")[0]);
        params.put("prm_unit",  paramRequest.get("prm_unit")[0]);
        params.put("prm_username",  paramRequest.get("prm_username")[0]);
        params.put("prm_password", passwordEncoder.encode(paramRequest.get("prm_password")[0]));
        params.put("prm_status",  2);
        params.put("prm_id_role",  null);

        Long id = plsqlService.save("p_master_user", params);

        Map<String, Object> info_user= plsqlService.findOne("P_MASTER_USER", id);
        String mail=String.valueOf(info_user.get("ALAMAT_EMAIL"));
        String subject="E-Catalog | Informasi pendaftaran Akun Baru";
        String mailContent="Kepada Yth. " +info_user.get("NAMA_LENGKAP")+ "\n Anda berhasil melakukan pendaftaran akun pada aplikasi E-Catalog Pusharlis,\n " +
                "dengan username : "  + info_user.get("USERNAME") + " pada tanggal : "+info_user.get("TGL_REGISTER") +".\n" +
                "Selanjutnya harap tunggu proses persetujuan oleh administrator.\n Terima Kasih \n " +
                "TTD\n PT. PLN (Persero) | Pusat Pemeliharaan Ketenagalistrikan";

        try {
            gmailService.sendEmail(mail,subject,mailContent);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(id != 0){
            modelMap.put("status", "sucess");
        }else{
            modelMap.put("status", "gagal");
        }
        return  "app.front.account.register";
    }

    @RequestMapping(value = "/checker", method = RequestMethod.POST)
    @ResponseBody
    public String checker(HttpServletRequest request, ModelMap modelMap) {

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "checker");

        String prm_check;
        if (paramRequest.get("prm_field")[0].equals("username")){
            prm_check=paramRequest.get("prm_username")[0];
        }else if(paramRequest.get("prm_field")[0].equals("email")){
            prm_check=paramRequest.get("prm_email")[0];
        }else{
            prm_check=paramRequest.get("prm_nip")[0];
        }
        paramSourceIn.put("prm_check", prm_check);
        paramSourceIn.put("prm_field", paramRequest.get("prm_field")[0]);

        String retValue = plsqlService.save(paramSourceIn);

        if (retValue.equals("false")){
            if (paramRequest.get("prm_field")[0].equals("email")){
                retValue="Email ini sudah digunakan";
            }
            if (paramRequest.get("prm_field")[0].equals("nip")){
                retValue="NIP ini sudah digunakan";
            }
            if (paramRequest.get("prm_field")[0].equals("username")){
                retValue="Username ini sudah digunakan";
            }
        }

        // Isi kode form untuk menghapus data
        return retValue;
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String formUpdate(ModelMap modelMap, HttpServletRequest request){
        // Isi kode form untuk mengedit data lama
        System.out.println(UserUtil.getAuthUser().getId());
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", UserUtil.getAuthUser().getId());
        modelMap.addAllAttributes(data);

        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
//        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("label", "update");
//        modelMap.put("listRole", listRole);
        modelMap.put("authUser", UserUtil.getAuthUser());
        request.getSession().setAttribute("prevPage", "/account/profile");
        return "app.front.account.profile";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
        paramSourceIn.put("prm_id_user",id);
        Long retValue = plsqlService.update("p_master_user", paramSourceIn);

        if(retValue  != 0){
            modelMap.put("save", true);
        }else{
            modelMap.put("save", false);
        }

        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/account/profile";
    }

    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public String password(HttpServletRequest request, ModelMap modelMap) {
        modelMap.put("authUser", UserUtil.getAuthUser());
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        //check old password first
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", UserUtil.getAuthUser().getId());
        String oldPassword = paramRequest.get("prm_old_password")[0];

        if (! passwordEncoder.matches(oldPassword, data.get("pass").toString())){
            modelMap.put("status_update_pass", false);
            return "redirect:/account/profile";
        }


        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "change_password");
        paramSourceIn.put("prm_id_user", paramRequest.get("prm_id_user")[0]);
        paramSourceIn.put("prm_password", passwordEncoder.encode(paramRequest.get("prm_password")[0]));

        String retValue = plsqlService.save(paramSourceIn);

        if (retValue.equals('0')) {
            modelMap.put("status_update_pass", false);
        } else {
            modelMap.put("status_update_pass", true);
        }
        // Isi kode form untuk menghapus data
        return "redirect:/account/profile";
    }

    //proses simpan file
    @RequestMapping(value = "/simpanPhoto", method = RequestMethod.POST)
//    @ResponseBody
    public String simpanPhoto(MultipartHttpServletRequest request){

//        Map<String,Object> retValue = new HashMap<>();

//        try {

        MultipartFile multipartFile = request.getFile("PRM_PHOTO");

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_MASTER_USER");
        paramSourceIn.put("func_name", "UPLOAD_FOTO");
        paramSourceIn.put("param_count", 4);

        //set prm_nomor_produk
        Map<String, Object> prm_id_user = new HashMap<>();
        prm_id_user.put("index", 1);
        prm_id_user.put("type", AppUtil.ECAT_STRING);
        prm_id_user.put("content", request.getParameter("PRM_ID_USER"));
        paramSourceIn.put("prm_id_user", prm_id_user);

        //set prm_name
        Map<String, Object> prm_name = new HashMap<>();
        prm_name.put("index", 2);
        prm_name.put("type", AppUtil.ECAT_STRING);
        prm_name.put("content", multipartFile.getOriginalFilename());
        paramSourceIn.put("prm_name", prm_name);

        //set prm_name
        Map<String, Object> prm_size = new HashMap<>();
        prm_size.put("index", 3);
        prm_size.put("type", AppUtil.ECAT_STRING);
        prm_size.put("content", multipartFile.getSize());
        paramSourceIn.put("prm_size", prm_size);
        try {

            File tempUserPhoto = File.createTempFile("file_photo", ".jpg");
            request.getFile("PRM_PHOTO").transferTo(tempUserPhoto);
            Map<String, Object> mapUserPhoto= new HashMap<>();
            mapUserPhoto.put("index", 4);
            mapUserPhoto.put("type", AppUtil.ECAT_FILE);
            mapUserPhoto.put("content", tempUserPhoto);
            paramSourceIn.put("prm_photo", mapUserPhoto);

        }catch (Exception e){
            e.printStackTrace();
        }

        String retValue = plsqlService.saveWithFile(paramSourceIn);

        return "redirect:/account/update?id="+request.getParameter("PRM_ID_USER");
    }
    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");

        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("listRole", listRole);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.account.profile";
    }

}
