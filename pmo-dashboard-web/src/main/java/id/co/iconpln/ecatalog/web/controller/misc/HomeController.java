package id.co.iconpln.ecatalog.web.controller.misc;

import id.co.iconpln.ecatalog.db.domain.AuthUser;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by simone on 09/06/15.
 */

@Controller
@RequestMapping("/home")
public class HomeController {
    @RequestMapping(value = "/")
    public String home(ModelMap modelMap){

            return "redirect:/product/product-list";

    }
}
