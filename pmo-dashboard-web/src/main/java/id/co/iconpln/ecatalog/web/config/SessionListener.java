package id.co.iconpln.ecatalog.web.config;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rezafit on 01/02/2017.
 */
@Service
public class SessionListener implements HttpSessionListener {

    @Autowired
    PlsqlService plsqlService;

    @Override
    public void sessionCreated(HttpSessionEvent event) {
        event.getSession().setMaxInactiveInterval(1800);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        Map<String, Object> userLog = new HashMap<>();
        userLog.put("pkg_name", "P_LOG_USER");
        userLog.put("func_name", "user_logout");

        userLog.put("p_session_id", event.getSession().getId());
        userLog.put("p_logout_method", "1");

//        String result = plsqlService.save(userLog);

    }
}
