package id.co.iconpln.ecatalog.web.controller.admin.reporting;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/reporting/customer")
public class ReportingCustomerAdminController {
    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.workshop.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create( ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long retValue = plsqlService.save("p_workshop",params);
        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }
        // Isi kode untuk menyimpan data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.reporting.customer.list";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.workshop.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.PUT)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
//        paramSourceIn.put("prm_id_workshop", request.getParameter("id"));

        Long retValue = plsqlService.update("p_workshop",paramSourceIn);

        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.reporting.customer.list";
    }

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") long id, ModelMap modelMap){
        //get default data
        Map<String, Object>  paramSourceIn = new HashMap<>();

        Map<String, Object> detailWorkshop = plsqlService.findOne("p_workshop",paramSourceIn);
        modelMap.put("detailWorkshop", detailWorkshop);
        // Isi kode form untuk melihat data detail
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.reporting.customer.detail";
    }

    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String findAll(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap)
    {
        //get default data
//        Map<String, Object>  params= new HashMap<>();
//        params.put("prm_start", start);
//        params.put("prm_length", length);
//        params.put("prm_search", " ");
//
//        List<Map<String, Object>> listWorkshop = plsqlService.findAll("p_workshop",params);
//        modelMap.put("listWorkshop", listWorkshop);
        // Pengambilan semua data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.reporting.customer.list";
    }

    // Menampilkan halaman seluruh data menggunakan json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public @ResponseBody List findJsonAll(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap)
    {
        //get default data
        Map<String, Object>  paramSourceIn = new HashMap<>();

        List<Map<String, Object>> listWorkshop = plsqlService.findAll("p_workshop",paramSourceIn);
//        modelMap.put("listWorkshop", listWorkshop);
        // pengambilan data format json
        return listWorkshop;
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap){

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_workshop",id);
        Long retValue = plsqlService.delete("p_workshop",paramSourceIn);
        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }

        // Isi kode form untuk menghapus data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.workshop.form";
    }

}

