package id.co.iconpln.ecatalog.web.controller.admin.setting;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping ("/admin/setting/service")
public class SettingServiceAdminController {
    @Autowired
    PlsqlService plsqlService;

    //Menampilkan Seluruh Data
    @RequestMapping(method = RequestMethod.GET)
    public String findAll(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap  )
    {
        //get default data
        Map<String, Object> params = new HashMap<>();

        params.put("prm_start",start);
        params.put("prm_length",length);
        params.put("prm_seacrh"," ");
        List<Map<String, Object>> listService = plsqlService.findAll("p_service",params);
        modelMap.put("listService",listService);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.setting.service.list";
    }

    //Menampilkan halaman Create
    @RequestMapping(value ="/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        modelMap.put("label","create");
        return "app.admin.setting.service.form";
    }
    //Proses penyimpanan Data
    @RequestMapping(method = RequestMethod.POST)
    public String create(ModelMap modelMap, HttpServletRequest request){

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

//        for (String keyName : paramRequest.keySet()){
//            params.put(keyName, paramRequest.get(keyName)[0]);
//        }

        params.put("prm_judul", paramRequest.get("prm_judul")[0]);
        params.put("prm_deskripsi",paramRequest.get("prm_deskripsi")[0]);

        Long id = plsqlService.save("p_service",params);
        if (id !=0){
            modelMap.put("status","success");
        }else{
            modelMap.put("status","gagal");
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/setting/service";

    }
    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("p_service", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.setting.service.form";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("p_service", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.setting.service.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }

        Long retValue = plsqlService.update("p_service", paramSourceIn);

        if (retValue > 0) {
            modelMap.put("message", "Data berhasil di simpan");
        } else {
            modelMap.put("message", "Data gagal di simpan");
        }
        // Isi kode untuk menyimpan hasil editan data
        return "redirect:/admin/setting/service";
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_service", id);
        Long retValue = plsqlService.delete("p_service", paramSourceIn);
        if (retValue > 0) {
            modelMap.put("delete", true);
        } else {
            modelMap.put("delete", false);
        }

        // Isi kode form untuk menghapus data
        return "redirect:/admin/setting/service";
    }

}

