package id.co.iconpln.ecatalog.web.controller.admin;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.db.service.SQLServerService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {

    @Autowired
    PlsqlService plsqlService;

    @Autowired
    SQLServerService sqlServerService;

    @RequestMapping(method = RequestMethod.GET)
    public String dashboard(ModelMap modelMap){

        //get total project
        Map<String, Object> paramIn = new HashMap<>();
        //Long totalProject = plsqlService.countAll("P_PRODUCT", paramIn);
        Long totalProject = 10L;
        modelMap.put("totalProject",totalProject);

        //get total RAB
        Map<String, String> paramIn2 = new HashMap<>();
        paramIn2.put("pkg_name", "P_DAHSBOARD");
        paramIn2.put("func_name", "get_total_rab");
        //Map totalRAB = plsqlService.findOne(paramIn2);
        modelMap.put("totalRAB", null);

//        Long totalRAB = plsqlService.countAll("P_DAHSBOARD", paramIn);
//        modelMap.put("totalRAB",totalRAB);


        //get total user approval (1)
        //Long totalUserApproval = plsqlService.countWhere("P_MASTER_USER", 1);
        Long totalUserApproval = 3L;
        modelMap.put("totalUserApproval",totalUserApproval);

        //get total user pending (2)
        //Long totalUserPending = plsqlService.countWhere("P_MASTER_USER", 2);
        Long totalUserPending = 6L;
        modelMap.put("totalUserPending",totalUserPending);


        //get TOP 5 Produk by Rate
        paramIn2 = new HashMap<>();
        paramIn2.put("pkg_name", "P_DAHSBOARD");
        paramIn2.put("func_name", "get_top5_produk_by_rate");
        //List<Map<String, Object>> listTop5ProductByRate = plsqlService.findAll(paramIn2);
        modelMap.put("listTop5ProductByRate", null);


        //get Counter HIT
        paramIn2 = new HashMap<>();
        paramIn2.put("pkg_name", "P_DAHSBOARD");
        paramIn2.put("func_name", "get_counter_hit");
        //List<Map<String, Object>> listCounterHit = plsqlService.findAll(paramIn2);
        modelMap.put("listCounterHit", null);

        //get datatable RAB
        long pageLength = 5; //panjang halaman
        //ambil data dari database
        //Map dataMap = plsqlService.find("P_PRODUCT", 0, pageLength, "NOMOR_PRODUK", "ASC","");
        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        //modelMap.addAllAttributes(null);
        //modelMap.addAttribute("length", pageLength);

        //modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.dashboard";
    }

    @RequestMapping(value = "/newDS", method = RequestMethod.GET)
    @ResponseBody
    public String testNewDatasource(){
        //sqlServerService.testConnection();
        return "test";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/rab-json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            //@RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_FIND_NILAI_RAB", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }
}
