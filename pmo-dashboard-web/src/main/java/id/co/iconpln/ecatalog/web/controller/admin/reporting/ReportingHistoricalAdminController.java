package id.co.iconpln.ecatalog.web.controller.admin.reporting;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.*;

@Controller
@RequestMapping("/admin/reporting/historikal-produk")
public class ReportingHistoricalAdminController {
    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap) {

        //get datatable RAB
        long pageLength = 5; //panjang halaman
        //ambil data dari database
        Map dataMap = plsqlService.find("P_PRODUCT", 0, pageLength, "NOMOR_PRODUK", "ASC", "");
        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);
        // Pengambilan data manufaktur
        Map<String, String> params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_MANUFAKTUR");
        params2.put("func_name", "find_all_select2");
        List listManufaktur = plsqlService.findAll(params2);
        modelMap.put("listManufaktur", listManufaktur);
        // Pengambilan data kategori produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_KATEGORI_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listKategoriProduk = plsqlService.findAll(params2);
        modelMap.put("listKategoriProduk", listKategoriProduk);

        // Pengambilan data jenis produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listJenisProduk = plsqlService.findAll(params2);
        modelMap.put("listJenisProduk", listJenisProduk);

        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.reporting.historikal.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            @RequestParam(value = "prm_workshop", defaultValue = "") String prm_workshop,
            @RequestParam(value = "prm_kategori", defaultValue = "") String prm_kategori,
            @RequestParam(value = "prm_tahun", defaultValue = "") String prm_tahun,
            @RequestParam(value = "prm_tgl_penugasan", defaultValue = "") String prm_tgl_penugasan,
            @RequestParam(value = "prm_jenis_produk", defaultValue = "") String prm_jenis_produk,
            @RequestParam(value = "filtering", defaultValue = "") Integer filtering,

            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> paramsIn = new HashMap<>();
        paramsIn.put("PRM_START", start);
        paramsIn.put("PRM_LENGTH", length);
        paramsIn.put("PRM_SORT_BY", sortBy);
        paramsIn.put("PRM_SORT_DIR", sortDir.toUpperCase());
        paramsIn.put("PRM_SEARCH", search.toUpperCase());
        paramsIn.put("prm_workshop", prm_workshop);
        paramsIn.put("prm_kategori", prm_kategori);
        paramsIn.put("prm_tahun", prm_tahun);
        paramsIn.put("prm_tgl_penugasan", prm_tgl_penugasan);
        paramsIn.put("prm_jenis_produk", prm_jenis_produk);

        if (filtering == 0) {
            map = plsqlService.find("P_HISTORIKAL_LAPORAN", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        } else {
            map = plsqlService.findFilter("P_HISTORIKAL_LAPORAN", paramsIn);
        }
        map.put("draw", draw);
        return map;
    }

    @RequestMapping(value = "/laporan_excel", method = RequestMethod.POST)
    @ResponseBody
    public String laporanExcel(HttpServletRequest request, HttpServletResponse response) throws IOException{
        Map<String, String[]> paramRequest = request.getParameterMap();
        //ambil colum header
        Map<String, String> paramSourceIn = new HashMap<>();
        List<Map<String,Object>> listHistorikal=null;

        if(paramRequest.get("FIL")[0].equals("1")){
            paramSourceIn.put("pkg_name", "P_HISTORIKAL_LAPORAN");
            paramSourceIn.put("func_name", "HISTORIKAL_EXCEL");
            for (String keyName : paramRequest.keySet()) {
                paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
            }
            listHistorikal= plsqlService.findAll(paramSourceIn);
        }else{
            paramSourceIn.put("pkg_name", "P_HISTORIKAL_LAPORAN");
            paramSourceIn.put("func_name", "historikal");
            listHistorikal= plsqlService.findAll(paramSourceIn);
        }
//        HSSFWorkbook wb = new HSSFWorkbook();
        //Read the spreadsheet that needs to be updated
        InputStream input_document = getClass().getResourceAsStream("/historikal_produk.xls");
        //Access the workbook
        HSSFWorkbook wb = new HSSFWorkbook(input_document);
        //Access the worksheet, so that we can update / modify it.
        HSSFSheet sheet = wb.getSheetAt(0);
        HSSFRow row = sheet.createRow(4);

        int baris=4;
        for(Map<String, Object> mapHistori : listHistorikal){
            int cellHeader = 0;
            Row rowContent = sheet.createRow((short)baris);
            for(String keyContent : mapHistori.keySet()) {
                Cell cell = rowContent.createCell(cellHeader);
                try{
                    cell.setCellValue(mapHistori.get(keyContent).toString());
                }catch (Exception e){
                    cell.setCellValue("");
                }
                cellHeader++;
            }
            baris++;
        }
//        HSSFCell cell = row.createCell(0);
//        cell.setCellValue("1. Cell");
        String path = request.getRealPath("");
        path = path.replace("\\", "/");

        try{
            wb.write(new FileOutputStream(path +"/assets/temp_file/historikal_produk.xls"));
            wb.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        return "true";
    }


    @RequestMapping(value = "/laporan_excel_bak", method = RequestMethod.GET)
    @ResponseBody
    public void exportExcel(HttpServletRequest request, HttpServletResponse response) {

        Map<String, String[]> paramRequest = request.getParameterMap();
        //ambil colum header
        Map<String, String> paramSourceIn = new HashMap<>();
        List<Map<String,Object>> listHistorikal=null;

//        if(paramRequest.get("FIL")[0].equals("1")){
//            paramSourceIn.put("pkg_name", "P_HISTORIKAL_LAPORAN");
//            paramSourceIn.put("func_name", "HISTORIKAL_EXCEL");
//            for (String keyName : paramRequest.keySet()) {
//                paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
//            }
//            listHistorikal= plsqlService.findAll(paramSourceIn);
//        }else{
//            paramSourceIn.put("pkg_name", "P_HISTORIKAL_LAPORAN");
//            paramSourceIn.put("func_name", "historikal");
//            listHistorikal= plsqlService.findAll(paramSourceIn);
//        }

        try {
            response.setContentType("application/vnd.ms-excel");
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", "laporan-history.xls");
            response.setHeader(headerKey, headerValue);

            Workbook wb = new HSSFWorkbook();
            Sheet sheet = wb.createSheet("new sheet");

            // Create a row and put some cells in it. Rows are 0 based.
            Row row = sheet.createRow((short) 1);

            // Aqua background
            CellStyle style = wb.createCellStyle();
            style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
            style.setFillPattern(CellStyle.BIG_SPOTS);
            Cell cell = row.createCell((short) 1);
            cell.setCellValue("X");
            cell.setCellStyle(style);

            // Orange "foreground", foreground being the fill foreground not the font color.
            style = wb.createCellStyle();
            style.setFillForegroundColor(IndexedColors.ORANGE.getIndex());
            style.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cell = row.createCell((short) 2);
            cell.setCellValue("X YUHUUUUU");
            cell.setCellStyle(style);

            // Write the output to a file
            OutputStream outStream = response.getOutputStream();
            wb.write(outStream);
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

