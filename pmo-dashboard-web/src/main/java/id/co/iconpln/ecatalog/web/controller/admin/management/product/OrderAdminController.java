package id.co.iconpln.ecatalog.web.controller.admin.management.product;

import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/admin/order")
public class OrderAdminController {

    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(HttpServletRequest request, ModelMap modelMap){
        // Isi kode untuk menyimpan data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.list";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.PUT)
    public String update(@RequestParam("id") long id, HttpServletRequest request,  ModelMap modelMap){
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.list";
    }

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk melihat data detail
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.detail";
    }

    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String findAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            ModelMap modelMap)
    {
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Pengambilan semua data
        return "app.admin.management-product.order.list";
    }

    // Menampilkan halaman seluruh data menggunakan json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public @ResponseBody List findJsonAll(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            ModelMap ModelMap)
    {
        // pengambilan data format json
        return null;
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk menghapus data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.order.list";
    }

}
