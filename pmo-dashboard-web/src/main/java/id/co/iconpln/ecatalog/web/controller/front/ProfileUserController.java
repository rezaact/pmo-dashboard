package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/profileaaa")
public class ProfileUserController {
    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(method = RequestMethod.GET)
    public String profile(ModelMap modelMap){
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.profile-user";
    }


}
