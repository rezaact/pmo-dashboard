package id.co.iconpln.ecatalog.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin/error")
public class ErrorAdminController {

    @RequestMapping(method = RequestMethod.GET)
    public String error(){
        return "app.front.home";
    }

}
