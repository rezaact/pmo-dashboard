package id.co.iconpln.ecatalog.web.controller.admin.master.data;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/master-data/customer")
public class MasterCustomerAdminController {
    @Autowired
    PlsqlService plsqlService;


    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        modelMap.put("label", "create");
        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.customer.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long id = plsqlService.save("P_MASTER_PEMBERI_PENUGASAN", params);
        modelMap.put("ret_code", id);
        if(id == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/customer";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_PEMBERI_PENUGASAN", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.customer.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
//        paramSourceIn.put("prm_id_workshop", request.getParameter("id"));

        Long retValue = plsqlService.update("P_MASTER_PEMBERI_PENUGASAN",paramSourceIn);

        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }

        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/customer";
    }

    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_PEMBERI_PENUGASAN", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.customer.form";
    }

    //Menampilkan Seluruh Data
    @RequestMapping(method = RequestMethod.GET)
    public String find( ModelMap modelMap, HttpServletRequest request )
    {
        long pageLength =5 ; //panjang halaman
        //get default data
        Map dataMap = plsqlService.find("P_MASTER_PEMBERI_PENUGASAN",0,pageLength,"NAMA_PEMBERI_PENUGASAN","ASC","");

        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        //panggil jsp
        return "app.admin.master-data.customer.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NAMA_PEMBERI_PENUGASAN") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_MASTER_PEMBERI_PENUGASAN", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap){

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("PRM_ID_PEMBERI_PENUGASAN",id);
        Long retValue = plsqlService.delete("P_MASTER_PEMBERI_PENUGASAN",paramSourceIn);
        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Dihapus. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Dihapus");
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/master-data/customer";
    }



}
