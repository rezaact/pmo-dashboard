package id.co.iconpln.ecatalog.web.controller.admin;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.service.impl.GmailService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/user")
public class UserAdminController {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private GmailService gmailService;

    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        modelMap.put("label", "create");
        // Isi kode form untuk membuat data baru
        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("listRole", listRole);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management.user.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create( ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        params.put("prm_nama_lengkap",  paramRequest.get("PRM_NAMA_LENGKAP")[0]);
        params.put("prm_nip",  paramRequest.get("PRM_NIP")[0]);
        params.put("prm_email",  paramRequest.get("PRM_EMAIL")[0]);
        params.put("prm_no_telp",  paramRequest.get("PRM_NO_TELP")[0]);
        params.put("prm_unit",  paramRequest.get("PRM_UNIT")[0]);
        params.put("prm_username",  paramRequest.get("PRM_USERNAME")[0]);
        params.put("prm_password", passwordEncoder.encode(paramRequest.get("PRM_PASSWORD")[0]));
        params.put("prm_status",  1);
        params.put("prm_id_role",  paramRequest.get("PRM_ID_ROLE")[0]);

        Long id = plsqlService.save("p_master_user", params);
        if(id != 0){
            modelMap.put("save", true);
        }else{
            modelMap.put("save", false);
        }
        // Isi kode untuk menyimpan data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/user";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");

        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("listRole", listRole);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management.user.form";
    }


    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
          paramSourceIn.put("prm_id_user",id);
        Long retValue = plsqlService.update("p_master_user", paramSourceIn);

        if(retValue  != 0){
            modelMap.put("save", true);
        }else{
            modelMap.put("save", false);
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/user";
    }

    // Proses update data
    @RequestMapping(value = "/approve", method = RequestMethod.GET)
    public String approve(@RequestParam("id") long id,
                          @RequestParam("role") long role,
                          ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "approve");
        paramSourceIn.put("prm_id_role", role);
        paramSourceIn.put("prm_id_user", id);

        String retValue = plsqlService.save(paramSourceIn);

        Map<String, Object> info_user= plsqlService.findOne("P_MASTER_USER", retValue);
        String mail=String.valueOf(info_user.get("ALAMAT_EMAIL"));
        String subject="E-Catalog | Persetujuan Pendaftaran Akun";
        String mailContent="Kepada Yth. " +info_user.get("NAMA_LENGKAP")+ "\n Bahwa akun aplikasi E-Catalog Pusharlis anda \n" +
                "dengan username : "  + info_user.get("USERNAME") + " pada tanggal pendaftaran : "+info_user.get("TGL_REGISTER") +".\n" +
                "Telah disetujui Administrator dengan hak akses sebagai "+info_user.get("nama_role")+ ". \nSelanjutnya silahkan dipergunakan sebagaimana mestinya.\nTerima Kasih \n" +
                "TTD\nPT. PLN (Persero) | Pusat Pemeliharaan Ketenagalistrikan";
        try {
            gmailService.sendEmail(mail,subject,mailContent);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (retValue.equals('0')) {
            modelMap.put("approve", false);
        } else {
            modelMap.put("approve", true);
        }
        modelMap.put("authUser", UserUtil.getAuthUser());

        return "redirect:/admin/user";
    }

    // Proses update data
    @RequestMapping(value = "/disapprove", params = "id", method = RequestMethod.GET)
    public String disapprove(@RequestParam("id") long id, ModelMap modelMap) {
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "disapprove");
        paramSourceIn.put("prm_id_user", id);

        String retValue = plsqlService.save(paramSourceIn);
        Map<String, Object> info_user= plsqlService.findOne("P_MASTER_USER", retValue);
        String mail=String.valueOf(info_user.get("ALAMAT_EMAIL"));
        String subject="E-Catalog | Persetujuan Pendaftaran Akun";
        String mailContent="Kepada Yth. " +info_user.get("NAMA_LENGKAP")+ "\nBahwa akun aplikasi E-Catalog Pusharlis anda \n" +
                "dengan username : "  + info_user.get("USERNAME") + " pada tanggal pendaftaran : "+info_user.get("TGL_REGISTER") +".\n" +
                "Tidak disetujui Administrator. Dikarenakan beberapa hal.\nTerima Kasih \n " +
                "TTD\nPT. PLN (Persero) | Pusat Pemeliharaan Ketenagalistrikan";
        try {
            gmailService.sendEmail(mail,subject,mailContent);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (retValue.equals('0')) {
            modelMap.put("disapprove", false);
        } else {
            modelMap.put("disapprove", true);
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/user";
    }

    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management.user.form";
    }


    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String find(
            @RequestParam(value = "approve", defaultValue = "") Boolean approve,
            @RequestParam(value = "banned", defaultValue = "") Boolean banned,
            @RequestParam(value = "disapprove", defaultValue = "") Boolean disapprove,
            @RequestParam(value = "delete", defaultValue = "") Boolean delete,
            @RequestParam(value = "save", defaultValue = "") Boolean save,
            ModelMap modelMap)
    {
        long pageLength =5;
        Map dataMap =plsqlService.find("p_master_user",0,pageLength,"ID_USER","ASC");
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length",pageLength);
        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("listRole", listRole);
        modelMap.put("approve", approve);
        modelMap.put("banned", banned);
        modelMap.put("disapprove", disapprove);
        modelMap.put("delete", delete);
        modelMap.put("save", save);

        //get total user banned (0)
        Long totalUserBanned = plsqlService.countWhere("P_MASTER_USER", 0);
        modelMap.put("totalUserBanned",totalUserBanned);

        //get total user approval (1)
        Long totalUserApproval = plsqlService.countWhere("P_MASTER_USER", 1);
        modelMap.put("totalUserApproval",totalUserApproval);

        //get total user pending (2)
        Long totalUserPending = plsqlService.countWhere("P_MASTER_USER", 2);
        modelMap.put("totalUserPending",totalUserPending);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-user.list";
    }

    // Menampilkan halaman seluruh data menggunakan json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "ID_USER") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_MASTER_USER", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_user", id);
        Long retValue = plsqlService.delete("P_MASTER_USER", paramSourceIn);
        if (retValue > 0) {
            modelMap.put("delete", true);
        } else {
            modelMap.put("delete", false);
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/user";
    }

    // Proses banned data
    @RequestMapping(value = "/banned", params = "id", method = RequestMethod.GET)
    public String banned(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_user", id);
        Long retValue = plsqlService.banned("p_master_user", paramSourceIn);
        if (retValue > 0) {
            modelMap.put("banned", true);
        } else {
            modelMap.put("banned", false);
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/user";
    }

    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public String password(HttpServletRequest request, ModelMap modelMap) {

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "change_password");
        paramSourceIn.put("prm_id_user", paramRequest.get("prm_id_user")[0]);
        paramSourceIn.put("prm_password", passwordEncoder.encode(paramRequest.get("prm_password")[0]));

        String retValue = plsqlService.save(paramSourceIn);

        if (retValue.equals('0')) {
            modelMap.put("save", false);
        } else {
            modelMap.put("save", true);
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/user";
    }

    //proses simpan file
    @RequestMapping(value = "/simpanPhoto", method = RequestMethod.POST)
//    @ResponseBody
    public String simpanPhoto(MultipartHttpServletRequest request){

//        Map<String,Object> retValue = new HashMap<>();

//        try {

            MultipartFile multipartFile = request.getFile("PRM_PHOTO");

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_MASTER_USER");
        paramSourceIn.put("func_name", "UPLOAD_FOTO");
        paramSourceIn.put("param_count", 4);

        //set prm_nomor_produk
        Map<String, Object> prm_id_user = new HashMap<>();
        prm_id_user.put("index", 1);
        prm_id_user.put("type", AppUtil.ECAT_STRING);
        prm_id_user.put("content", request.getParameter("PRM_ID_USER"));
        paramSourceIn.put("prm_id_user", prm_id_user);

        //set prm_name
        Map<String, Object> prm_name = new HashMap<>();
        prm_name.put("index", 2);
        prm_name.put("type", AppUtil.ECAT_STRING);
        prm_name.put("content", multipartFile.getOriginalFilename());
        paramSourceIn.put("prm_name", prm_name);

        //set prm_name
        Map<String, Object> prm_size = new HashMap<>();
        prm_size.put("index", 3);
        prm_size.put("type", AppUtil.ECAT_STRING);
        prm_size.put("content", multipartFile.getSize());
        paramSourceIn.put("prm_size", prm_size);
        try {

            File tempUserPhoto = File.createTempFile("file_photo", ".jpg");
            request.getFile("PRM_PHOTO").transferTo(tempUserPhoto);
            Map<String, Object> mapUserPhoto= new HashMap<>();
            mapUserPhoto.put("index", 4);
            mapUserPhoto.put("type", AppUtil.ECAT_FILE);
            mapUserPhoto.put("content", tempUserPhoto);
            paramSourceIn.put("prm_photo", mapUserPhoto);

        }catch (Exception e){
            e.printStackTrace();
        }

        String retValue = plsqlService.saveWithFile(paramSourceIn);

        return "redirect:/admin/user/update?id="+request.getParameter("PRM_ID_USER");
    }

}
