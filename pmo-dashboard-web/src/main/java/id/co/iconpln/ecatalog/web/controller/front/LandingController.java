package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/landing")
public class LandingController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(method = RequestMethod.GET)
    public String dashboard(ModelMap modelMap, HttpServletRequest request){
//        DefaultSavedRequest savedRequest = (DefaultSavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        String ret;
        try{
            String page = request.getSession().getAttribute("prevPage").toString();
            ret = "redirect:".concat(page);
        }
        catch (Exception err){
            modelMap.put("authUser", UserUtil.getAuthUser());
            ret =  "app.front.landing.main"; //<!-- Modif -->
        }

        return ret;
    }



}
