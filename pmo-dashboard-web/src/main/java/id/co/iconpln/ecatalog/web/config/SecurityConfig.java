package id.co.iconpln.ecatalog.web.config;

import id.co.iconpln.ecatalog.db.service.SecurityService;
import id.co.iconpln.ecatalog.web.config.security.LogoutListener;
import id.co.iconpln.ecatalog.web.filter.SiteHitCounterFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.switchuser.SwitchUserFilter;


/**
 * Created by Deny Prasetyo,S.T
 * Java(Script) Developer and Trainer
 * Software Engineer
 * jasoet87@gmail.com
 * <p/>
 * http://github.com/jasoet
 * http://bitbucket.com/jasoet
 *
 * @jasoet
 */

@Configuration
@ComponentScan({
        "id.co.iconpln.ecatalog.web.filter"
})
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private SecurityService securityService;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SiteHitCounterFilter siteHitCounterFilter;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean(name = "logoutListener")
    public ApplicationListener<SessionDestroyedEvent> logoutListener() {
        return new LogoutListener();
    }
    

    @Bean(name = "daoAuthenticationProvider")
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(securityService);
        provider.setPasswordEncoder(passwordEncoder);
        return provider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //http.addFilterAfter(siteHitCounterFilter, SwitchUserFilter.class).csrf().disable().
        http.csrf().disable().
                formLogin().usernameParameter("username").passwordParameter("password")
                .loginProcessingUrl("/account/login_process")//ini URL untuk proses login. proses diurus spring
                .loginPage("/account/login")//url halaman login --> halaman kita buat.
                .failureUrl("/account/login?error=t")//jika terjadi gagal login. diurus spring.
//                .defaultSuccessUrl("/product/product-list")//jika sukses login. --> halaman yang kita buat.
                .defaultSuccessUrl("/login/success", true)//jika sukses login. --> halaman yang kita buat.
                .and()
                .logout()
                .logoutUrl("/account/logout")//URL untuk proses logout. proses diurusi spring.
                .logoutSuccessUrl("/account/login")//jika sukses logout. --> halaman kita buat.
                .invalidateHttpSession(true)
                .and()
                .authorizeRequests()


                //---- mau masuk halaman apa saja bebas.
//                .antMatchers(
//                        "/"
//                ).permitAll();

        //----- ini untuk memakai autentikasi setiap masuk halaman web

                //yang bisa diakses anonymous user tarus disini, atau tanpa login bisa diakses.
                .antMatchers(
                        "/account/login",
                        "/account/logout",
                        "/account/login_process",
                        "/account/register/**",
                        "/account/checker",
                        "/product/**",
                        "/contact/**",
                        "/portfolio",
                        "/service",
                        "/assets/**",
                        "/file/**",
                        "/profile",
                        "/history-produk/**",
                        "/dashboard",
                        /*"/landing",
                        "/dashboard-cod/bulanan",
                        "/dashboard-cod/bulanan/getdata-cod-bulanan",
                        "/dashboard-cod/tahunan",
                        "/dashboard-cod/tahunan/getdata-cod-tahunan",
                        "/dashboard-scurve/ruptl",
                        "/dashboard-scurve/supply",
                        "/dashboard-scurve/getdata-supply-demand",
                        "/dashboard-milestone/**",
                        "/dashboard-milestone/getdata-box-milestone",*/
                        "/scheduler/**"
                ).permitAll()
                //yang hanya bisa diakses ketika login saja taruh disini
                .antMatchers(
                        "/landing",
                        "/dashboard-cod/**",
                        "/dashboard-scurve/**",
                        "/dashboard-milestone/**",
                        "/dashboard-cod-capacity/**",
                        "/dashboard-ruptl-capacity/**",
                        "/admin/**",
                        "/admin/profile/**",
                        "/account/**",
                        "/admin/user/password",
                        "admin/master-data/lokasi-pemasangan/**",
                        "/admin/reporting/historikal-produk/**"
                ).hasAnyRole("ADMINISTRATOR", "SECRET")
                .antMatchers(
                   "/account/profile"
                ).hasAnyRole("REGISTER","SECRET","PRIVATE","ADMINISTRATOR")
                .antMatchers(
                        "/"
                ).fullyAuthenticated()

                .anyRequest().fullyAuthenticated();



    }

    @Override
    protected void configure(AuthenticationManagerBuilder config) {
        config.authenticationProvider(daoAuthenticationProvider());
    }
}