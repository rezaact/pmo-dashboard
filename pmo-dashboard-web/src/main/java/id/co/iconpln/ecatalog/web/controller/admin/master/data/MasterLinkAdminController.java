package id.co.iconpln.ecatalog.web.controller.admin.master.data;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/admin/master-data/link")
public class MasterLinkAdminController {
    @Autowired
    PlsqlService plsqlService;


    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        modelMap.put("label", "create");
        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.link.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(ModelMap modelMap, MultipartHttpServletRequest request) {

        MultipartFile multipartFile = request.getFile("PRM_GAMBAR");

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_MASTER_LINK");
        paramSourceIn.put("func_name", "simpan");
        paramSourceIn.put("param_count", 6);

        //set
        Map<String, Object> prm_url_info = new HashMap<>();
        prm_url_info.put("index", 1);
        prm_url_info.put("type", AppUtil.ECAT_STRING);
        prm_url_info.put("content", request.getParameter("PRM_URLLINK"));
        paramSourceIn.put("prm_urllink", prm_url_info);

        Map<String, Object> prm_keterangan = new HashMap<>();
        prm_keterangan.put("index", 2);
        prm_keterangan.put("type", AppUtil.ECAT_STRING);
        prm_keterangan.put("content", request.getParameter("PRM_KETERANGAN"));
        paramSourceIn.put("prm_keterangan", prm_keterangan);

        //set prm_content_type
        Map<String, Object> prm_content_type = new HashMap<>();
        prm_content_type.put("index", 3);
        prm_content_type.put("type", AppUtil.ECAT_STRING);
        prm_content_type.put("content", multipartFile.getContentType());
        paramSourceIn.put("prm_content_type", prm_content_type);

        //set prm_name
        Map<String, Object> prm_name = new HashMap<>();
        prm_name.put("index", 4);
        prm_name.put("type", AppUtil.ECAT_STRING);
        prm_name.put("content", multipartFile.getOriginalFilename());
        paramSourceIn.put("prm_name", prm_name);

        //set prm_file_size
        Map<String, Object> prm_file_size = new HashMap<>();
        prm_file_size.put("index", 5);
        prm_file_size.put("type", AppUtil.ECAT_STRING);
        prm_file_size.put("content", multipartFile.getSize());
        paramSourceIn.put("prm_file_size", prm_file_size);

        try {
            //File prm_multi_drawing_pdf
            File tempMultiDrawingPDF = File.createTempFile("tmp","");
            multipartFile.transferTo(tempMultiDrawingPDF);
            Map<String, Object> mapMultiDrawingPDF = new HashMap<>();
            mapMultiDrawingPDF.put("index", 6);
            mapMultiDrawingPDF.put("type", AppUtil.ECAT_FILE);
            mapMultiDrawingPDF.put("content", tempMultiDrawingPDF);
            paramSourceIn.put("prm_gambar", mapMultiDrawingPDF);

        }catch (Exception e){
            e.printStackTrace();
        }

        plsqlService.saveWithFile(paramSourceIn);

        /*Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long id = plsqlService.save("P_MASTER_PEMBERI_PENUGASAN", params);
        modelMap.put("ret_code", id);
        if(id == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }*/

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/link";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_LINK", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.link.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, MultipartHttpServletRequest request){

        MultipartFile multipartFile = request.getFile("PRM_GAMBAR");

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_MASTER_LINK");
        paramSourceIn.put("func_name", "edit");
        paramSourceIn.put("param_count", 7);

        //set
        Map<String, Object> prm_id_url_info = new HashMap<>();
        prm_id_url_info.put("index", 1);
        prm_id_url_info.put("type", AppUtil.ECAT_STRING);
        prm_id_url_info.put("content", request.getParameter("PRM_ID_URLLINK"));
        paramSourceIn.put("prm_id_urllink", prm_id_url_info);

        Map<String, Object> prm_url_info = new HashMap<>();
        prm_url_info.put("index", 2);
        prm_url_info.put("type", AppUtil.ECAT_STRING);
        prm_url_info.put("content", request.getParameter("PRM_URLLINK"));
        paramSourceIn.put("prm_urllink", prm_url_info);

        Map<String, Object> prm_keterangan = new HashMap<>();
        prm_keterangan.put("index", 3);
        prm_keterangan.put("type", AppUtil.ECAT_STRING);
        prm_keterangan.put("content", request.getParameter("PRM_KETERANGAN"));
        paramSourceIn.put("prm_keterangan", prm_keterangan);

        //set prm_content_type
        Map<String, Object> prm_content_type = new HashMap<>();
        prm_content_type.put("index", 4);
        prm_content_type.put("type", AppUtil.ECAT_STRING);
        prm_content_type.put("content", multipartFile.getContentType());
        paramSourceIn.put("prm_content_type", prm_content_type);

        //set prm_name
        Map<String, Object> prm_name = new HashMap<>();
        prm_name.put("index", 5);
        prm_name.put("type", AppUtil.ECAT_STRING);
        prm_name.put("content", multipartFile.getOriginalFilename());
        paramSourceIn.put("prm_name", prm_name);

        //set prm_file_size
        Map<String, Object> prm_file_size = new HashMap<>();
        prm_file_size.put("index", 6);
        prm_file_size.put("type", AppUtil.ECAT_STRING);
        prm_file_size.put("content", multipartFile.getSize());
        paramSourceIn.put("prm_file_size", prm_file_size);

        try {
            //File prm_multi_drawing_pdf
            File tempMultiDrawingPDF = File.createTempFile("tmp","");
            multipartFile.transferTo(tempMultiDrawingPDF);
            Map<String, Object> mapMultiDrawingPDF = new HashMap<>();
            mapMultiDrawingPDF.put("index", 7);
            mapMultiDrawingPDF.put("type", AppUtil.ECAT_FILE);
            mapMultiDrawingPDF.put("content", tempMultiDrawingPDF);
            paramSourceIn.put("prm_gambar", mapMultiDrawingPDF);

        }catch (Exception e){
            e.printStackTrace();
        }

        plsqlService.saveWithFile(paramSourceIn);
       /* Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
//        paramSourceIn.put("prm_id_workshop", request.getParameter("id"));

        Long retValue = plsqlService.update("P_MASTER_LINK",paramSourceIn);

        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }*/

        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/link";
    }

    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_LINK", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.link.form";
    }

    //Menampilkan Seluruh Data
    @RequestMapping(method = RequestMethod.GET)
    public String find( ModelMap modelMap, HttpServletRequest request )
    {
        long pageLength =5 ; //panjang halaman
        //get default data
        Map dataMap = plsqlService.find("P_MASTER_LINK",0,pageLength,"KETERANGAN","ASC","");

        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        //panggil jsp
        return "app.admin.master-data.link.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "KETERANGAN") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_MASTER_LINK", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap){

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("PRM_ID_INFO",id);
        Long retValue = plsqlService.delete("P_MASTER_LINK",paramSourceIn);
        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Dihapus. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Dihapus");
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/master-data/link";
    }



}
