package id.co.iconpln.ecatalog.web.controller.misc;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.db.service.ProductGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/misc/product/group")
public class MasterProductGroupController {

    @Autowired
    PlsqlService plsqlService;

    @Autowired
    ProductGroupService productGroupService;

    // Menampilkan halaman seluruh data menggunakan json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public List findJsonAll(ModelMap ModelMap) {
        List<Map<String, Object>> tree = productGroupService.findTree();
        return tree;
    }
}
