package id.co.iconpln.ecatalog.web.controller.admin;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/profile")
public class UserProfileController {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman update
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String formUpdate(ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", UserUtil.getAuthUser().getId());
        modelMap.addAllAttributes(data);

        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", null);
        params.put("prm_length", null);
        params.put("prm_search", " ");
        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("label", "update");
        modelMap.put("listRole", listRole);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.profile.user.form";
    }


    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
          paramSourceIn.put("prm_id_user",id);
        Long retValue = plsqlService.update("p_master_user", paramSourceIn);

        if(retValue  != 0){
            modelMap.put("save", true);
        }else{
            modelMap.put("save", false);
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/profile/update?";
    }

    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_USER", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.profile.user.form";
    }

    @RequestMapping(value = "/password", method = RequestMethod.POST)
    public String password(HttpServletRequest request, ModelMap modelMap) {

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "p_master_user");
        paramSourceIn.put("func_name", "change_password");
        paramSourceIn.put("prm_id_user", paramRequest.get("prm_id_user")[0]);
        paramSourceIn.put("prm_password", passwordEncoder.encode(paramRequest.get("prm_password")[0]));

        String retValue = plsqlService.save(paramSourceIn);

        if (retValue.equals('0')) {
            modelMap.put("save", false);
        } else {
            modelMap.put("save", true);
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode form untuk menghapus data
        return "redirect:/admin/profile/update?";
    }

    //proses simpan file
    @RequestMapping(value = "/simpanPhoto", method = RequestMethod.POST)
//    @ResponseBody
    public String simpanPhoto(MultipartHttpServletRequest request){

//        Map<String,Object> retValue = new HashMap<>();

//        try {

            MultipartFile multipartFile = request.getFile("PRM_PHOTO");

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_MASTER_USER");
        paramSourceIn.put("func_name", "UPLOAD_FOTO");
        paramSourceIn.put("param_count", 4);

        //set prm_nomor_produk
        Map<String, Object> prm_id_user = new HashMap<>();
        prm_id_user.put("index", 1);
        prm_id_user.put("type", AppUtil.ECAT_STRING);
        prm_id_user.put("content", request.getParameter("PRM_ID_USER"));
        paramSourceIn.put("prm_id_user", prm_id_user);

        //set prm_name
        Map<String, Object> prm_name = new HashMap<>();
        prm_name.put("index", 2);
        prm_name.put("type", AppUtil.ECAT_STRING);
        prm_name.put("content", multipartFile.getOriginalFilename());
        paramSourceIn.put("prm_name", prm_name);

        //set prm_name
        Map<String, Object> prm_size = new HashMap<>();
        prm_size.put("index", 3);
        prm_size.put("type", AppUtil.ECAT_STRING);
        prm_size.put("content", multipartFile.getSize());
        paramSourceIn.put("prm_size", prm_size);
        try {

            File tempUserPhoto = File.createTempFile("file_photo", ".jpg");
            request.getFile("PRM_PHOTO").transferTo(tempUserPhoto);
            Map<String, Object> mapUserPhoto= new HashMap<>();
            mapUserPhoto.put("index", 4);
            mapUserPhoto.put("type", AppUtil.ECAT_FILE);
            mapUserPhoto.put("content", tempUserPhoto);
            paramSourceIn.put("prm_photo", mapUserPhoto);

        }catch (Exception e){
            e.printStackTrace();
        }

        String retValue = plsqlService.saveWithFile(paramSourceIn);

        return "redirect:/admin/profile/update?";
    }

}
