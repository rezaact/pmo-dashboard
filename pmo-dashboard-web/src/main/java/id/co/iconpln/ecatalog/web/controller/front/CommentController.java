package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/com")
public class CommentController {
    @Autowired
    PlsqlService plsqlService;
    // proses menyimpan data
    @RequestMapping(method = RequestMethod.POST)
    public Map<String, Object> simpan(HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

        params.put("PRM_NOMOR_PRODUK", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        params.put("PRM_CREATED_BY", paramRequest.get("PRM_NAME")[0]);
        params.put("PRM_EMAIL", paramRequest.get("PRM_EMAIL")[0]);
        params.put("PRM_KOMENTAR", paramRequest.get("PRM_KOMENTAR")[0]);
        params.put("PRM_RATE",paramRequest.get("PRM_RATE")[0]);
        params.put("PRM_STATUS",1);

        Long id = plsqlService.save("P_KOMENTAR_FRONT",params);
        params.put("prm_id", String.valueOf(id));
        params.put("prm_start", 0);
        params.put("prm_length", 10);
        params.put("prm_search", " ");
        List<Map<String, Object>> detailKomentar = plsqlService.findAll("P_KOMENTAR_FRONT", params);

        Map<String, Object> retValue= new HashMap<>();
        retValue.put("detailKomentar", detailKomentar);
        retValue.put("totalKomentar", detailKomentar.size());
        return retValue;
//                "redirect:/product/product-list/product/product-detail?id=="+paramRequest.get("prm_nomor_produk")[0];
    }
}
