package id.co.iconpln.ecatalog.service.impl;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rezafit on 02/12/2016.
 */
@Service
public class SynchronizedDataJob implements Job {

    @Autowired
    PlsqlService plsqlService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_SYNCH_TABLE");
        params.put("func_name", "find_all");

        List<Map<String, Object>> listRec = plsqlService.findAll(params);
        for(Map<String, Object> mapRec : listRec){
            System.out.println("ORCL_TBL_NAME = " + mapRec.get("ORCL_TBL_NAME"));
        }

    }
}
