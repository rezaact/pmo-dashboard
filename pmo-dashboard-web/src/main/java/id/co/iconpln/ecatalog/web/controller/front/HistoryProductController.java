package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by barka01 on 02/06/15.
 */

@Controller
@RequestMapping("/history-produk")
public class HistoryProductController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(value="/paging-custom", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> pagingCustom(
            @RequestParam(value = "page", defaultValue = "1") long page,
            @RequestParam(value = "length", defaultValue = "1") long length,
            @RequestParam(value = "nomor_produk", defaultValue = "") String nomorProduk
            ){
        System.out.println("paging paging paging");

        //keperluan paging
        page = page-1;

        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_HISTORIKAL_PRODUK_FRONT");
        paramsIn.put("func_name", "FIND_CUSTOM");
        paramsIn.put("PRM_START", String.valueOf(page));
        paramsIn.put("PRM_LENGTH", "1");
        paramsIn.put("PRM_SORT_BY", null);
        paramsIn.put("PRM_SORT_DIR", null);
        paramsIn.put("PRM_SEARCH", null);
        paramsIn.put("PRM_NOMOR_PRODUK", nomorProduk);
        Map<String, Object> historyProduct = plsqlService.findOne(paramsIn);

        //get doc history
        paramsIn.put("pkg_name","P_HISTORIKAL_PRODUK_FRONT");
        paramsIn.put("func_name","doc_history_new");
        paramsIn.put("prm_id_history", String.valueOf(historyProduct.get("ID_HISTORY")));
        List<Map<String, Object>> docHistoryNew = plsqlService.findAll(paramsIn);

        Map<String, Object> retValue = new HashMap<>();
        retValue.put("historyProduct", historyProduct);
        retValue.put("docHistoryNew", docHistoryNew);

        return retValue;
    }

}