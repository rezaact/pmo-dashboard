package id.co.iconpln.ecatalog.web.controller;

import id.co.iconpln.ecatalog.db.domain.AuthUser;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by rezafit on 10/01/2017.
 */
@Controller
public class IndexController {

    @RequestMapping(value = "/")
    public String loginSuccess(ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        AuthUser authUser = UserUtil.getAuthUser();
//        modelMap.put("check",authUser);
        if(authUser.getRole().getAuthority().equals("ROLE_ADMINISTRATOR") ||
                authUser.getRole().getAuthority().equals("ROLE_SECRET")){
            return "redirect:/dashboard-cod-capacity";
        }else{
            return "redirect:/account/login";
        }

    }
}
