package id.co.iconpln.ecatalog.web.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/e-catalog")
public class EcatalogController {

    @RequestMapping(method = RequestMethod.GET)
    public String ecatalog(){
        return "app.admin.e-catalog";
    }

}
