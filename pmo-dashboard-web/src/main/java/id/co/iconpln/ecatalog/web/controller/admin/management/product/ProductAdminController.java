package id.co.iconpln.ecatalog.web.controller.admin.management.product;

import id.co.iconpln.ecatalog.db.service.FileService;
import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import id.iconpln.ecatalog.utils.NumericUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/product")
public class ProductAdminController {

    @Autowired
    FileService fileService;
    @Autowired
    PlsqlService plsqlService;


    private static final int IMG_SMALL = 1;
    private static final int IMG_MEDIUM = 2;


    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){

        // Pengambilan data manufaktur
        Map<String, String> params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_MANUFAKTUR");
        params2.put("func_name", "find_all_select2");
        List listManufaktur = plsqlService.findAll(params2);
        modelMap.put("listManufaktur", listManufaktur);

        // Pengambilan data kategori produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_KATEGORI_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listKategoriProduk = plsqlService.findAll(params2);
        modelMap.put("listKategoriProduk", listKategoriProduk);

        // Pengambilan data jenis produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listJenisProduk = plsqlService.findAll(params2);
        modelMap.put("listJenisProduk", listJenisProduk);

        // Pengambilan data jenis komponen
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_KOMPONEN");
        params2.put("func_name", "find_all_select2");
        List listJenisKomponen = plsqlService.findAll(params2);
        modelMap.put("listJenisKomponen", listJenisKomponen);

        // Pengambilan data jenis pekerjaan
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PEKERJAAN");
        params2.put("func_name", "find_all_select2");
        List listJenisPekerjaan = plsqlService.findAll(params2);
        modelMap.put("listJenisPekerjaan", listJenisPekerjaan);

        // Isi kode form untuk membuat data baru
        modelMap.put("label", "create");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.product.list";

    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(HttpServletRequest request, ModelMap modelMap){
        Map<String, String[]> paramRequest = request.getParameterMap();

        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name","P_PRODUCT");
        params.put("func_name","create_new_product");
        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        //String stringNumeric = NumericUtil.convertToRupiahString((String)params.get("prm_harga_produk"));
        //params.put("prm_harga_produk", stringNumeric);
        //menghilangkan spasi pada NOMOR produk
        String PRM_NOMOR_PRODUK = request.getParameter("PRM_NOMOR_PRODUK");
        PRM_NOMOR_PRODUK = PRM_NOMOR_PRODUK.replace(" ","");

        params.put("PRM_NOMOR_PRODUK",PRM_NOMOR_PRODUK);
        String retValue = plsqlService.save(params);
        Map<String, Object> mapMessage = plsqlService.returnMapMessage(retValue);
        modelMap.addAllAttributes(mapMessage);
        modelMap.put("success", true);
        modelMap.put("success_create", true);

        modelMap.put("ret_code", 1);
        if(retValue.contains("0:")){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/product";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") String id, ModelMap modelMap){

        // Pengambilan data manufaktur
        Map<String, String> params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_MANUFAKTUR");
        params2.put("func_name", "find_all_select2");
        List listManufaktur = plsqlService.findAll(params2);
        modelMap.put("listManufaktur", listManufaktur);

        // Pengambilan data kategori produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_KATEGORI_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listKategoriProduk = plsqlService.findAll(params2);
        modelMap.put("listKategoriProduk", listKategoriProduk);

        // Pengambilan data jenis produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listJenisProduk = plsqlService.findAll(params2);
        modelMap.put("listJenisProduk", listJenisProduk);

        // Pengambilan data jenis komponen
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_KOMPONEN");
        params2.put("func_name", "find_all_select2");
        List listJenisKomponen = plsqlService.findAll(params2);
        modelMap.put("listJenisKomponen", listJenisKomponen);

        // Pengambilan data jenis pekerjaan
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PEKERJAAN");
        params2.put("func_name", "find_all_select2");
        List listJenisPekerjaan = plsqlService.findAll(params2);
        modelMap.put("listJenisPekerjaan", listJenisPekerjaan);

        // Isi kode form untuk mengedit data lama
        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_PRODUCT");
        params.put("func_name", "get_one");
        params.put("prm_nomor_product", id);
        Map<String, Object> data = plsqlService.findOne(params);
        modelMap.addAllAttributes(data);

        modelMap.put("label", "update");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.product.list";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") String id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_PRODUCT");
        params.put("func_name", "get_one");
        params.put("prm_nomor_product", id);
        Map<String, Object> data = plsqlService.findOne(params);
        modelMap.addAllAttributes(data);

        // Pengambilan data manufaktur
        Map<String, String> params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_MANUFAKTUR");
        params2.put("func_name", "find_all_select2");
        List listManufaktur = plsqlService.findAll(params2);
        modelMap.put("listManufaktur", listManufaktur);

        // Pengambilan data jenis produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listJenisProduk = plsqlService.findAll(params2);
        modelMap.put("listJenisProduk", listJenisProduk);

        // Pengambilan data jenis komponen
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_KOMPONEN");
        params2.put("func_name", "find_all_select2");
        List listJenisKomponen = plsqlService.findAll(params2);
        modelMap.put("listJenisKomponen", listJenisKomponen);

        // Pengambilan data kategori produk
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_KATEGORI_PRODUK");
        params2.put("func_name", "find_all_select2");
        List listKategoriProduk = plsqlService.findAll(params2);
        modelMap.put("listKategoriProduk", listKategoriProduk);


        // Pengambilan data jenis pekerjaan
        params2 = new HashMap<>();
        params2.put("pkg_name", "P_MASTER_JENIS_PEKERJAAN");
        params2.put("func_name", "find_all_select2");
        List listJenisPekerjaan = plsqlService.findAll(params2);
        modelMap.put("listJenisPekerjaan", listJenisPekerjaan);

        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.product.list";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") String id, ModelMap modelMap, HttpServletRequest request) {

        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("pkg_name", "P_PRODUCT");
        paramSourceIn.put("func_name", "edit_baru");

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
        paramSourceIn.put("prm_nomor_produk",id);

        //Long retValue = plsqlService.update("p_product", paramSourceIn);
        String retValue = plsqlService.save(paramSourceIn);
        //Map mapTest = plsqlService.returnMapMessage(retValue);

        /*if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }*/
        modelMap.put("ret_code", id);
        if(retValue.contains("0:")){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }


        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("retValue", retValue);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/product";
    }

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk melihat data detail
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.product.detail";
    }

    // Menampilkan data produk
    @RequestMapping(method = RequestMethod.GET)
    public String find(
            @RequestParam(value = "success", defaultValue = "") Boolean success,
                       ModelMap modelMap, HttpServletRequest request) {
        long pageLength = 5; //panjang halaman
        //ambil data dari database
        Map dataMap = plsqlService.find("P_PRODUCT", 0, pageLength, "NOMOR_PRODUK", "ASC","");
        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("success", success);
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        //panggil jsp
        return "app.admin.management-product.product.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_PRODUCT", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") String id, ModelMap modelMap){

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("pkg_name", "P_PRODUCT");
        paramSourceIn.put("func_name", "hapus_baru");
        paramSourceIn.put("prm_nomor_produk", id);
        plsqlService.save(paramSourceIn);

        modelMap.put("success", true);
        modelMap.put("success_delete", true);
        modelMap.put("message", "Data Berhasil Dihapus");

        modelMap.put("ret_code", 1);
        modelMap.put("ret_class", "note-success");
        modelMap.put("ret_message", "Data Berhasil Dihapus");

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/product";
    }

    //proses simpan file
    @RequestMapping(value = "/saveProductFile", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveProductFile(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            // Isi temporary file
            MultipartFile multipartFile = request.getFile("fileHPP");
            File tempProductFile = File.createTempFile(multipartFile.getOriginalFilename(),"");
            multipartFile.transferTo(tempProductFile);
            Map<String, Object> params = new HashMap<>();
            params.put("prm_content_type",multipartFile.getContentType());
            params.put("prm_content",tempProductFile);
            params.put("prm_name",multipartFile.getOriginalFilename());
            params.put("prm_file_size",multipartFile.getSize());
            fileService.saveToDB(params);

            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            e.printStackTrace();
        }

        return retValue;
    }

    //proses get file
    @RequestMapping(value = "/simpanDrawingMultiPDF", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> simpanDrawingMultiPDFGet(HttpServletRequest request){


        //sementara di hardcode dulu. PR untuk besok. nnti harus diambil
        // dengan menggunakan parameter atau pake session dulu.
        String prm_nomor_produk = "IMO-GRD-101";

        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "get_multi_drawing_pdf");
        params.put("prm_nomor_produk", prm_nomor_produk);
        List<Map<String, Object>> listDrawingMultiPDF = plsqlService.findAll(params);

        Map<String, Object> retValue = new HashMap<>();
        retValue.put("files", listDrawingMultiPDF);
        return retValue;
    }

    //proses simpan file
    @RequestMapping(value = "/simpanDrawingMultiPDF", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> simpanDrawingMultiPDFPost(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            MultipartFile multipartFile = request.getFile("PRM_MULTI_DRAWING_PDF");

            Map<String, Object> paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_PRODUCT");
            paramSourceIn.put("func_name", "simpan_drawing_multi_PDF");
            paramSourceIn.put("param_count", 5);

            //set prm_nomor_produk
            Map<String, Object> prm_nomor_produk = new HashMap<>();
            prm_nomor_produk.put("index", 1);
            prm_nomor_produk.put("type", AppUtil.ECAT_STRING);
            prm_nomor_produk.put("content", request.getParameter("PRM_NOMOR_PRODUK"));
            paramSourceIn.put("prm_nomor_produk", prm_nomor_produk);

            //set prm_content_type
            Map<String, Object> prm_content_type = new HashMap<>();
            prm_content_type.put("index", 2);
            prm_content_type.put("type", AppUtil.ECAT_STRING);
            prm_content_type.put("content", multipartFile.getContentType());
            paramSourceIn.put("prm_content_type", prm_content_type);

            //set prm_name
            Map<String, Object> prm_name = new HashMap<>();
            prm_name.put("index", 3);
            prm_name.put("type", AppUtil.ECAT_STRING);
            prm_name.put("content", multipartFile.getOriginalFilename());
            paramSourceIn.put("prm_name", prm_name);

            //set prm_file_size
            Map<String, Object> prm_file_size = new HashMap<>();
            prm_file_size.put("index", 4);
            prm_file_size.put("type", AppUtil.ECAT_STRING);
            prm_file_size.put("content", multipartFile.getSize());
            paramSourceIn.put("prm_file_size", prm_file_size);

            try {
                //File prm_multi_drawing_pdf
                File tempMultiDrawingPDF = File.createTempFile("tmp","");
                multipartFile.transferTo(tempMultiDrawingPDF);
                Map<String, Object> mapMultiDrawingPDF = new HashMap<>();
                mapMultiDrawingPDF.put("index", 5);
                mapMultiDrawingPDF.put("type", AppUtil.ECAT_FILE);
                mapMultiDrawingPDF.put("content", tempMultiDrawingPDF);
                paramSourceIn.put("prm_multi_drawing_pdf", mapMultiDrawingPDF);

            }catch (Exception e){
                e.printStackTrace();
            }

            plsqlService.saveWithFile(paramSourceIn);


            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            retValue.put("success", false);
            retValue.put("message", e.getMessage());
        }

        return retValue;
    }


    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/get2Dthumbnail", method = RequestMethod.GET)
    @ResponseBody
    public Map get2Dthumbnail(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            @RequestParam(value = "prm_nomor_produk", defaultValue = "") String prmNomorProduk,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_TRANS_FILES_FOR_DELETE", start, length, sortBy,
                sortDir.toUpperCase(), search.toUpperCase(), prmNomorProduk);
        map.put("draw", draw);
        System.out.println("test");
        return map;
    }

    //proses get file
    @RequestMapping(value = "/set_primary_2D", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> setPrimary2D(HttpServletRequest request){

        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_FILES_2D_THUMBNAIL");
        params.put("func_name", "set_primary_2D");
        params.put("prm_nomor_produk", request.getParameter("prm_nomor_produk"));
        params.put("prm_id_file", request.getParameter("prm_id_file"));
        String retValue = plsqlService.save(params);
        Map retMap = plsqlService.returnMapMessage(retValue);
        return retMap ;
    }

    //proses simpan file
    @RequestMapping(value = "/simpan2Dthumbnail", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> simpan2DthumbnailPost(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            MultipartFile multipartFile = request.getFile("PRM_2D_THUMBNAIL");

            Map<String, Object> paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_PRODUCT");
            paramSourceIn.put("func_name", "SIMPAN_2D_THUMBNAIL");
            paramSourceIn.put("param_count", 5);

            //set prm_nomor_produk
            Map<String, Object> prm_nomor_produk = new HashMap<>();
            prm_nomor_produk.put("index", 1);
            prm_nomor_produk.put("type", AppUtil.ECAT_STRING);
            prm_nomor_produk.put("content", request.getParameter("PRM_NOMOR_PRODUK"));
            paramSourceIn.put("prm_nomor_produk", prm_nomor_produk);

            //set prm_content_type
            Map<String, Object> prm_content_type = new HashMap<>();
            prm_content_type.put("index", 2);
            prm_content_type.put("type", AppUtil.ECAT_STRING);
            prm_content_type.put("content", multipartFile.getContentType());
            paramSourceIn.put("prm_content_type", prm_content_type);

            //set prm_name
            Map<String, Object> prm_name = new HashMap<>();
            prm_name.put("index", 3);
            prm_name.put("type", AppUtil.ECAT_STRING);
            prm_name.put("content", multipartFile.getOriginalFilename());
            paramSourceIn.put("prm_name", prm_name);

            //set prm_file_size
            Map<String, Object> prm_file_size = new HashMap<>();
            prm_file_size.put("index", 4);
            prm_file_size.put("type", AppUtil.ECAT_STRING);
            prm_file_size.put("content", multipartFile.getSize());
            paramSourceIn.put("prm_file_size", prm_file_size);

            try {
                //File prm_multi_drawing_pdf
                File tempMultiDrawingPDF = File.createTempFile("tmp","");
                multipartFile.transferTo(tempMultiDrawingPDF);
                Map<String, Object> mapMultiDrawingPDF = new HashMap<>();
                mapMultiDrawingPDF.put("index", 5);
                mapMultiDrawingPDF.put("type", AppUtil.ECAT_FILE);
                mapMultiDrawingPDF.put("content", tempMultiDrawingPDF);
                paramSourceIn.put("prm_multi_drawing_pdf", mapMultiDrawingPDF);

            }catch (Exception e){
                e.printStackTrace();
            }

            plsqlService.saveWithFile(paramSourceIn);


            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            retValue.put("success", false);
            retValue.put("message", e.getMessage());
        }

        return retValue;
    }

    @RequestMapping(value = "/delete-file", method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> deleteFile(HttpServletRequest request) {

        String id_file = request.getParameter("id");
        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "hapus_file");
        params.put("prm_id_file", id_file);

        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;
    }


    //proses simpan file
    @RequestMapping(value = "/saveProductImage3D", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveProductImage3D(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            String nomor_produk = request.getParameter("PRM_NOMOR_PRODUK");
            String prm_jenis_upload = request.getParameter("PRM_JENIS_UPLOAD");
            System.out.println(nomor_produk);

            // Isi temporary file
            MultipartFile multipartFile = request.getFile("fileImage3D");
            File tempProductFile = File.createTempFile(multipartFile.getOriginalFilename(),"");
            multipartFile.transferTo(tempProductFile);
            Map<String, Object> params = new HashMap<>();
            params.put("prm_content_type",multipartFile.getContentType());
            params.put("prm_content",tempProductFile);
            params.put("prm_name",multipartFile.getOriginalFilename());
            params.put("prm_file_size",multipartFile.getSize());
            params.put("prm_nomor_produk",nomor_produk);
            params.put("prm_jenis_upload",prm_jenis_upload);
            fileService.simpanGambar3D(params);


            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            e.printStackTrace();
        }

        return retValue;
    }

    @RequestMapping(value = "/construct-tree", params = "id", method = RequestMethod.GET)
    @ResponseBody
    public List<Map<String, Object>> constructTree(@RequestParam("id") String id){

        Map<String, String> paramSourceIn = null;
        List<Map<String,Object>> listProductGroup = null;
        Map<String, Object> retValue = new HashMap<>();

        if(id ==null || id.equals("#") || id.equals("0")){

            listProductGroup = new ArrayList<>();
            paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_PRODUCT");
            paramSourceIn.put("func_name", "get_group_tree");
            paramSourceIn.put("prm_id_parent", null);
            listProductGroup = plsqlService.findAll(paramSourceIn);
//            listProductGroup = plsqlService.findAll(paramSourceIn);

        }else{

            listProductGroup = new ArrayList<>();
            paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_PRODUCT");
            paramSourceIn.put("func_name", "get_group_tree");
            paramSourceIn.put("prm_id_parent",id);
            listProductGroup = plsqlService.findAll(paramSourceIn);

        }


//        retValue.put("nodes", listProductGroup);
        return listProductGroup;
    }


    @RequestMapping(value = "/delete-2d-thumbnail", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete2DThumbnail(HttpServletRequest request) {

        String id_file = request.getParameter("id");
        String nomor_produk = request.getParameter("nomor_produk");
        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "delete_2d_thumbnail");
        params.put("prm_nomor_produk", nomor_produk);
        params.put("prm_id_file", id_file);

        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;
    }

    @RequestMapping(value = "/hapus-image-3D", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> hapusImage3D(HttpServletRequest request) {

        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "hapus_image_3D");
        params.put("prm_nomor_produk", request.getParameter("prm_nomor_produk"));
        params.put("prm_jns_upload", request.getParameter("prm_jns_upload"));

        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;

    }


    //proses get file
    @RequestMapping(value = "/simpan2DZoom", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> simpan2DZoomGet(HttpServletRequest request){


        //sementara di hardcode dulu. PR untuk besok. nnti harus diambil
        // dengan menggunakan parameter atau pake session dulu.
        String prm_nomor_produk = request.getParameter("nomor_produk");

        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "get_2D_zoom");
        params.put("prm_nomor_produk", prm_nomor_produk);
        List<Map<String, Object>> listDrawingMultiPDF = plsqlService.findAll(params);

        Map<String, Object> retValue = new HashMap<>();
        retValue.put("files", listDrawingMultiPDF);
        return retValue;
    }


    //proses simpan file
    @RequestMapping(value = "/simpan2DZoom", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> simpan2DZoomPost(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            MultipartFile multipartFile = request.getFile("PRM_2D_ZOOM");

            Map<String, Object> paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_TRANS_PRODUCT_FILE");
            paramSourceIn.put("func_name", "SIMPAN_FILE");
            paramSourceIn.put("param_count", 10);

            //set prm_nomor_produk
            Map<String, Object> prm_nomor_produk = new HashMap<>();
            prm_nomor_produk.put("index", 1);
            prm_nomor_produk.put("type", AppUtil.ECAT_STRING);
            prm_nomor_produk.put("content", request.getParameter("PRM_NOMOR_PRODUK"));
            paramSourceIn.put("prm_nomor_produk", prm_nomor_produk);

            //set prm_content_type
            Map<String, Object> prm_content_type = new HashMap<>();
            prm_content_type.put("index", 2);
            prm_content_type.put("type", AppUtil.ECAT_STRING);
            prm_content_type.put("content", multipartFile.getContentType());
            paramSourceIn.put("prm_content_type", prm_content_type);

            //set prm_name
            Map<String, Object> prm_name = new HashMap<>();
            prm_name.put("index", 3);
            prm_name.put("type", AppUtil.ECAT_STRING);
            prm_name.put("content", multipartFile.getOriginalFilename());
            paramSourceIn.put("prm_name", prm_name);

            //set prm_file_size
            Map<String, Object> prm_file_size = new HashMap<>();
            prm_file_size.put("index", 4);
            prm_file_size.put("type", AppUtil.ECAT_STRING);
            prm_file_size.put("content", multipartFile.getSize());
            paramSourceIn.put("prm_file_size", prm_file_size);

            //set prm_file_size
            Map<String, Object> prm_jns_upload = new HashMap<>();
            prm_jns_upload.put("index", 5);
            prm_jns_upload.put("type", AppUtil.ECAT_STRING);
            prm_jns_upload.put("content", request.getParameter("PRM_JENIS_UPLOAD"));
            paramSourceIn.put("prm_jns_upload", prm_jns_upload);

            System.out.println("jenis file = " + request.getParameter("PRM_JENIS_UPLOAD"));
            try {

                if(request.getParameter("PRM_JENIS_UPLOAD").equals("2")){
                    // jenis upload 2 : upload file gambar. harus dibuat multi size

                    //File prm_original_image
                    File tempMulti = File.createTempFile("tmp","");
                    multipartFile.transferTo(tempMulti);
                    Map<String, Object> map2DZoom= new HashMap<>();
                    map2DZoom.put("index", 6);
                    map2DZoom.put("type", AppUtil.ECAT_FILE);
                    map2DZoom.put("content", tempMulti);
                    paramSourceIn.put("prm_content_file", map2DZoom);

                    // --- File prm_file_small ---
                    BufferedImage originalImageSmall = ImageIO.read(tempMulti);
                    int type = originalImageSmall.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImageSmall.getType();
                    BufferedImage resizeImageJpgSmall = resizeImage(originalImageSmall, type, IMG_SMALL);
                    File tempImageSmall = File.createTempFile("tmp", "jpg");
                    ImageIO.write(resizeImageJpgSmall, "jpg", tempImageSmall);

                    Map<String, Object> dummyFileSmall = new HashMap<>();
                    dummyFileSmall.put("index", 7);
                    dummyFileSmall.put("type", AppUtil.ECAT_FILE);
                    dummyFileSmall.put("content", tempImageSmall);
                    paramSourceIn.put("prm_content_file_small", dummyFileSmall);

                    //set prm_file_size small
                    Map<String, Object> dummyFileSizeSmall = new HashMap<>();
                    dummyFileSizeSmall.put("index", 8);
                    dummyFileSizeSmall.put("type", AppUtil.ECAT_STRING);
                    dummyFileSizeSmall.put("content", tempImageSmall.length());
                    paramSourceIn.put("prm_file_size_small", dummyFileSizeSmall);


                    // --- File prm_file_medium ---
                    BufferedImage originalImageMedium = ImageIO.read(tempMulti);
                    type = originalImageMedium.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImageMedium.getType();
                    BufferedImage resizeImageJpgMedium = resizeImage(originalImageMedium, type, IMG_MEDIUM);
                    File tempImageMedium = File.createTempFile("tmp", "jpg");
                    ImageIO.write(resizeImageJpgMedium, "jpg", tempImageMedium);

                    System.out.println("lenght = " + tempImageMedium.length());
                    System.out.println("total space = " + tempImageMedium.getTotalSpace());

                    Map<String, Object> dummyFileMedium= new HashMap<>();
                    dummyFileMedium.put("index", 9);
                    dummyFileMedium.put("type", AppUtil.ECAT_FILE);
                    dummyFileMedium.put("content", tempImageMedium);
                    paramSourceIn.put("prm_content_file_medium", dummyFileMedium);

                    //set prm_file_size medium
                    Map<String, Object> dummyFileSizeMedium = new HashMap<>();
                    dummyFileSizeMedium.put("index", 10);
                    dummyFileSizeMedium.put("type", AppUtil.ECAT_STRING);
                    dummyFileSizeMedium.put("content", tempImageMedium.length());
                    paramSourceIn.put("prm_file_size_medium", dummyFileSizeMedium);

                }else{
                    // jenis upload 1 : upload file pdf/zip. tidak perlu dibuat multi size
                    File tempMulti = File.createTempFile("tmp","");
                    multipartFile.transferTo(tempMulti);
                    Map<String, Object> map2DZoom= new HashMap<>();
                    map2DZoom.put("index", 6);
                    map2DZoom.put("type", AppUtil.ECAT_FILE);
                    map2DZoom.put("content", tempMulti);
                    paramSourceIn.put("prm_content_file", map2DZoom);

                    Map<String, Object> dummyFileSmall = new HashMap<>();
                    dummyFileSmall.put("index", 7);
                    dummyFileSmall.put("type", AppUtil.ECAT_FILE);
                    dummyFileSmall.put("content", null);
                    paramSourceIn.put("prm_content_file_small", dummyFileSmall);

                    //set prm_file_size small
                    Map<String, Object> dummyFileSizeSmall = new HashMap<>();
                    dummyFileSizeSmall.put("index", 8);
                    dummyFileSizeSmall.put("type", AppUtil.ECAT_STRING);
                    dummyFileSizeSmall.put("content", 0);
                    paramSourceIn.put("prm_file_size_small", dummyFileSizeSmall);

                    Map<String, Object> dummyFileMedium = new HashMap<>();
                    dummyFileMedium.put("index", 9);
                    dummyFileMedium.put("type", AppUtil.ECAT_FILE);
                    dummyFileMedium.put("content", null);
                    paramSourceIn.put("prm_content_file_medium", dummyFileMedium);

                    //set prm_file_size medium
                    Map<String, Object> dummyFileSizeMedium = new HashMap<>();
                    dummyFileSizeMedium.put("index", 10);
                    dummyFileSizeMedium.put("type", AppUtil.ECAT_STRING);
                    dummyFileSizeMedium.put("content", 0);
                    paramSourceIn.put("prm_file_size_medium", dummyFileSizeMedium);

                }



            }catch (Exception e){
                e.printStackTrace();
            }
            //sout

            String test = plsqlService.saveWithFile(paramSourceIn);

            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            retValue.put("success", false);
            retValue.put("message", e.getMessage());
        }

        return retValue;
    }

    @RequestMapping(value = "/delete-2d-zoom", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> delete2DZoom(HttpServletRequest request) {

        String id_file = request.getParameter("id");
        String nomor_produk = request.getParameter("nomor_produk");
        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_PRODUCT_FILE");
        params.put("func_name", "delete_2d_zoom");
        params.put("prm_nomor_produk", nomor_produk);
        params.put("prm_id_file", id_file);

        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;
    }

    @RequestMapping(value = "/hapus_gambar_2D", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> hapusGambar2D(HttpServletRequest request) {

        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_FILES_2D_THUMBNAIL");
        params.put("func_name", "hapus_gambar_2d");
        params.put("prm_id_product_file", request.getParameter("prm_id_product_file"));
        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;
    }

    // Menampilkan halaman create
    @RequestMapping(value = "/readmore", params = "id", method = RequestMethod.GET)
    public String formReadmore(@RequestParam("id") String id, ModelMap modelMap){

        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_PRODUCT");
        params.put("func_name", "get_one");
        params.put("prm_nomor_product", id);
        Map<String, Object> data = plsqlService.findOne(params);
        modelMap.addAllAttributes(data);

        // Isi kode form untuk membuat data baru
        modelMap.put("label", "readmore");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.product.readmore";
    }

    @RequestMapping(value = "/readmore/create", method = RequestMethod.POST)
    public String createReadmore(HttpServletRequest request, ModelMap modelMap){
        Map<String, String[]> paramRequest = request.getParameterMap();

        Map<String, Object> params = new HashMap<>();
        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long retValue = plsqlService.save("P_MASTER_READMORE",params);
        System.out.println(retValue);
        modelMap.put("success", true);
        modelMap.put("success_create", true);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/product";
    }

    @RequestMapping(value = "/readmore/update", params = "id", method = RequestMethod.POST)
    public String updateReadmore(@RequestParam("id") String id, ModelMap modelMap, HttpServletRequest request){

        // Isi kode form untuk mengedit data lama
        Map<String, String[]> paramRequest = request.getParameterMap();

        Map<String, Object> params = new HashMap<>();
        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        params.put("prm_nomor_product",id);

        Long retValue = plsqlService.update("P_MASTER_READMORE", params);
        System.out.println(retValue);
        modelMap.put("success", true);
        modelMap.put("success_update", true);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/product";
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int fileSize){

        int IMG_WIDTH = 64;
        int IMG_HEIGHT = 64;

        switch (fileSize){
            //small
            case 1 :
                IMG_WIDTH= 64;
                IMG_HEIGHT = 64;
                break;
            case 2 :
                IMG_WIDTH= 320;
                IMG_HEIGHT = 240;
                break;
            default:
                IMG_WIDTH= 320;
                IMG_HEIGHT = 240;
                break;
        }

        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        return resizedImage;
    }
}
