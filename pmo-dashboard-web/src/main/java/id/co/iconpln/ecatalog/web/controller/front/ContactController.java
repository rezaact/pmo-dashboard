package id.co.iconpln.ecatalog.web.controller.front;


import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/contact")
public class ContactController {
    @Autowired
    PlsqlService plsqlService;

    //menampilkan halaman data
    @RequestMapping(
            params = "id",
            method = RequestMethod.GET)
    public String findAll(ModelMap modelMap,
             @RequestParam(value = "id") String id
    ){
        Map<String, String>  params = new HashMap<>();
        params.put("pkg_name", "P_PRODUCT_FRONT");
        params.put("func_name", "get_one_id_image");
        params.put("prm_nomor_produk", String.valueOf(id));

        Map<String, Object> idImage= plsqlService.findOne(params);
//        List<Map<String, Object>> listRole = plsqlService.findAll("P_MASTER_ROLE", params);
        modelMap.put("idImage", idImage);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.contact";
    }
    //menampilkan halaman data
    @RequestMapping(method = RequestMethod.GET)
    public String findAllMenu(ModelMap modelMap){
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.contact";
    }
//    // proses penyimpanan data
    @RequestMapping(value ="/create", method = RequestMethod.POST)
    public String sent(ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

        params.put("prm_nomor_produk", paramRequest.get("prm_nomor_produk")[0]);
        params.put("prm_nama", paramRequest.get("prm_nama")[0]);
        params.put("prm_email",paramRequest.get("prm_email")[0]);
        params.put("prm_telp",paramRequest.get("prm_telp")[0]);
        params.put("prm_pesan",paramRequest.get("prm_pesan")[0]);

        Long id = plsqlService.save("p_contact_us",params);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/contact?id="+paramRequest.get("prm_nomor_produk")[0];
    }

    @RequestMapping(value ="/create/non", method = RequestMethod.POST)
    public String nologin(ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

        params.put("prm_nomor_produk", paramRequest.get("prm_nomor_produk")[0]);
        params.put("prm_nama", paramRequest.get("prm_nama")[0]);
        params.put("prm_email",paramRequest.get("prm_email")[0]);
        params.put("prm_telp",paramRequest.get("prm_telp")[0]);
        params.put("prm_pesan",paramRequest.get("prm_pesan")[0]);

        Long id = plsqlService.save("p_contact_us",params);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/contact";
    }
}
