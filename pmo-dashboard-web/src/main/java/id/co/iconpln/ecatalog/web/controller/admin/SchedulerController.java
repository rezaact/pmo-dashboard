package id.co.iconpln.ecatalog.web.controller.admin;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.db.service.SQLServerService;
import id.iconpln.ecatalog.service.SchedulerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rezafit on 02/12/2016.
 */

@Controller
@RequestMapping("/scheduler")
public class SchedulerController {

    @Autowired
    SchedulerService schedulerService;

    @Autowired
    PlsqlService plsqlService;

    @Autowired
    SQLServerService sqlServerService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/start", method = RequestMethod.GET)
    @ResponseBody
    public String startScheduler(ModelMap modelMap){

        try{
            //schedulerService.start();

            System.out.println("status : START at " + new Date());

            Map<String, String> params = new HashMap<>();
            params.put("pkg_name", "P_SYNCH_TABLE");
            params.put("func_name", "find_all_table");
            params.put("p_tmp", null);

            List<Map<String, Object>> listTable = plsqlService.findAll(params);
            String [] sqlInsert = null;
            for(Map<String, Object> mapTable : listTable){
                //System.out.println("ORCL_TBL_NAME = " + mapRec.get("ORCL_TBL_NAME"));
                System.out.println("------Inserting table " + mapTable.get("TMP_ORCL_TBL_NAME") + " to " + mapTable.get("ORCL_TBL_NAME") + "--------");
                Map<String, String> paramsCol = new HashMap<>();
                paramsCol.put("pkg_name", "P_SYNCH_TABLE");
                paramsCol.put("func_name", "find_all_table_column");
                paramsCol.put("p_table_name", mapTable.get("SQLSERV_TBL_NAME").toString());
                List<Map<String, Object>> listColumn = plsqlService.findAll(paramsCol);
                mapTable.put("listColumn", listColumn);
                sqlInsert = sqlServerService.testConnection(mapTable);
                plsqlService.batchUpdate(sqlInsert);
            }


            System.out.println("status : SUCCESS at " + new Date());
            //sqlServerService.testConnection(listRec);


        }catch (Exception e){
            //e.printStackTrace();
            System.out.println("status : ERROR, " + e.getMessage());
        }

        return null;
    }

    @RequestMapping(value = "/sssss", method = RequestMethod.GET)
    @ResponseBody
    public String stopScheduler(ModelMap modelMap, HttpServletRequest req){

        String pass = req.getParameter("pass");

        System.out.println("pass encode  = " + passwordEncoder.encode(pass));
        /*try{
            schedulerService.stop();
        }catch (Exception e){
            e.printStackTrace();
        }*/
        return null;
    }


    @RequestMapping(value = "/stop", method = RequestMethod.GET)
    @ResponseBody
    public String FUNC_PMO_BOBOT_MULTI_ASSET(ModelMap modelMap, HttpServletRequest req){

        String pass = req.getParameter("pass");

        System.out.println("pass encode  = " + passwordEncoder.encode(pass));
        /*try{
            schedulerService.stop();
        }catch (Exception e){
            e.printStackTrace();
        }*/
        return null;
    }

}
