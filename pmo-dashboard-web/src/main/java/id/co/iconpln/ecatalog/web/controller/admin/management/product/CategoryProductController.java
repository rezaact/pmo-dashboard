package id.co.iconpln.ecatalog.web.controller.admin.management.product;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by barka01 on 14/05/15.
 */
@Controller
@RequestMapping("/admin/category/product")
public class CategoryProductController {

    @Autowired
    PlsqlService plsqlService;

    // Menampilkan data json untuk plugin select2
    @RequestMapping(value = "/find_all_select2", method = RequestMethod.GET)
    public @ResponseBody
    List<Map<String, Object>> findAllSelect2(){

        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_MASTER_KATEGORI_PRODUK");
        params.put("func_name", "find_all_select2");
        return plsqlService.findAll(params);
    }


}
