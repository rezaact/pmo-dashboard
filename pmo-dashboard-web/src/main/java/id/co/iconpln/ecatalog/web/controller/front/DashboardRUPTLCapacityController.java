package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/dashboard-ruptl-capacity")
public class DashboardRUPTLCapacityController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap, HttpServletRequest request){

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        modelMap.put("authUser", UserUtil.getAuthUser());
        request.getSession().setAttribute("prevPage", "/dashboard-ruptl-capacity");
        return "app.front.dashboard.ruptl.main"; //xml
    }


    @RequestMapping(value = "/get-data-electrifikasi",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap get_data_electrifikasi(HttpServletRequest request){
        String typeFilter = request.getParameter("typeFilter");
        String tahun = request.getParameter("tahun");
        String status = request.getParameter("status");
        String program = request.getParameter("program");

        //gen map
        Map<String, Object> pGenChart_OverAll = new HashMap<>();
        pGenChart_OverAll.put("pkg_name", "P_FILTER_ELEKTRIFIKASI");
        pGenChart_OverAll.put("func_name", "gen_map_elektrifikasi");
        pGenChart_OverAll.put("p_type_filter", typeFilter);
        pGenChart_OverAll.put("p_tahun", tahun);
        pGenChart_OverAll.put("p_status", status);
        pGenChart_OverAll.put("p_program", program);
        plsqlService.save(pGenChart_OverAll);

//        //get map
        Map<String, String> pGetChart_All = new HashMap<>();
        pGetChart_All.put("pkg_name", "P_DASH_MAP");
        pGetChart_All.put("func_name", "gen_json_map");

        String typeMap = null;

        switch (Integer.parseInt(typeFilter)){
            case 1 :
                typeMap = "123";
                break;
            case 2 :
                if(status.equals("1")){
                    typeMap = "124";
                }
                else{
                    typeMap = "125";
                }
                break;
            default: typeMap = "126";
        }

        pGetChart_All.put("p_id_rcwd", typeMap);
        pGetChart_All.put("p_filter", null);
        String mapElectricity = plsqlService.findString(pGetChart_All);
        mapElectricity = mapElectricity.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        ModelMap modelMap = new ModelMap();
        modelMap.put("map", mapElectricity);


        Map<String, String> pGetKeterangan = new HashMap<>();
        pGetKeterangan.put("pkg_name", "P_FILTER_ELEKTRIFIKASI");
        pGetKeterangan.put("func_name", "get_keterangan");
        pGetKeterangan.put("p_type_filter", typeFilter);
        pGetKeterangan.put("p_tahun", tahun);
        pGetKeterangan.put("p_status", status);
        pGetKeterangan.put("p_program", program);

        List<Map<String, Object>> listKeterangan = plsqlService.findAll(pGetKeterangan);
        modelMap.put("keterangan", listKeterangan);

        return modelMap;
    }

    @RequestMapping(value = "/get-data-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap get_data_capacity(HttpServletRequest request){

        String cmbJenis = request.getParameter("cmbJenis");
        String cmbSubJenis = request.getParameter("cmbSubJenis");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");
        String cmbGroup = request.getParameter("cmbGroup");
//        String CmbFilterIsActual = request.getParameter("CmbFilterIsActual");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetGenCapacity = new HashMap<>();
        pGetGenCapacity.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetGenCapacity.put("func_name", "gen_capacity_v3");
        //TODO : tambah param type.
        pGetGenCapacity.put("p_jenis", cmbJenis);
        pGetGenCapacity.put("p_subjenis", cmbSubJenis);
        pGetGenCapacity.put("p_thblawal", cmbThnAwal);
        pGetGenCapacity.put("p_thblakhir", cmbThnAkhir);
//        pGetGenCapacity.put("p_isactual", CmbFilterIsActual);

        List<Map<String, Object>> listGenCapcity = plsqlService.findAll(pGetGenCapacity);
        modelMap.put("listGenCapcity",listGenCapcity);

        //tambahan buat chart
        //generation
        //gen chart phase
        Map<String, Object> pGenChart_OverAll = new HashMap<>();
        pGenChart_OverAll.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGenChart_OverAll.put("func_name", "gen_pie_detail_data");
        pGenChart_OverAll.put("p_jenis", cmbJenis);
        pGenChart_OverAll.put("p_subjenis", cmbSubJenis);
        pGenChart_OverAll.put("p_Group", cmbGroup);
        pGenChart_OverAll.put("p_detail_tipe", 3);
        pGenChart_OverAll.put("p_thblawal", cmbThnAwal);
        pGenChart_OverAll.put("p_thblakhir", cmbThnAkhir);
        pGenChart_OverAll.put("p_asset_type", 17);
        if(cmbJenis.equals("3")){
            plsqlService.save(pGenChart_OverAll);
        }

        //get chart phase
        Map<String, String> pGetChart_All = new HashMap<>();
        pGetChart_All.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetChart_All.put("func_name", "get_chart_cocd_by_region");
        pGetChart_All.put("p_region", String.valueOf(113));
        String strChart_All = null;


        if(cmbJenis.equals("3")){
            strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("chartAllGenerationPhase", strChart_All);
        }
        else{
            modelMap.put("chartAllGenerationPhase", "");
        }

        //gen chart type
        pGenChart_OverAll.put("p_detail_tipe", 2);
        pGenChart_OverAll.put("p_asset_type", 17);
        plsqlService.save(pGenChart_OverAll);
        //get chart type
        pGetChart_All.put("p_region", String.valueOf(104));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllGenerationType", strChart_All);

        //gen chart region
        pGenChart_OverAll.put("p_detail_tipe", 1);
        pGenChart_OverAll.put("p_asset_type", 17);
        plsqlService.save(pGenChart_OverAll);
        //get chart region
        pGetChart_All.put("p_region", String.valueOf(112));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllGenerationRegion", strChart_All);
        //end generation

        //transmission
        //gen chart phase
        pGenChart_OverAll.put("p_detail_tipe", 3);
        pGenChart_OverAll.put("p_asset_type", 18);
        if(cmbJenis.equals("3")){
            plsqlService.save(pGenChart_OverAll);
        }
        //get chart phase
        pGetChart_All.put("p_region", String.valueOf(113));


        if(cmbJenis.equals("3")){
            strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("chartAllTransmissionPhase", strChart_All);
        }
        else{
            modelMap.put("chartAllTransmissionPhase", "");
        }

        //gen chart type
        pGenChart_OverAll.put("p_detail_tipe", 2);
        pGenChart_OverAll.put("p_asset_type", 18);
        plsqlService.save(pGenChart_OverAll);
        //get chart type
        pGetChart_All.put("p_region", String.valueOf(104));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllTransmissionType", strChart_All);

        //gen chart region
        pGenChart_OverAll.put("p_detail_tipe", 1);
        pGenChart_OverAll.put("p_asset_type", 18);
        plsqlService.save(pGenChart_OverAll);
        //get chart region
        pGetChart_All.put("p_region", String.valueOf(112));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllTransmissionRegion", strChart_All);
        //end transmission

        //substation
        //gen chart phase
        pGenChart_OverAll.put("p_detail_tipe", 3);
        pGenChart_OverAll.put("p_asset_type", 19);

        if(cmbJenis.equals("3")){
            plsqlService.save(pGenChart_OverAll);
        }
        //get chart phase
        pGetChart_All.put("p_region", String.valueOf(113));

        if(cmbJenis.equals("3")){
            strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("chartAllSubstationPhase", strChart_All);
        }
        else{
            modelMap.put("chartAllSubstationPhase", "");
        }


        //gen chart type
        pGenChart_OverAll.put("p_detail_tipe", 2);
        pGenChart_OverAll.put("p_asset_type", 19);
        plsqlService.save(pGenChart_OverAll);
        //get chart type
        pGetChart_All.put("p_region", String.valueOf(104));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllSubstationType", strChart_All);

        //gen chart region
        pGenChart_OverAll.put("p_detail_tipe", 1);
        pGenChart_OverAll.put("p_asset_type", 19);
        plsqlService.save(pGenChart_OverAll);
        //get chart region
        pGetChart_All.put("p_region", String.valueOf(112));
        strChart_All = plsqlService.findString(pGetChart_All);
        strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
        modelMap.put("chartAllSubstationRegion", strChart_All);
        //endsubstation


        //for test tab only
//        modelMap.put("chartAllGenerationType", strChart_All);
//        modelMap.put("chartAllGenerationRegion", strChart_All);
//        modelMap.put("chartAllTransmissionPhase", strChart_All);
//        modelMap.put("chartAllTransmissionType", strChart_All);
//        modelMap.put("chartAllTransmissionRegion", strChart_All);
//        modelMap.put("chartAllSubstationPhase", strChart_All);
//        modelMap.put("chartAllSubstationType", strChart_All);
//        modelMap.put("chartAllSubstationRegion", strChart_All);

        return modelMap;
    }

    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    public String detail(ModelMap modelMap, HttpServletRequest request){

        String jenis = request.getParameter("cmbJenis");
        String subJenis = request.getParameter("cmbSubJenis");
        String tahunAkhir = request.getParameter("cmbThnAkhir");
        String tahunAwal = request.getParameter("cmbThnAwal");
        String detailType = request.getParameter("detailType");
        String group = request.getParameter("group");
        String AssetType = request.getParameter("AssetType");
//        String CmbFilterIsActual = request.getParameter("CmbFilterIsActual");


        modelMap.put("authUser", UserUtil.getAuthUser());
        modelMap.put("jenis", jenis);
        modelMap.put("subJenis", subJenis);
        modelMap.put("tahunAkhir", tahunAkhir);
        modelMap.put("tahunAwal", tahunAwal);
        modelMap.put("detailType", detailType);
        modelMap.put("group", group);
        modelMap.put("AssetType", AssetType);
//      modelMap.put("CmbFilterIsActual", CmbFilterIsActual);


        //generate data OverAll
        Map<String, Object> pGenChart_OverAll = new HashMap<>();
        pGenChart_OverAll.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGenChart_OverAll.put("func_name", "gen_pie_detail_data");
        pGenChart_OverAll.put("p_jenis", jenis);
        pGenChart_OverAll.put("p_subjenis", subJenis);
        pGenChart_OverAll.put("p_Group", group);
        pGenChart_OverAll.put("p_detail_tipe", detailType);
        pGenChart_OverAll.put("p_thblawal", tahunAwal);
        pGenChart_OverAll.put("p_thblakhir", tahunAkhir);
        pGenChart_OverAll.put("p_asset_type", AssetType);
        plsqlService.save(pGenChart_OverAll);


        //Jika Kondisi Detail by Phase dan by Asset
//        if (detailType.equals("2") || detailType.equals("3") ){
        if (detailType.equals("1")){
            Map<String, String> pGetChart_All = new HashMap<>();
            pGetChart_All.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_All.put("func_name", "get_chart_cocd_by_region");
            pGetChart_All.put("p_region", String.valueOf(112));
//            pGetChart_All.put("p_region", String.valueOf(112));
            String strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_All", strChart_All);
        }
        else if (detailType.equals("2")){
            //Generate Data Chart for Tabs



            //generate chart OverAll
            Map<String, String> pGetChart_All = new HashMap<>();
            pGetChart_All.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_All.put("func_name", "get_chart_cocd_by_region");
            pGetChart_All.put("p_region", String.valueOf(104));
            String strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_All", strChart_All);

            // Sumatera
            Map<String, String> pGetChart_Sumatera = new HashMap<>();
            pGetChart_Sumatera.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_Sumatera.put("func_name", "get_chart_cocd_by_region");
            pGetChart_Sumatera.put("p_region", String.valueOf(105));
            String strChart_Sumatera = plsqlService.findString(pGetChart_Sumatera);
            strChart_Sumatera = strChart_Sumatera.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_Sumatera", strChart_Sumatera);

            // Jawa Bagian Barat
            Map<String, String> pGetChart_JBB = new HashMap<>();
            pGetChart_JBB.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBB.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBB.put("p_region", String.valueOf(106));
            String strChart_JBB = plsqlService.findString(pGetChart_JBB);
            strChart_JBB = strChart_JBB.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBB", strChart_JBB);

            // Jawa Bagian Tengah
            Map<String, String> pGetChart_JBT = new HashMap<>();
            pGetChart_JBT.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBT.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBT.put("p_region", String.valueOf(107));
            String strChart_JBT = plsqlService.findString(pGetChart_JBT);
            strChart_JBT = strChart_JBT.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBT", strChart_JBT);

            // Jawa Bagian Timur dan Bali
            Map<String, String> pGetChart_JBTB = new HashMap<>();
            pGetChart_JBTB.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBTB.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBTB.put("p_region", String.valueOf(108));
            String strChart_JBTB= plsqlService.findString(pGetChart_JBTB);
            strChart_JBTB = strChart_JBTB.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBTB", strChart_JBTB);

            // Kalimantan
            Map<String, String> pGetChart_KAL = new HashMap<>();
            pGetChart_KAL.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_KAL.put("func_name", "get_chart_cocd_by_region");
            pGetChart_KAL.put("p_region", String.valueOf(109));
            String strChart_KAL = plsqlService.findString(pGetChart_KAL);
            strChart_KAL = strChart_KAL.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_KAL", strChart_KAL);

            // Sulawesi dan Nusa Tenggara
            Map<String, String> pGetChart_SNT = new HashMap<>();
            pGetChart_SNT.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_SNT.put("func_name", "get_chart_cocd_by_region");
            pGetChart_SNT.put("p_region", String.valueOf(110));
            String strChart_SNT= plsqlService.findString(pGetChart_SNT);
            strChart_SNT = strChart_SNT.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_SNT", strChart_SNT);

            // Maluku dan Papua
            Map<String, String> pGetChart_MP = new HashMap<>();
            pGetChart_MP.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_MP.put("func_name", "get_chart_cocd_by_region");
            pGetChart_MP.put("p_region", String.valueOf(111));
            String strChart_MP = plsqlService.findString(pGetChart_MP);
            strChart_MP = strChart_MP.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_MP", strChart_MP);



        }else{
            //generate chart OverAll
            Map<String, String> pGetChart_All = new HashMap<>();
            pGetChart_All.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_All.put("func_name", "get_chart_cocd_by_region");
            pGetChart_All.put("p_region", String.valueOf(113));
//            pGetChart_All.put("p_region", String.valueOf(112));
            String strChart_All = plsqlService.findString(pGetChart_All);
            strChart_All = strChart_All.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_All", strChart_All);


            // Sumatera
            Map<String, String> pGetChart_Sumatera = new HashMap<>();
            pGetChart_Sumatera.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_Sumatera.put("func_name", "get_chart_cocd_by_region");
            pGetChart_Sumatera.put("p_region", String.valueOf(105));
            String strChart_Sumatera = plsqlService.findString(pGetChart_Sumatera);
            strChart_Sumatera = strChart_Sumatera.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_Sumatera", strChart_Sumatera);

            // Jawa Bagian Barat
            Map<String, String> pGetChart_JBB = new HashMap<>();
            pGetChart_JBB.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBB.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBB.put("p_region", String.valueOf(106));
            String strChart_JBB = plsqlService.findString(pGetChart_JBB);
            strChart_JBB = strChart_JBB.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBB", strChart_JBB);

            // Jawa Bagian Tengah
            Map<String, String> pGetChart_JBT = new HashMap<>();
            pGetChart_JBT.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBT.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBT.put("p_region", String.valueOf(107));
            String strChart_JBT = plsqlService.findString(pGetChart_JBT);
            strChart_JBT = strChart_JBT.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBT", strChart_JBT);

            // Jawa Bagian Timur dan Bali
            Map<String, String> pGetChart_JBTB = new HashMap<>();
            pGetChart_JBTB.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_JBTB.put("func_name", "get_chart_cocd_by_region");
            pGetChart_JBTB.put("p_region", String.valueOf(108));
            String strChart_JBTB= plsqlService.findString(pGetChart_JBTB);
            strChart_JBTB = strChart_JBTB.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_JBTB", strChart_JBTB);

            // Kalimantan
            Map<String, String> pGetChart_KAL = new HashMap<>();
            pGetChart_KAL.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_KAL.put("func_name", "get_chart_cocd_by_region");
            pGetChart_KAL.put("p_region", String.valueOf(109));
            String strChart_KAL = plsqlService.findString(pGetChart_KAL);
            strChart_KAL = strChart_KAL.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_KAL", strChart_KAL);

            // Sulawesi dan Nusa Tenggara
            Map<String, String> pGetChart_SNT = new HashMap<>();
            pGetChart_SNT.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_SNT.put("func_name", "get_chart_cocd_by_region");
            pGetChart_SNT.put("p_region", String.valueOf(110));
            String strChart_SNT= plsqlService.findString(pGetChart_SNT);
            strChart_SNT = strChart_SNT.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_SNT", strChart_SNT);

            // Maluku dan Papua
            Map<String, String> pGetChart_MP = new HashMap<>();
            pGetChart_MP.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
            pGetChart_MP.put("func_name", "get_chart_cocd_by_region");
            pGetChart_MP.put("p_region", String.valueOf(111));
            String strChart_MP = plsqlService.findString(pGetChart_MP);
            strChart_MP = strChart_MP.replaceAll("\\n","").replaceAll("\r","").replaceAll(System.lineSeparator(),"");
            modelMap.put("strChart_MP", strChart_MP);

        }

//        request.getSession().setAttribute("prevPage", "/dashboard-cod-capacity/detail");
        request.getSession().setAttribute("prevPage", "/dashboard-ruptl-capacity");

        if (detailType.equals("1")){
            return "app.front.dashboard.ruptl.capacity.detail.regional";
        }else if(detailType.equals("2")){
            return "app.front.dashboard.ruptl.capacity.detail.assets";
        }else{
            return "app.front.dashboard.ruptl.capacity.detail.phase";
        }

        ///return (detailType.equals("1"))?"app.front.dashboard.cod.capacity.detail.regional":"app.front.dashboard.cod.capacity.detail"; //xml
    }

    @RequestMapping(value = "/grid",method = RequestMethod.POST)
    @ResponseBody
    public Map getGrid(
            @RequestParam (value = "draw", defaultValue = "0") String draw,
            @RequestParam(value = "start", defaultValue = "0") String start,
            @RequestParam(value = "length", defaultValue = "10") String length,
            @RequestParam(value = "order[0][column]", defaultValue = "0") String sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            //param untuk filter data
            @RequestParam(value = "p_jenis", defaultValue = "") String p_jenis,
            @RequestParam(value = "p_subjenis", defaultValue = "") String p_subjenis,
            @RequestParam(value = "p_Group", defaultValue = "") String p_Group,
            @RequestParam(value = "p_detail_tipe", defaultValue = "") String p_detail_tipe,
            @RequestParam(value = "p_thblawal", defaultValue = "") String p_thblawal,
            @RequestParam(value = "p_thblakhir", defaultValue = "") String p_thblakhir,
            @RequestParam(value = "p_asset_type", defaultValue = "") String AssetType,
//              @RequestParam(value = "p_isactual", defaultValue = "") String CmbFilterIsActual,
            HttpServletRequest request){


        //paging
        int intStart = Integer.parseInt(start) +1;
        int intLast = Integer.parseInt(start) + Integer.parseInt(length);

        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, String> pGetDetailGen= new HashMap<>();
        pGetDetailGen.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetDetailGen.put("func_name", "func_kapasitas_detail_v4");
        pGetDetailGen.put("p_jenis", p_jenis);
        pGetDetailGen.put("p_subjenis", p_subjenis);
        pGetDetailGen.put("p_Group", p_Group);
        pGetDetailGen.put("p_detail_tipe", p_detail_tipe);
        pGetDetailGen.put("p_thblawal", p_thblawal);
        pGetDetailGen.put("p_thblakhir", p_thblakhir);
        pGetDetailGen.put("p_first", String.valueOf(intStart));
        pGetDetailGen.put("p_last", String.valueOf(intLast));
        pGetDetailGen.put("p_search", search);
        pGetDetailGen.put("p_asset_type", String.valueOf(AssetType));
//        pGetDetailGen.put("p_isactual", String.valueOf(CmbFilterIsActual));
        Map<String, Object> map = plsqlService.findAllForGrid(pGetDetailGen);
        map.put("draw", draw);

        return map;
    }



    //example
/*    @RequestMapping(value = "/get-data-detail-cod-gen-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailCODGenCapacity(HttpServletRequest request){

        String type = request.getParameter("type");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String startPeriod = request.getParameter("startPeriod");


        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetDetailGen= new HashMap<>();
        pGetDetailGen.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetDetailGen.put("func_name", "get_detail_demand");

        //TODO : tambah param type.
        //pGetAssetCapacity.put("p_type", type);
        //pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetDetailGen.put("p_reqion_id", String.valueOf(0));
        //pGetAssetCapacity.put("p_Group", cmbProgram);
        pGetDetailGen.put("p_thbl", String.valueOf(201609));


        List<Map<String, Object>> listDetailGen = plsqlService.findAll(pGetDetailGen);
        modelMap.put("data",listDetailGen);

        return modelMap;
    }*/

}
