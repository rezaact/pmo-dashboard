package id.co.iconpln.ecatalog.web.controller.admin.Message;


import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/message")
public class MessageController {
    @Autowired
    PlsqlService plsqlService;

    //Menampilkan Seluruh Data
    @RequestMapping(method = RequestMethod.GET)
    public String find( ModelMap modelMap  )
    {
        long pageLength =5 ; //panjang halaman
        //get default data
       Map dataMap = plsqlService.find("P_CONTACT_US",0,pageLength,"NAMA","ASC","");

        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());
        //panggil jsp
        return "app.admin.message.list";
    }

    //Proses delete data
    @RequestMapping(value = "/delete", params = "id",method = RequestMethod.GET)
    public String delete(
            @RequestParam("id") long id, ModelMap modelMap)
    {
        Map<String, Object> paramSourceIn =new HashMap<>();
        paramSourceIn.put("prm_id_kontak",id);
        Long retValue = plsqlService.delete("p_contact_us", paramSourceIn);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/message";

    }
    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NAMA") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_CONTACT_US", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    //Proses update status warna pesan
    @RequestMapping(value="/update", params = "id", method = RequestMethod.GET)
    public String update(
            @RequestParam("id")long id, ModelMap modelMap)
    {
        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_id_kontak", id);
        paramSourceIn.put("prm_status",1);
        Long retValue = plsqlService.update("p_contact_us",paramSourceIn);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/message";
    }
//    @RequestMapping(value = "/reply",params = "id",method = RequestMethod.POST)
//    public  String reply( @RequestParam("id")long id, ModelMap modelMap)
//   {
//       Map<String, Object> paramSourceIn = new HashMap<>();
//       paramSourceIn.put("prm_id_kontak", id);
////       paramSourceIn.put("prm_status",1);
//       Long retValue = plsqlService.update("p_contact_us",paramSourceIn);
//       modelMap.put("authUser", UserUtil.getAuthUser());
//        return "app.admin.message.form";
//    }


    // Menampilkan halaman update
    @RequestMapping(value = "/reply", params = "id", method = RequestMethod.GET)
    public String formReply(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_CONTACT_US", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "reply");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.message.form";
    }
    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String reply(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
//
        Long retValue = plsqlService.update("P_CONTACT_US",paramSourceIn);

        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/message";
    }
}
