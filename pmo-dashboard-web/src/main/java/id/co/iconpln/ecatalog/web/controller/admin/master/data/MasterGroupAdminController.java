package id.co.iconpln.ecatalog.web.controller.admin.master.data;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/master-data/group")
public class MasterGroupAdminController {
    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap){
        // Isi kode form untuk membuat data baru
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.workshop.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create( ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long retValue = plsqlService.save("p_workshop",params);
        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }
        modelMap.put("authUser", UserUtil.getAuthUser());
        // Isi kode untuk menyimpan data baru
        return "app.admin.master-data.group.list";
    }

    // Proses Penyimpanan Node
    @RequestMapping(value = "/nodeCreate",  method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> nodeCreate(
            @RequestParam("parent") String prm_parent_id,
            @RequestParam("name") String prm_nama,
            @RequestParam("position") String  prm_level,
            @RequestParam("related") String prm_related
    )
    {
        //save
        Map<String, Object> params = new HashMap<>();
        params.put("prm_parent_id", prm_parent_id);
        params.put("prm_nama", prm_nama);
        params.put("prm_position", prm_level);
        params.put("prm_related", prm_related);
        Long prm_new_id= plsqlService.save("P_MASTER_PRODUCT_GROUP", params);

        //get object untuk membuat json sesuai dengan kebutuhan treetables
        params = new HashMap<>();
        params.put("prm_id_group", prm_new_id);
        params.put("prm_return_type", "TYPE_TREE_TABLE"); //untuk dokumentasi lihat dokumentasi pada package
        Map<String, Object> retValue = plsqlService.findOne("P_MASTER_PRODUCT_GROUP",params);

        return retValue;
    }


    // Proses Penyimpanan Node
    @RequestMapping(value = "/nodeUpdate", method = RequestMethod.GET)
    @ResponseBody
    public   Map<String, Object> nodeUpdate(
            @RequestParam("id") String prm_id_group,
            @RequestParam("name") String prm_nama)
    {


        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_MASTER_PRODUCT_GROUP");
        params.put("func_name", "edit");
        params.put("prm_id_group", prm_id_group);
        params.put("prm_nama", prm_nama);
        plsqlService.save(params);

        //get object untuk membuat json sesuai dengan kebutuhan treetables
        params = new HashMap<>();
        params.put("prm_id_group", prm_id_group);
        params.put("prm_return_type", "TYPE_TREE_TABLE"); //untuk dokumentasi lihat dokumentasi pada package
        Map<String, Object> retValue = plsqlService.findOne("P_MASTER_PRODUCT_GROUP",params);

        return retValue;
    }


    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        return "app.admin.workshop.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.PUT)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
//        paramSourceIn.put("prm_id_workshop", request.getParameter("id"));

        Long retValue = plsqlService.update("p_workshop",paramSourceIn);

        if(retValue>0){
            modelMap.put("message", "Data berhasil di simpan");
        }else{
            modelMap.put("message", "Data gagal di simpan");
        }
        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.group.list";
    }

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") long id, ModelMap modelMap){
        //get default data
        Map<String, Object>  paramSourceIn = new HashMap<>();

        Map<String, Object> detailWorkshop = plsqlService.findOne("p_workshop",paramSourceIn);
        modelMap.put("detailWorkshop", detailWorkshop);
        // Isi kode form untuk melihat data detail
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.group.detail";
    }

    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String findAll(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap)
    {
        //get default data
//        Map<String, Object>  params= new HashMap<>();
//        params.put("prm_start", start);
//        params.put("prm_length", length);
//        params.put("prm_search", " ");
//
//        List<Map<String, Object>> listWorkshop = plsqlService.findAll("p_workshop",params);
//        modelMap.put("listWorkshop", listWorkshop);
        // Pengambilan semua data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.group.list";
    }

    // Menampilkan halaman seluruh data menggunakan json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public @ResponseBody List findJsonAll(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap)
    {
        //get default data
        Map<String, Object>  paramSourceIn = new HashMap<>();

        List<Map<String, Object>> listWorkshop = plsqlService.findAll("p_workshop",paramSourceIn);
//        modelMap.put("listWorkshop", listWorkshop);
        // pengambilan data format json
        return listWorkshop;
    }

    @RequestMapping(value = "/nodeDelete", params = "id", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> nodeDelete(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> params = new HashMap<>();
        //get object untuk membuat json sesuai dengan kebutuhan treetables

        params.put("prm_id_group", id);
        params.put("prm_return_type", "TYPE_TREE_TABLE"); //untuk dokumentasi lihat dokumentasi pada package
        Map<String, Object> retValue = plsqlService.findOne("P_MASTER_PRODUCT_GROUP", params);

        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_id_group", id);
        Long data = plsqlService.delete("P_MASTER_PRODUCT_GROUP", paramSourceIn);
        if (data > 0) {
            modelMap.put("message", "Data berhasil di simpan");
        } else {
            modelMap.put("message", "Data gagal di simpan");
        }

        // Isi kode form untuk menghapus data
        return retValue;
    }

    @RequestMapping(value = "/construct-tree", params = "id", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> constructTree(@RequestParam("id") String id, ModelMap modelMap){

        Map<String, String> paramSourceIn = null;
        List<Map<String,Object>> listProductGroup = null;
        Map<String, Object> retValue = new HashMap<>();

        if(id ==null || id.equals("") || id.equals("0")){

            listProductGroup = new ArrayList<>();
            paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_MASTER_PRODUCT_GROUP");
            paramSourceIn.put("func_name", "get_group_tree");
            paramSourceIn.put("prm_id_parent", null);
            listProductGroup = plsqlService.findAll(paramSourceIn);
            listProductGroup = plsqlService.findAll(paramSourceIn);

        }else{

            listProductGroup = new ArrayList<>();
            paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_MASTER_PRODUCT_GROUP");
            paramSourceIn.put("func_name", "get_group_tree");
            paramSourceIn.put("prm_id_parent", id);
            listProductGroup = plsqlService.findAll(paramSourceIn);

        }

        modelMap.put("authUser", UserUtil.getAuthUser());
        retValue.put("nodes", listProductGroup);

        return retValue;
    }





}


