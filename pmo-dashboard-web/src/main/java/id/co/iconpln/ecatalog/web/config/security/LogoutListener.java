package id.co.iconpln.ecatalog.web.config.security;

import org.springframework.context.ApplicationListener;
import org.springframework.security.core.session.SessionDestroyedEvent;

/**
 * Created by Deny Prasetyo,S.T.
 * Java(Scala) Developer and Trainer
 * jasoet87@gmail.com
 * [at] jasoet
 * github.com/jasoet
 */


public class LogoutListener implements ApplicationListener<SessionDestroyedEvent> {
    @Override
    public void onApplicationEvent(SessionDestroyedEvent sessionDestroyedEvent) {
        System.out.println("SessionDestroyedEvent");
        sessionDestroyedEvent.getSecurityContexts().forEach(System.out::println);
    }
}
