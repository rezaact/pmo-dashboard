package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/dashboard-milestone")
public class DashboardMilestoneController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap){
        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        Map<String, String> pGetAllPhase = new HashMap<>();
        pGetAllPhase.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllPhase.put("func_name", "get_all_phase");
        pGetAllPhase.put("p_tmp", null);
        List<Map<String, Object>> listComboPhase = plsqlService.findAll(pGetAllPhase);
        modelMap.put("listComboPhase",listComboPhase);

        modelMap.put("nama_var", "nilai_var");//Testing Var to View

        modelMap.put("authUser", UserUtil.getAuthUser());

        return "app.front.dashboard.milestone"; //xml
    }

    @RequestMapping(value = "/getdata-box-milestone",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_box_milestone(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbPhase = request.getParameter("cmbPhase");
        String cmbType = request.getParameter("cmbType");
        String cmbOwnership = request.getParameter("cmbOwnership");


        Map<String, String> pGetBoxMileStone = new HashMap<>();
        pGetBoxMileStone.put("pkg_name", "P_FILTER_MILESTONE");
        pGetBoxMileStone.put("func_name", "get_boxmillestoneoveral");
        pGetBoxMileStone.put("p_RegionID", cmbRegion);
        pGetBoxMileStone.put("p_ProgramID", cmbProgram);
        pGetBoxMileStone.put("p_TypeID", cmbType);
        pGetBoxMileStone.put("p_PhaseID", cmbPhase);
        pGetBoxMileStone.put("p_IsIPP", cmbOwnership);
        //pGetBoxMileStone.put("Out_Message", null);
        List<Map<String, Object>> listBoxMileStone = plsqlService.findAll(pGetBoxMileStone);
        retValue.put("box", listBoxMileStone);


        /*Map<String, String> pTestAjah = new HashMap<>();
        pTestAjah.put("pkg_name", "P_FILTER_MILESTONE");
        pTestAjah.put("func_name", "get_test");
        pTestAjah.put("p_test", "clikuba");
        String pTestAjahStr  = plsqlService.findString(pTestAjah);*/

        //generate data
        Map<String, Object> pchartmillestoneoveralData = new HashMap<>();
        pchartmillestoneoveralData.put("pkg_name", "P_FILTER_MILESTONE");
        pchartmillestoneoveralData.put("func_name", "get_chartmillestoneoveral_data");
        pchartmillestoneoveralData.put("p_RegionID", Integer.valueOf(cmbRegion));
        pchartmillestoneoveralData.put("p_ProgramID", Integer.valueOf(cmbProgram));
        pchartmillestoneoveralData.put("p_TypeID", Integer.valueOf(cmbType));
        pchartmillestoneoveralData.put("p_PhaseID", Integer.valueOf(cmbPhase));
        pchartmillestoneoveralData.put("p_IsIPP", Integer.valueOf(cmbOwnership));
        plsqlService.save(pchartmillestoneoveralData);

        //generate chart
        Map<String, String> pGetOverAllMileStone = new HashMap<>();
        pGetOverAllMileStone.put("pkg_name", "P_FILTER_MILESTONE");
        pGetOverAllMileStone.put("func_name", "get_chartmillestoneoveral");
        pGetOverAllMileStone.put("p_RegionID", cmbRegion);
        pGetOverAllMileStone.put("p_ProgramID", cmbProgram);
        pGetOverAllMileStone.put("p_TypeID", cmbType);
        pGetOverAllMileStone.put("p_PhaseID", cmbPhase);
        pGetOverAllMileStone.put("p_IsIPP", cmbOwnership);
        String strOverAllMileStone = plsqlService.findString(pGetOverAllMileStone);
        retValue.put("strOverAllMileStone", strOverAllMileStone);


        //triger data All Milestone except overall milestone
        //TODO : by reza [On Progress]
        Map<String, Object> pTgrData = new HashMap<>();
        pTgrData.put("pkg_name", "P_FILTER_MILESTONE");
        pTgrData.put("func_name", "gen_data_chart_tahapan");
        pTgrData.put("p_RegionID", Integer.valueOf(cmbRegion));
        pTgrData.put("p_ProgramID", Integer.valueOf(cmbProgram));
        pTgrData.put("p_TypeID", Integer.valueOf(cmbType));
        pTgrData.put("p_PhaseID", Integer.valueOf(cmbPhase));
        pTgrData.put("p_IsIPP", Integer.valueOf(cmbOwnership));
        plsqlService.save(pTgrData);

        //pre_qualification
        Map<String, String> pPreQualification = new HashMap<>();
        pPreQualification.put("pkg_name", "P_FILTER_MILESTONE");
        pPreQualification.put("func_name", "gen_chart_pre_qualification");
        pPreQualification.put("p_user_session", null);
        String strPreQualification = plsqlService.findString(pPreQualification);
        retValue.put("strPreQualification", strPreQualification);

        //generate json chart untuk  chart request for proposal
        //TODO : by Deni
        //request_for_proposal
        Map<String, String> pRequestForProposal = new HashMap<>();
        pRequestForProposal.put("pkg_name", "P_FILTER_MILESTONE");
        pRequestForProposal.put("func_name", "gen_chart_request_for_proposal");
        pRequestForProposal.put("p_user_session", null);
        String strRequestForProposal= plsqlService.findString(pRequestForProposal);
        retValue.put("strRequestForProposal", strRequestForProposal);

        //generate json chart untuk  chart latter of intent
        //TODO : by Deni
        Map<String, String> pLaterOfIntent = new HashMap<>();
        pLaterOfIntent.put("pkg_name", "P_FILTER_MILESTONE");
        pLaterOfIntent.put("func_name", "gen_chart_later_of_intent");//
        pLaterOfIntent.put("p_user_session", null);
        String strLaterOfIntent= plsqlService.findString(pLaterOfIntent);
        retValue.put("strLaterOfIntent", strLaterOfIntent);

        //generate json chart untuk  chart PPA Signing
        //TODO : by Deni
        Map<String, String> pPPASigning = new HashMap<>();
        pPPASigning.put("pkg_name", "P_FILTER_MILESTONE");
        pPPASigning.put("func_name", "gen_chart_ppa_signing");
        pPPASigning.put("p_user_session", null);
        String strPPASigning = plsqlService.findString(pPPASigning);
        retValue.put("strPPASigning", strPPASigning);

        //generate json chart untuk  chart Final closing
        //TODO : by Deni
        Map<String, String> pFinancialClosing = new HashMap<>();
        pFinancialClosing.put("pkg_name", "P_FILTER_MILESTONE");
        pFinancialClosing.put("func_name", "gen_chart_financial_closing");
        pFinancialClosing.put("p_user_session", null);
        String strFinancialClosing= plsqlService.findString(pFinancialClosing);
        retValue.put("strFinancialClosing", strFinancialClosing);

        //generate json chart untuk  chart Kontruksi
        //TODO : by Deni
        Map<String, String> pKonstruksi = new HashMap<>();
        pKonstruksi.put("pkg_name", "P_FILTER_MILESTONE");
        pKonstruksi.put("func_name", "gen_chart_konstruksi");
        pKonstruksi.put("p_user_session", null);
        String strKonstruksi= plsqlService.findString(pKonstruksi);
        retValue.put("strKonstruksi", strKonstruksi);

        //generate json chart untuk  chart COD
        //TODO : by Deni
        Map<String, String> pCOD = new HashMap<>();
        pCOD.put("pkg_name", "P_FILTER_MILESTONE");
        pCOD.put("func_name", "gen_chart_cod");
        pCOD.put("p_user_session", null);
        String strCOD = plsqlService.findString(pCOD);
        retValue.put("strCOD", strCOD);


        Map<String, String> pMap = new HashMap<>();
        pMap.put("pkg_name", "P_FILTER_MILESTONE");
        pMap.put("func_name", "gen_char_map");
        pMap.put("p_user_session", null);
        String strMap = plsqlService.findString(pMap);
        retValue.put("strMap", strMap);


        //-----------------------------------------------
        //Chart PLN
        // : by deni
        //chart_feasibility_study
        Map<String, String> DataFS = new HashMap<>();
        DataFS.put("pkg_name", "P_FILTER_MILESTONE");
        DataFS.put("func_name", "gen_chart_feasibility_study");
        DataFS.put("p_user_session", null);
        String strDataFS = plsqlService.findString(DataFS);
        retValue.put("strDataFS", strDataFS);

        //gen_chart_biddoc
        Map<String, String> DataBidDoc = new HashMap<>();
        DataBidDoc.put("pkg_name", "P_FILTER_MILESTONE");
        DataBidDoc.put("func_name", "gen_chart_biddoc");
        DataBidDoc.put("p_user_session", null);
        String strDataBidDoc = plsqlService.findString(DataBidDoc);
        retValue.put("strDataBidDoc", strDataBidDoc);

        //gen_chart_proc_process
        Map<String, String> DataProcProcess = new HashMap<>();
        DataProcProcess.put("pkg_name", "P_FILTER_MILESTONE");
        DataProcProcess.put("func_name", "gen_chart_proc_process");
        DataProcProcess.put("p_user_session", null);
        String strDataProcProcess = plsqlService.findString(DataProcProcess);
        retValue.put("strDataProcProcess", strDataProcProcess);

        //gen_chart_construction_process
        Map<String, String> DataConstructionProcess = new HashMap<>();
        DataConstructionProcess.put("pkg_name", "P_FILTER_MILESTONE");
        DataConstructionProcess.put("func_name", "gen_chart_construction_process");
        DataConstructionProcess.put("p_user_session", null);
        String strDataConstructionProcess = plsqlService.findString(DataConstructionProcess);
        retValue.put("strDataConstructionProcess", strDataConstructionProcess);

        //gen_chart_commisioning
        Map<String, String> DataCommisioning = new HashMap<>();
        DataCommisioning.put("pkg_name", "P_FILTER_MILESTONE");
        DataCommisioning.put("func_name", "gen_chart_commisioning");
        DataCommisioning.put("p_user_session", null);
        String strCommisioning = plsqlService.findString(DataCommisioning);
        retValue.put("strCommisioning", strCommisioning);

        //gen_chart_fac
        Map<String, String> DataFAC = new HashMap<>();
        DataFAC.put("pkg_name", "P_FILTER_MILESTONE");
        DataFAC.put("func_name", "gen_chart_fac");
        DataFAC.put("p_user_session", null);
        String strFAC = plsqlService.findString(DataFAC);
        retValue.put("strFAC", strFAC);

        //end Chart PLN
        //-----------------------------------------------

        return retValue;
    }

    @RequestMapping(value = "getdata-milestone-chart-popup",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdataMilestoneChartPopup(ModelMap modelMap, HttpServletRequest request){

        Map<String, Object> retValue = new HashMap<>();

        //generate data
        Map<String, Object> pchartmillestoneoveralData = new HashMap<>();
        pchartmillestoneoveralData.put("pkg_name", "P_FILTER_MILESTONE");
        pchartmillestoneoveralData.put("func_name", "get_chartmillestoneoveral_data");
        //pchartmillestoneoveralData.put("p_RegionID", Integer.valueOf(cmbRegion));
        //pchartmillestoneoveralData.put("p_ProgramID", Integer.valueOf(cmbProgram));
        //pchartmillestoneoveralData.put("p_TypeID", Integer.valueOf(cmbType));
        //pchartmillestoneoveralData.put("p_PhaseID", Integer.valueOf(cmbPhase));
        pchartmillestoneoveralData.put("p_IsIPP", 0);
        plsqlService.save(pchartmillestoneoveralData);

        //generate chart
        Map<String, String> pGetOverAllMileStone = new HashMap<>();
        pGetOverAllMileStone.put("pkg_name", "P_FILTER_MILESTONE");
        pGetOverAllMileStone.put("func_name", "get_chartmillestoneoveral");
        //pGetOverAllMileStone.put("p_RegionID", cmbRegion);
        //pGetOverAllMileStone.put("p_ProgramID", cmbProgram);
        //pGetOverAllMileStone.put("p_TypeID", cmbType);
        //pGetOverAllMileStone.put("p_PhaseID", cmbPhase);
        pGetOverAllMileStone.put("p_IsIPP", String.valueOf(1));
        String strOverAllMileStone = plsqlService.findString(pGetOverAllMileStone);
        retValue.put("strOverAllMileStone", strOverAllMileStone);

        return retValue;
    }

    @RequestMapping(value = "/getdata-box-milestone-test",method = RequestMethod.POST)
    @ResponseBody
    public String getdata_box_milestone_test(ModelMap modelMap, HttpServletRequest request){

        Map<String, Object> pTgrData = new HashMap<>();
        pTgrData.put("pkg_name", "P_FILTER_MILESTONE");
        pTgrData.put("func_name", "get_test");
        pTgrData.put("p_RegionID", 0);
        pTgrData.put("p_ProgramID", 0);
        pTgrData.put("p_TypeID", 0);
        pTgrData.put("p_PhaseID", 0);
        pTgrData.put("p_IsIPP", 0);
        plsqlService.save(pTgrData);

        return "ok";
    }

    @RequestMapping(value = "/get-grid-milestone",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> get_grid_milestone(ModelMap modelMap, HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbPhase = request.getParameter("cmbPhase");
        String cmbType = request.getParameter("cmbType");
        String cmbOwnership = request.getParameter("cmbOwnership");

        Map<String, Object> retValue = new HashMap<>();

        Map<String, String> pGetBoxMileStone = new HashMap<>();
        pGetBoxMileStone.put("pkg_name", "P_FILTER_MILESTONE");
        pGetBoxMileStone.put("func_name", "get_gridmillestone");
        pGetBoxMileStone.put("p_RegionID", cmbRegion);
        pGetBoxMileStone.put("p_ProgramID", cmbProgram);
        pGetBoxMileStone.put("p_TypeID", cmbType);
        pGetBoxMileStone.put("p_PhaseID", cmbPhase);
        pGetBoxMileStone.put("p_IsIPP", cmbOwnership);
        //pGetBoxMileStone.put("Out_Message", null);
        List<Map<String, Object>> listBoxMileStone = plsqlService.findAll(pGetBoxMileStone);
        retValue.put("data", listBoxMileStone);


        return retValue;
    }

    @RequestMapping(value = "/getdata-chart-milestone",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getDataChartMilestone(ModelMap modelMap, HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbPhase = request.getParameter("cmbPhase");
        String cmbType = request.getParameter("cmbType");
        String cmbOwnership = request.getParameter("cmbOwnership");

        Map<String, Object> retValue = new HashMap<>();

        //generate data
        Map<String, Object> pchartmillestoneoveralData = new HashMap<>();
        pchartmillestoneoveralData.put("pkg_name", "P_FILTER_MILESTONE");
        pchartmillestoneoveralData.put("func_name", "get_chartmillestoneoveral_data");
        pchartmillestoneoveralData.put("p_RegionID", Integer.valueOf(cmbRegion));
        pchartmillestoneoveralData.put("p_ProgramID", Integer.valueOf(cmbProgram));
        pchartmillestoneoveralData.put("p_TypeID", Integer.valueOf(cmbType));
        pchartmillestoneoveralData.put("p_PhaseID", Integer.valueOf(cmbPhase));
        pchartmillestoneoveralData.put("p_IsIPP", Integer.valueOf(cmbOwnership));
        plsqlService.save(pchartmillestoneoveralData);

        //generate chart
        Map<String, String> pGetOverAllMileStone = new HashMap<>();
        pGetOverAllMileStone.put("pkg_name", "P_FILTER_MILESTONE");
        pGetOverAllMileStone.put("func_name", "get_chartmillestoneoveral");
        pGetOverAllMileStone.put("p_RegionID", cmbRegion);
        pGetOverAllMileStone.put("p_ProgramID", cmbProgram);
        pGetOverAllMileStone.put("p_TypeID", cmbType);
        pGetOverAllMileStone.put("p_PhaseID", cmbPhase);
        pGetOverAllMileStone.put("p_IsIPP", cmbOwnership);
        String strOverAllMileStone = plsqlService.findString(pGetOverAllMileStone);
        retValue.put("strOverAllMileStone", strOverAllMileStone);

        return retValue;
    }

    @RequestMapping(value = "/getdata-asset-project",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getdataAssetProject(HttpServletRequest request){
        String projectId = request.getParameter("p_projectid");



        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetProject = new HashMap<>();
        pGetAssetProject.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAssetProject.put("func_name", "get_asset_project");
        pGetAssetProject.put("p_projectid", projectId);
        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetProject);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

}
