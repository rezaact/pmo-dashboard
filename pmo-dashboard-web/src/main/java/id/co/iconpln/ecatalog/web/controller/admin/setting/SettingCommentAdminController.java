package id.co.iconpln.ecatalog.web.controller.admin.setting;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/setting/comment")
public class SettingCommentAdminController {
    @Autowired
    PlsqlService plsqlService;                

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") long id, ModelMap modelMap){
        //get default data
        Map<String, Object>  paramSourceIn = new HashMap<>();

        Map<String, Object> detailWorkshop = plsqlService.findOne("p_workshop",paramSourceIn);
        modelMap.put("detailWorkshop", detailWorkshop);
        // Isi kode form untuk melihat data detail
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.setting.comment.detail";
    }

    //Menampilkan Seluruh Data
    @RequestMapping(method = RequestMethod.GET)
    public String find( ModelMap modelMap  )
    {
        long pageLength =5 ; //panjang halaman
        //get default data
        Map dataMap = plsqlService.find("P_KOMENTAR_END",0,pageLength,"NAMA","ASC","");

        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());
        //panggil jsp
        return "app.admin.setting.comment.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NAMA") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_KOMENTAR_END", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    //Proses delete data
    @RequestMapping(value = "/delete", params = "id",method = RequestMethod.GET)
    public String delete(
            @RequestParam("id") long id, ModelMap modelMap)
    {
        Map<String, Object> paramSourceIn =new HashMap<>();
        paramSourceIn.put("PRM_ID_KOMENTAR",id);
        Long retValue = plsqlService.delete("P_KOMENTAR_END", paramSourceIn);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/setting/comment";

    }
}
