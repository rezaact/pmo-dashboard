package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.service.impl.GmailService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import sun.util.resources.cldr.so.CalendarData_so_ET;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;


@Controller
@RequestMapping("/dashboard-cod")
public class DashboardCODController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(value = "/bulanan",method = RequestMethod.GET)
    public String bulanan(ModelMap modelMap){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.bulanan"; //xml
    }

    @RequestMapping(value = "/bulanan/getdata-cod-bulanan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_chartbulanan(ModelMap modelMap, HttpServletRequest request) {
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbType = request.getParameter("cmbType");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbDate = request.getParameter("cmbDate");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");

        /*
        CHART GENERATION
        ----------------
         */
        //Data generation pembangkit
        Map<String, Object> pGetCapacityData_gen_m = new HashMap<>();
        pGetCapacityData_gen_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_gen_m.put("func_name", "get_Capacity_data_m");//get_Capacity_data_m
        pGetCapacityData_gen_m.put("p_type", 17);
        pGetCapacityData_gen_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_gen_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_gen_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_gen_m.put("p_year", Integer.valueOf(cmbDate));
        pGetCapacityData_gen_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_gen_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_gen_m);


        //Chart Generation pembangkit
        Map<String, String> pGetGeneration_gen = new HashMap<>();
        pGetGeneration_gen.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_gen.put("func_name", "get_CapacityChart_m");//
        pGetGeneration_gen.put("p_type", String.valueOf(17));
        String strGetGeneration_gen = plsqlService.findString(pGetGeneration_gen);
        retValue.put("strGetGeneration_gen", strGetGeneration_gen);


        /*
        CHART TRANSMITION
        ----------------
         */
        //Data generation transmition
        Map<String, Object> pGetCapacityData_trans_m = new HashMap<>();
        pGetCapacityData_trans_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_trans_m.put("func_name", "get_Capacity_data_m");
        pGetCapacityData_trans_m.put("p_type", 18);
        pGetCapacityData_trans_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_trans_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_trans_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_trans_m.put("p_year", Integer.valueOf(cmbDate));
        pGetCapacityData_trans_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_trans_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_trans_m);

        //Chart Generation transmition
        Map<String, String> pGetGeneration_trans = new HashMap<>();
        pGetGeneration_trans.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_trans.put("func_name", "get_CapacityChart_m");
        pGetGeneration_trans.put("p_tmp", String.valueOf(18));
        String strGetGeneration_tran = plsqlService.findString(pGetGeneration_trans);
        retValue.put("strGetGeneration_tran", strGetGeneration_tran);


        /*
        CHART SUBTATION
        ----------------
         */
        //Data
        Map<String, Object> pGetSubtationData_sub_m = new HashMap<>();
        pGetSubtationData_sub_m.put("pkg_name", "P_FILTER_COD");
        pGetSubtationData_sub_m.put("func_name", "get_Capacity_data_m");
        pGetSubtationData_sub_m.put("p_type", 19);
        pGetSubtationData_sub_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetSubtationData_sub_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetSubtationData_sub_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetSubtationData_sub_m.put("p_year", Integer.valueOf(cmbDate));
        pGetSubtationData_sub_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetSubtationData_sub_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetSubtationData_sub_m);

        //Chart
        Map<String, String> pGetSubtation_sub = new HashMap<>();
        pGetSubtation_sub.put("pkg_name", "P_FILTER_COD");
        pGetSubtation_sub.put("func_name", "get_CapacityChart_m");
        pGetSubtation_sub.put("p_type", String.valueOf(19));
        String strGetSubtation_sub = plsqlService.findString(pGetSubtation_sub);
        retValue.put("strGetSubtation_sub", strGetSubtation_sub);


        return retValue;
    };


    @RequestMapping(value = "/tahunan",method = RequestMethod.GET)
    public String tahunan(ModelMap modelMap){
        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.tahunan"; //xml
    }

    @RequestMapping(value = "/tahunan/getdata-cod-tahunan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_charttahunan(ModelMap modelMap, HttpServletRequest request) {
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");

        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");

        /*
        CHART GENERATION
        ----------------
         */
        //Data generation pembangkit
        Map<String, Object> pGetCapacityData_gen_m = new HashMap<>();
        pGetCapacityData_gen_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_gen_m.put("func_name", "gen_Capacity_data_thn");//gen_Capacity_data  //gen_Capacity_data_thn
        pGetCapacityData_gen_m.put("p_type", 17);
        pGetCapacityData_gen_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_gen_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_gen_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_gen_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetCapacityData_gen_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetCapacityData_gen_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_gen_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_gen_m);

        //Chart Generation pembangkit
        Map<String, String> pGetGeneration_gen = new HashMap<>();
        pGetGeneration_gen.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_gen.put("func_name", "get_CapacityChart_thn");//get_CapacityChart_thn  //get_CapacityChart
        pGetGeneration_gen.put("p_type", String.valueOf(17));
        String strGetGeneration_gen = plsqlService.findString(pGetGeneration_gen);
        retValue.put("strGetGeneration_gen", strGetGeneration_gen);

        /*
        CHART TRANSMITION
        ----------------
         */
        //Data generation transmition
        Map<String, Object> pGetCapacityData_trans_m = new HashMap<>();
        pGetCapacityData_trans_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_trans_m.put("func_name", "gen_Capacity_data_thn");
        pGetCapacityData_trans_m.put("p_type", 18);
        pGetCapacityData_trans_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_trans_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_trans_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_trans_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetCapacityData_trans_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetCapacityData_trans_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_trans_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_trans_m);


        //Chart Generation transmition
        Map<String, String> pGetGeneration_trans = new HashMap<>();
        pGetGeneration_trans.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_trans.put("func_name", "get_CapacityChart_thn");
        pGetGeneration_trans.put("p_type", String.valueOf(18));
        String strGetGeneration_tran = plsqlService.findString(pGetGeneration_trans);
        retValue.put("strGetGeneration_tran", strGetGeneration_tran);


        /*
        CHART SUBTATION
        ----------------
         */
        //Data
        Map<String, Object> pGetSubtationData_sub_m = new HashMap<>();
        pGetSubtationData_sub_m.put("pkg_name", "P_FILTER_COD");
        pGetSubtationData_sub_m.put("func_name", "gen_Capacity_data_thn");
        pGetSubtationData_sub_m.put("p_type", 19);
        pGetSubtationData_sub_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetSubtationData_sub_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetSubtationData_sub_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetSubtationData_sub_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetSubtationData_sub_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetSubtationData_sub_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetSubtationData_sub_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetSubtationData_sub_m);

        //Chart
        Map<String, String> pGetSubtation_sub = new HashMap<>();
        pGetSubtation_sub.put("pkg_name", "P_FILTER_COD");
        pGetSubtation_sub.put("func_name", "get_CapacityChart_thn");
        pGetSubtation_sub.put("p_type", String.valueOf(19));
        String strGetSubtation_sub = plsqlService.findString(pGetSubtation_sub);
        retValue.put("strGetSubtation_sub", strGetSubtation_sub);


        return retValue;
    };


    @RequestMapping(value = "/tahunan/getdata-asset-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getdataAssetCapacityTahunan(HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");
        String categoryLabel = request.getParameter("categoryLabel");
        String datasetName = request.getParameter("datasetName");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_pmo_detail_cod_thn");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", "17");
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram); //Progam
        pGetAssetCapacity.put("p_strat_year", cmbThnAwal);
        pGetAssetCapacity.put("p_end_year", cmbThnAkhir);
        pGetAssetCapacity.put("p_cod_plan", cmbPlan);
        pGetAssetCapacity.put("p_cod_actual", cmbActual);
        pGetAssetCapacity.put("p_category_label", categoryLabel);
        pGetAssetCapacity.put("p_datasetName", datasetName);


        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }


    @RequestMapping(value = "/bulanan/getdata-asset-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getdataAssetCapacityBulanan(HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbDate = request.getParameter("cmbDate");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");
        String type = request.getParameter("type");
        String categoryLabel = request.getParameter("categoryLabel");
        String datasetName = request.getParameter("datasetName");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_pmo_detail_cod_bln");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", type);
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram);
        pGetAssetCapacity.put("p_year", cmbDate);
        pGetAssetCapacity.put("p_cod_plan", cmbPlan);
        pGetAssetCapacity.put("p_cod_actual", cmbActual);
        pGetAssetCapacity.put("p_catp_yearegoryLabel", categoryLabel);
        pGetAssetCapacity.put("p_datasetNama", datasetName);


        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

    @RequestMapping(value = "/estimate",method = RequestMethod.GET)
    public String estimate(ModelMap modelMap){
        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.estimate"; //xml
    }

    @RequestMapping(value = "/getdata-cod-estimate",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_cod_estimate(ModelMap modelMap, HttpServletRequest request) {
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");//Group
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbThnAwal = request.getParameter("cmbStartDate");
        String cmbThnAkhir = request.getParameter("cmbEndDate");
        String cmbKapasitas = request.getParameter("cmbKapasitas");

        /*
        CHART GENERATION
        ----------------
         */
        //Data generation pembangkit
        Map<String, Object> pGetCapacityData_gen_m = new HashMap<>();
        pGetCapacityData_gen_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_gen_m.put("func_name", "gen_kapasitas_pembangkit");
        pGetCapacityData_gen_m.put("p_type", 17);
        pGetCapacityData_gen_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_gen_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_gen_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_gen_m.put("p_start_year", Integer.valueOf(cmbThnAwal));
        pGetCapacityData_gen_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetCapacityData_gen_m.put("p_kap_awal", Integer.valueOf(cmbKapasitas));

        plsqlService.save(pGetCapacityData_gen_m);

        //Chart Generation pembangkit
        Map<String, String> pGetGeneration_gen = new HashMap<>();
        pGetGeneration_gen.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_gen.put("func_name", "get_kapasitas_pembangkit_chart");
        pGetGeneration_gen.put("p_type", String.valueOf(17));
        String strGetGeneration_gen = plsqlService.findString(pGetGeneration_gen);
        retValue.put("strGetGeneration_gen", strGetGeneration_gen);

        return retValue;
    };


    @RequestMapping(value = "/get-data-detail-cod",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailCOD(HttpServletRequest request){

        String type = request.getParameter("type");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String categoryLabel = request.getParameter("categoryLabel");
        String cmbKapasitasAwal = request.getParameter("cmbKapasitasAwal");
        String startPeriod = request.getParameter("startPeriod");
        String endPeriod = request.getParameter("endPeriod");


        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_kapasitas_detail");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", type);
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram);
        pGetAssetCapacity.put("p_kap_awal", cmbKapasitasAwal);
        pGetAssetCapacity.put("p_start_year", startPeriod);
        pGetAssetCapacity.put("p_end_year", endPeriod);
        pGetAssetCapacity.put("p_thbl", categoryLabel);


        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

    @RequestMapping(value = "/get-data-detail-cod-v2",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailCODV2(HttpServletRequest request){

        String type = request.getParameter("type");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String categoryLabel = request.getParameter("categoryLabel");


        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_kapasitas_detail_v2");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", type);
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram);
        pGetAssetCapacity.put("p_thbl", categoryLabel);

        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

    @RequestMapping(value = "/gen-capacity",method = RequestMethod.GET)
    public String gen_capacity(ModelMap modelMap, HttpServletRequest request){

        //Load data for combo filter
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        Map<String, String> pGetAllTypePembangkit = new HashMap<>();
        pGetAllTypePembangkit.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllTypePembangkit.put("func_name", "get_all_pembangkit");
        pGetAllTypePembangkit.put("p_tmp", null);
        List<Map<String, Object>> listComboTypePembangkit = plsqlService.findAll(pGetAllTypePembangkit);
        modelMap.put("listComboTypePembangkit",listComboTypePembangkit);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.gen.capacity"; //xml
    }

    @RequestMapping(value = "/get-data-detail-cod-gen-capacity-filter",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailCODGenCapacityFilter(HttpServletRequest request){
        ModelMap modelMap = new ModelMap();

        //Filter & Load Default
        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbtype = request.getParameter("cmbType");
        String startPeriod = request.getParameter("startPeriod");

        Map<String, String> pGetGenCapacity= new HashMap<>();
        pGetGenCapacity.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetGenCapacity.put("func_name", "gen_capacity");

        //TODO : tambah param type.
        pGetGenCapacity.put("p_program", String.valueOf(cmbProgram));
        pGetGenCapacity.put("p_Region", String.valueOf(cmbRegion));
        pGetGenCapacity.put("p_ownership", String.valueOf(cmbOwnership));
        pGetGenCapacity.put("p_tipepembangkit", String.valueOf(cmbtype));
        pGetGenCapacity.put("p_Group", String.valueOf(0));
        pGetGenCapacity.put("p_thbl", String.valueOf(startPeriod));

        List<Map<String, Object>> listGenCapacity = plsqlService.findAll(pGetGenCapacity);
        modelMap.put("listGenCapacity",listGenCapacity);

        return modelMap;
    }

    //Data Detail Modal
    @RequestMapping(value = "/get-data-detail-cod-gen-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailCODGenCapacity(HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String startPeriod = request.getParameter("startPeriod");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetDetailGen= new HashMap<>();
        pGetDetailGen.put("pkg_name", "P_FILTER_COD_GEN_CAPACITY");
        pGetDetailGen.put("func_name", "get_detail_demand");

        //TODO : tambah param type.
        pGetDetailGen.put("p_reqion_id", String.valueOf(cmbRegion));
        pGetDetailGen.put("p_thbl", String.valueOf(startPeriod));


        List<Map<String, Object>> listDetailGen = plsqlService.findAll(pGetDetailGen);
        modelMap.put("data",listDetailGen);

        return modelMap;
    }

}
