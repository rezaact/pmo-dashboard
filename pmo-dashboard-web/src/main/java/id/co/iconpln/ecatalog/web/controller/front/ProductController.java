package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.domain.AuthUser;
import id.co.iconpln.ecatalog.db.service.FileService;
import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.SearchUtils;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

import org.apache.commons.codec.binary.Base64;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    PlsqlService plsqlService;

    @Autowired
    FileService fileService;

    @RequestMapping(value = "/product-list", method = RequestMethod.GET)
    public String productList(ModelMap modelMap,@RequestParam(value = "draw", defaultValue = "0") int draw,
                              @RequestParam(value = "page", defaultValue = "1") long page,
                              @RequestParam(value = "length", defaultValue = "10") long length,
                              @RequestParam(value = "search", defaultValue = "") String search,
                              @RequestParam(value = "paramsearch", defaultValue = "NAMA_PRODUK") String paramSearch,
                              @RequestParam(value = "paramdir", defaultValue = "default") String paramDir) {

        System.out.println("--page = " + page);
        System.out.println("length = " + length);
        System.out.println("search = " + search);
        System.out.println("paramsearch = " + paramSearch);
        System.out.println("paramDir = " + paramDir);
        //Map<String, Object> listProduk = plsqlService.find("P_PRODUCT_FRONT", page,length,paramSearch,paramDir,"%"+search+"%");

        /*
        tambahan variable
         */
        String idWorkshop="1,2,3,4,5,6";
        String idKat = "";
        String idJenis="";
        String idKom="";
        Map<String, Object> listProduk = plsqlService.findProduk("P_PRODUCT_FRONT", page,length,paramSearch,paramDir,"%"+search+"%",idWorkshop,"%"+idKat+"%","%"+idJenis+"%","%"+idKom+"%");


        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", 0);
        params.put("prm_length", 10);
        params.put("prm_search", " ");
        List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR_FRONT", params);
        List<Map<String, Object>> listKategoriMap = plsqlService.findAll("P_KATEGORI", params);
        List<Map<String, Object>> listKategori = getListKategori(listKategoriMap);
        List<Map<String, Object>> listKatProduk = getListProduk(listKategoriMap);
        List<Map<String, Object>> listKomponen = getListKomponen(listKategoriMap);
        modelMap.put("listWorkshop", listWorkshop);
        modelMap.put("listKategori", listKategori);
        modelMap.put("listProduk", listKatProduk);
        modelMap.put("listKomponen", listKomponen);
        modelMap.put("listProdukList", listProduk);
        Long totalRecords= Long.valueOf(listProduk.get("recordsTotal").toString());
        Long totalPage=(totalRecords%length==0)?totalRecords/length:(totalRecords/length+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(page);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(search);
        searchUtils.setSearchDir(paramDir);
        searchUtils.setSearchLength(length);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("authUser", UserUtil.getAuthUser());


        //code untuk mengambil data produk unggulan
        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_unggulan");
        List<Map<String, Object>> listProdukUnggulan = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukUnggulan", listProdukUnggulan);

        //code untuk mengambil data produk best seller
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_best_seller");
        List<Map<String, Object>> listProdukBestSeller = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukBestSeller", listProdukBestSeller);

        //code untuk mengambil data splash screen
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_SPLASH_SCREEN");
        paramsIn.put("func_name", "get_splash_screen_all");
        List<Map<String, Object>> listSpashScreen = plsqlService.findAll(paramsIn);
        modelMap.put("listSpashScreen", listSpashScreen);

        //code untuk mengambil data url info
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_URLINFO");
        paramsIn.put("func_name", "get_urlinfo_all");
        List<Map<String, Object>> listUrlInfo= plsqlService.findAll(paramsIn);
        modelMap.put("listUrlInfo", listUrlInfo);

        //code untuk mengambil data url info
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_URLLINK");
        paramsIn.put("func_name", "get_urllink_all");
        List<Map<String, Object>> listUrlLink= plsqlService.findAll(paramsIn);
        modelMap.put("listUrlLink", listUrlLink);

        if(UserUtil.getAuthUser() !=null){
            modelMap.put("showSplashScreen", "false");
        }else{
            modelMap.put("showSplashScreen", "true");
        }

        return "app.front.product.list";
    }

    @RequestMapping(value = "/product-grid", method = RequestMethod.GET)
    public String productGrid(ModelMap modelMap,@RequestParam(value = "draw", defaultValue = "0") int draw,
                              @RequestParam(value = "page", defaultValue = "1") long page,
                              @RequestParam(value = "length", defaultValue = "10") long length,
                              @RequestParam(value = "search", defaultValue = "") String search,
                              @RequestParam(value = "paramsearch", defaultValue = "NAMA_PRODUK") String paramSearch,
                              @RequestParam(value = "paramdir", defaultValue = "default") String paramDir) {
        Map<String, Object> listProduk = plsqlService.find("P_PRODUCT_FRONT", page,length,paramSearch,paramDir,"%"+search+"%");

        Map<String, Object> params = new HashMap<>();
        params.put("prm_start", 0);
        params.put("prm_length", 10);
        params.put("prm_search", " ");
        List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR_FRONT", params);
        List<Map<String, Object>> listKategoriMap = plsqlService.findAll("P_KATEGORI", params);
        List<Map<String, Object>> listKategori = getListKategori(listKategoriMap);
        List<Map<String, Object>> listKatProduk = getListProduk(listKategoriMap);
        List<Map<String, Object>> listKomponen = getListKomponen(listKategoriMap);
        modelMap.put("listWorkshop", listWorkshop);
        modelMap.put("listKategori", listKategori);
        modelMap.put("listProduk", listKatProduk);
        modelMap.put("listKomponen", listKomponen);
        modelMap.put("listProdukList", listProduk);
        Long totalRecords= Long.valueOf(listProduk.get("recordsTotal").toString());
        Long totalPage=(totalRecords%length==0)?totalRecords/length:(totalRecords/length+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(page);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(search);
        searchUtils.setSearchDir(paramDir);
        searchUtils.setSearchLength(length);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("authUser", UserUtil.getAuthUser());

        //code untuk mengambil data produk unggulan
        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_unggulan");
        List<Map<String, Object>> listProdukUnggulan = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukUnggulan", listProdukUnggulan);

        //code untuk mengambil data produk unggulan
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_best_seller");
        List<Map<String, Object>> listProdukBestSeller = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukBestSeller", listProdukBestSeller);

        return "app.front.product.grid";
    }

    @RequestMapping(value = "/product-detail", method = RequestMethod.GET)
    public String productDetail(
            @RequestParam(value = "id") String id,
            @RequestParam(value = "doc_type", defaultValue = "001") String docType,
            ModelMap modelMap) {
        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_id", String.valueOf(id));
        paramSourceIn.put("prm_start", 0);
        paramSourceIn.put("prm_length", 10);
        paramSourceIn.put("prm_search", " ");
        Map<String, Object> detailProduk = plsqlService.findOne("P_PRODUCT_FRONT", paramSourceIn);
        List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR_FRONT", paramSourceIn);
        List<Map<String, Object>> listKategoriMap = plsqlService.findAll("P_KATEGORI", paramSourceIn);
        List<Map<String, Object>> listKategori = getListKategori(listKategoriMap);
        List<Map<String, Object>> listKatProduk = getListProduk(listKategoriMap);
        List<Map<String, Object>> listKomponen = getListKomponen(listKategoriMap);

        paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_search", id);
        paramSourceIn.put("prm_start", "1");
        paramSourceIn.put("prm_length", "5");
        Map<String, Object> detailKomentar = plsqlService.find("P_KOMENTAR_FRONT", 1,5,"","",id);
        Map<String, String> paramsChild = new HashMap<>();
        paramsChild.put("pkg_name", "P_KOMENTAR_FRONT");
        paramsChild.put("func_name", "find_child");
        paramsChild.put("prm_nomor_produk", id);
        List commentChild = plsqlService.findAll(paramsChild);

        Long totalRecords= Long.valueOf(detailKomentar.get("recordsTotal").toString());
        Long totalPage=(totalRecords%5==0)?totalRecords/5:(totalRecords/5+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(1L);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(id);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(1)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(1)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("detailKomentar", detailKomentar);
        modelMap.put("totalKomentar", totalRecords);
        modelMap.put("commentChild", commentChild);

        modelMap.put("listWorkshop", listWorkshop);
        modelMap.put("listKategori", listKategori);
        modelMap.put("listProduk", listKatProduk);
        modelMap.put("listKomponen", listKomponen);
        modelMap.put("detailProduk", detailProduk);
        //modelMap.put("listImage", listImage);
        modelMap.put("authUser", UserUtil.getAuthUser());

        //--- get data for ID_IMAGE_THUMBNAIL -----
        Map<String,String> params = new HashMap<>();
        params.put("pkg_name", "P_PRODUCT_FRONT");
        params.put("func_name", "get_id_image_thumbnail");
        params.put("prm_nomor_produk", String.valueOf(id));
        List<Map<String, Object>> listImageThumbnail = plsqlService.findAll(params);
         modelMap.put("listImageThumbnail", listImageThumbnail);

        //-- init document type (image type)
        modelMap.put("doc_type", docType);


        //code untuk mengambil gambar 2D zoom
        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_id_image_zoom");
        paramsIn.put("prm_nomor_produk", String.valueOf(id));
        Map<String, Object> image2DZoom= plsqlService.findOne(paramsIn);
        modelMap.put("image2DZoom", image2DZoom);

        //code untuk mengambil gambar 2D POPUP
        paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_id_image_popup");
        paramsIn.put("prm_nomor_produk", String.valueOf(id));
        List<Map<String, Object>> listImage2DPopup= plsqlService.findAll(paramsIn);
        modelMap.put("listImage2DPopup", listImage2DPopup);

        //code untuk mengambil data produk unggulan
        paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_unggulan");
        List<Map<String, Object>> listProdukUnggulan = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukUnggulan", listProdukUnggulan);

        //code untuk mengambil data produk unggulan
        paramsIn  = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_best_seller");
        List<Map<String, Object>> listProdukBestSeller = plsqlService.findAll(paramsIn);
        modelMap.put("listProdukBestSeller", listProdukBestSeller);

        //code untuk mengisi dokumen produk File
        paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_start", 0);
        paramSourceIn.put("prm_length", 0);
        paramSourceIn.put("prm_search", id);//nomor_produk
        List<Map<String, Object>> listFileDownload = plsqlService.findAll("P_DOKUMEN_PRODUK_FRONT", paramSourceIn);
        modelMap.put("listFileDownload", listFileDownload);

        //code untuk mengambil data history produk untuk yang pertama
        paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_HISTORIKAL_PRODUK_FRONT");
        paramsIn.put("func_name", "FIND_CUSTOM");
        paramsIn.put("PRM_START", "0");
        paramsIn.put("PRM_LENGTH", "1");
        paramsIn.put("PRM_SORT_BY", null);
        paramsIn.put("PRM_SORT_DIR", null);
        paramsIn.put("PRM_SEARCH", null);
        paramsIn.put("PRM_NOMOR_PRODUK", String.valueOf(id));
        Map<String, Object> historyProduct = plsqlService.findOne(paramsIn);
        if(historyProduct == null){
            historyProduct = new HashMap<>();
            historyProduct.put("page","0");
        }else{
            historyProduct.put("page","1");
        }

        /*paramsIn.put("pkg_name","P_HISTORIKAL_PRODUK_FRONT");
        paramsIn.put("func_name","doc_history");
        paramsIn.put("prm_nomor_produk",String.valueOf(id));
        paramsIn.put("prm_id_history", String.valueOf(historyProduct.get("ID_HISTORY")));
        List<Map<String, Object>> docHistory= plsqlService.findAll(paramsIn);*/
        //modelMap.put("docProduct",docHistory);

        //get doc history
        List<Map<String, Object>> docHistoryNew = null;
        try {
            paramsIn.put("pkg_name","P_HISTORIKAL_PRODUK_FRONT");
            paramsIn.put("func_name","doc_history_new");
            paramsIn.put("prm_id_history", String.valueOf(historyProduct.get("ID_HISTORY")));
            docHistoryNew = plsqlService.findAll(paramsIn);
        }catch (Exception e){
            e.printStackTrace();
        }
        modelMap.put("docHistoryNew", docHistoryNew);


        modelMap.put("historyProduct", historyProduct);


        return "app.front.product.detail";
    }

    private List<Map<String, Object>> getListKategori(List<Map<String, Object>> datas) {
        List<Map<String, Object>> listReturn = new ArrayList<Map<String, Object>>();
        Set tempSet = new HashSet();
        for (Map<String, Object> map : datas) {
            if (tempSet.add(map.get("ID_KATEGORI"))) {
                listReturn.add(map);
            }
        }
        return listReturn;
    }

    private List<Map<String, Object>> getListProduk(List<Map<String, Object>> datas) {
        List<Map<String, Object>> listReturn = new ArrayList<Map<String, Object>>();
        Set tempSet = new HashSet();
        for (Map<String, Object> map : datas) {
            if (map.get("ID_PRODUK") != null) {
                if (tempSet.add(map.get("ID_PRODUK"))) {
                    listReturn.add(map);
                }
            }
        }
        return listReturn;
    }

    private List<Map<String, Object>> getListKomponen(List<Map<String, Object>> datas) {
        List<Map<String, Object>> listReturn = new ArrayList<Map<String, Object>>();
        Set tempSet = new HashSet();
        for (Map<String, Object> map : datas) {
            if (map.get("ID_KOMPONEN") != null) {
                if (tempSet.add(map.get("ID_KOMPONEN"))) {
                    listReturn.add(map);
                }
            }
        }
        return listReturn;
    }



    @RequestMapping(value = "/get_image_sprint", method = RequestMethod.GET)
    @ResponseBody
    public void getImageSprint(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("image/jpg, image/jpeg");

        //init parameter
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, String> params = new HashMap<>();
        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }

        InputStream is = fileService.getImageSprint(params);
        FileInputStream fis = null;
        File tmpFile = null;
        try {
            tmpFile = File.createTempFile("tmp_jpg", ".jpg");
            if (is != null) {
                fis = new FileInputStream(tmpFile);
                byte[] buffer = new byte[1024 * 1024];
                int bytesRead = 0;
                do {
                    bytesRead = is.read(buffer, 0, buffer.length);
                    response.getOutputStream().write(buffer, 0, bytesRead);
                } while (bytesRead == buffer.length);

                fis.close();
                is.close();
                response.getOutputStream().flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("---no image----");
        }
    }

    @RequestMapping(value = "/get_image", method = RequestMethod.GET)
    @ResponseBody
    public void getImage(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("image/jpg, image/jpeg");

        //hardcode
        String idFiles = request.getParameter("id_file");

        InputStream is = fileService.getFromDB(idFiles);
        FileInputStream fis = null;
        File tmpFile = null;
        try {
            tmpFile = File.createTempFile("tmp_jpg", ".jpg");
            if (is != null) {
                fis = new FileInputStream(tmpFile);
                byte[] buffer = new byte[1024 * 1024];
                int bytesRead = 0;
                do {
                    bytesRead = is.read(buffer, 0, buffer.length);
                    response.getOutputStream().write(buffer, 0, bytesRead);
                } while (bytesRead == buffer.length);

                fis.close();
                is.close();
                response.getOutputStream().flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println("---no image----");
        }
    }

    @RequestMapping(value="/comment", method = RequestMethod.POST)
    public String simpan(ModelMap modelMap,HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

        params.put("PRM_NOMOR_PRODUK", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        params.put("PRM_CREATED", paramRequest.get("PRM_NAMA")[0]);
        params.put("PRM_EMAIL", paramRequest.get("PRM_EMAIL")[0]);
        params.put("PRM_KOMENTAR", paramRequest.get("PRM_KOMENTAR")[0]);
        params.put("PRM_RATE",paramRequest.get("PRM_RATE")[0]);
        params.put("PRM_PARENT",paramRequest.get("PRM_PARENT")[0]);
        params.put("PRM_STATUS",1);
        String noProduk=paramRequest.get("PRM_NOMOR_PRODUK")[0];

        Long id = plsqlService.save("P_KOMENTAR_FRONT",params);
//        params.put("prm_id", String.valueOf(id));
        Map<String, String> paramsChild = new HashMap<>();
        paramsChild.put("pkg_name", "P_KOMENTAR_FRONT");
        paramsChild.put("func_name", "find_child");
        paramsChild.put("prm_nomor_produk", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        List commentChild = plsqlService.findAll(paramsChild);
        params.put("prm_search", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        params.put("prm_length", "5");
        params.put("prm_start", "1");
        Map<String, Object> detailKomentar = plsqlService.find("P_KOMENTAR_FRONT", 1,5,"","",noProduk);
        Long totalRecords= Long.valueOf(detailKomentar.get("recordsTotal").toString());
        Long totalPage=(totalRecords%5==0)?totalRecords/5:(totalRecords/5+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(1l);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(noProduk);

        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(1)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(1)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("detailKomentar", detailKomentar);
        modelMap.put("commentChild", commentChild);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.product.detail.comment";
        /*List<Map<String, Object>> detailKomentar = plsqlService.findAll("P_KOMENTAR_FRONT", params);

        Map<String, Object> retValue= new HashMap<>();
        retValue.put("detailKomentar", detailKomentar);
        retValue.put("totalKomentar", detailKomentar.size());*/
        //return "redirect:/product/product-detail-comment?search="+id;
        //productDetailComment(new ModelMap(),1L,5L,id.toString());
    }

    @RequestMapping(value = "/product-list-data", method = RequestMethod.GET)
    public String productListData(ModelMap modelMap,@RequestParam(value = "draw", defaultValue = "0") int draw,
                              @RequestParam(value = "page", defaultValue = "1") long page,
                              @RequestParam(value = "length", defaultValue = "10") long length,
                              @RequestParam(value = "search", defaultValue = "") String search,
                              @RequestParam(value = "paramsearch", defaultValue = "NAMA_PRODUK") String paramSearch,
                              @RequestParam(value = "paramdir", defaultValue = "default") String paramDir,
                              @RequestParam(value = "idworkshop", defaultValue = "") String idWorkshop,
                              @RequestParam(value = "idkat", defaultValue = "") String idKat,
                                      @RequestParam(value = "idjenis", defaultValue = "") String idJenis,
                                      @RequestParam(value = "idkom", defaultValue = "") String idKom) {



        if(idWorkshop.isEmpty()){
            Map<String, Object> params = new HashMap<>();
            params.put("prm_start", 0);
            params.put("prm_length", 10);
            params.put("prm_search", " ");
            List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR_FRONT", params);

            for (Map<String, Object> map : listWorkshop) {
                idWorkshop+=map.get("ID_WORKSHOP")+",";
            }
            idWorkshop=idWorkshop.substring(0,idWorkshop.length()-1);
        }

        System.out.println("-XX- page = " + page);
        System.out.println("length = " + length);
        System.out.println("search = " + search);
        System.out.println("paramsearch = " + paramSearch);
        System.out.println("paramDir = " + paramDir);
        Map<String, Object> listProduk = plsqlService.findProduk("P_PRODUCT_FRONT", page,length,paramSearch,paramDir,"%"+search+"%",idWorkshop,"%"+idKat+"%","%"+idJenis+"%","%"+idKom+"%");

        modelMap.put("listProdukList", listProduk);
        Long totalRecords= Long.valueOf(listProduk.get("recordsTotal").toString());
        Long totalPage=(totalRecords%length==0)?totalRecords/length:(totalRecords/length+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(page);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(search);
        searchUtils.setSearchDir(paramDir);
        searchUtils.setSearchLength(length);
        searchUtils.setIdKategori(idKat);
        searchUtils.setIdJenisProduk(idJenis);
        searchUtils.setIdKomponen(idKom);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("showSplashScreen", "false");
        return "app.front.product.list.data";
    }

    @RequestMapping(value = "/product-grid-data", method = RequestMethod.GET)
    public String productGridData(ModelMap modelMap,@RequestParam(value = "draw", defaultValue = "0") int draw,
                                  @RequestParam(value = "page", defaultValue = "1") long page,
                                  @RequestParam(value = "length", defaultValue = "10") long length,
                                  @RequestParam(value = "search", defaultValue = "") String search,
                                  @RequestParam(value = "paramsearch", defaultValue = "NAMA_PRODUK") String paramSearch,
                                  @RequestParam(value = "paramdir", defaultValue = "default") String paramDir,
                                  @RequestParam(value = "idworkshop", defaultValue = "") String idWorkshop,
                                  @RequestParam(value = "idkat", defaultValue = "") String idKat,
                                  @RequestParam(value = "idjenis", defaultValue = "") String idJenis,
                                  @RequestParam(value = "idkom", defaultValue = "") String idKom) {

        if(idWorkshop.isEmpty()){
            Map<String, Object> params = new HashMap<>();
            params.put("prm_start", 0);
            params.put("prm_length", 10);
            params.put("prm_search", " ");
            List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR_FRONT", params);

            for (Map<String, Object> map : listWorkshop) {
                idWorkshop+=map.get("ID_WORKSHOP")+",";
            }
            idWorkshop=idWorkshop.substring(0,idWorkshop.length()-1);
        }

        Map<String, Object> listProduk = plsqlService.findProduk("P_PRODUCT_FRONT", page,length,paramSearch,paramDir,"%"+search+"%",idWorkshop,"%"+idKat+"%","%"+idJenis+"%","%"+idKom+"%");

        modelMap.put("listProdukList", listProduk);
        Long totalRecords= Long.valueOf(listProduk.get("recordsTotal").toString());
        Long totalPage=(totalRecords%length==0)?totalRecords/length:(totalRecords/length+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(page);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(search);
        searchUtils.setSearchDir(paramDir);
        searchUtils.setSearchLength(length);
        searchUtils.setIdKategori(idKat);
        searchUtils.setIdJenisProduk(idJenis);
        searchUtils.setIdKomponen(idKom);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.product.grid.data";
    }

    @RequestMapping(value = "/product-detail-comment", method = RequestMethod.GET)
    public String productDetailComment(ModelMap modelMap,
                                  @RequestParam(value = "page", defaultValue = "1") long page,
                                  @RequestParam(value = "length", defaultValue = "5") long length,
                                  @RequestParam(value = "search", defaultValue = "") String search) {
        Map<String, Object> detailKomentar = plsqlService.find("P_KOMENTAR_FRONT", page,5,"","",search);
        Long totalRecords= Long.valueOf(detailKomentar.get("recordsTotal").toString());
        Long totalPage=(totalRecords%5==0)?totalRecords/5:(totalRecords/5+1);
        SearchUtils searchUtils=new SearchUtils();
        searchUtils.setCurrentPage(page);
        searchUtils.setTotalPage(totalPage);
        searchUtils.setSearchText(search);
        modelMap.put("searchUtils", searchUtils);
        modelMap.addAttribute("kiri", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[0]);
        modelMap.addAttribute("kanan", AppUtil.leftRightPagination(Integer.valueOf(String.valueOf(page)), Integer.valueOf(String.valueOf(totalPage)))[1]);
        modelMap.put("detailKomentar", detailKomentar);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.product.detail.comment";
    }

    @RequestMapping(value="/star", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> start(HttpServletRequest request){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object>params = new HashMap<>();

        String noProduk=paramRequest.get("PRM_NOMOR_PRODUK")[0];

//        params.put("prm_id", String.valueOf(id));
        Map<String, String> paramsChild = new HashMap<>();
        paramsChild.put("pkg_name", "P_KOMENTAR_FRONT");
        paramsChild.put("func_name", "find_child");
        paramsChild.put("prm_nomor_produk", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        List commentChild = plsqlService.findAll(paramsChild);

        params.put("prm_search", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        params.put("prm_length", "5");
        params.put("prm_start", "1");
        Map<String, Object> detailKomentar = plsqlService.find("P_KOMENTAR_FRONT", 1,5,"","",noProduk);

        Map<String, Object> retValue = new HashMap<>();
        retValue.put("detailKomentar", detailKomentar);
        retValue.put("commentChild", commentChild);

        return retValue;
    }

    @RequestMapping(value = "/product-readmore", method = RequestMethod.GET)
    @ResponseBody
    public Map productReadmore(@RequestParam("id") String id, ModelMap modelMap) {
        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_PRODUCT_FRONT");
        paramsIn.put("func_name", "get_produk_readmore");
        paramsIn.put("prm_id", String.valueOf(id));
        Map<String, Object> productReadmore= plsqlService.findOne(paramsIn);
        modelMap.addAllAttributes(productReadmore);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return productReadmore;
    }

    @RequestMapping(value = "/total-comment", method = RequestMethod.POST)
    @ResponseBody
    public Map totalComment(HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, String> paramsIn = new HashMap<>();
        paramsIn.put("pkg_name", "P_KOMENTAR_FRONT");
        paramsIn.put("func_name", "AVG_STAR");
        paramsIn.put("prm_nomor_produk", paramRequest.get("PRM_NOMOR_PRODUK")[0]);
        Map<String, Object> commentValue= plsqlService.findOne(paramsIn);

        Map<String, Object> retValue = new HashMap<>();
        retValue.put("commentValue", commentValue);
        return retValue;
    }
}
