package id.co.iconpln.ecatalog.web.controller.admin.management.product;

import id.co.iconpln.ecatalog.db.service.FileService;
import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.AppUtil;
import id.co.iconpln.ecatalog.web.util.UserUtil;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/history")
public class HistoryAdminController {
    @Autowired
    FileService fileService;
    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(value = "/create", params="id", method = RequestMethod.GET)
    public String formCreate(@RequestParam("id") String id,
                             @RequestParam(value = "success", defaultValue = "") Boolean success,
                             @RequestParam(value = "success_create", defaultValue = "") Boolean success_create,
                             ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_MASTER_LOKASI_PEMASANGAN");
        params.put("func_name", "FIND_ALL");
        List<Map<String, Object>> lokasiPemasangan = plsqlService.findAll(params);
        params.put("pkg_name", "P_MASTER_PEMBERI_PENUGASAN");
        params.put("func_name", "FIND_ALL");
        List<Map<String, Object>> pemberiPenugasan= plsqlService.findAll(params);
        modelMap.put("nomor_produk", id );
        modelMap.put("lokasiPemasangan", lokasiPemasangan);
        modelMap.put("pemberiPenugasan", pemberiPenugasan);
        modelMap.put("success", success);
        modelMap.put("success_create", success_create);
        modelMap.put("authUser", UserUtil.getAuthUser());
        modelMap.put("label","create");
        return "app.admin.management-product.history.list";
    }

    @RequestMapping(value = "/list", params = "id", method = RequestMethod.GET)
    public String list(@RequestParam("id") String id, HttpServletRequest request,
                         ModelMap modelMap){
        // Isi kode form untuk membuat data baru
        long pageLength = 5; //panjang halaman
        //ambil data dari database
        Map dataMap = plsqlService.find("P_PRODUCT", 0, pageLength, "NOMOR_PRODUK", "ASC");
        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);
        modelMap.addAttribute("nomor_produk", id);

        modelMap.put("authUser", UserUtil.getAuthUser());
        modelMap.put("label","history");

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        return "app.admin.management-product.history.list";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(HttpServletRequest request, ModelMap modelMap){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }
        String prm_nomer_produk = paramRequest.get("PRM_NOMOR_PRODUK")[0];
        Long retValue = plsqlService.save("P_HISTORIKAL_PRODUK", paramSourceIn);

        modelMap.put("success", true);
        modelMap.put("success_create", true);

        modelMap.put("ret_code", 1);
        modelMap.put("ret_class", "note-success");
        modelMap.put("ret_message", "Data Berhasil Disimpan");

        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/history/list?id="+prm_nomer_produk;
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") String id,
                             @RequestParam(value = "success", defaultValue = "") Boolean success,
                             @RequestParam(value = "success_update", defaultValue = "") Boolean success_update,
                             ModelMap modelMap){
        // Isi kode form untuk mengedit data lama
        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_MASTER_LOKASI_PEMASANGAN");
        params.put("func_name", "FIND_ALL");
        List<Map<String, Object>> lokasiPemasangan = plsqlService.findAll(params);
        params.put("pkg_name", "P_MASTER_PEMBERI_PENUGASAN");
        params.put("func_name", "FIND_ALL");
        List<Map<String, Object>> pemberiPenugasan= plsqlService.findAll(params);
        Map<String, Object> listHistory = plsqlService.findOne("P_HISTORIKAL_PRODUK", id);
        modelMap.addAllAttributes(listHistory);
        modelMap.put("ID_HISTORY", id );
        modelMap.put("lokasiPemasangan", lokasiPemasangan);
        modelMap.put("pemberiPenugasan", pemberiPenugasan);
        modelMap.put("label", "update");
        modelMap.put("success", success);
        modelMap.put("success_update", success_update);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.management-product.history.list";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") String id, HttpServletRequest request,  ModelMap modelMap){
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }

        Long retValue = plsqlService.update("P_HISTORIKAL_PRODUK", paramSourceIn);

        modelMap.put("success", true);
        modelMap.put("success_update", true);

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));


        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/history/update?id="+id;
    }

    // Menampilkan halaman detail
    @RequestMapping(params = "id", method = RequestMethod.GET)
    public String detail(@RequestParam("id") String id, ModelMap modelMap){
        // Isi kode form untuk melihat data detail

        Map<String, Object> listHistory = plsqlService.findOne("P_HISTORIKAL_PRODUK", id);
        modelMap.addAllAttributes(listHistory);
        Map<String, String> params = new HashMap<>();
        params.put("pkg_name", "P_HISTORIKAL_PRODUK");
        params.put("func_name", "FIND_FILE");
        params.put("prm_id_history", id);
        List<Map<String, Object>> listFile= plsqlService.findAll(params);
        modelMap.put("listFile", listFile);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());


        return "app.admin.management-product.history.list";
    }

    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String find(ModelMap modelMap)
    {
        long pageLength = 5; //panjang halaman
        //ambil data dari database
        Map dataMap = plsqlService.find("P_PRODUCT", 0, pageLength, "NOMOR_PRODUK", "ASC");
        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);
        //bukan
        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());
        //panggil jsp
        return "app.admin.management-product.history.list";
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            @RequestParam(value = "prm_nomor_produk", defaultValue = "") String prm_nomor_produk,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_HISTORIKAL_PRODUK", start, length, sortBy, sortDir.toUpperCase(), search.toUpperCase(),prm_nomor_produk);
        map.put("draw", draw);

        return map;
    }


    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request){

        //Ambil NOMOR PRODUK
        String nomor_produk = request.getParameter("nomor_produk");

        // Isi kode form untuk menghapus data
        Map<String, Object> paramSourceIn = new HashMap<>();
        paramSourceIn.put("prm_id_history", id);
        Long retValue= plsqlService.delete("P_HISTORIKAL_PRODUK",paramSourceIn);
        if (retValue > 0) {
            modelMap.put("success", true);
            modelMap.put("message", "Data berhasil di simpan");
        } else {
            modelMap.put("success", false);
            modelMap.put("message", "Data gagal di simpan");
        }


        modelMap.put("authUser", UserUtil.getAuthUser());
        modelMap.put("ret_code", 1);
        modelMap.put("ret_class", "note-success");
        modelMap.put("ret_message", "Data Berhasil Dihapus");

        return "redirect:/admin/history/list?id="+nomor_produk;
    }
    //proses simpan file
    @RequestMapping(value = "/simpanFile", method = RequestMethod.POST)
    public String simpanFile(MultipartHttpServletRequest request){

//        Map<String,Object> retValue = new HashMap<>();

//        try {

//            MultipartFile multipartFile = request.getFile("PRM_MULTI_DRAWING_PDF");

            Map<String, Object> paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_HISTORIKAL_PRODUK");
            paramSourceIn.put("func_name", "SIMPAN_FILE");
            paramSourceIn.put("param_count", 5);

            //set prm_nomor_produk
            Map<String, Object> prm_nomor_produk = new HashMap<>();
            prm_nomor_produk.put("index", 1);
            prm_nomor_produk.put("type", AppUtil.ECAT_STRING);
            prm_nomor_produk.put("content", request.getParameter("PRM_NOMOR_PRODUK"));
            paramSourceIn.put("PRM_NOMOR_PRODUK", prm_nomor_produk);

            try {
//                //File prm_multi_drawing_pdf
////                File tempMultiDrawingPDF = File.createTempFile("tmp","");
////                multipartFile.transferTo(tempMultiDrawingPDF);
//                //set prm_content_type
                File tempFilePenugasan = File.createTempFile("file_penugasan", ".pdf");
                request.getFile("PRM_PDF_PENUGASAN").transferTo(tempFilePenugasan);
                Map<String, Object> mapFilePenugasan = new HashMap<>();
                mapFilePenugasan.put("index", 2);
                mapFilePenugasan.put("type", AppUtil.ECAT_FILE);
                mapFilePenugasan.put("content", tempFilePenugasan);
                paramSourceIn.put("prm_pdf_penugasan", mapFilePenugasan);

                File tempFileRabProduksi= File.createTempFile("file_rab_produksi", ".pdf");
                request.getFile("PRM_FILE_RAB_PRODUKSI").transferTo(tempFileRabProduksi);
                Map<String, Object> mapFileRabProduksi= new HashMap<>();
                mapFileRabProduksi.put("index", 3);
                mapFileRabProduksi.put("type", AppUtil.ECAT_FILE);
                mapFileRabProduksi.put("content", tempFileRabProduksi);
                paramSourceIn.put("prm_file_rab_produksi", mapFileRabProduksi);

                File tempFileHpp= File.createTempFile("file_hpp", ".pdf");
                request.getFile("PRM_FILE_HPP").transferTo(tempFileHpp);
                Map<String, Object> mapFileHpp= new HashMap<>();
                mapFileHpp.put("index", 4);
                mapFileHpp.put("type", AppUtil.ECAT_FILE);
                mapFileHpp.put("content", tempFileHpp);
                paramSourceIn.put("prm_file_hpp", mapFileHpp);

                File tempFileRealisasiProduksi= File.createTempFile("file_realisasi_produksi", ".pdf");
                request.getFile("PRM_FILE_REALISASI_PRODUKSI").transferTo(tempFileRealisasiProduksi);
                Map<String, Object> mapFileRealisasiProduksi = new HashMap<>();
                mapFileRealisasiProduksi.put("index", 5);
                mapFileRealisasiProduksi.put("type", AppUtil.ECAT_FILE);
                mapFileRealisasiProduksi.put("content", tempFileRealisasiProduksi);
                paramSourceIn.put("prm_file_realisasi_produksi", mapFileRealisasiProduksi);


            }catch (Exception e){
                e.printStackTrace();
            }

            String retValue = plsqlService.saveWithFile(paramSourceIn);


//            //membuat object file
//            Map<String, Object> mapFile = new HashMap<>();
//            mapFile.put("deleteType", "DELETE");
//            mapFile.put("deleteUrl", "/delete");
//            mapFile.put("name", multipartFile.getOriginalFilename());
//            mapFile.put("size", multipartFile.getSize());
//            mapFile.put("thumbnailUrl", "-");
//            mapFile.put("type", multipartFile.getContentType());
//            mapFile.put("url", "/download");

            //List object File
//            List<Map<String,Object>> listFiles = new ArrayList<>();
//            listFiles.add(mapFile);

            //return value
//            retValue.put("files", listFiles);
//
//        }catch (Exception e){
//            retValue.put("success", false);
//            retValue.put("message", e.getMessage());
//        }

        return "redirect:/admin/history/list?id="+request.getParameter("PRM_NOMOR_PRODUK");
    }


    //proses simpan file
    @RequestMapping(value = "/simpanHistoryFile", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> simpanHistoryFilePost(MultipartHttpServletRequest request){

        Map<String,Object> retValue = new HashMap<>();

        try {

            MultipartFile multipartFile = request.getFile("PRM_FILE_HISTORY");

            Map<String, Object> paramSourceIn = new HashMap<>();
            paramSourceIn.put("pkg_name", "P_TRANS_HISTORY_PRODUCT_FILE");
            paramSourceIn.put("func_name", "SIMPAN_FILE");
            paramSourceIn.put("param_count", 6);

            //set prm_nomor_produk
            Map<String, Object> prm_historikal_produk = new HashMap<>();
            prm_historikal_produk.put("index", 1);
            prm_historikal_produk.put("type", AppUtil.ECAT_STRING);
            prm_historikal_produk.put("content", request.getParameter("PRM_ID_HISTORY"));
            paramSourceIn.put("prm_historikal_produk", prm_historikal_produk);

            //set prm_content_type
            Map<String, Object> prm_content_type = new HashMap<>();
            prm_content_type.put("index", 2);
            prm_content_type.put("type", AppUtil.ECAT_STRING);
            prm_content_type.put("content", multipartFile.getContentType());
            paramSourceIn.put("prm_content_type", prm_content_type);

            //set prm_name
            Map<String, Object> prm_name = new HashMap<>();
            prm_name.put("index", 3);
            prm_name.put("type", AppUtil.ECAT_STRING);
            prm_name.put("content", multipartFile.getOriginalFilename());
            paramSourceIn.put("prm_name", prm_name);

            //set prm_file_size
            Map<String, Object> prm_file_size = new HashMap<>();
            prm_file_size.put("index", 4);
            prm_file_size.put("type", AppUtil.ECAT_STRING);
            prm_file_size.put("content", multipartFile.getSize());
            paramSourceIn.put("prm_file_size", prm_file_size);

            //set prm_file_size
            Map<String, Object> prm_jns_upload = new HashMap<>();
            prm_jns_upload.put("index", 5);
            prm_jns_upload.put("type", AppUtil.ECAT_STRING);
            prm_jns_upload.put("content", request.getParameter("PRM_JENIS_UPLOAD"));
            paramSourceIn.put("prm_jns_upload", prm_jns_upload);

            try {
                //File prm_multi_drawing_pdf
                File tempMulti = File.createTempFile("tmp","");
                multipartFile.transferTo(tempMulti);
                Map<String, Object> map2DZoom= new HashMap<>();
                map2DZoom.put("index", 6);
                map2DZoom.put("type", AppUtil.ECAT_FILE);
                map2DZoom.put("content", tempMulti);
                paramSourceIn.put("prm_content_file", map2DZoom);

            }catch (Exception e){
                e.printStackTrace();
            }
            //sout

            String test = plsqlService.saveWithFile(paramSourceIn);

            //membuat object file
            Map<String, Object> mapFile = new HashMap<>();
            mapFile.put("deleteType", "DELETE");
            mapFile.put("deleteUrl", "/delete");
            mapFile.put("name", multipartFile.getOriginalFilename());
            mapFile.put("size", multipartFile.getSize());
            mapFile.put("thumbnailUrl", "-");
            mapFile.put("type", multipartFile.getContentType());
            mapFile.put("url", "/download");

            //List object File
            List<Map<String,Object>> listFiles = new ArrayList<>();
            listFiles.add(mapFile);

            //return value
            retValue.put("files", listFiles);

        }catch (Exception e){
            retValue.put("success", false);
            retValue.put("message", e.getMessage());
        }

        return retValue;
    }

    // Menampilkan data produk dengan kembalian json
    @RequestMapping(value = "/findAllProductHistoryFileAll", method = RequestMethod.GET)
    @ResponseBody
    public Map get2Dthumbnail(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "NOMOR_PRODUK") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            @RequestParam(value = "search[value]", defaultValue = "") String search,
            @RequestParam(value = "PRM_ID_HISTORY", defaultValue = "") String prmIdHistory,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_TRANS_HISTORY_PRODUCT_FILE", start, length, sortBy,
                sortDir.toUpperCase(), search.toUpperCase(), prmIdHistory);
        map.put("draw", draw);
        return map;
    }

    @RequestMapping(value = "/hapus_product_history_file", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> hapus_product_history_file(HttpServletRequest request) {

        Map<String, Object> params = new HashMap<>();
        params.put("pkg_name", "P_TRANS_HISTORY_PRODUCT_FILE");
        params.put("func_name", "ps");
        params.put("prm_id_product_history_file", request.getParameter("prm_id_product_history_file"));
        String retMessage = plsqlService.save(params);
        Map<String, Object> retValue = plsqlService.returnMapMessage(retMessage);

        return retValue;
    }


}
