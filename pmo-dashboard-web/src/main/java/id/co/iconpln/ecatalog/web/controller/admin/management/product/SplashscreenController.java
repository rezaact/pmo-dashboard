package id.co.iconpln.ecatalog.web.controller.admin.management.product;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by barka01 on 05/10/15.
 */

@Controller
@RequestMapping("/admin/splashcreen")
public class SplashscreenController {

    @Autowired
    PlsqlService plsqlService;

    //Menampilkan Halaman Pertama
    @RequestMapping(method = RequestMethod.GET)
    public String find( ModelMap modelMap, HttpServletRequest request  )
    {
        long pageLength =5 ; //panjang halaman
        //get default data
        Map dataMap = plsqlService.find("P_MASTER_MATERIAL",0,pageLength,"NAMA_MATERIAL","ASC","");

        //data dan panjang halaman dimasukkan sebagai parameter di jsp
        modelMap.addAllAttributes(dataMap);
        modelMap.addAttribute("length", pageLength);

        modelMap.put("label", "list");
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        //panggil jsp
        return "app.admin.master-data.material.list";
    }
}
