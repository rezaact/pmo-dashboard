package id.co.iconpln.ecatalog.web.controller.admin.master.data;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/admin/master-data/workshop")
public class MasterWorkshopAdminController {
    @Autowired
    PlsqlService plsqlService;

    // Menampilkan halaman create
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String formCreate(ModelMap modelMap) {
        modelMap.put("label", "create");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.workshop.form";
    }

    // Proses penyimpanan data
    @RequestMapping(method = RequestMethod.POST)
    public String create(ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> params = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            params.put(keyName, paramRequest.get(keyName)[0]);
        }
        Long id = plsqlService.save("P_MASTER_MANUFAKTUR", params);
        modelMap.put("ret_code", id);
        if(id == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }


        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/workshop";
    }

    // Menampilkan halaman update
    @RequestMapping(value = "/update", params = "id", method = RequestMethod.GET)
    public String formUpdate(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_MANUFAKTUR", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "update");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.workshop.form";
    }

    // Proses update data
    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id") long id, ModelMap modelMap, HttpServletRequest request) {
        Map<String, String[]> paramRequest = request.getParameterMap();
        Map<String, Object> paramSourceIn = new HashMap<>();

        for (String keyName : paramRequest.keySet()) {
            paramSourceIn.put(keyName, paramRequest.get(keyName)[0]);
        }

        Long retValue = plsqlService.update("P_MASTER_MANUFAKTUR", paramSourceIn);

        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Disimpan. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Disimpan");
        }

        // Isi kode untuk menyimpan hasil editan data
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "redirect:/admin/master-data/workshop";
    }

    // Menampilkan halaman detail
    @RequestMapping(value = "/detail", params = "id", method = RequestMethod.GET)
    public String formDetail(@RequestParam("id") long id, ModelMap modelMap) {
        // Isi kode form untuk mengedit data lama
        Map<String, Object> data = plsqlService.findOne("P_MASTER_MANUFAKTUR", id);
        modelMap.addAllAttributes(data);
        modelMap.put("label", "detail");
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.admin.master-data.workshop.form";
    }

    // Menampilkan halaman seluruh data
    @RequestMapping(method = RequestMethod.GET)
    public String findAll(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            @RequestParam(value = "delete", defaultValue = "") Boolean delete,
            @RequestParam(value = "banned", defaultValue = "") Boolean banned,
            @RequestParam(value = "active", defaultValue = "") Boolean active,
            @RequestParam(value = "update", defaultValue = "") Boolean update,
            ModelMap modelMap, HttpServletRequest request) {
        //get default data
        Map<String, Object> params = new HashMap<>();

        params.put("prm_start", start);
        params.put("prm_length", length);
        params.put("prm_search", " ");
        List<Map<String, Object>> listWorkshop = plsqlService.findAll("P_MASTER_MANUFAKTUR", params);
        modelMap.put("listWorkshop", listWorkshop);
        modelMap.put("delete", delete);
        modelMap.put("banned", banned);
        modelMap.put("active", active);
        modelMap.put("update", update);
        modelMap.put("authUser", UserUtil.getAuthUser());

        modelMap.put("ret_code", request.getParameter("ret_code"));
        modelMap.put("ret_class", request.getParameter("ret_class"));
        modelMap.put("ret_message", request.getParameter("ret_message"));

        return "app.admin.master-data.workshop.list";
    }

    @RequestMapping(value = "/json", method = RequestMethod.GET)
    @ResponseBody
    public Map findJson(
            @RequestParam(value = "draw", defaultValue = "0") int draw,
            @RequestParam(value = "start", defaultValue = "0") long start,
            @RequestParam(value = "length", defaultValue = "10") long length,
            @RequestParam(value = "columns[0][data]", defaultValue = "ID_WORKSHOP") String firstColumn,
            @RequestParam(value = "order[0][column]", defaultValue = "0") int sortIndex,
            @RequestParam(value = "order[0][dir]", defaultValue = "ASC") String sortDir,
            HttpServletRequest request
    ) {
        String sortBy = request.getParameter("columns[" + sortIndex + "][data]");
        Map<String, Object> map = plsqlService.find("P_MASTER_MANUFAKTUR", start, length, sortBy, sortDir.toUpperCase());
        map.put("draw", draw);
        return map;
    }

    // Proses delete data
    @RequestMapping(value = "/delete", params = "id", method = RequestMethod.GET)
    public String delete(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_workshop", id);
        Long retValue = plsqlService.delete("P_MASTER_MANUFAKTUR", paramSourceIn);
        modelMap.put("ret_code", retValue);
        if(retValue == 0){
            modelMap.put("ret_class", "note-danger");
            modelMap.put("ret_message", "Data Gagal Dihapus. Cek Koneksi Internet/Jaringan Anda");
        }else{
            modelMap.put("ret_class", "note-success");
            modelMap.put("ret_message", "Data Berhasil Dihapus");
        }

        // Isi kode form untuk menghapus data
        return "redirect:/admin/master-data/workshop";
    }

    // Proses banned data
    @RequestMapping(value = "/banned", params = "id", method = RequestMethod.GET)
    public String banned(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_workshop", id);
        Long retValue = plsqlService.banned("p_workshop", paramSourceIn);
        if (retValue > 0) {
            modelMap.put("banned", true);
        } else {
            modelMap.put("banned", false);
        }

        // Isi kode form untuk menghapus data
        return "redirect:/admin/master-data/workshop";
    }

    // Proses active data
    @RequestMapping(value = "/active", params = "id", method = RequestMethod.GET)
    public String active(@RequestParam("id") long id, ModelMap modelMap) {

        Map<String, Object> paramSourceIn = new HashMap<>();

        paramSourceIn.put("prm_id_workshop", id);
        Long retValue = plsqlService.active("p_workshop", paramSourceIn);
        if (retValue > 0) {
            modelMap.put("active", true);
        } else {
            modelMap.put("active", false);
        }

        // Isi kode form untuk menghapus data
        return "redirect:/admin/master-data/workshop";
    }

}
