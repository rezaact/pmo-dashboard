package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/dashboard-scurve")
public class DashboardScurveController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(value = "/ruptl",method = RequestMethod.GET)
    public String bulanan(ModelMap modelMap, HttpServletRequest request){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());


        request.getSession().setAttribute("prevPage", "/dashboard-scurve/ruptl");

        return "app.front.dashboard.scurve.ruptl"; //xml
    }

    @RequestMapping(value = "/supply",method = RequestMethod.GET)
    public String tahunan(ModelMap modelMap, HttpServletRequest request){
        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());

        request.getSession().setAttribute("prevPage", "/dashboard-scurve/supply");
        return "app.front.dashboard.scurve.supply"; //xml
    }

    @RequestMapping(value = "/getdata-supply-demand",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_supply_demand(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");
        String cmbKapasitas = request.getParameter("cmbKapasitas");
        String cmbDemand = request.getParameter("cmbDemand");
        String cmbSusut = request.getParameter("cmbSusut");
        String cmbTumbuh = request.getParameter("cmbTumbuh");

        //Data generation pembangkit
        Map<String, Object> pGetDataSupplay_m = new HashMap<>();
        pGetDataSupplay_m.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_m.put("func_name", "gen_Capacity_data_Demand");
        pGetDataSupplay_m.put("p_programid", Integer.valueOf(cmbProgram));
        pGetDataSupplay_m.put("p_regionid", Integer.valueOf(cmbRegion));
        pGetDataSupplay_m.put("p_tahunawal", Integer.valueOf(cmbThnAwal));
        pGetDataSupplay_m.put("p_tahunakhir", Integer.valueOf(cmbThnAkhir));
        pGetDataSupplay_m.put("p_ExistCapacity", Integer.valueOf(cmbKapasitas));
        pGetDataSupplay_m.put("p_ExistDemand", Integer.valueOf(cmbDemand));
        pGetDataSupplay_m.put("p_SusutBulan", cmbSusut);
        pGetDataSupplay_m.put("p_TumbuhBulan", cmbTumbuh);

        plsqlService.save(pGetDataSupplay_m);

        //Chart Generation
        Map<String, String> pGetDataSupplay_gen = new HashMap<>();
        pGetDataSupplay_gen.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_gen.put("func_name", "get_CapacityChart_Demand");//
        pGetDataSupplay_gen.put("p_type", "DEMAND");
        String strGetDataSupplay_gen = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("strGetDataSupplay_gen", strGetDataSupplay_gen);

        return retValue;

    }

    //Chart
    @RequestMapping(value = "/getdata-scurve-ruptl",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdataScurveRuptl(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String startPeriod = request.getParameter("startPeriod");
        String endPeriod = request.getParameter("endPeriod");


        //Data generation pembangkit
        Map<String, Object> pGetDataSupplay_m = new HashMap<>();
        pGetDataSupplay_m.put("p_group", Integer.valueOf(program));
        pGetDataSupplay_m.put("p_region", Integer.valueOf(region));
        pGetDataSupplay_m.put("p_thblawal", startPeriod);
        pGetDataSupplay_m.put("p_thblakhir", endPeriod);
        pGetDataSupplay_m.put("p_session", "1");
        pGetDataSupplay_m.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_m.put("func_name", "gen_scurve_ruptl_data_asset");
        pGetDataSupplay_m.put("p_asset_type", "17");
        plsqlService.save(pGetDataSupplay_m);
        //for transmission
        pGetDataSupplay_m.put("p_asset_type", "18");
        plsqlService.save(pGetDataSupplay_m);
        //GI
        pGetDataSupplay_m.put("p_asset_type", "19");
        plsqlService.save(pGetDataSupplay_m);

        //Chart Generation
        Map<String, String> pGetDataSupplay_gen = new HashMap<>();
        pGetDataSupplay_gen.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_gen.put("func_name", "get_scurve_ruptl_chart");//
        //GEN
        pGetDataSupplay_gen.put("p_type", "17");
        String strGetDataSupplay_gen = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("strGetDataSupplay_gen", strGetDataSupplay_gen);
        //TRANS
        pGetDataSupplay_gen.put("p_type", "18");
        String dataTransmission = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataTransmission", dataTransmission);
        //GI
        pGetDataSupplay_gen.put("p_type", "19");
        String dataGI = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataGI", dataGI);

        return retValue;

    }

    //Detail Chart
    @RequestMapping(value = "/getdata-detail-scurve-ruptl",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailScurveRuptl(HttpServletRequest request){
        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String thbl = request.getParameter("thbl");
        String assettype = request.getParameter("assettype");
        String planactual = request.getParameter("planactual");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetData = new HashMap<>();
        pGetData.put("pkg_name", "P_FILTER_SCURVE");
        pGetData.put("func_name", "get_detail_scurve_byasset");
        pGetData.put("p_Region", region);
        pGetData.put("p_Group", program);
        pGetData.put("p_thbl", thbl);
        pGetData.put("p_planactual", planactual);
        pGetData.put("p_assettype", assettype);
        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetData);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

    //Update 29-01-2017
    // --------------------------------------

    /*
           Menu RUPTL PMO
           Akses by URL
           -----------------------------------
     */
    @RequestMapping(value = "/ruptl_pmo",method = RequestMethod.GET)
    public String index_ruptl_pmo(ModelMap modelMap, HttpServletRequest request){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        request.getSession().setAttribute("prevPage", "/dashboard-scurve/ruptl_pmo");
        return "app.front.dashboard.scurve.ruptl.pmo"; //xml
    }
    //Chart
    @RequestMapping(value = "/getdata-scurve-ruptl-pmo",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdataScurveRuptlPMO(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String startPeriod = request.getParameter("startPeriod");
        String endPeriod = request.getParameter("endPeriod");


        //Data generation pembangkit
        Map<String, Object> pGetDataSupplay_m = new HashMap<>();
        pGetDataSupplay_m.put("p_group", Integer.valueOf(program));
        pGetDataSupplay_m.put("p_region", Integer.valueOf(region));
        pGetDataSupplay_m.put("p_thblawal", startPeriod);
        pGetDataSupplay_m.put("p_thblakhir", endPeriod);
        pGetDataSupplay_m.put("p_session", "1");
        pGetDataSupplay_m.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_m.put("func_name", "gen_scurve_ruptl_assetpmo");//
        pGetDataSupplay_m.put("p_asset_type", "17");
        plsqlService.save(pGetDataSupplay_m);
        //for transmission
        pGetDataSupplay_m.put("p_asset_type", "18");
        plsqlService.save(pGetDataSupplay_m);
        //GI
        pGetDataSupplay_m.put("p_asset_type", "19");
        plsqlService.save(pGetDataSupplay_m);

        //Chart Generation
        Map<String, String> pGetDataSupplay_gen = new HashMap<>();
        pGetDataSupplay_gen.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_gen.put("func_name", "get_scurve_ruptl_chart");//
        //GEN
        pGetDataSupplay_gen.put("p_type", "17");
        String strGetDataSupplay_gen = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("strGetDataSupplay_gen", strGetDataSupplay_gen);
        //TRANS
        pGetDataSupplay_gen.put("p_type", "18");
        String dataTransmission = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataTransmission", dataTransmission);
        //GI
        pGetDataSupplay_gen.put("p_type", "19");
        String dataGI = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataGI", dataGI);

        return retValue;

    }

    //Detail Chart
    @RequestMapping(value = "/getdata-detail-scurve-ruptl-pmo",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailScurveRuptlPMO(HttpServletRequest request){
        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String thbl = request.getParameter("thbl");
        String assettype = request.getParameter("assettype");
        String planactual = request.getParameter("planactual");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetData = new HashMap<>();
        pGetData.put("pkg_name", "P_FILTER_SCURVE");
        pGetData.put("func_name", "get_detail_scurve_byasset");
        pGetData.put("p_Region", region);
        pGetData.put("p_Group", program);
        pGetData.put("p_thbl", thbl);
        pGetData.put("p_planactual", planactual);
        pGetData.put("p_assettype", assettype);
        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetData);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

    /*
           Menu RUPTL PMO By Phase
           Akses by URL
           -----------------------------------
     */

    @RequestMapping(value = "/ruptl_pmophase",method = RequestMethod.GET)
    public String index_ruptl_pmophase(ModelMap modelMap, HttpServletRequest request){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        request.getSession().setAttribute("prevPage", "/dashboard-scurve/ruptl_pmophase");
        return "app.front.dashboard.scurve.ruptl.pmo.phase"; //xml
    }
    //Chart
    @RequestMapping(value = "/getdata-scurve-ruptl-pmo-phase",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdataScurveRuptlPMOPhase(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String startPeriod = request.getParameter("startPeriod");
        String endPeriod = request.getParameter("endPeriod");


        //Data generation pembangkit
        Map<String, Object> pGetDataSupplay_m = new HashMap<>();
        pGetDataSupplay_m.put("p_group", Integer.valueOf(program));
        pGetDataSupplay_m.put("p_region", Integer.valueOf(region));
        pGetDataSupplay_m.put("p_thblawal", startPeriod);
        pGetDataSupplay_m.put("p_thblakhir", endPeriod);
        pGetDataSupplay_m.put("p_session", "1");
        pGetDataSupplay_m.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_m.put("func_name", "gen_scurve_assetpmoheader");//gen_scurve_ruptl_data_asset
        pGetDataSupplay_m.put("p_asset_type", "17");
        plsqlService.save(pGetDataSupplay_m);
        //for transmission
        pGetDataSupplay_m.put("p_asset_type", "18");
        plsqlService.save(pGetDataSupplay_m);
        //GI
        pGetDataSupplay_m.put("p_asset_type", "19");
        plsqlService.save(pGetDataSupplay_m);

        //Chart Generation
        Map<String, String> pGetDataSupplay_gen = new HashMap<>();
        pGetDataSupplay_gen.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_gen.put("func_name", "get_scurve_ruptl_chart");//
        //GEN
        pGetDataSupplay_gen.put("p_type", "17");
        String strGetDataSupplay_gen = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("strGetDataSupplay_gen", strGetDataSupplay_gen);
        //TRANS
        pGetDataSupplay_gen.put("p_type", "18");
        String dataTransmission = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataTransmission", dataTransmission);
        //GI
        pGetDataSupplay_gen.put("p_type", "19");
        String dataGI = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataGI", dataGI);

        return retValue;

    }

    //Detail Chart
    @RequestMapping(value = "/getdata-detail-scurve-ruptl-pmo-phase",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailScurveRuptlPMOPhase(HttpServletRequest request){
        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String thbl = request.getParameter("thbl");
        String assettype = request.getParameter("assettype");
        String planactual = request.getParameter("planactual");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetData = new HashMap<>();
        pGetData.put("pkg_name", "P_FILTER_SCURVE");
        pGetData.put("func_name", "get_detail_scurve_byasset");//get_detail_scurve_byasset
        pGetData.put("p_Region", region);
        pGetData.put("p_Group", program);
        pGetData.put("p_thbl", thbl);
        pGetData.put("p_planactual", planactual);
        pGetData.put("p_assettype", assettype);
        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetData);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }


    /*
           Menu RUPTL by Phase
           Akses by Menu
           -----------------------------------
     */
    @RequestMapping(value = "/ruptl_phase",method = RequestMethod.GET)
    public String index_ruptl_phase(ModelMap modelMap, HttpServletRequest request){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        request.getSession().setAttribute("prevPage", "/dashboard-scurve/ruptl_phase");
        return "app.front.dashboard.scurve.ruptl.phase"; //xml
    }
    //Chart
    @RequestMapping(value = "/getdata-scurve-ruptl-phase",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdataScurveRuptlPhase(ModelMap modelMap, HttpServletRequest request){
        Map<String, Object> retValue = new HashMap<>();

        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String startPeriod = request.getParameter("startPeriod");
        String endPeriod = request.getParameter("endPeriod");


        //Data generation pembangkit
        Map<String, Object> pGetDataSupplay_m = new HashMap<>();
        pGetDataSupplay_m.put("p_group", Integer.valueOf(program));
        pGetDataSupplay_m.put("p_region", Integer.valueOf(region));
        pGetDataSupplay_m.put("p_thblawal", startPeriod);
        pGetDataSupplay_m.put("p_thblakhir", endPeriod);
        pGetDataSupplay_m.put("p_session", "1");
        pGetDataSupplay_m.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_m.put("func_name", "gen_scurve_ruptl_asset_phase");//gen_scurve_ruptl_data_asset
        pGetDataSupplay_m.put("p_asset_type", "17");
        plsqlService.save(pGetDataSupplay_m);
        //for transmission
        pGetDataSupplay_m.put("p_asset_type", "18");
        plsqlService.save(pGetDataSupplay_m);
        //GI
        pGetDataSupplay_m.put("p_asset_type", "19");
        plsqlService.save(pGetDataSupplay_m);

        //Chart Generation
        Map<String, String> pGetDataSupplay_gen = new HashMap<>();
        pGetDataSupplay_gen.put("pkg_name", "P_FILTER_SCURVE");
        pGetDataSupplay_gen.put("func_name", "get_scurve_ruptl_chart");//
        //GEN
        pGetDataSupplay_gen.put("p_type", "17");
        String strGetDataSupplay_gen = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("strGetDataSupplay_gen", strGetDataSupplay_gen);
        //TRANS
        pGetDataSupplay_gen.put("p_type", "18");
        String dataTransmission = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataTransmission", dataTransmission);
        //GI
        pGetDataSupplay_gen.put("p_type", "19");
        String dataGI = plsqlService.findString(pGetDataSupplay_gen);
        retValue.put("dataGI", dataGI);

        return retValue;

    }

    //Detail Chart
    @RequestMapping(value = "/getdata-detail-scurve-ruptl-phase",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getDataDetailScurveRuptlPhase(HttpServletRequest request){
        String region = request.getParameter("region");
        String program = request.getParameter("program");
        String thbl = request.getParameter("thbl");
        String assettype = request.getParameter("assettype");
        String planactual = request.getParameter("planactual");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetData = new HashMap<>();
        pGetData.put("pkg_name", "P_FILTER_SCURVE");
        pGetData.put("func_name", "get_detail_scurve_byasset");
        pGetData.put("p_Region", region);
        pGetData.put("p_Group", program);
        pGetData.put("p_thbl", thbl);
        pGetData.put("p_planactual", planactual);
        pGetData.put("p_assettype", assettype);
        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetData);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }





}
