package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/service")
public class ServiceController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(method = RequestMethod.GET)
    public String service(
            @RequestParam(value = "start", defaultValue = "0") int start,
            @RequestParam(value = "length", defaultValue = "10") int length,
            ModelMap modelMap   )
    {
        //get default data
        Map<String, Object> params = new HashMap<>();

        params.put("prm_start",start);
        params.put("prm_length",length);
        params.put("prm_seacrh"," ");
        List<Map<String, Object>> listService = plsqlService.findAll("p_service",params);
        modelMap.put("listService",listService);
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.service";
    }
}
