package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


@Controller
@RequestMapping("/dashboard-cod-new")
public class DashboardCapacityPlanController {

    @Autowired
    PlsqlService plsqlService;

    @RequestMapping(value = "/bulanan",method = RequestMethod.GET)
    public String bulanan(ModelMap modelMap){

        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.bulanan"; //xml
    }

    @RequestMapping(value = "/bulanan/getdata-cod-bulanan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_chartbulanan(ModelMap modelMap, HttpServletRequest request) {
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbType = request.getParameter("cmbType");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbDate = request.getParameter("cmbDate");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");

        /*
        CHART GENERATION
        ----------------
         */
        //Data generation pembangkit
        Map<String, Object> pGetCapacityData_gen_m = new HashMap<>();
        pGetCapacityData_gen_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_gen_m.put("func_name", "get_Capacity_data_m");//get_Capacity_data_m
        pGetCapacityData_gen_m.put("p_type", 17);
        pGetCapacityData_gen_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_gen_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_gen_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_gen_m.put("p_year", Integer.valueOf(cmbDate));
        pGetCapacityData_gen_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_gen_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_gen_m);


        //Chart Generation pembangkit
        Map<String, String> pGetGeneration_gen = new HashMap<>();
        pGetGeneration_gen.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_gen.put("func_name", "get_CapacityChart_m");//
        pGetGeneration_gen.put("p_type", String.valueOf(17));
        String strGetGeneration_gen = plsqlService.findString(pGetGeneration_gen);
        retValue.put("strGetGeneration_gen", strGetGeneration_gen);


        /*
        CHART TRANSMITION
        ----------------
         */
        //Data generation transmition
        Map<String, Object> pGetCapacityData_trans_m = new HashMap<>();
        pGetCapacityData_trans_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_trans_m.put("func_name", "get_Capacity_data_m");
        pGetCapacityData_trans_m.put("p_type", 18);
        pGetCapacityData_trans_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_trans_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_trans_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_trans_m.put("p_year", Integer.valueOf(cmbDate));
        pGetCapacityData_trans_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_trans_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_trans_m);

        //Chart Generation transmition
        Map<String, String> pGetGeneration_trans = new HashMap<>();
        pGetGeneration_trans.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_trans.put("func_name", "get_CapacityChart_m");
        pGetGeneration_trans.put("p_tmp", String.valueOf(18));
        String strGetGeneration_tran = plsqlService.findString(pGetGeneration_trans);
        retValue.put("strGetGeneration_tran", strGetGeneration_tran);


        /*
        CHART SUBTATION
        ----------------
         */
        //Data
        Map<String, Object> pGetSubtationData_sub_m = new HashMap<>();
        pGetSubtationData_sub_m.put("pkg_name", "P_FILTER_COD");
        pGetSubtationData_sub_m.put("func_name", "get_Capacity_data_m");
        pGetSubtationData_sub_m.put("p_type", 19);
        pGetSubtationData_sub_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetSubtationData_sub_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetSubtationData_sub_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetSubtationData_sub_m.put("p_year", Integer.valueOf(cmbDate));
        pGetSubtationData_sub_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetSubtationData_sub_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetSubtationData_sub_m);

        //Chart
        Map<String, String> pGetSubtation_sub = new HashMap<>();
        pGetSubtation_sub.put("pkg_name", "P_FILTER_COD");
        pGetSubtation_sub.put("func_name", "get_CapacityChart_m");
        pGetSubtation_sub.put("p_type", String.valueOf(19));
        String strGetSubtation_sub = plsqlService.findString(pGetSubtation_sub);
        retValue.put("strGetSubtation_sub", strGetSubtation_sub);


        return retValue;
    };


    @RequestMapping(value = "/tahunan",method = RequestMethod.GET)
    public String tahunan(ModelMap modelMap){
        //example for retreiving data from database
        Map<String, String> pGetAllReqion = new HashMap<>();
        pGetAllReqion.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllReqion.put("func_name", "get_all_region");
        pGetAllReqion.put("p_tmp", null);
        List<Map<String, Object>> listComboRegion = plsqlService.findAll(pGetAllReqion);
        modelMap.put("listComboRegion",listComboRegion);

        Map<String, String> pGetAllProgram = new HashMap<>();
        pGetAllProgram.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllProgram.put("func_name", "get_all_assetgroup");
        pGetAllProgram.put("p_tmp", null);
        List<Map<String, Object>> listComboProgram = plsqlService.findAll(pGetAllProgram);
        modelMap.put("listComboProgram",listComboProgram);

        Map<String, String> pGetAllType = new HashMap<>();
        pGetAllType.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllType.put("func_name", "get_all_type");
        pGetAllType.put("p_tmp", null);
        List<Map<String, Object>> listComboType = plsqlService.findAll(pGetAllType);
        modelMap.put("listComboType",listComboType);

        Map<String, String> pGetAllOwnership = new HashMap<>();
        pGetAllOwnership.put("pkg_name", "P_FILTER_MILESTONE");
        pGetAllOwnership.put("func_name", "get_all_ownership");
        pGetAllOwnership.put("p_tmp", null);
        List<Map<String, Object>> listComboOwnership = plsqlService.findAll(pGetAllOwnership);
        modelMap.put("listComboOwnership",listComboOwnership);

        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.dashboard.cod.tahunan"; //xml
    }

    @RequestMapping(value = "/tahunan/getdata-cod-tahunan",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> getdata_charttahunan(ModelMap modelMap, HttpServletRequest request) {
        Map<String, Object> retValue = new HashMap<>();

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");

        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");

        /*
        CHART GENERATION
        ----------------
         */
        //Data generation pembangkit
        Map<String, Object> pGetCapacityData_gen_m = new HashMap<>();
        pGetCapacityData_gen_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_gen_m.put("func_name", "gen_Capacity_data_thn");//gen_Capacity_data  //gen_Capacity_data_thn
        pGetCapacityData_gen_m.put("p_type", 17);
        pGetCapacityData_gen_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_gen_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_gen_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_gen_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetCapacityData_gen_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetCapacityData_gen_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_gen_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_gen_m);

        //Chart Generation pembangkit
        Map<String, String> pGetGeneration_gen = new HashMap<>();
        pGetGeneration_gen.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_gen.put("func_name", "get_CapacityChart_thn");//get_CapacityChart_thn  //get_CapacityChart
        pGetGeneration_gen.put("p_type", String.valueOf(17));
        String strGetGeneration_gen = plsqlService.findString(pGetGeneration_gen);
        retValue.put("strGetGeneration_gen", strGetGeneration_gen);

        /*
        CHART TRANSMITION
        ----------------
         */
        //Data generation transmition
        Map<String, Object> pGetCapacityData_trans_m = new HashMap<>();
        pGetCapacityData_trans_m.put("pkg_name", "P_FILTER_COD");
        pGetCapacityData_trans_m.put("func_name", "gen_Capacity_data_thn");
        pGetCapacityData_trans_m.put("p_type", 18);
        pGetCapacityData_trans_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetCapacityData_trans_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetCapacityData_trans_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetCapacityData_trans_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetCapacityData_trans_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetCapacityData_trans_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetCapacityData_trans_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetCapacityData_trans_m);


        //Chart Generation transmition
        Map<String, String> pGetGeneration_trans = new HashMap<>();
        pGetGeneration_trans.put("pkg_name", "P_FILTER_COD");
        pGetGeneration_trans.put("func_name", "get_CapacityChart_thn");
        pGetGeneration_trans.put("p_type", String.valueOf(18));
        String strGetGeneration_tran = plsqlService.findString(pGetGeneration_trans);
        retValue.put("strGetGeneration_tran", strGetGeneration_tran);


        /*
        CHART SUBTATION
        ----------------
         */
        //Data
        Map<String, Object> pGetSubtationData_sub_m = new HashMap<>();
        pGetSubtationData_sub_m.put("pkg_name", "P_FILTER_COD");
        pGetSubtationData_sub_m.put("func_name", "gen_Capacity_data_thn");
        pGetSubtationData_sub_m.put("p_type", 19);
        pGetSubtationData_sub_m.put("p_Ownership", Integer.valueOf(cmbOwnership));
        pGetSubtationData_sub_m.put("p_Region", Integer.valueOf(cmbRegion));
        pGetSubtationData_sub_m.put("p_Group", Integer.valueOf(cmbProgram)); //Program
        pGetSubtationData_sub_m.put("p_strat_year", Integer.valueOf(cmbThnAwal));
        pGetSubtationData_sub_m.put("p_end_year", Integer.valueOf(cmbThnAkhir));
        pGetSubtationData_sub_m.put("p_cod_plan", Integer.valueOf(cmbPlan));
        pGetSubtationData_sub_m.put("p_cod_actual", Integer.valueOf(cmbActual));

        plsqlService.save(pGetSubtationData_sub_m);

        //Chart
        Map<String, String> pGetSubtation_sub = new HashMap<>();
        pGetSubtation_sub.put("pkg_name", "P_FILTER_COD");
        pGetSubtation_sub.put("func_name", "get_CapacityChart_thn");
        pGetSubtation_sub.put("p_type", String.valueOf(19));
        String strGetSubtation_sub = plsqlService.findString(pGetSubtation_sub);
        retValue.put("strGetSubtation_sub", strGetSubtation_sub);


        return retValue;
    };


    @RequestMapping(value = "/tahunan/getdata-asset-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getdataAssetCapacityTahunan(HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbThnAwal = request.getParameter("cmbThnAwal");
        String cmbThnAkhir = request.getParameter("cmbThnAkhir");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");
        String categoryLabel = request.getParameter("categoryLabel");
        String datasetName = request.getParameter("datasetName");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_pmo_detail_cod_thn");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", "17");
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram); //Progam
        pGetAssetCapacity.put("p_strat_year", cmbThnAwal);
        pGetAssetCapacity.put("p_end_year", cmbThnAkhir);
        pGetAssetCapacity.put("p_cod_plan", cmbPlan);
        pGetAssetCapacity.put("p_cod_actual", cmbActual);
        pGetAssetCapacity.put("p_category_label", categoryLabel);
        pGetAssetCapacity.put("p_datasetName", datasetName);


        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }


    @RequestMapping(value = "/bulanan/getdata-asset-capacity",method = RequestMethod.POST)
    @ResponseBody
    public ModelMap getdataAssetCapacityBulanan(HttpServletRequest request){

        String cmbRegion = request.getParameter("cmbRegion");
        String cmbProgram = request.getParameter("cmbProgram");
        String cmbOwnership = request.getParameter("cmbOwnership");
        String cmbDate = request.getParameter("cmbDate");
        String cmbPlan = request.getParameter("cmbPlan");
        String cmbActual = request.getParameter("cmbActual");
        String type = request.getParameter("type");
        String categoryLabel = request.getParameter("categoryLabel");
        String datasetName = request.getParameter("datasetName");

        ModelMap modelMap = new ModelMap();
        Map<String, String> pGetAssetCapacity = new HashMap<>();
        pGetAssetCapacity.put("pkg_name", "P_FILTER_COD");
        pGetAssetCapacity.put("func_name", "get_pmo_detail_cod_bln");
        //TODO : tambah param type.
        pGetAssetCapacity.put("p_type", type);
        pGetAssetCapacity.put("p_Ownership", cmbOwnership);
        pGetAssetCapacity.put("p_Region", cmbRegion);
        pGetAssetCapacity.put("p_Group", cmbProgram);
        pGetAssetCapacity.put("p_year", cmbDate);
        pGetAssetCapacity.put("p_cod_plan", cmbPlan);
        pGetAssetCapacity.put("p_cod_actual", cmbActual);
        pGetAssetCapacity.put("p_catp_yearegoryLabel", categoryLabel);
        pGetAssetCapacity.put("p_datasetNama", datasetName);


        List<Map<String, Object>> listAssetProject = plsqlService.findAll(pGetAssetCapacity);
        modelMap.put("data",listAssetProject);

        return modelMap;
    }

}
