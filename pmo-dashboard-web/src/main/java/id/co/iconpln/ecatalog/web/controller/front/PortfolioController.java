package id.co.iconpln.ecatalog.web.controller.front;

import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/portfolio")
public class PortfolioController {

    @RequestMapping(method = RequestMethod.GET)
    public String portfolio(ModelMap modelMap){
        modelMap.put("authUser", UserUtil.getAuthUser());
        return "app.front.portfolio";
    }
    @RequestMapping(value="/test", method = RequestMethod.GET)
    public String profile(ModelMap modelMap){
        return "app.front.profile-user";
    }

}
