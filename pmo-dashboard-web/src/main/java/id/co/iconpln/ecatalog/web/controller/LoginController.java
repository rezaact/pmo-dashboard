package id.co.iconpln.ecatalog.web.controller;


import eu.bitwalker.useragentutils.UserAgent;
import id.co.iconpln.ecatalog.db.domain.AuthUser;
import id.co.iconpln.ecatalog.db.service.PlsqlService;
import id.co.iconpln.ecatalog.web.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by rich on 5/17/2015.
 */
@Controller
@RequestMapping("/login")

public class LoginController {
    @Autowired
    PlsqlService plsqlService;
    // Menampilkan halaman update
    @RequestMapping(value = "/success")
    public String loginSuccess(ModelMap modelMap, HttpServletRequest req){
        // Isi kode form untuk mengedit data lama
        AuthUser authUser = UserUtil.getAuthUser();
//        modelMap.put("check",authUser);
        System.out.println(authUser.getRole().getAuthority());
        if(authUser.getRole().getAuthority().equals("ROLE_ADMINISTRATOR") ||
                authUser.getRole().getAuthority().equals("ROLE_SECRET")){
            //login log
            UserAgent userAgent = UserAgent.parseUserAgentString(req.getHeader("User-Agent"));
            Map<String, Object> userLog = new HashMap<>();
            userLog.put("pkg_name", "P_LOG_USER");
            userLog.put("func_name", "user_login");

            userLog.put("p_user_id",  authUser.getId());
            userLog.put("p_browser_name", userAgent.getBrowser().getName());
            try{
                userLog.put("p_browser_version", userAgent.getBrowser().getVersion(req.getHeader("User-Agent")).getVersion());
            }
            catch (Exception err){
                userLog.put("p_browser_version", "ios webview");
            }

            userLog.put("p_ip_host", req.getLocalAddr());
            userLog.put("p_ip_server", req.getRemoteAddr());
            userLog.put("p_os_name", userAgent.getOperatingSystem().getName());
            userLog.put("p_session_id", req.getSession().getId());

            if (plsqlService.save(userLog).equals("1")){
                return "redirect:/dashboard-ruptl-capacity";
            }

        }

        return "redirect:/account/login";

    }

}
