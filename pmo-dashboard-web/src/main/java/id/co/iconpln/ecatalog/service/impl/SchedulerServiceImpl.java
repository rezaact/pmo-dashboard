package id.co.iconpln.ecatalog.service.impl;

import id.iconpln.ecatalog.service.SchedulerService;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Service;

import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by rezafit on 02/12/2016.
 */
@Service
public class SchedulerServiceImpl implements SchedulerService{

    private Scheduler sched;

    @Override
    public void start() throws Exception {
        SchedulerFactory sf = new StdSchedulerFactory();
        sched = sf.getScheduler();

        /*JobDetail job = JobBuilder.newJob(HelloJob.class)
                .withIdentity("dummyJobName", "group1").build();*/

        JobDetail job = JobBuilder.newJob(SynchronizedDataJob.class)
                .withIdentity("SynchronizedDataJob", "group1").build();

        CronTrigger trigger = newTrigger()
                .withIdentity("trigger1", "group1")
                .withSchedule(CronScheduleBuilder.cronSchedule("0/10 * * * * ?"))
                .build();

        sched.scheduleJob(job, trigger);
        sched.start();

        System.out.println("scheuller SynchronizedDataJob start");
    }

    @Override
    public void stop() throws Exception {
        sched.shutdown(true);
        System.out.println("scheuller SynchronizedDataJob stop");
    }
}
