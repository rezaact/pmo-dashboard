<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            NEWS
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="sidebar-products">
            <div class="scroller" style="height: 250px">
            <c:forEach var="urlInfo" items="${listUrlInfo}" varStatus="number">
                <div class="item">
                    <a href="${urlInfo.URL_INFO}" target="_blank">
                        <img src="<c:url value="/file/${urlInfo.ID_FILE}"/>" alt="hot_info">
                    </a>
                    <h3><a href="${urlInfo.URL_INFO}" target="_blank">${urlInfo.KETERANGAN}</a></h3>
                    <%--<div class="price">Rp. ${produkBestSeller.HARGA_PRODUK}</div>--%>
                </div>
            </c:forEach>
            </div>
        </div>
    </div>
</div>