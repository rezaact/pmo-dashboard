<div class="row margin-bottom-40 front-steps-wrapper front-steps-count-3">
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step1">
            <h2>Tahapan 1</h2>
            <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step2">
            <h2>Tahapan 2</h2>
            <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
    <div class="col-md-4 col-sm-4 front-step-col">
        <div class="front-step front-step3">
            <h2>Tahapan 3</h2>
            <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
        </div>
    </div>
</div>