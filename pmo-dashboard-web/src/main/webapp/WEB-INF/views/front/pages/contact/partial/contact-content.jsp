<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="col-md-12">
    <%--<img alt="Berry Lace Dress" src="<c:url value="/product/get_image?id_file=${idImage.ID_FILE}"/>">--%>

  <div class="content-page">
      <div class="col-md-9 col-sm-9">
          <h1 style="margin-top: 10px">Hubungi Kami</h1>
          <c:if test="${idImage.NOMOR_PRODUK == null }">
            <p>Hubungi kami secara online dengan mengisi form dibawah ini:</p>
          </c:if>
          <c:if test="${idImage.NOMOR_PRODUK != null }">
              <p>Hubungi kami secara online untuk informasi produk dibawah ini:</p>
              <div class="row product-item">
                  <div class="col-md-4 col-sm-4">
                          <%--<img class="img-responsive" alt="" src="<c:url value="/product/get_image/${produk.ID_FILE}"/>">--%>
                          <%--<img src="http://placehold.it/300x200" class="img-responsive" alt=""/>--%>
                      <img src="<c:url value="/product/get_image?id_file=${idImage.ID_FILE}"/>" class="img-responsive" alt=""/>
                  </div>
                  <div class="col-md-5 col-sm-5">
                      <h2><a href="<c:url value="/product/product-detail?id=${idImage.NOMOR_PRODUK}"/>">${idImage.NAMA_PRODUK}</a></h2>
                      <p>${idImage.DESKRIPSI_PRODUK}</p>
                      <dl class="dl-horizontal product-meta">
                          <dt>Tahun Produk</dt>
                          <dd>:&nbsp;&nbsp;${idImage.TAHUN_PRODUK}</dd>
                          <dt>Dimensi Produk</dt>
                          <dd>:&nbsp;&nbsp;${idImage.DIMENSI}</dd>
                      </dl>
                  </div>
                  <div class="col-md-3 col-sm-3">

                          <%--<h3><b style="font-size: 20px">Rp.--%>
                          <%--<fmt:formatNumber type="number" maxIntegerDigits="3" value="${produk.HARGA_PRODUK}" />,-</b></h3>--%>
                  <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                      <span class="text-capitalize">estimated</span>
                      <h3><b style="font-size: 20px">Rp.${idImage.HARGA_PRODUK}</b></h3>
                  </c:if>
                  </div>
              </div>
          </c:if>
          <div class="col-md-12 col-sm-12">
          <!-- BEGIN FORM-->
          <form action="<c:if test="${authUser.getClass().simpleName == 'AuthUser'}"><c:url value="/contact/create"/></c:if><c:if test="${authUser.getClass().simpleName != 'AuthUser'}"><c:url value="/contact/create/non"/></c:if>" class="form-horizontal form-bordered  contact-form"  method="post">
              <input type="hidden" class="form-control" name="prm_nomor_produk" value="${idImage.NOMOR_PRODUK}">
              <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
              <div class="form-group" style="display:none">
                  <label class="control-label">Nama</label>
                  <div class="input-icon right">
                      <i class="fa"></i>
                            <input type="text" class="form-control" name="prm_nama" value="${authUser.getNamaLengkap()}">
                  </div>
              </div>
              </c:if>
              <c:if test="${authUser.getClass().simpleName != 'AuthUser'}">
              <div class="form-group">
                  <label class="control-label">Nama</label>
                  <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="text" class="form-control" name="prm_nama">
                  </div>
              </div>
              </c:if>
              <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
              <div class="form-group" style="display:none">
                  <label class="control-label ">Email</label>
                  <div class="input-icon right">
                      <i class="fa"></i>
                            <input type="email" class="form-control" name="prm_email" value="${authUser.getEmail()}" >
                  </div>
              </div>
              </c:if>
              <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                  <div class="form-group" style="display:none">
                      <label class="control-label ">Nomor Telp</label>
                      <div class="input-icon right">
                          <i class="fa"></i>
                          <input type="text" class="form-control" name="prm_telp" value="${authUser.getNomorTelp()}" >
                      </div>
                  </div>
              </c:if>
               <c:if test="${authUser.getClass().simpleName != 'AuthUser'}">
                  <div class="form-group" >
                      <label class="control-label ">Email</label>
                      <div class="input-icon right">
                          <i class="fa"></i>
                          <input type="email" class="form-control" name="prm_email" value="${authUser.getEmail()}" >
                        </div>
                  </div>
               </c:if>
              <c:if test="${authUser.getClass().simpleName != 'AuthUser'}">
                  <div class="form-group" >
                      <label class="control-label ">Nomor Telp</label>
                      <div class="input-icon right">
                          <i class="fa"></i>
                          <input type="text" class="form-control" name="prm_telp" value="${authUser.getNomorTelp()}" >
                      </div>
                  </div>
              </c:if>
              <div class="form-group">
                  <label class="control-label ">Pesan</label>
                  <div class="input-icon right">
                      <i class="fa"></i>
                      <textarea class="wysihtml5 form-control" rows="6" id="prm_pesan" name="prm_pesan" ></textarea>
                  </div>
              </div>
              <button type="submit" class="btn green">Kirim</button>
              <a href="<c:url value="/contact"/>" class="btn red" >Batal</a>
          </form>
          <!-- END FORM-->
          </div>
      </div>
  <div class="row">
      <div class="col-md-3 col-sm-3 sidebar2">
          <h2>Kontak Kami</h2>
          <address>
              <strong>PT PLN (Persero) <br/>
              Pusat Pemeliharaan Ketenagalistrikan</strong><br>
              Jl. Banten 10 Bandung <br>
              <%--<i class="fa fa-phone"></i>  (022) 7236791 <br/>
              <i class="fa fa-fax"></i> (022) 7236794 <br/>--%>
              <i class="fa fa-envelope"></i> <a href="mailto:pusharlis@pln.co.id">pusharlis@pln.co.id</a>
          </address>
      </div>
  </div>
  </div>
</div>
<!-- END CONTENT -->