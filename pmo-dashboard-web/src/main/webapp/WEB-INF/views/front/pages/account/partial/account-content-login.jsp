<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->
<div class="col-md-5 col-sm-5">
  <h1>Login</h1>
  <div class="content-form-page">
    <div class="row">
      <div class="">
        <form class="form-horizontal form-without-legend" action="<c:url value='/account/login_process'/>" method="post" role="form">
            <c:if test="${not empty param.get('error')}">
                <div class="alert alert-danger alert-noexist">
                    <button class="close" data-close="alert"></button>
                    <span>Proses login gagal</span>
                </div>
            </c:if>
          <div class="form-group">
            <label for="email" class="col-lg-4 control-label">Username <span class="require">*</span></label>
            <div class="col-lg-8">
              <input type="text" class="form-control" id="email" name="username">
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-lg-4 control-label">Password <span class="require">*</span></label>
            <div class="col-lg-8">
              <input type="password" class="form-control" id="password" name="password">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0">
              <%--<a href="page-forgotton-password.html">Lupa password?</a>--%>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
              <button type="submit" class="btn btn-primary">Login</button>
            </div>
          </div>
        </form>
      </div>
      <%--<div class="col-md-4 col-sm-4 pull-right">--%>
        <%--<div class="form-info">--%>
          <%--<h2>Informasi<em> Penting</em> </h2>--%>
          <%--<p>Jika Anda belum mempunyai akun silahkan klik di bawah ini</p>--%>
          <%--<a href="<c:url value="/account/register"/> " class="btn btn-default">Registrasi</a>--%>
        <%--</div>--%>
      <%--</div>--%>
    </div>
  </div>
</div>
<!-- END CONTENT -->

