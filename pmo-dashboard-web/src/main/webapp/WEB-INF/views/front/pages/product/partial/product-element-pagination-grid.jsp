<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGINATOR -->
<div class="row">
    <div class="col-md-4 col-sm-4 items-info">Items ${listProdukList.firstUrutan} to ${listProdukList.lastUrutan} of ${listProdukList.recordsTotal} total</div>
    <div class="col-md-8 col-sm-8">
        <ul class="pagination pull-right">
            <c:if test="${searchUtils.totalPage >1}">
            <c:if test="${searchUtils.currentPage-kiri>1}">
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=1'/>')"><a href="#">&laquo;</a></li>
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.currentPage-1}'/>')"><a href="#"><</a></li>
            </c:if>
            <c:forEach var="i" begin="${searchUtils.currentPage-kiri}" step="1" end="${searchUtils.currentPage-1}">
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${i}'/>')"><a href="#">${i}</a></li>
            </c:forEach><%----%>
            <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.currentPage}'/>')"><span><a href="#">${searchUtils.currentPage}</a></span></li>
            <c:forEach var="i" begin="${searchUtils.currentPage+1}" step="1" end="${searchUtils.currentPage+kanan}">
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${i}'/>')"><a href="#">${i}</a></li>
            </c:forEach>

            <c:if test="${searchUtils.currentPage+kanan<searchUtils.totalPage}">
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.currentPage+1}'/>')"><a href="#">></a></li>
                <li onclick="paginationClick('<c:url value='/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.totalPage}'/>')"><a href="#">&raquo;</a></li>
            </c:if>
            </c:if>
            <%--<c:if test="${searchUtils.totalPage >1}">
            <li><a href="<c:url value='/product/product-grid?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=1'/>">&laquo;</a></li>
            <c:forEach var="page" begin="1" end="${searchUtils.totalPage}">
                <c:choose>
                    <c:when test="${page eq searchUtils.currentPage}">
                        <li><span><a href="<c:url value='/product/product-grid?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}'/>">${page}</a></span></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<c:url value="/product/product-grid?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}"/>"> ${page}</a></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <li><a href="<c:url value='/product/product-grid?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.totalPage}'/>">&raquo;</a></li>
            </c:if>--%>
            <%--<li><a href="#">&laquo;</a></li>
            <li><span><a href="#">1</a></span></li>
            <li><a href=""> 2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>--%>
        </ul>
    </div>
</div>
<!-- END PAGINATOR -->