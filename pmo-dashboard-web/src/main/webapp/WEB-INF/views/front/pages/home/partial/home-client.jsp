<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row margin-bottom-40 our-clients">
    <div class="col-md-3">
        <h2><a href="#">Klien kami</a></h2>
        <p>Lorem dipsum folor margade sitede lametep eiusmod psumquis dolore.</p>
    </div>
    <div class="col-md-9">
        <div class="owl-carousel owl-carousel6-brands">
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/1_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/1.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/2_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/2.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/3_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/3.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/4_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/4.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/client_5_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/client_5.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/6_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/6.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/7_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/7.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
            <div class="client-item">
                <a href="#">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/client_8_gray.png"/>" class="img-responsive" alt="">
                    <img src="<c:url value="/assets/frontend/pages/img/clients/client_8.png"/>" class="color-img img-responsive" alt="">
                </a>
            </div>
        </div>
    </div>
</div>