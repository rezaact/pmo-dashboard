<div class="row service-box margin-bottom-40">
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-building purple"></i></em>
            <span>Workshop dan Pemeliharaan I</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan I berkedudukan di Merak – Banten. UWP I  Merak dipimpin oleh seorang Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-cubes yellow"></i></em>
            <span>Workshop dan Pemeliharaan II</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan II berkedudukan di Klender – Jakarta. UWP II  Jakarta dipimpin oleh seorang Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-database grey"></i></em>
            <span>Workshop dan Pemeliharaan III</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan III berkedudukan di  Bandung. UWP III Bandung dipimpin oleh seorang Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-location-arrow blue"></i></em>
            <span>Workshop dan Pemeliharaan IV</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan IV berkedudukan di Dayeuhkolot, Kabupaten Bandung. UWP IV Dayeuhkolot dipimpin oleh seorang Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-check red"></i></em>
            <span>Workshop dan Pemeliharaan V</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan V berkedudukan di  Semarang. UWP V Semarang dipimpin oleh seorang Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
    <div class="col-md-4 col-sm-4">
        <div class="service-box-heading">
            <em><i class="fa fa-compress green"></i></em>
            <span>Workshop dan Pemeliharaan VI</span>
        </div>
        <p>Unit Workshop dan Pemeliharaan VI berkedudukan di Kota Surabaya. UWP  VI Surabaya dipimpin oleh Manajer Unit dan dibantu oleh Asisten Manajer Teknik dan Asisten Manajer Administrasi.</p>
    </div>
</div>