<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach var="komentar" items="${detailKomentar.data}">
    <div class="row-review-item">
        <div class="review-item clearfix">
            <div class="review-avatar">
                <img class="img-circle" src="<c:url value="/assets/frontend/pages/img/comment/avatar.png"/>" alt="..."/>
            </div>
            <div class="review-item-submitted">
                <strong class="text-capitalize">${komentar.CREATED_BY}</strong>
                <em>
                    <fmt:formatDate value="${komentar.CREATED_DATE}" pattern="dd MMMM yyyy HH:mm:ss" />
                        <a href="#collapse-${komentar.ID_KOMENTAR}" aria-controls="collapse-${komentar.ID_KOMENTAR}" style="margin-left: 10px" data-toggle="collapse" aria-expanded="false">Reply</a>
                </em>
                <div class="rateit" data-rateit-value="${komentar.RATE}" data-rateit-ispreset="true" data-rateit-readonly="true" id="rate-${komentar.ID_KOMENTAR}"></div>
            </div>
            <div class="review-item-content">
                <p>${komentar.KOMENTAR}</p>
            </div>
        </div>
    <c:forEach var="child" items="${commentChild}">
        <c:if test="${child.KOMENTAR_PARENT == komentar.ID_KOMENTAR}">
            <div class="row-review-item col-md-offset-1 well margin-bottom-10">
                <div class="review-item clearfix">
                    <div class="review-avatar">
                        <img class="img-circle" src="<c:url value="/assets/frontend/pages/img/comment/avatar.png"/>" alt="..."/>
                    </div>
                    <div class="review-item-submitted">
                        <strong class="text-capitalize">${child.CREATED_BY}</strong>
                        <em>
                            <fmt:formatDate value="${child.CREATED_DATE}" pattern="dd MMMM yyyy HH:mm:ss" />
                        </em>
                        <div class="rateit" data-rateit-value="${child.RATE}" data-rateit-ispreset="true" data-rateit-readonly="true" id="rate-${child.ID_KOMENTAR}"></div>

                    </div>
                    <div class="review-item-content">
                        <p>${child.KOMENTAR}</p>
                    </div>
                </div>
            </div>
        </c:if>
    </c:forEach>
    </div>


    <div class="collapse" id="collapse-${komentar.ID_KOMENTAR}">
        <div class="well">
            <form action="#" class="reviews-form comment-reply" role="form">
                <input type="hidden" name="PRM_NOMOR_PRODUK" VALUE="${komentar.NOMOR_PRODUK}" id="no-prd"/>
                <input type="hidden" name="PRM_PARENT" value="${komentar.ID_KOMENTAR}"/>
                <h2>Tulis sebuah komentar</h2>
                <div class="form-group" <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                    style="display:none";
                </c:if> >
                    <label for="name_com">Nama <span class="require">*</span></label>
                    <input type="text" class="form-control" name="PRM_NAME" id="name_com" value="${authUser.getNamaLengkap()}">
                </div>
                <div class="form-group" <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                    style="display:none";
                </c:if> >
                    <label for="email_com">Email</label>
                    <input type="text" class="form-control" name="PRM_EMAIL" id="email_com" value="${authUser.getEmail()}">
                </div>
                <div class="form-group">
                    <label for="review">Komentar <span class="require">*</span></label>
                    <textarea class="form-control" name="PRM_KOMENTAR" rows="8"></textarea>
                </div>
    <div class="form-group">
        <label for="main-rating">Rating</label>
        <input type="range" value="4" step="0.25" id="main-rating-${komentar.ID_KOMENTAR}">
        <div class="rateit" id="rate_com${komentar.ID_KOMENTAR}" data-rateit-backingfld="#main-rating-${komentar.ID_KOMENTAR}" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
        </div>
    </div>
    <div class="padding-top-20">
        <button type="button" class="btn btn-primary btn-action-reply">Kirim</button>
    </div>
    </form>
    </div>
    </div>
</c:forEach>
<div class="row">
    <div class="col-md-8 col-sm-8">
        <ul class="pagination pull-right">
            <c:if test="${searchUtils.totalPage >1}">
                <c:if test="${searchUtils.currentPage-kiri>1}">
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=1'/>')"><a href="javascript:void(0);">&laquo;</a></li>
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${searchUtils.currentPage-1}'/>')"><a href="javascript:void(0);"><</a></li>
                </c:if>
                <c:forEach var="i" begin="${searchUtils.currentPage-kiri}" step="1" end="${searchUtils.currentPage-1}">
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${i}'/>')"><a href="javascript:void(0);">${i}</a></li>
                </c:forEach>
                <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${searchUtils.currentPage}'/>')"><span><a href="javascript:void(0);">${searchUtils.currentPage}</a></span></li>
                <c:forEach var="i" begin="${searchUtils.currentPage+1}" step="1" end="${searchUtils.currentPage+kanan}">
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${i}'/>')"><a href="javascript:void(0);">${i}</a></li>
                </c:forEach>

                <c:if test="${searchUtils.currentPage+kanan<searchUtils.totalPage}">
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${searchUtils.currentPage+1}'/>')"><a href="javascript:void(0);">></a></li>
                    <li onclick="paginationDetailClick('<c:url value='/product/product-detail-comment?search=${searchUtils.searchText}&page=${searchUtils.totalPage}'/>')"><a href="javascript:void(0);">&raquo;</a></li>
                </c:if>
            </c:if>

        </ul>
    </div>
</div>