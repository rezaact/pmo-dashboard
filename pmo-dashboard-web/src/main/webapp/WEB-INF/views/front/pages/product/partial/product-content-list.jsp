<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%--<form action="<c:url value="/product/product-list"/>" method="get">--%>
<input type="hidden" id="linkUrl" value="<c:url value="/product/product-list-data"/>"/>
<!-- BEGIN CONTENT -->
<div class="col-md-9 col-sm-7">
    <tiles:insertAttribute name="product-element-search"/>
    <tiles:insertAttribute name="product-element-breadcrumb"/>
    <!-- BEGIN PRODUCT LIST -->
    <div id="product-container">
        <c:forEach var="produk" items="${listProdukList.data}" varStatus="number">
            <div class="row product-item">

                <div class="col-md-4 col-sm-4">
                        <%--<img class="img-responsive" alt="" src="<c:url value="/product/get_image/${produk.ID_FILE}"/>">--%>
                        <%--<img src="http://placehold.it/300x200" class="img-responsive" alt=""/>--%>
                    <c:choose>
                        <c:when test="${not empty produk.ID_FILE}">
                            <a href="<c:url value="/product/product-detail?id=${produk.NOMOR_PRODUK}"/>"><img src="<c:url value="/file/${produk.ID_FILE}"/>" class="img-responsive" alt=""/></a>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="/product/product-detail?id=${produk.NOMOR_PRODUK}"/>"><img src="<c:url value="/assets/global/img/default.png"/>" class="img-responsive" alt=""/></a>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="col-md-5 col-sm-5">
                    <h2><a href="<c:url value="/product/product-detail?id=${produk.NOMOR_PRODUK}"/>">${produk.NAMA_PRODUK}</a></h2>
                    <p>
                        <c:set var="shortDesc" value="${fn:substring(produk.DESKRIPSI_PRODUK, 0, 300)}" />
                            ${shortDesc}
                    </p>
                    <dl class="dl-horizontal product-meta">
                        <dt>Tahun Produk</dt>
                        <dd>:&nbsp;&nbsp;${produk.TAHUN_PRODUK}</dd>
                        <dt>Dimensi Produk</dt>
                        <dd>:&nbsp;&nbsp;${produk.DIMENSI}</dd>
                    </dl>
                </div>
                <div class="col-md-3 col-sm-3 text-right <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">margin-top-20</c:if>">
                        <%--<h3><b style="font-size: 20px">Rp.--%>
                        <%--<fmt:formatNumber type="number" maxIntegerDigits="3" value="${produk.HARGA_PRODUK}" />,-</b></h3>--%>
                    <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                        <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE'}">
                            <c:if test="${not empty produk.HARGA_PRODUK}">
                                <span class="text-capitalize">${produk.JNS_HARGA_PRODUK}</span>
                                <h3><b style="font-size: 20px">
                                    <c:choose>
                                        <c:when test="${not empty produk.HARGA_PRODUK}">
                                            Rp.${produk.HARGA_PRODUK}
                                        </c:when>
                                        <c:otherwise>
                                            -
                                        </c:otherwise>
                                    </c:choose>
                                </b></h3>
                            </c:if>
                        </c:if>
                    </c:if>
                    <a href="<c:url value="/contact?id=${produk.NOMOR_PRODUK}"/>" class="btn btn-primary pull-right">Hubungi Kami</a>
                </div>
            </div>
        </c:forEach>
        <c:if test="${listProdukList.recordsTotal eq '0'}">
            <div class="product-not-found">
                <i class="fa fa-times-circle-o"></i>
                Produk tidak ditemukan
            </div>
        </c:if>
    </div>
    <tiles:insertAttribute name="product-element-pagination"/>
</div>
<!-- END CONTENT -->
<br/>

<c:if test="${showSplashScreen eq 'true'}">
    <tiles:insertAttribute name="product-element-popup"/>
</c:if>



<%--</form>--%>
