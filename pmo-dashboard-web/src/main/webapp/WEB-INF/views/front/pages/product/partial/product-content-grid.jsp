<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<%--<form action="<c:url value="/product/product-grid"/>" method="get">--%>
<!-- BEGIN CONTENT -->
<div class="col-md-9 col-sm-7">
    <tiles:insertAttribute name="product-element-search"/>
    <tiles:insertAttribute name="product-element-breadcrumb"/>
  <!-- BEGIN PRODUCT LIST -->
    <input type="hidden" id="linkUrl" value="<c:url value="/product/product-grid-data"/>"/>
  <div class="row product-list" id="product-container">
      <c:forEach var="produk" items="${listProdukList.data}" varStatus="number">
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
            <c:choose>
                <c:when test="${not empty produk.ID_FILE}">
                    <img src="<c:url value="/file/${produk.ID_FILE}"/>" class="img-responsive" alt="...">
                </c:when>
                <c:otherwise>
                    <img src="<c:url value="/assets/global/img/default.png"/>" class="img-responsive" alt="..."/>
                </c:otherwise>
            </c:choose>
          <div>
            <a href="<c:url value="/file/${produk.ID_FILE}"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/product/product-detail?id=${produk.NOMOR_PRODUK}"/>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/file/${produk.ID_FILE}"/>">${produk.NAMA_PRODUK}</a></h3>
          <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
          <div class="pi-price">
            <c:choose>
              <c:when test="${not empty produk.HARGA_PRODUK}">
                Rp.${produk.HARGA_PRODUK}
              </c:when>
              <c:otherwise>
                -
              </c:otherwise>
            </c:choose>
          </div>
          </c:if>
        <a href="<c:url value="/contact?id=${produk.NOMOR_PRODUK}"/>" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
      </c:forEach>
      <c:if test="${listProdukList.recordsTotal eq '0'}">Produk tidak ditemukan</c:if>
   <%--
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>">Produk 2</a></h3>
        <div class="pi-price">Rp. 15.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 3</a></h3>
        <div class="pi-price">Rp. 7.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->--%>
  </div>
  <%--<div class="row product-list">
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 4</a></h3>
        <div class="pi-price">Rp. 3.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 5</a></h3>
        <div class="pi-price">Rp. 2.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 6</a></h3>
        <div class="pi-price">Rp. 17.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
  </div>
  <div class="row product-list">
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 7</a></h3>
        <div class="pi-price">Rp. 34.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 8</a></h3>
        <div class="pi-price">Rp. 78.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
    <!-- PRODUCT ITEM START -->
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="product-item">
        <div class="pi-img-wrapper">
          <img src="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="img-responsive" alt="Berry Lace Dress">
          <div>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/>" class="btn btn-default fancybox-button">Perbesar</a>
            <a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>" class="btn btn-default fancybox-fast-view">Lihat</a>
          </div>
        </div>
        <h3><a href="<c:url value="/assets/frontend/pages/img/spritespin/01.jpg"/><?= base_url('product-detail')?>">Produk 9</a></h3>
        <div class="pi-price">Rp. 35.000.000,-</div>
        <a href="#" class="btn btn-default add2cart">Hubungi Kami</a>
      </div>
    </div>
    <!-- PRODUCT ITEM END -->
  </div>--%>
  <!-- END PRODUCT LIST -->
    <tiles:insertAttribute name="product-element-pagination"/>
</div>

<!-- END CONTENT -->
<%--
</form>--%>
