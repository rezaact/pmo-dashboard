<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Services block BEGIN -->
<div class="services-block content content-center" id="services">
  <div class="container">
    <h2>Layanan <strong>Kami</strong></h2>
    <h4>Pusat Pemeliharaan Ketenagalistrikan</h4>
    <div class="row">
      <c:forEach var="service" items="${listService}">
      <div class="col-md-3 col-sm-3 col-xs-12 item">
        <i class="${service.ICON}"></i>
        <h3>${service.JUDUL}</h3>
        <p>${service.DESKRIPSI}</p>
      </div>
      </c:forEach>
    </div>
  </div>
</div>
<!-- Services block END -->