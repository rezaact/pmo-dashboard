<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="page-slider margin-bottom-40">
<div class="fullwidthbanner-container revolution-slider">
<div class="fullwidthabnner">
<ul id="revolutionul">
<!-- THE NEW SLIDE -->
<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
    <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/bg9.jpg'/>" alt="">

    <div class="caption lft slide_title_white slide_item_left"
         data-x="30"
         data-y="90"
         data-speed="400"
         data-start="1500"
         data-easing="easeOutExpo">
        Pusat Pemeliharaan<br><span class="slide_title_white_bold">PUSHARLIS</span>
    </div>
    <div class="caption lft slide_subtitle_white slide_item_left"
         data-x="0"
         data-y="245"
         data-speed="400"
         data-start="2000"
         data-easing="easeOutExpo">
        Maintenance, Repair dan Overhaul (MRO) ketenagalistrikan
    </div>
    <div class="caption lfb"
         data-x="640"
         data-y="0"
         data-speed="700"
         data-start="1000"
         data-easing="easeOutExpo">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/lady.png'/>" alt="Image 1">
    </div>
</li>

<!-- THE FIRST SLIDE -->
<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
    <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/bg1.jpg'/>" alt="">

    <div class="caption lft slide_title slide_item_left"
         data-x="30"
         data-y="105"
         data-speed="400"
         data-start="1500"
         data-easing="easeOutExpo">
        Be a Profesional?
    </div>
    <div class="caption lft slide_subtitle slide_item_left"
         data-x="30"
         data-y="180"
         data-speed="400"
         data-start="2000"
         data-easing="easeOutExpo">
        This is what you were looking for
    </div>
    <div class="caption lft slide_desc slide_item_left"
         data-x="30"
         data-y="220"
         data-speed="400"
         data-start="2500"
         data-easing="easeOutExpo">
        REKRUTMEN PEGAWAI PT PLN (PERSERO) PUSHARLIS<br>
        UNTUK PENEMPATAN SELURUH INDONESIA.
    </div>
    <a class="caption lft btn green slide_btn slide_item_left" href="#"
       data-x="30"
       data-y="290"
       data-speed="400"
       data-start="3000"
       data-easing="easeOutExpo">
        Apply Now!
    </a>
    <div class="caption lfb"
         data-x="640"
         data-y="55"
         data-speed="700"
         data-start="1000"
         data-easing="easeOutExpo">

        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/man-winner.png'/>" alt="Image 1">
    </div>
</li>

<!-- THE SECOND SLIDE -->
<li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
    <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/bg2.jpg'/>" alt="">
    <div class="caption lfl slide_title slide_item_left"
         data-x="30"
         data-y="125"
         data-speed="400"
         data-start="3500"
         data-easing="easeOutExpo">
        Kebutuhan Listrik
    </div>
    <div class="caption lfl slide_subtitle slide_item_left"
         data-x="30"
         data-y="200"
         data-speed="400"
         data-start="4000"
         data-easing="easeOutExpo">
        Maintenance, Repair dan Overhaul (MRO)
    </div>
    <div class="caption lfl slide_desc slide_item_left"
         data-x="30"
         data-y="245"
         data-speed="400"
         data-start="4500"
         data-easing="easeOutExpo">
        Sesuai Keputusan Direksi No. 067.K/DIR/2011 <br>
        tanggal 25 Februari 2011 organisasi diubah menjadi<br>
        PLN Pusat Pemeliharaan Ketenagalistrikan (PLN Pusharlis)
    </div>
    <div class="caption lfr slide_item_right"
         data-x="635"
         data-y="105"
         data-speed="1200"
         data-start="1500"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/mac.png'/>" alt="Image 1">
    </div>
    <div class="caption lfr slide_item_right"
         data-x="580"
         data-y="245"
         data-speed="1200"
         data-start="2000"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/ipad.png'/>" alt="Image 1">
    </div>
    <div class="caption lfr slide_item_right"
         data-x="735"
         data-y="290"
         data-speed="1200"
         data-start="2500"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/iphone.png'/>" alt="Image 1">
    </div>
    <div class="caption lfr slide_item_right"
         data-x="835"
         data-y="230"
         data-speed="1200"
         data-start="3000"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/macbook.png'/>" alt="Image 1">
    </div>
    <div class="caption lft slide_item_right"
         data-x="865"
         data-y="45"
         data-speed="500"
         data-start="5000"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/hint1-red.png'/>" id="rev-hint1" alt="Image 1">
    </div>
    <div class="caption lfb slide_item_right"
         data-x="355"
         data-y="355"
         data-speed="500"
         data-start="5500"
         data-easing="easeOutBack">
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/hint2-red.png'/>" id="rev-hint2" alt="Image 1">
    </div>
</li>

<!-- THE FORTH SLIDE -->
<li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400" data-thumb="assets/frontend/pages/img/revolutionslider/thumbs/thumb2.jpg">
    <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
    <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/bg4.jpg'/>" alt="">
    <div class="caption lft slide_title"
         data-x="30"
         data-y="105"
         data-speed="400"
         data-start="1500"
         data-easing="easeOutExpo">
        Diakses Dimana saja!
    </div>
    <div class="caption lft slide_subtitle"
         data-x="30"
         data-y="180"
         data-speed="400"
         data-start="2000"
         data-easing="easeOutExpo">
        Mobile Frienly Design
    </div>
    <div class="caption lft slide_desc"
         data-x="30"
         data-y="225"
         data-speed="400"
         data-start="2500"
         data-easing="easeOutExpo">
        Pengalaman yang berbeda akan dirasakan para pengguna smartphone<br>
        AWESOME USER EXPERIENCE
    </div>
    <div class="caption lft start"
         data-x="670"
         data-y="55"
         data-speed="400"
         data-start="2000"
         data-easing="easeOutBack"  >
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/iphone_left.png'/>" alt="Image 2">
    </div>

    <div class="caption lft start"
         data-x="850"
         data-y="55"
         data-speed="400"
         data-start="2400"
         data-easing="easeOutBack"  >
        <img src="<c:url value='/assets/frontend/pages/img/revolutionslider/iphone_right.png'/>" alt="Image 3">
    </div>
</li>
</ul>
<div class="tp-bannertimer tp-bottom"></div>
</div>
</div>
</div>