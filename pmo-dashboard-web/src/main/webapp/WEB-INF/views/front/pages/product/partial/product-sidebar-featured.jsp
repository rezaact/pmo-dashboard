<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            Produk Unggulan
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="sidebar-products">
            <div class="scroller" style="height: 250px">
            <c:forEach var="produkUnggulan" items="${listProdukUnggulan}" varStatus="number">
                <div class="item">
                    <a href="<c:url value="/product/product-detail?id=${produkUnggulan.NOMOR_PRODUK}"/>">
                        <img src="<c:url value="/file/${produkUnggulan.ID_FILE}"/>" alt="Some Shoes in Animal with Cut Out">
                            <%--<img src="<c:url value="/file/${produk.ID_FILE}"/>" class="img-responsive" alt=""/>--%>
                    </a>
                    <h3><a href="<c:url value="/product/product-detail?id=${produkUnggulan.NOMOR_PRODUK}"/>">${produkUnggulan.NAMA_PRODUK}</a></h3>
                    <%--<div class="price">Rp. ${produkUnggulan.HARGA_PRODUK}</div>--%>
                </div>
            </c:forEach>
            </div>
        </div>
    </div>
</div>