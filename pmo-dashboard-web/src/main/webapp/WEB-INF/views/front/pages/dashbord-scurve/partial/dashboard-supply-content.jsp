<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
<div class="col-md-12">


<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">GRAFIK SUPPLY VS DEMAND</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">

                <div class="form-group">
                    <%--<label>${nama_var}</label>--%>
                    <label>Program</label>
                        <select class="CmbProgram form-control OnFilterAll">
                            <option value="0">--Choose Program--</option>
                            <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">
                                <option value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                            </c:forEach>
                        </select>
                </div>

                <div class="form-group">
                    <label>Region</label>
                    <select class="cmbRegion form-control OnFilterAll">
                        <option value="0">--Choose Region--</option>
                        <c:forEach var="dataComboRegion" items="${listComboRegion}" varStatus="number">
                            <option value="${dataComboRegion.ID}">${dataComboRegion.STRINGVALUE}</option>
                        </c:forEach>
                    </select>
                </div>


            </div>

            <div class="col-md-4">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Kapasitas Saat ini (MW)</label>
                            <div class="input-group">
                                <input value="50000" type="text" class="input-group form-control kapasitas OnFilterAll_Click">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Demand Saat ini (MW)</label>
                            <div class="input-group">
                                <input value="40000"  type="text" class="input-group form-control demand OnFilterAll_Click">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Tahun Awal</label>
                        <div class="input-group date date-picker">
                            <input value="2016" type="text" class="starperiod form-control OnFilterAll">
                        </div>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="form-group">
                        <label>Tahun Akhir</label>
                        <div class="input-group date date-picker">
                            <input value="2017" type="text" class="endperiod form-control OnFilterAll">
                        </div>
                    </div>
                    </div>
                </div>


            </div>
            <div class="col-md-5">

                <div class="CmbPenyusutan">

                    <label>Variabel Penyusutan Kapasitas (%) Bulan</label>
                    <input type="text" id="rangePenyusutan" value="" name="range" />
                </div>

                <div class="CmbPertumbuhan">
                    <label>Variabel Pertumbuhan Demand (%) Bulan</label>
                    <input type="text" id="rangePertumbuhan" value="" name="range" />

                </div>
            </div>

        </div>




        <!-- Navigasi -->
        <ul class="nav nav-tabs">
            <li><a data-toggle="tab" href="#Generation">Data Table</a></li>
            <li class="active"><a data-toggle="tab" href="#Transmission">S-Curve</a></li>
        </ul>

        <div class="tab-content">
            <div id="Generation" class="tab-pane fade">
                <p>
                <div class="table-responsive">
                    <table class="table" id="table-supply-demand">
                        <thead>
                        <tr>
                            <th>Year</th>
                            <th>Month</th>
                            <th>Demand</th>
                            <th>Existing</th>
                            <th>Project</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

                </p>
            </div>
            <div id="Transmission" class="tab-pane fade in active">
                <div id="chartScurveSupplay"></div>
            </div>

        </div>

    </div>
</div>






</div>
</div>
<!-- END CONTENT -->
