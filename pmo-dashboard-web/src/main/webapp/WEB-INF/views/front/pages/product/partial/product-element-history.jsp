<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="tab-pane fade" id="tab-historical-product">
    <!-- BEGIN PAGINATOR -->
    <div class="row" style="font-weight: bold;padding: 10px 0;">
        <div class="col-md-4 col-sm-4 items-info">Items <span id="history_page">${historyProduct.page}</span> of <span id="history_total_page">${historyProduct.TOTAL_COUNT}</span> </div>
        <div class="col-md-8 col-sm-8">
            <ul class="pagination pull-right">
                <%--<c:if test="${searchUtils.totalPage >1}">--%>
                <%--<c:if test="${searchUtils.currentPage-kiri>1}">--%>
                <li onclick="paginationClick('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">&laquo;</a></li>
                <li onclick="paginationClickPrev('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)"><</a></li>
                <%--</c:if>--%>

                <%--<c:if test="${searchUtils.currentPage+kanan<searchUtils.totalPage}">--%>
                <li onclick="paginationClickNext('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">></a></li>
                <li onclick="paginationClick('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">&raquo;</a></li>
                <%--</c:if>--%>
                <%--</c:if>--%>
                <%-- <c:if test="${searchUtils.totalPage >1}">
                     <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=1'/>')"><a href="#">&laquo;</a></li>
                     <c:forEach var="page" begin="1" end="${searchUtils.totalPage}">
                         <c:choose>
                             <c:when test="${page eq searchUtils.currentPage}">
                                 <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}'/>')"><span><a href="#">${page}</a></span></li>
                             </c:when>
                             <c:otherwise>
                                 <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}'/>')"><a href="#"> ${page}</a></li>
                             </c:otherwise>
                         </c:choose>
                     </c:forEach>
                     <li  onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.totalPage}'/>')"><a href="#">&raquo;</a></li>
                 </c:if>--%>

            </ul>
        </div>
    </div>
    <!-- END PAGINATOR -->
<table class="table table-bordered">
    <thead>
    <tr>
        <th style="width: 35%">Uraian</th>
        <th>Data</th>
    </tr>
    </thead>
    <tbody>
    <tr class="table-headline">
        <td colspan="2"><strong>Detail penggunaan Produk</strong></td>
    </tr>
    <tr>
        <td>Lokasi Pemasangan</td>
        <td class="NAMA_LOKASI">
            <c:choose>
                <c:when test="${not empty historyProduct.NAMA_LOKASI}">
                    ${historyProduct.NAMA_LOKASI}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Nomor Penugasan</td>
        <td class="NOMOR_PENUGASAN">
            <c:choose>
                <c:when test="${not empty historyProduct.NOMOR_PENUGASAN}">
                    ${historyProduct.NOMOR_PENUGASAN}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Perihal</td>
        <td class="PERIHAL">
            <c:choose>
                <c:when test="${not empty historyProduct.PERIHAL}">
                    ${historyProduct.PERIHAL}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Tanggal Penugasan</td>
        <td class="TANGGAL_PENUGASAN">

            <c:choose>
                <c:when test="${not empty historyProduct.TANGGAL_PENUGASAN}">
                    ${historyProduct.TANGGAL_PENUGASAN}
                    <%--<fmt:formatDate value="${historyProduct.TANGGAL_PENUGASAN}" pattern="dd MMMM yyyy" />--%>
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>

        </td>
    </tr>
    <tr>
        <td>Time Schedule Pelaksanaan Penugasan</td>
        <td class="JADWAL_PELAKSANAAN_PENUGASAN">
            <c:choose>
                <c:when test="${not empty historyProduct.JADWAL_PELAKSANAAN_PENUGASAN}">
                    ${historyProduct.JADWAL_PELAKSANAAN_PENUGASAN} Hari
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Realisasi Waktu Pelaksanaan</td>
        <td class="REALISASI_PELAKSANAAN_PENUGASA">
            <c:choose>
                <c:when test="${not empty historyProduct.REALISASI_PELAKSANAAN_PENUGASA}">
                    ${historyProduct.REALISASI_PELAKSANAAN_PENUGASA} Hari
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Pemberi Kerja</td>
        <td class="PEMBERI_PENUGASAN">
            <c:choose>
                <c:when test="${not empty historyProduct.NAMA_PEMBERI_PENUGASAN}">
                    ${historyProduct.NAMA_PEMBERI_PENUGASAN}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <tr>
        <td>Jumlah Items</td>
        <td class="JUMLAH_ITEM_PEKERJAAN">
            <c:choose>
                <c:when test="${not empty historyProduct.JUMLAH_ITEM_PEKERJAAN}">
                    ${historyProduct.JUMLAH_ITEM_PEKERJAAN}
                </c:when>
                <c:otherwise>
                    -
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
    <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
        <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' }">

            <tr class="table-headline">
                <td colspan="2"><strong>Biaya Penggunaan Produk</strong></td>
            </tr>
            <tr>
                <td>RAB Produksi (Rupiah)</td>
                <td class="RAB_PRODUKSI">
                    <c:choose>
                        <c:when test="${not empty historyProduct.RAB_PRODUKSI}">
                            Rp. ${historyProduct.RAB_PRODUKSI}
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Harga Pokok Produksi</td>
                <td class="HARGA_POKOK_PRODUKSI">
                    <c:choose>
                        <c:when test="${not empty historyProduct.HARGA_POKOK_PRODUKSI}">
                            Rp. ${historyProduct.HARGA_POKOK_PRODUKSI}
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
            <tr>
                <td>Realisasi Biaya Produksi</td>
                <td class="currencyIcon REALISASI_BIAYA_PRODUKSI">
                    <c:choose>
                        <c:when test="${not empty historyProduct.REALISASI_BIAYA_PRODUKSI}">
                            Rp. ${historyProduct.REALISASI_BIAYA_PRODUKSI}
                        </c:when>
                        <c:otherwise>
                            -
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>

            <tr class="table-headline">
                <td colspan="2"><strong>Link Dokumen</strong></td>
            </tr>
            <tr>
                <td>File Penugasan</td>
                <td class="FILE_PENUGASAN">
                    <c:set var="flag" value="0"/>
                    <c:forEach items="${docHistoryNew}" var="file">
                        <c:if test="${file.DOC_TYPE eq '0'}">
                            <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                            <c:set var="flag" value="1"/>
                        </c:if>
                    </c:forEach>
                    <c:if test="${flag eq '0'}">
                        -
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>File RAB Produksi</td>
                <td class="FILE_RAB_PRODUKSI">
                    <c:set var="flag" value="0"/>
                    <c:forEach items="${docHistoryNew}" var="file">
                        <c:if test="${file.DOC_TYPE eq '1'}">
                            <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                            <c:set var="flag" value="1"/>
                        </c:if>
                    </c:forEach>
                    <c:if test="${flag eq '0'}">
                        -
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>File HPP</td>
                <td class="FILE_HPP">
                    <c:set var="flag" value="0"/>
                    <c:forEach items="${docHistoryNew}" var="file">
                        <c:if test="${file.DOC_TYPE eq '2'}">
                            <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                            <c:set var="flag" value="1"/>
                        </c:if>
                    </c:forEach>
                    <c:if test="${flag eq '0'}">
                        -
                    </c:if>
                </td>
            </tr>
            <tr>
                <td>File Realisasi</td>
                <td class="FILE_REALISASI">
                    <c:set var="flag" value="0"/>
                    <c:forEach items="${docHistoryNew}" var="file">
                        <c:if test="${file.DOC_TYPE eq '3'}">
                            <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                            <c:set var="flag" value="1"/>
                        </c:if>
                    </c:forEach>
                    <c:if test="${flag eq '0'}">
                        -
                    </c:if>
                </td>
            </tr>

        </c:if>
    </c:if>
    </tbody>
</table>
    <!-- BEGIN PAGINATOR -->
    <div class="row" style="font-weight: bold;margin-top: -10px;">
        <div class="col-md-4 col-sm-4 items-info">Items <span id="history_page">${historyProduct.page}</span> of <span id="history_total_page">${historyProduct.TOTAL_COUNT}</span> </div>
        <div class="col-md-8 col-sm-8">
            <ul class="pagination pull-right">
                <%--<c:if test="${searchUtils.totalPage >1}">--%>
                <%--<c:if test="${searchUtils.currentPage-kiri>1}">--%>
                <li onclick="paginationClick('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">&laquo;</a></li>
                <li onclick="paginationClickPrev('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)"><</a></li>
                <%--</c:if>--%>

                <%--<c:if test="${searchUtils.currentPage+kanan<searchUtils.totalPage}">--%>
                <li onclick="paginationClickNext('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">></a></li>
                <li onclick="paginationClick('${historyProduct.NOMOR_PRODUK}')"><a href="javascript:void(0)">&raquo;</a></li>
                <%--</c:if>--%>
                <%--</c:if>--%>
                <%-- <c:if test="${searchUtils.totalPage >1}">
                     <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=1'/>')"><a href="#">&laquo;</a></li>
                     <c:forEach var="page" begin="1" end="${searchUtils.totalPage}">
                         <c:choose>
                             <c:when test="${page eq searchUtils.currentPage}">
                                 <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}'/>')"><span><a href="#">${page}</a></span></li>
                             </c:when>
                             <c:otherwise>
                                 <li onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${page}'/>')"><a href="#"> ${page}</a></li>
                             </c:otherwise>
                         </c:choose>
                     </c:forEach>
                     <li  onclick="paginationClick('<c:url value='/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.totalPage}'/>')"><a href="#">&raquo;</a></li>
                 </c:if>--%>

            </ul>
        </div>
    </div>
    <!-- END PAGINATOR -->
</div>