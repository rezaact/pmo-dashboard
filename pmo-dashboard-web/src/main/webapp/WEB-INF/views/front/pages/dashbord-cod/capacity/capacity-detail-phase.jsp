<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<script>
    jenis = '${jenis}';
    subJenis = '${subJenis}';
    tahunAkhir = '${tahunAkhir}';
    tahunAwal = '${tahunAwal}';
    detailType = '${detailType}';
    group = '${group}';
    AssetType = '${AssetType}';
    //strChart_All = JSON.parse('${strChart_All}');
    strChart_All = '${strChart_All}';

    strChart_Sumatera = '${strChart_Sumatera}';
    strChart_JBB = '${strChart_JBB}';
    strChart_JBT = '${strChart_JBT}';
    strChart_JBTB = '${strChart_JBTB}';

    strChart_KAL = '${strChart_KAL}';
    strChart_SNT = '${strChart_SNT}';
    strChart_MP = '${strChart_MP}';
    <%--CmbFilterIsActual = '${CmbFilterIsActual}';--%>
</script>

<div class="row">
    <div class="col-md-12">

        <!-- Navigasi -->
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#generation">Pie Chart</a></li>
            <li><a data-toggle="tab" href="#transmission">Data Grid</a></li>
        </ul>

        <!-- Generation -->
        <div class="tab-content">
            <div id="generation" class="tab-pane fade in active">
                <div class="table-responsive">


                    <div class="panel panel-success export_pdf">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="detail-title-header">Chart</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="text-align: center" id="chartContainer"></div>
                                    <div class="row">
                                        <div style="text-align: center" id="chartContainer-regional-1" class="col-md-4 chartContainer-regional-1"></div>
                                        <div style="text-align: center" id="chartContainer-regional-2" class="col-md-4 chartContainer-regional-2" ></div>
                                        <div style="text-align: center" id="chartContainer-regional-3" class="col-md-4 chartContainer-regional-3"></div>
                                        <div style="text-align: center" id="chartContainer-regional-4" class="col-md-4 chartContainer-regional-4"></div>
                                        <div style="text-align: center" id="chartContainer-regional-5" class="col-md-4 chartContainer-regional-5"></div>
                                        <div style="text-align: center" id="chartContainer-regional-6" class="col-md-4 chartContainer-regional-6"></div>
                                        <div style="text-align: center" id="chartContainer-regional-7" class="col-md-4 chartContainer-regional-7"></div>

                                    </div>
                                </div>
                            </div>
                            <!--- Data Regional yang tidak ada datanya -->
                            <div class="row">
                                <div class="col-md-12">

                                    <div style="display: none" class="panel panel-default panel_region">
                                        <div class="panel-heading">Tidak ada data pada regional : </div>
                                        <div class="panel-body">
                                            <div class="data_region"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>

            <div id="transmission" class="tab-pane fade in">
                <div class="table-responsive">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="detail-title">Generation Capacity Detail</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="table-capacity-detail" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Phase</th>
                                                <th>No</th>
                                                <th>Nama Pembangkit</th>
                                                <th>Type Pembangkit</th>
                                                <th>Regional</th>
                                                <th width="5%">Kapasitas</th>
                                                <th>Milestone</th>
                                                <th>Progress</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>



    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <%--<a href='<c:url value="/dashboard-cod-capacity"/>' class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>--%>
            <a href='<c:url value="/dashboard-cod-capacity"/>' class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
            <%--<a href="#" id="create_pdf" class="btn btn-success"><span class="fa fa-print"></span> Export</a>--%>

    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="hasil-canvas">

        </div>
    </div>
</div>
<!-- END CONTENT -->