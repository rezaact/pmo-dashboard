<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
<div class="col-md-14">

<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">Dashboard Milestone IPP and PLN</h3>
    </div>
    <div class="panel-body">


    <div class="row">
        <div class="col-md-5">

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Regional</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select class="CmbRegion form-control OnFilterAll">
                            <option value="0">--Choose Region--</option>
                            <c:forEach var="dataComboRegion" items="${listComboRegion}" varStatus="number">
                                <option value="${dataComboRegion.ID}">${dataComboRegion.STRINGVALUE}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Program</label>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <select class="CmbProgram form-control OnFilterAll">
                            <option value="0">--Choose Program--</option>
                            <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">
                                <option value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                            </c:forEach>
                        </select>

                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Type</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="CmbType form-control OnFilterAll">
                            <option value="0">--Choose Type--</option>
                            <c:forEach var="dataComboType" items="${listComboType}" varStatus="number">
                                <option value="${dataComboType.ID}">${dataComboType.STRINGVALUE}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Phase</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <select class="CmbPhase form-control OnFilterAll">
                            <option value="0">--Choose Phase--</option>
                            <c:forEach var="dataComboPhase" items="${listComboPhase}" varStatus="number">
                                <option value="${dataComboPhase.ID}">${dataComboPhase.STRINGVALUE}</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-3">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Ownership</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <c:forEach var="dataComboOwnership" items="${listComboOwnership}" varStatus="number">
                        <c:choose>
                            <c:when test="${dataComboOwnership.ID == '0'}">
                                <label class="radio">
                                    <input name="OWH" class="CmbOwnership OnFilterAll" checked type="radio" value="${dataComboOwnership.ID}" id="radioIPP${dataComboOwnership.ID}">${dataComboOwnership.VALUESTRING}
                                </label>
                            </c:when>
                            <c:otherwise>
                                <label class="radio">
                                    <input name="OWH" class="CmbOwnership OnFilterAll" type="radio" value="${dataComboOwnership.ID}" id="radioPLN${dataComboOwnership.ID}">${dataComboOwnership.VALUESTRING}
                                </label>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>


                    </div>
                </div>
            </div>
        </div>

    </div>

<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne" style="text-align:center">
        <h4 class="panel-title"> <a href="#collapseOne" role="button" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne" class="" > PROCURENMENT MILESTONES </a> </h4>
    </div>
    <div class="panel-collapse collapse in" role="tabpanel" id="collapseOne" aria-labelledby="headingOne" aria-expanded="true"> <div class="panel-body">

        <div class="row">
            <div class="col-sm-12">

                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <!-- <div class="box_all"></div>-->

                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default box-milestone">
                            <p class="text-center">
                                <span id="PRE-QUALIFICATION-NAME"></span><br><br>
                                <span id="PRE-QUALIFICATION"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default box-milestone">
                            <p class="text-center">
                                <span id="REQUEST-FOR-PROPOSAL-NAME"></span><br><br>
                                <span id="REQUEST-FOR-PROPOSAL"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default box-milestone">
                            <p class="text-center">
                                <span id="LATTER-OF-INTENT-NAME"></span><br>
                                <span id="LATTER-OF-INTENT-NAME2"></span><br>
                                <span id="LATTER-OF-INTENT"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">
                            <p class="text-center">
                                <span id="PPA-SIGNING-NAME"></span><br>
                                <span id="PPA-SIGNING-NAME2"></span><br>
                                <span id="PPA-SIGNING"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">
                            <p class="text-center">
                                <span id="FINANCIAL-CLOSING-NAME"></span><br><br>
                                <span id="FINANCIAL-CLOSING"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">
                            <p class="text-center">
                                <span id="KONSTRUKSI-NAME"></span><br><br>
                                <span id="KONSTRUKSI"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>

                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default">
                            <p class="text-center">
                                <span id="COD-NAME"></span><br><br>
                                <span id="COD"></span><br>
                                PROJECT(S)
                            </p>
                        </button>
                    </div>

                </div>

        </div>



    </div>

    </div>
</div>

<!-- Nav Tabs -->
<ul class="nav nav-tabs">
<li><a data-toggle="tab" id="project-tab" href="#Project">Project Milestone</a></li>
<li class="active"><a data-toggle="tab" href="#Progress">Progress Summary</a></li>
</ul>

<div class="tab-content">
<div id="Project" class="tab-pane fade">
    <div class="milestone-datatable-div">
        <table id="milestone-datatable" class="table table-bordered">
            <thead>
                <tr>
                    <th rowspan="2" style="width: 10%important;">&nbsp;</th>
                    <th rowspan="2" style="width: 20%important;">Project Name</th>
                    <th rowspan="2">Project Ownership</th>
                    <th rowspan="2">Power Price</th>
                    <th colspan="3">PQ</th>
                    <th colspan="3">RFP</th>
                    <th colspan="3">LOI</th>
                    <th colspan="3">PPA</th>
                    <th colspan="3">FC</th>
                    <th colspan="3">Konstruksi</th>
                    <th colspan="3">COD</th>

                    <th colspan="2">Engineering</th>
                    <th colspan="2">Procurement</th>
                    <th colspan="2">Construction</th>
                    <th colspan="2">Total Progress</th>

                </tr>
                <tr>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Planned</th>
                    <th>Actual</th>
                    <th>Indicator</th>

                    <th>Plan</th>
                    <th>Actual</th>

                    <th>Plan</th>
                    <th>Actual</th>

                    <th>Plan</th>
                    <th>Actual</th>

                    <th>Plan</th>
                    <th>Actual</th>

                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <div class="milestone-datatable-div">
        <table id="milestone-datatable-pln" class="table table-bordered">
            <thead>
            <tr>
                <th rowspan="2" style="width: 10%important;">&nbsp;</th>
                <th rowspan="2" style="width: 20%important;">Project Name</th>
                <th rowspan="2">Project Ownership</th>
                <th rowspan="2">Power Price</th>
                <th colspan="3">FS</th>
                <th colspan="3">Bid Doc</th>
                <th colspan="3">Procurement Process</th>
                <th colspan="3">Construction Process</th>
                <th colspan="3">Commissioning</th>
                <th colspan="3">COD</th>
                <th colspan="3">FAC</th>

                <th colspan="2">Engineering</th>
                <th colspan="2">Procurement</th>
                <th colspan="2">Construction</th>
                <th colspan="2">Total Progress</th>

            </tr>
            <tr>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Planned</th>
                <th>Actual</th>
                <th>Indicator</th>

                <th>Plan</th>
                <th>Actual</th>

                <th>Plan</th>
                <th>Actual</th>

                <th>Plan</th>
                <th>Actual</th>

                <th>Plan</th>
                <th>Actual</th>

            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <table class="table table-bordered table-striped"> <colgroup> <col class="col-xs-1"> <col class="col-xs-7"> </colgroup>
                <thead>
                    <tr>
                        <th colspan="2">Legends for milestones deviation: </th>
                    </tr>

                </thead>

                <tbody>
                    <tr>
                        <td><i class="fa fa-square" aria-hidden="true" style="color:#4DB848"></i> (Good)</td>
                        <td>2 weeks before milestone plan date</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-square" aria-hidden="true" style="color:#F6F06A"></i> (Caution)</td>
                        <td>1 week before milestone plan date</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-square" aria-hidden="true" style="color:#F27789"></i> (Warning)</td>
                        <td>Less than 1 week before milestone plan date or exceeding milestone plan date</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-square" aria-hidden="true" style="color:#A349A4"></i> (Completed)</td>
                        <td>Milestone already executed</td>
                    </tr>
                    <tr>
                        <td><i class="fa fa-square" aria-hidden="true" style="color:#E4E4E4"></i> (N/A)</td>
                        <td>No plan date for milestone</td>
                    </tr>
                 </tbody>
            </table>

        </div>
        <div class="col-md-6">
            <table class="table table-bordered table-striped"> <colgroup> <col class="col-xs-1"> <col class="col-xs-7"> </colgroup>
                <thead>
                <tr>
                    <th colspan="2">Legends for EPC Progress: </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td><i class="fa fa-square" aria-hidden="true" style="color:#4DB848"></i> (Good)</td>
                    <td>Deviation range: -5% to 5%</td>
                </tr>
                <tr>
                    <td><i class="fa fa-square" aria-hidden="true" style="color:#F6F06A"></i> (Caution)</td>
                    <td>Deviation range: -10% &le; x &lt; -5%, or 5% &lt; x &le; 10%</td>
                </tr>
                <tr>
                    <td><i class="fa fa-square" aria-hidden="true" style="color:#F27789"></i> (Warning)</td>
                    <td>Deviation range: &lt; -10%, or &gt; 10%</td>
                </tr>
                <tr>
                    <td><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color: yellow"></i> (Warning)</td>
                    <td>Plan Physical Progress Unavailable</td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>

    </p>
</div>
<div id="Progress" class="tab-pane fade in active">

    <div class="row">
        <div class="col-sm-7">
            <!-- OverAll -->
            <div id="chartGeneration"></div>
        </div>
        <div class="col-sm-5">
<%--            <div class="row">
            </div>--%>
            <%--<div class="row">--%>
            <%--</div>--%>
        </div>
    </div>

    <!-- Map Chart -->
    <div id="testFusionMap"></div>

    <h3>PERFORMANCE</h3>
    <!-- Nav Tabs -->
    <ul class="nav nav-tabs">
        <%--<li><a data-toggle="tab" href="#PERFORMANCE_P">Bar Chart</a></li>--%>
        <li class="active"><a data-toggle="tab" href="#PERFORMANCE_S">Donut Chart</a></li>
    </ul>

    <div class="tab-content">
        <div id="PERFORMANCE_P" class="tab-pane fade">
            <p>
            <div class="row">
                <div class="col-sm-12">
                    <%--<div id="chartGeneration4"></div>--%>
                </div>
            </div>
            </p>
        </div>
        <div id="PERFORMANCE_S" class="tab-pane fade in active">
            <p>
            <div class="row">

                <!--IPP -->
                <div class="divIPP">
                    <div class="col-sm-6">
                        <div id="divChartPreQualification"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="divChartRequestForProposal"></div>
                    </div>
                </div>

                <!--PLN -->
                <div class="divPLN">
                    <div class="col-sm-6">
                        <div id="divChartFeasibilityStudy"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="divChartBiddoc"></div>
                    </div>
                </div>

            </div>

            <div class="row">
                <!--IPP -->
                <div class="divIPP">
                    <div class="col-sm-6">
                        <div id="DivLaterOfIntent"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="DivPPASigning"></div>
                    </div>
                </div>

                <!--PLN -->
                <div class="divPLN">
                    <div class="col-sm-6">
                        <div id="divChartProcurementProcess"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="divChartConstructionProcess"></div>
                    </div>
                </div>

            </div>


            <div class="row">

                <!--IPP -->
                <div class="divIPP">
                    <div class="col-sm-6">
                        <div id="DivFinancialClosing"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="DivKonstruksi"></div>
                    </div>
                </div>

                <!--PLN -->
                <div class="divPLN">
                    <div class="col-sm-6">
                        <div id="divChartCOmmisioning"></div>
                    </div>
                    <div class="col-sm-6">
                        <div id="divChartFAC"></div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div id="DivCOD"></div>
                </div>
            </div>
            </p>
        </div>
    </div>
</div>
    </p>
</div>


<!-- end Nav Tabs -->


    </div>
</div>


</div>
</div>

<!-- END CONTENT -->

<%--BEGIN MODAL DETAIL PROJECT--%>
<!-- Modal -->
<div class="modal fade" id="detail-project-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Project Detail Information</h4>
            </div>
            <div class="modal-body">
                <h2 id="PROJECTNAME"></h2>
                <h6>PROFILES</h6>
                <table>
                    <tbody>
                        <tr>
                            <td valign="top" width="20%">Asset</td>
                            <td>
                                <table id="table-asset" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Asset Name</th>
                                            <th>Plan Output</th>
                                            <th>Actual Output</th>
                                            <th>Estimate COD</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    <tr>
                        <td>Current Status</td>
                        <td id="current-status"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL DETAIL PROJECT--%>

<%--BEGIN MODAL MAP DETAIL--%>
<!-- Modal -->
<div class="modal fade" id="detail-progress-sum-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail</h4>
            </div>
            <div class="modal-body">
                <!-- Map Chart -->
                <div class="row">
                    <div class="col-md-12">
                        <div id="detail-donut"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL DETAIL PROJECT--%>

<%--BEGIN MODAL DATATABLE DETAIL--%>
<!-- Modal -->
<div class="modal fade" id="detail-milestone-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="margin-top: 100px;width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="milestone-datatable-div">
                            <table id="milestone-datatable-modal" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="width: 20%!important;">Project Name</th>
                                    <th rowspan="2" >Project Ownership</th>
                                    <th rowspan="2" >Power Price</th>
                                    <th colspan="3">PQ</th>
                                    <th colspan="3">RFP</th>
                                    <th colspan="3">LOI</th>
                                    <th colspan="3">PPA</th>
                                    <th colspan="3">FC</th>
                                    <th colspan="3">Konstruksi</th>
                                    <th colspan="3">COD</th>

                                    <th colspan="2">Engineering</th>
                                    <th colspan="2">Procurement</th>
                                    <th colspan="2">Construction</th>
                                    <th colspan="2">Total Progress</th>

                                </tr>
                                <tr>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div class="milestone-datatable-div">
                            <table id="milestone-datatable-pln-modal" class="table table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2" style="width: 20%!important;">Project Name</th>
                                    <th rowspan="2" >Project Ownership</th>
                                    <th rowspan="2" >Power Price</th>
                                    <th colspan="3">FS</th>
                                    <th colspan="3">Bid Doc</th>
                                    <th colspan="3">Procurement Process</th>
                                    <th colspan="3">Construction Process</th>
                                    <th colspan="3">Commissioning</th>
                                    <th colspan="3">COD</th>
                                    <th colspan="3">FAC</th>

                                    <th colspan="2">Engineering</th>
                                    <th colspan="2">Procurement</th>
                                    <th colspan="2">Construction</th>
                                    <th colspan="2">Total Progress</th>

                                </tr>
                                <tr>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Planned</th>
                                    <th>Actual</th>
                                    <th>Indicator</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                    <th>Plan</th>
                                    <th>Actual</th>

                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL DETAIL PROJECT--%>
