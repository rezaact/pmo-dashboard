<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="main">
  <div class="container">
    <div class="row margin-bottom-40">

        <tiles:insertAttribute name="account-sidebar"/>
        <tiles:insertAttribute name="account-content-register" ignore="true"/>
        <tiles:insertAttribute name="account-content-profile" ignore="true"/>
        <tiles:insertAttribute name="account-content-login" ignore="true"/>


  </div>
</div>
</div>