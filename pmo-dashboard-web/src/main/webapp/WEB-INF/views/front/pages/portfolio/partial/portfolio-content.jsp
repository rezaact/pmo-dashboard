<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="content-page">
 <div class="services-block content content-center" id="services">
  <div class="container">
   <h2>Porto<strong>folio</strong></h2>
   <h4>Pusat Pemeliharaan Ketenagalistrikan</h4>
 <div class="row-header">
    <div class="row">
        <div class="col-md-4 col-sm-4 text-center">
         <img src="<c:url value="/assets/frontend/pages/img/portfolio/3.PNG"/> "style="width: 200px; height: 180px">
         <div class="border-right">
          <h4><strong>UWP I MERAK</strong></h4>
          <p>Jl. Pulorida PO. Box 6, Ds. Suralaya, Kec.Pulomerak <br/>
           Merak. Banten <br/>
           Tlp  0254-571265 <br/>
           Fax 0254-571273
          </p>
         </div>
        </div>
        <div class="col-md-4 col-md-4 col-sm-4 text-center">
         <img src="<c:url value="/assets/frontend/pages/img/portfolio/III.PNG"/> " style="width: 190px">
         <h4><strong>UWP III BANDUNG</strong></h4>
         <P>JL. Banten No 10, Bandung 40272 <br/>
          Tlp022-7208176 <br/>
          Fax 022-7208176
        </div>
        <div class="col-md-4 col-sm-4 text-center">
         <img src="<c:url value="/assets/frontend/pages/img/portfolio/5.PNG"/> "style="width: 200px; height: 180px">
         <div class="border-left" >
         <h4><strong>UWP V SEMARANG</strong></h4>
         <p>
          Jl. Siliwangi No 379, Semarang 50146 <br/>
          Tlp 024-7608250 <br/>
          Fax 024-7608250
         </p>
         </p>
        </div>
        </div>
     </div>
    <div class="row">
     <div class="col-md-4 col-sm-4 text-center">
      <img src="<c:url value="/assets/frontend/pages/img/portfolio/4.PNG"/> "style="width: 200px; height: 180px">
        <div class="border-right">
        <h4><strong>UWP II JAKARTA</strong></h4>
        <P>JL. Raya Bekasi Timur Km 17, Jakarta Timur 13930 <br/>
         Tlp 021-4613230 <br/>
         Fax 021-4519066
        </p>
       </div>
      </div>
     <div class="col-md-4 col-sm-4 text-center">
      <img src="<c:url value="/assets/frontend/pages/img/portfolio/2.PNG"/> " style="width: 200px; height: 180px">
      <h4><strong>UWP IV DAYEUHKOLOT</strong></h4>
      <p>Jl. Raya Deyeuhkolot km 09, Bandung <br/>
       Tlp 022-5230669 <br/>
       Fax 022-5230539</p>
     </div>
     <div class="col-md-4 col-sm-4 text-center">
      <img src="<c:url value="/assets/frontend/pages/img/portfolio/6.PNG"/> "style="width: 200px; height: 180px">
      <div class="border-left">
      <h4><strong>UWP VI SURABAYA</strong></h4>
      <p>
       Jl. Ngagel Timur No 16, Surabaya 60285 <br/>
       Tlp 031-5023731 <br/>
       Fax 031-5023731
      </p>
       </div>
     </div>
    </div>
     <div class="row">
      <div class="col-md-12 col-sm-12 text-center">
       <div class="border-bottom-head border-top-head padding-top-10">
       <h1>Klien<strong> / Client</strong></h1>
        </div>
      </div>
        <div class="col-md-6 text-left">
         <ol>
          <li>PT PLN (Persero) Wilayah Aceh</li>
          <li>PT PLN (Persero) Wilayah Sumatera Utara </li>
          <li>PT PLN (Persero) Wilayah Sumatera Barat </li>
          <li>PT PLN (Persero) Wilayah Sumatera Selatan, Jambi dan Bengkulu  </li>
          <li>PT PLN (Persero) Wilayah Riau dan KEPRI </li>
          <li>PT PLN (Persero) Wilayah Bangka Belitung</li>
          <li>PT PLN (Persero) Wilayah Kalimantan Barat </li>
          <li>PT PLN (Persero) Wilayah Kalimantan Selatan dan Kalimantan Tengah </li>
          <li>PT PLN (Persero) Wilayah Kalimantan Timur dan Utara</li>
          <li>PT PLN (Persero) Wilayah Sulawesi Utara, Sulawesi Tengah dan Gorontalo</li>
          <li>PT PLN (Persero) Wilayah Sulsel, Sulteng dan Sulawesi Barat</li>
          <li>PT PLN (Persero) Wilayah Maluku dan Maluku Utara</li>
          <li>PT PLN (Persero) Wilayah Papua dan Papua Barat</li>
          <li>PT PLN (Persero) Wilayah Nusa Tenggara Barat</li>
          <li>PT PLN (Persero) Wilayah Nusa Tenggara Timur</li>
         </ol>
        </div>
      <div class="col-md-6 text-left">
       <ol start="16">
        <li>PT PLN (Persero) Distribusi Jawa Timur </li>
        <li>PT PLN (Persero) Distribusi Jawa Tengah dan DI Yogyakarta </li>
        <li>PT PLN (Persero) Distribusi Jawa Barat dan Banten </li>
        <li>PT PLN (Persero) Distribusi Jakarta Raya dan Tangerang </li>
        <li>PT PLN (Persero) Distribusi Bali </li>
        <li>PT PLN (Persero) Pembangkit Sumatera Bagian Utara </li>
        <li>PT PLN (Persero) Pembangkit Sumatera Bagian Selatan</li>
        <li>PT PLN (Persero) Unit Pembangkit Jawa Bali </li>
        <li>PT PLN (Persero) PUSDIKLAT</li>
        <li>PT PLN (Persero) PUSLITBANG</li>
        <li>PT Indonesia Power </li>
        <li>PT PLN Batu Bara </li>
        <li>PT Pembangkit Jawa Bali</li>
       </ol>
      </div>
     </div>
</div>
  </div>
</div>
</div>