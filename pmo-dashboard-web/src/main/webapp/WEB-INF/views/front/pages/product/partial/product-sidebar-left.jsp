<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <!-- BEGIN SIDEBAR -->
  <div class="sidebar col-md-3 col-sm-5">
      <tiles:insertAttribute name="product-sidebar-workshop" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-category" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-available" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-featured" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-bestseller" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-urlinfo" ignore="true"/>
      <tiles:insertAttribute name="product-sidebar-urllink" ignore="true"/>
  </div>
  <!-- END SIDEBAR -->