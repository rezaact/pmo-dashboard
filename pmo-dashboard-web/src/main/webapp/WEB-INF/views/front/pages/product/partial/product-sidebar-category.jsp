<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            kategori
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="scroller" style="height:150px">
            <ul class="list-group margin-bottom-25 sidebar-menu">

                <c:forEach var="kategori" items="${listKategori}" varStatus="number">
                <li class="list-group-item clearfix dropdown">
                    <a href="javascript:void(0);" onclick="onKategoriClick('${kategori.ID_KATEGORI}','','');">
                        <i class="fa fa-angle-right"></i>
                            ${kategori.NAMA_KATEGORI}
                    </a>
                    <ul class="dropdown-menu" style="display:none;">
                        <c:forEach var="produk" items="${listProduk}" varStatus="number">
                            <c:if test="${produk.ID_KATEGORI eq kategori.ID_KATEGORI}">
                        <li class="list-group-item dropdown clearfix">
                            <a href="javascript:void(0);" onclick="onKategoriClick('${produk.ID_KATEGORI}','${produk.ID_PRODUK}','');"><i class="fa fa-angle-right"></i> ${produk.NAMA_PRODUK} </a>
                            <ul class="dropdown-menu" style="display:none;">
                                <c:forEach var="komponen" items="${listKomponen}" varStatus="number">
                                    <c:if test="${komponen.ID_PRODUK eq produk.ID_PRODUK}">
                                <li onclick="onKategoriClick('${komponen.ID_KATEGORI}','${komponen.ID_PRODUK}','${komponen.ID_KOMPONEN}');"><a href="javascript:void(0);"><i class="fa fa-angle-right"></i> ${komponen.NAMA_KOMPONEN}</a></li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>