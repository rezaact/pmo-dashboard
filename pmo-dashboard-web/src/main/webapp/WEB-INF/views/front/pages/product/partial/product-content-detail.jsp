<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
    NOMOR_PRODUK = '${detailProduk.NOMOR_PRODUK}';
    JML_FILE = ${detailProduk.jml_file};
    DOC_TYPE = '${doc_type}';
</script>
<%--<div class="row">--%>
<%--<div class="col-md-12">--%>

<%--</div>--%>
<%--</div>--%>
<!-- BEGIN CONTENT -->
<div class="col-md-9 col-sm-7">
    <form action="<c:url value="/product/product-list"/>" method="get">
        <div class="input-group">
            <input type="text" name="search" id="search" placeholder="Pencarian produk" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="submit">Pencarian</button>
                    </span>
        </div>
    </form>
    <br/>
    <div class="product-page">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <c:choose>
                    <c:when test="${not empty image2DZoom && not empty listImageThumbnail}">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active pull-right"><a href="#image-3d" aria-controls="image-3d" role="tab" data-toggle="tab" style="margin-right: 0px">3D</a></li>
                            <li role="presentation" class="pull-right"><a href="#image-2d" aria-controls="image-2d" role="tab" data-toggle="tab">2D</a></li>
                        </ul>
                        <div class="tab-content padding-0">
                            <div role="tabpanel" class="tab-pane active fade in" id="image-3d">
                                <div class="product-main-image">
                                    <div class="spritespin"></div>
                                </div>
                                <div class="product-other-images">
                                    <c:forEach var="imageThumbnail" items="${listImageThumbnail}" varStatus="number">
                                        <%--<a href="<c:url value="/product/product-detail?id=${detailProduk.NOMOR_PRODUK}&doc_type=${imageThumbnail.DOC_TYPE}"/>" class="fancybox-button" rel="photos-lib">
                                        </a>--%>
                                        <a href="<c:url value="/product/product-detail?id=${detailProduk.NOMOR_PRODUK}&doc_type=${imageThumbnail.DOC_TYPE}"/>" class="fancybox-button" rel="photos-lib"> <img alt="Berry Lace Dress" src="<c:url value="/product/get_image?id_file=${imageThumbnail.ID_FILE}"/>">
                                        </a>
                                        <%--<img alt="Berry Lace Dress" src="<c:url value="/product/get_image?id_file=${imageThumbnail.ID_FILE}"/>" class="manual" style="cursor:pointer">--%>
                                    </c:forEach>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="image-2d">
                                <div class="product-main-image product-main-image-zoom">
                                    <img src="<c:url value="/product/get_image?id_file=${image2DZoom.ID_FILE}"/>"  alt="..." class="img-responsive" data-BigImgsrc="<c:url value="/product/get_image?id_file=${image2DZoom.ID_FILE}"/>">
                                </div>
                                <div class="product-other-images">
                                    <c:forEach var="image2DPopup" items="${listImage2DPopup}" varStatus="number">
                                        <%--<a href="<c:url value='/product/get_image?id_file=${image2DPopup.ID_FILE}'/>" class="fancybox-button" rel="photos-lib">--%>
                                        <img alt="..." src="<c:url value='/product/get_image?id_file=${image2DPopup.ID_FILE}'/>" class="manual" style="cursor:pointer">
                                        <%--</a>--%>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:when test="${not empty image2DZoom}">
                        <div class="product-main-image product-main-image-zoom">
                            <img src="<c:url value="/product/get_image?id_file=${image2DZoom.ID_FILE}"/>"  alt="Cool green dress with red bell" class="img-responsive" data-BigImgsrc="<c:url value="/product/get_image?id_file=${image2DZoom.ID_FILE}"/>">
                        </div>
                        <div class="product-other-images">
                            <c:forEach var="image2DPopup" items="${listImage2DPopup}" varStatus="number">
                                <%--<a href="#" class="fancybox-button" rel="photos-lib">--%>
                                <img alt="Berry Lace Dress" src="<c:url value='/product/get_image?id_file=${image2DPopup.ID_FILE}'/>" class="manual" style="cursor:pointer">
                                <%--</a>--%>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:when test="${not empty listImageThumbnail}">
                        <div class="product-main-image">
                            <div class="spritespin"></div>
                        </div>
                        <div class="product-other-images">
                            <c:forEach var="imageThumbnail" items="${listImageThumbnail}" varStatus="number">
                                <%--<a href="<c:url value="/product/product-detail?id=${detailProduk.NOMOR_PRODUK}&doc_type=${imageThumbnail.DOC_TYPE}"/>" class="fancybox-button " rel="photos-lib">--%>
                                <img alt="Berry Lace Dress" src="<c:url value="/product/get_image?id_file=${imageThumbnail.ID_FILE}"/>" class="manual" style="cursor:pointer">
                                <%--</a>--%>
                            </c:forEach>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <img src="<c:url value="/assets/global/img/default.png"/>" class="img-responsive" alt="..."/>
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="col-md-6 col-sm-6">
                <h1>${detailProduk.NAMA_PRODUK}</h1>
                <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                    <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE'}">
                        <div class="price-availability-block clearfix">
                            <div class="price">
                                <c:choose>
                                    <c:when test="${not empty detailProduk.HARGA_PRODUK}">
                                        <strong><span>Rp.</span>${detailProduk.HARGA_PRODUK}</strong>
                                        <em class="text-capitalize">${detailProduk.JNS_HARGA_PRODUK}</em>
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </c:if>
                </c:if>
                <div class="description" style="margin-bottom: 15px">
                    <%--<c:set var="shortDescription" value="${detailProduk.DESKRIPSI_PRODUK}"/>--%>
                    <%--<c:set var="longDescription" value="${fn:substring(shortDescription, 0, 200)}" />--%>
                    ${detailProduk.DESKRIPSI_PRODUK}
                    <c:if test="${detailProduk.DESKRIPSI != null}">
                        <a href="#readmore-${detailProduk.NOMOR_PRODUK}" data-toggle="modal" style="display: block;margin-top: 5px;margin-bottom: 5px;">Baca selanjutnya</a>
                        <div class="modal fade modal-readmore" id="readmore-${detailProduk.NOMOR_PRODUK}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-red">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">${detailProduk.NAMA_PRODUK}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="media">
                                            <div class="media-left pull-left">
                                                <a href="#">
                                                    <img class="media-object" src="<c:url value="/product/get_image?id_file=${image2DZoom.ID_FILE}"/>" alt="..." width="300">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Deskripsi</h4>
                                                    ${detailProduk.DESKRIPSI_PRODUK}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
                <div class="product-page-options">
                    <div class="availability">
                        Stok Tersedia: <strong>0</strong>
                    </div>
                </div>
                <div class="product-page-cart">
                    <a href="<c:url value="/contact?id=${detailProduk.NOMOR_PRODUK}"/>" class="btn btn-primary" target="_blank">Hubungi Kami</a>
                </div>
                <div class="review" id="rateProduk">
                    <%--<input type="range" value="4" step="0.25" id="backing4">--%>
                    <div class="rateit" data-rateit-value="${detailProduk.RATE}" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                    <%--<div class="rateit" data-rateit-backingfld="#backing4" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">--%>
                </div>
                <%--<a href="#tab-comment" data-target="tab">${detailProduk.JML_KOMENTAR} komentar</a>&nbsp;&nbsp;--%>
            </div>
        </div>

        <div class="product-page-content">
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#tab-detail-product" data-toggle="tab">Detail Produk</a></li>
                <li><a href="#tab-historical-product" data-toggle="tab">Histori Produk</a></li>
                <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                    <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' }">
                        <li><a href="#tab-document-product" data-toggle="tab">Dokumen Produk </a></li>
                    </c:if>
                </c:if>
                <li><a href="#tab-comment" data-toggle="tab">Komentar (<span id="total-komentar">${detailProduk.JML_KOMENTAR}</span>)</a></li>
            </ul>
            <div id="myTabContent" class="tab-content padding-0">
                <div class="tab-pane fade in active" id="tab-detail-product">
                    <table class="table table-bordered" >
                        <thead>
                        <tr>
                            <th style="width: 35%">Uraian</th>
                            <th>Data</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="table-headline">
                            <td colspan="2"><strong>Material Produk</strong></td>
                        </tr>
                        <tr>
                            <td>
                                Deskripsi Material
                            </td>
                            <td>
                                <c:choose>
                                    <c:when test="${not empty detailProduk.DESKRIPSI_MATERIAL}">
                                        ${detailProduk.DESKRIPSI_MATERIAL}
                                    </c:when>
                                    <c:otherwise>
                                        -
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_REGISTER' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_SECRET' }">
                                <tr>
                                    <td>View Drawing</td>
                                    <c:set var="flag" value="0"/>
                                    <td>
                                        <c:forEach items="${listFileDownload}" var="file">
                                            <c:if test="${file.DOC_TYPE eq '007'}">
                                                <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                                                <c:set var="flag" value="1"/>
                                            </c:if>
                                        </c:forEach>
                                        <c:if test="${flag eq '0'}">
                                            -
                                        </c:if>
                                    </td>
                                </tr>
                            </c:if>
                        </c:if>
                        <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' }">
                                <tr class="table-headline">
                                    <td colspan="2">
                                        <strong>Kemampuan Produksi Per Manufaktur</strong>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td>
                                        Produk per Bulan
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty detailProduk.KEMAMPUAN_PRODUK_PERBULAN}">
                                                ${detailProduk.KEMAMPUAN_PRODUK_PERBULAN} Buah
                                            </c:when>
                                            <c:otherwise>
                                                -
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        Produk per Tahun
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty detailProduk.KEMAMPUAN_PRODUK_PERTAHUN}">
                                                ${detailProduk.KEMAMPUAN_PRODUK_PERTAHUN} Buah
                                            </c:when>
                                            <c:otherwise>
                                                -
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                                <tr class="table-headline">
                                    <td colspan="2">
                                        <strong>Drawing</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Nomor Drawing per masing-masing File
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty detailProduk.NOMOR_DRAWING}">
                                                ${detailProduk.NOMOR_DRAWING}
                                            </c:when>
                                            <c:otherwise>
                                                -
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:if>
                        </c:if>
                        </tbody>
                    </table>

                </div>
                <tiles:insertAttribute name="product-history"/>
                <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                    <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
                        <div class="tab-pane fade" id="tab-document-product">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width: 35%">Uraian</th>
                                    <th>Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="3"><strong>Link Dokumen</strong></td>
                                </tr>
                                <tr>
                                    <td>File Drawing PDF</td>
                                    <c:set var="flag" value="0"/>
                                    <td>
                                        <c:forEach items="${listFileDownload}" var="file">
                                            <c:if test="${file.DOC_TYPE eq '007'}">
                                                <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                                                <c:set var="flag" value="1"/>
                                            </c:if>
                                        </c:forEach>
                                        <c:if test="${flag eq '0'}">
                                            -
                                        </c:if>
                                    </td>
                                </tr>
                                <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
                                    <tr>
                                        <td>File Drawing CAD</td>
                                        <c:set var="flag" value="0"/>
                                        <td>
                                                <%--    <a href="JavaScript:void(0)">-</a>--%>
                                            <c:forEach items="${listFileDownload}" var="file">
                                                <c:if test="${file.DOC_TYPE eq '008'}">
                                                    <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                                                    <c:set var="flag" value="1"/>
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${flag eq '0'}">
                                                -
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:if>
                                    <%--<tr>
                                        <td>File Penugasan</td>
                                        <c:set var="flag" value="0"/>
                                        <td>
                                        &lt;%&ndash;    <a href="JavaScript:void(0)">-</a>&ndash;%&gt;
                                            <c:forEach items="${listFileDownload}" var="file">
                                                <c:if test="${file.DOC_TYPE eq '003'}">
                                                    <a href="<c:url value="/file/download/${file.ID_FILE}"/>">${file.NAME}</a> </br>
                                                    <c:set var="flag" value="1"/>
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${flag eq '0'}">
                                                -
                                            </c:if>
                                        </td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </div>
                    </c:if>
                </c:if>
                <div class="tab-pane fade" id="tab-comment" style="padding: 15px 0px 0px 0px">
                    <!--<p>There are no reviews for this product.</p>-->
                    <div id="isi-comment">
                        <tiles:insertAttribute name="product-comment"/>

                    </div>

                    <!-- BEGIN FORM-->
                    <hr/>
                    <div class="well">
                        <form action="<c:url value="/comment/create"/> " class="reviews-form comment-form" role="form">
                            <input type="hidden" name="PRM_NOMOR_PRODUK" VALUE="${detailProduk.NOMOR_PRODUK}" id="no-prd"/>
                            <input type="hidden" name="PRM_PARENT" value="-1" id="parent"/>
                            <h2>Tulis sebuah komentar</h2>
                            <div class="form-group" <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                                style="display:none";
                            </c:if> >
                                <label for="name_com">Nama <span class="require">*</span></label>
                                <input type="text" class="form-control" name="PRM_NAME" id="name_com" value="${authUser.getNamaLengkap()}">
                            </div>
                            <div class="form-group" <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                                style="display:none";
                            </c:if>  >
                                <label for="email_com">Email</label>
                                <input type="text" class="form-control" name="PRM_EMAIL" id="email_com" value="${authUser.getEmail()}">

                            </div>
                            <div class="form-group">
                                <label for="review">Komentar <span class="require">*</span></label>
                                <textarea class="form-control" name="PRM_KOMENTAR" rows="8" id="review"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="main-rating">Rating</label>
                                <input type="range" value="4" step="0.25" id="main-rating">
                                <div class="rateit" id="rate_com" data-rateit-backingfld="#main-rating" data-rateit-resetable="false"  data-rateit-ispreset="true" data-rateit-min="0" data-rateit-max="5">
                                </div>
                            </div>
                            <div class="padding-top-20">
                                <button type="submit" class="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                    </div>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
        <c:if test="${detailProduk.FLAG_PRODUK_BARU == 1}">
            <div class="sticker sticker-sale"></div>
        </c:if>
    </div>
</div>
</div>
<!-- END CONTENT -->

