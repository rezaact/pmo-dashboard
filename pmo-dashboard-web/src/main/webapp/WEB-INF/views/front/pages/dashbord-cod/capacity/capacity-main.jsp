<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->



<div class="row">
    <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">RUPTL Capacity</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="radio">
                                <label><input id="RKapasitasSD" name="Kapasitas" class="PilihKapasitas" type="radio" value="3"> Kapasitas sampai dengan </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker" data-date-minviewmode="months">
                                    <input id="DateNowStart" type="text" class="KapasitasSD form-control">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="radio">
                                <label><input id="RKapasitasBD" name="Kapasitas" type="radio" class="PilihKapasitas" value="2"> Kapasitas berjalan berdasarkan</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <select id="CmbMonths" class="KapasitasBD form-control">
                                    <option value="1">Bulan</option>
                                    <option value="2">Tahun</option>
                                </select>
                            </div>
                            <div class="col-md-1 col-sm-12 col-xs-12">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker" data-date-minviewmode="months">
                                    <input id="DateRunningPeriodeMonth" type="text" class="form-control KapasitasBD">
                            <span class="input-group-btn">
                                <button class="btn default DateRunningPeriodeMonth_img" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                                </div>
                                <div class="input-group date date-picker" data-date-minviewmode="years">
                                    <input id="DateRunningPeriodeYear" type="text" class="form-control KapasitasBD">
                            <span class="input-group-btn">
                                <button class="DateRunningPeriodeYear_img btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="radio">
                                <label><input name="Kapasitas" type="radio" class="PilihKapasitas" value="1"> Kapasitas periode tertentu</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker KapasitasPT" data-date-minviewmode="months">
                                    <input id="DateStartPeriode" type="text" class="form-control KapasitasPT">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-12 col-xs-12">
                                <div>
                                    S.D.
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker KapasitasPT" data-date-minviewmode="months">
                                    <input id="DateEndPeriode" type="text" class="form-control KapasitasPT">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <%--        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                <select id="CmbFilterIsActual" class="form-control">
                                        <option value="PLAN">Plan</option>
                                        <option value="ACTUAL">Actual</option>
                                        <option value="ALL">All</option>
                                    </select>
                                </div>
                            </div>
                        </div>--%>



                <div class="btn-group"><button id="Btn_FilterAll" class="btn-sm btn btn-danger" type="button" title="Preview" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdPreview" data-hotkey="Ctrl+P" data-toggle="button"><span class="glyphicon glyphicon-search"></span> Submit</button></div>

            </div>

            <br>
            <hr>
            <!-- Navigasi -->
            <ul class="nav nav-tabs">
                <li class="load-data-tabs" id="generation-tab-header"><a data-toggle="tab" href="#generation">Generation</a></li>
                <li class="load-data-tabs" id="transmission-tab-header"><a data-toggle="tab" href="#transmission">Transmission</a></li>
                <li class="load-data-tabs" id="sub-station-tab-header"><a data-toggle="tab" href="#sub-station">Sub Station</a></li>
            </ul>

            <!-- Generation -->
            <div class="tab-content">
                <div id="generation" class="tab-pane fade in">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase total-header">Total</span></h1>
                                </th>
                                <th class="progres">&nbsp;</th>
                                <th width="10%" class="text-right" id="total-kapasitas" >
                                    <h1><span id="span_total" class=" caption-subject font-green-sharp bold uppercase"></span></h1>
                                </th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase">MW</span></h1>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="text-right progres">S-Curve Progress</td>
                                <td class="text-right">Kapasitas</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <%--<td>&nbsp;</td>--%>
                            </tr>
                            <tr>
                                <td width="6%"></td>
                                <td width="17%">Program 7.000 MW</td>
                                <td width="7%" class="text-right progres" id="percen-program-7"></td>
                                <td class="text-right" id="program-7"></td>
                                <td>MW</td>
                                <td width="60%">
                                    <form id="form-main-Generation" method="post" action="<c:url value="/dashboard-cod-capacity/detail"/>">
                                        <div class="btn-group">
                                            <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-7-generation" type="submit" value="1" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-7-generation" type="submit" value="2" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-7-generation btn-detail-by-phase" type="submit" value="3" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Phase
                                            </button>

                                        </div>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <tr>
                                <td></td>
                                <td>Program 35.000 MW</td>
                                <td class="text-right progres" id="percen-program-35"></td>
                                <td class="text-right" id="program-35"></td>
                                <td>MW</td>
                                <td>

                                    <div class="btn-group">
                                        <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-35-generation" type="submit" value="1" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-35-generation" type="submit" value="2" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-35-generation btn-detail-by-phase" type="submit" value="3" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Phase</button>

                                    </div>
                                    </form>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <%--                    <tr class="row-program-sebelumnya">
                                                    <td width="15%"></td>
                                                    <td>Program Sebelumnya</td>
                                                    <td class="text-right" id="program-sebelumnya"></td>
                                                    <td>MW</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Transmission -->
                <div id="transmission" class="tab-pane fade in">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase total-header">Total</span></h1>
                                </th>
                                <th>&nbsp;</th>
                                <th width="10%" class="text-right total-kapasitas" >
                                    <h1><span id="" class="span_total caption-subject font-green-sharp bold uppercase"></span></h1>
                                </th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase">KMS</span></h1>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="text-right progres">S-Curve Progress</td>
                                <td class="text-right">Kapasitas</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <%--<td>&nbsp;</td>--%>
                            </tr>
                            <tr>
                                <td width="6%"></td>
                                <td width="17%">Program 7.000 MW</td>
                                <td width="7%" class="text-right percen-program-7 progres"></td>
                                <td class="text-right program-7" ></td>
                                <td>KMS</td>
                                <td width="60%">
                                    <form id="form-main-Transmission" method="post" action="<c:url value="/dashboard-cod-capacity/detail"/>">
                                        <div class="btn-group">
                                            <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-7-transmission" type="submit" value="1" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-7-transmission" type="submit" value="2" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-7-transmission btn-detail-by-phase" type="submit" value="3" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Phase</button>

                                        </div>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <tr>
                                <td></td>
                                <td>Program 35.000 MW</td>
                                <td class="text-right percen-program-35 progres" id=""></td>
                                <td class="text-right program-35" id=""></td>
                                <td>KMS</td>
                                <td>

                                    <div class="btn-group">
                                        <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-35-transmission" type="submit" value="1" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-35-transmission" type="submit" value="2" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-35-transmission btn-detail-by-phase" type="submit" value="3" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Phase</button>

                                    </div>
                                    </form>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <%--                    <tr class="row-program-sebelumnya">
                                                    <td width="15%"></td>
                                                    <td>Program Sebelumnya</td>
                                                    <td class="text-right" id="program-sebelumnya"></td>
                                                    <td>MW</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- sub-station -->
                <div id="sub-station" class="tab-pane fade in">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th></th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase total-header">Total</span></h1>
                                </th>
                                <th>&nbsp;</th>
                                <th width="10%" class="text-right total-kapasitas" id="" >
                                    <h1><span id="" class="span_total caption-subject font-green-sharp bold uppercase"></span></h1>
                                </th>
                                <th>
                                    <h1><span class="caption-subject font-green-sharp bold uppercase">MVA</span></h1>
                                </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="text-right progres">S-Curve Progress</td>
                                <td class="text-right">Kapasitas</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <%--<td>&nbsp;</td>--%>
                            </tr>
                            <tr>
                                <td width="6%"></td>
                                <td width="17%">Program 7.000 MW</td>
                                <td width="7%" class="text-right percen-program-7 progres" id=""></td>
                                <td class="text-right program-7" id=""></td>
                                <td>MVA</td>
                                <td width="60%">
                                    <form id="form-main-SubStation" method="post" action="<c:url value="/dashboard-cod-capacity/detail"/>">
                                        <div class="btn-group">
                                            <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-7-gi" type="submit" value="1" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-7-gi" type="submit" value="2" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                            <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-7-gi btn-detail-by-phase" type="submit" value="3" data-column="19"><span class="glyphicon glyphicon-search"></span> Detail by Phase</button>

                                        </div>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <tr>
                                <td></td>
                                <td>Program 35.000 MW</td>
                                <td class="text-right percen-program-35 progres"></td>
                                <td class="text-right program-35"></td>
                                <td>MVA</td>
                                <td>

                                    <div class="btn-group">
                                        <button style="width: 150px;" class="btn-sm btn btn-danger btn-xs btn-submit btn-program-35-gi" type="submit" value="1" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Regional</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-success btn-xs btn-submit btn-program-35-gi" type="submit" value="2" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Type</button>
                                        <button style="width: 150px;" class="btn-sm btn btn-info btn-xs btn-submit btn-program-35-gi btn-detail-by-phase" type="submit" value="3" data-column="22"><span class="glyphicon glyphicon-search"></span> Detail by Phase</button>

                                    </div>
                                    </form>
                                </td>
                                <%--<td></td>--%>

                            </tr>
                            <%--                    <tr class="row-program-sebelumnya">
                                                    <td width="15%"></td>
                                                    <td>Program Sebelumnya</td>
                                                    <td class="text-right" id="program-sebelumnya"></td>
                                                    <td>MW</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>





        </div>
    </div>
</div>
<!-- END CONTENT -->
