<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--<script src='https://www.google.com/recaptcha/api.js'></script>--%>
<!-- BEGIN CONTENT -->
<div class="col-md-9 col-sm-9">
  <h1>Buat Akun Baru</h1>
  <div class="content-form-page">
    <div class="row">
      <div class="col-md-7 col-sm-7">
        <%--<form class="form-horizontal" role="form">--%>
          <form action="<c:if test="${label == 'create'}"><c:url value="/account/register"/></c:if>" class="form-horizontal form-bordered register-form" method="post">
              <div class="alert alert-empty alert-danger display-hide" style="margin-bottom: 0;margin-top: 15px">
                  <button class="close" data-close="alert"></button>
                  <p>Pengisian form pendaftaran belum sesuai</p>
              </div>
              <div class="alert alert-danger display-hide" style="<c:if test="${status == 'gagal'}">display:block;</c:if>margin-bottom: 0;margin-top: 15px">
                  <button class="close" data-close="alert"></button>
                  <p>Mohon maaf <span id="keterangan">username dan email</span> sudah digunakan </p>
              </div>
              <div class="alert alert-success display-hide" style="<c:if test="${status == 'sucess'}">display:block;</c:if>margin-bottom: 0;margin-top: 15px">
                  <button class="close" data-close="alert"></button>
                  <p>Data berhasil disimpan</p>
              </div>
              <fieldset>
            <legend>Daftar Pribadi</legend>
                  <div class="form-group">
                      <label for="firstname" class="col-lg-5 control-label">Nama Lengkap <span class="require">*</span></label>
                      <div class="col-lg-7">
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <input type="text" class="form-control" id="firstname" name="prm_nama_lengkap">
                          </div>
                      </div>
                  </div>
              <div class="form-group">
                 <label for="firstname" class="col-lg-5 control-label">NIP </label>
                 <div class="col-lg-7">
                     <div class="input-icon right">
                         <i class="fa"></i>
                            <input type="text" class="form-control" name="prm_nip">
                     </div>
                 </div>
              </div>

            <div class="form-group">
                  <label for="firstname" class="col-lg-5 control-label">Unit</label>
                  <div class="col-lg-7">
                      <div class="input-icon right">
                          <i class="fa"></i>
                       <input type="text" class="form-control" name="prm_unit">
                          </div>
                  </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-lg-5 control-label">Email <span class="require">*</span></label>
              <div class="col-lg-7">
                  <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="email" class="form-control" name="prm_email" id="email">
                  </div>
              </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-lg-5 control-label">No Telp<span class="require">*</span></label>
                   <div class="col-lg-7">
                       <div class="input-icon right">
                           <i class="fa"></i>
                           <input type="text" class="form-control" name="prm_no_telp" id="no_telp">
                       </div>
                   </div>
             </div>
          </fieldset>
          <fieldset>
            <legend>Akun User</legend>
              <div class="form-group">
                  <label for="username" class="col-lg-5 control-label">Username <span class="require">*</span></label>
                  <div class="col-lg-7">
                      <div class="input-icon right">
                          <i class="fa"></i>
                          <input type="text" class="form-control" name="prm_username" id="username">
                      </div>
                  </div>
              </div>
            <div class="form-group">
              <label for="password" class="col-lg-5 control-label">Password <span class="require">*</span></label>
              <div class="col-lg-7">
                  <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="password" name="prm_password" class="form-control" id="password">
                  </div>
              </div>
            </div>
            <div class="form-group">
              <label for="confirm-password" class="col-lg-5 control-label">Konfirmasi Password <span class="require">*</span></label>
              <div class="col-lg-7">
                  <div class="input-icon right">
                      <i class="fa"></i>
                      <input type="password" class="form-control" name="prm_repassword" id="confirm-password">
                  </div>
              </div>
            </div>
              <%--<div class="form-group">
                  <label for="confirm-password" class="col-lg-5 control-label"></label>
                  <div class="col-lg-7">
                      <div class="input-icon right">
                          <i class="fa"></i>
                          <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
                          <div class="g-recaptcha" data-sitekey="6Lc93AgTAAAAAJuq9e9xuQKxRouhAjvK6L5vpKuz"></div>
                      </div>
                  </div>
              </div>--%>
          </fieldset>
          <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">
              <button type="submit" class="btn btn-primary">Buat Akun Baru</button>
              <a href="<c:url value="/account/login"/>" class="btn btn-default">Batal</a>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-4 col-sm-4 pull-right">
        <div class="form-info">
          <h2>Informasi <em>Penting</em></h2>
          <p>Isilah Data di samping dengan lengkap dan Data pribadi sesuai dengan data yang berada di KTP</p>
          <p>Gunakanlah Email yang valid dan No telfon yang masih bisa di hubungi</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END CONTENT -->
