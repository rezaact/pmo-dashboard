<%--
  Created by IntelliJ IDEA.
  User: Icon
  Date: 12/31/2016
  Time: 10:01 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="modal fade" id="detail-asset-capacity-pembangkit-saat-ini" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="judul">Detail Pembangkit Saat Ini</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#actual">Actual</a></li>
                            <li><a data-toggle="tab" href="#kapasitas-terpasang">Kapasitas Terpasang</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="actual" class="tab-pane fade in active">
                                <div class="table-responsive">
                                    <table id="table-actual" class="table">
                                        <thead>
                                        <tr>
                                            <th>Region</th>
                                            <th>Tahun</th>
                                            <th>Bulan</th>
                                            <th>Capacity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="kapasitas-terpasang" class="tab-pane fade in">
                                <div class="table-responsive">
                                    <table id="table-kapasitas-terpasang" class="table">
                                        <thead>
                                        <tr>
                                            <th>Region</th>
                                            <th>Tahun</th>
                                            <th>Bulan</th>
                                            <th>Capacity</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL --%>