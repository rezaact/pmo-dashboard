<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->
<div class="portlet-body">
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#Progress" data-toggle="tab" aria-expanded="true"> Progress </a>
        </li>
        <li class="">
            <a href="#Elektrifikasi" data-toggle="tab" aria-expanded="false"> Elektrifikasi </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="Progress">
            <br>
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"> FILTER </a>
                        </h4>
                    </div>
                    <div id="collapse_3_1" class="panel-collapse collapse">
                        <div class="panel-body">
                                <!-- Content collapes -->
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <!-- Content filter -->
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label> Jenis Program </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-12 col-xs-12">

                                                        <select id="cmbGroup" class="cmbGroup form-control">
                                                            <option value="">--All Program--</option>
                                                            <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">

                                                                <c:if test="${dataComboProgram.ASSETGROUPID == 19}">
                                                                    <option value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                                                                </c:if>

                                                                <c:if test="${dataComboProgram.ASSETGROUPID == 22}">
                                                                    <option selected value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                                                                </c:if>
                                                            </c:forEach>
                                                        </select>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label><input id="RKapasitasSD" name="Kapasitas" class="PilihKapasitas" type="radio" value="3"> Kapasitas sampai dengan </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <div class="input-group date date-picker" data-date-minviewmode="months">
                                                            <input id="DateNowStart" type="text" class="KapasitasSD form-control">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row FilterKapasitasRUPTL">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label><input id="RKapasitasBD" name="Kapasitas" type="radio" class="PilihKapasitas" value="2"> Kapasitas berjalan berdasarkan</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <select id="CmbMonths" class="KapasitasBD form-control" disabled="">
                                                            <option value="1">Bulan</option>
                                                            <option value="2">Tahun</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-1 col-sm-12 col-xs-12">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <div class="input-group date date-picker" data-date-minviewmode="months">
                                                            <input id="DateRunningPeriodeMonth" type="text" class="form-control KapasitasBD" disabled="">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn default DateRunningPeriodeMonth_img" type="button">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                        </div>
                                                        <div class="input-group date date-picker" data-date-minviewmode="years">
                                                            <input id="DateRunningPeriodeYear" type="text" class="form-control KapasitasBD" disabled="" style="display: none;">
                                                                    <span class="input-group-btn">
                                                                        <button class="DateRunningPeriodeYear_img btn default" type="button" style="display: none;">
                                                                            <i class="fa fa-calendar"></i>
                                                                        </button>
                                                                    </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="row FilterKapasitasRUPTL">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label><input name="Kapasitas" type="radio" class="PilihKapasitas" value="1"> Kapasitas periode tertentu</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-7 col-sm-12 col-xs-12">
                                                <div class="row">
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <div class="input-group date date-picker KapasitasPT" data-date-minviewmode="months">
                                                            <input id="DateStartPeriode" type="text" class="form-control KapasitasPT" disabled="">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-12 col-xs-12">
                                                        <div>
                                                            S.D.
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                                        <div class="input-group date date-picker KapasitasPT" data-date-minviewmode="months">
                                                            <input id="DateEndPeriode" type="text" class="form-control KapasitasPT" disabled="">
                                                                <span class="input-group-btn">
                                                                    <button class="btn default" type="button">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </button>
                                                                </span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-7">
                                                <div class="btn-group">
                                                    <button id="Btn_FilterAll" class="btn-sm btn btn-danger" type="button" title="Preview" tabindex="-1" data-provider="bootstrap-markdown" data-handler="bootstrap-markdown-cmdPreview" data-hotkey="Ctrl+P" data-toggle="button">
                                                        <span class="glyphicon glyphicon-search"></span> Submit</button>
                                                </div>
                                                <div class="btn-group">&nbsp;&nbsp;&nbsp;<a class="AdvancedFilter" href="#">Advanced Filter</a></div>
                                            </div>
                                        </div>



                                        <!-- end Content filter -->


                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                            <!-- end Content collapes -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4" style="padding-right: 5px; padding-left: 5px;">
                    <!-- Content type -->
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Generation</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Kapasitas </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-xs-5 col-sm-5">
                                        <div class="input-group">
                                            <strong id="Generation-lbl-kapasitas">..</strong> <strong> MW</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row FilterKapasitasRUPTLPeriode">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Progress s-curve </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 ">
                                        <div class="input-group">
                                            <strong id="Generation-lbl-progress"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active tabGeneration-Region">
                                        <a href="#Generation-Region" data-toggle="tab"> Region </a>
                                    </li>
                                    <li class="tabGeneration-Type">
                                        <a href="#Generation-Type" data-toggle="tab"> Type </a>
                                    </li>
                                    <li class="FilterKapasitasRUPTLPeriode tabGeneration-Phase">
                                        <a href="#Generation-Phase" data-toggle="tab"> Phase </a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Generation-Region">

                                        <form id="form-main-generation-region" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>

                                        <div style="text-align: center" id="Generation-Region-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Generation-Type">
                                        <form id="form-main-generation-type" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button  class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Generation-Type-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Generation-Phase">
                                        <form id="form-main-generation-phase" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Generation-Phase-ChartContainer"></div>
                                    </div>

                                </div>
                                <div class="clearfix margin-bottom-20"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="padding-right: 5px; padding-left: 5px;">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Transmission</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Kapasitas </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 ">
                                        <div class="input-group">
                                            <strong id="Transmission-lbl-kapasitas">..</strong> <strong> KMS</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row FilterKapasitasRUPTLPeriode">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Progress s-curve </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <div class="input-group">
                                            <strong id="Transmission-lbl-progress"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active tabTransmission-Region">
                                        <a href="#Transmission-Region" data-toggle="tab"> Region </a>
                                    </li>
                                    <li class="tabTransmission-Type">
                                        <a href="#Transmission-Type" data-toggle="tab"> Type </a>
                                    </li>
                                    <li class="FilterKapasitasRUPTLPeriode tabTransmission-Phase">
                                        <a href="#Transmission-Phase" data-toggle="tab"> Phase </a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Transmission-Region">
                                        <form id="form-main-transmission-region" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Transmission-Region-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Transmission-Type">
                                        <form id="form-main-transmission-type" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Transmission-Type-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Transmission-Phase">
                                        <form id="form-main-transmission-phase" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Transmission-Phase-ChartContainer"></div>
                                    </div>

                                </div>
                                <div class="clearfix margin-bottom-20"> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="padding-right: 5px; padding-left: 5px;">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Subtation</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Kapasitas </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 ">
                                        <div class="input-group">
                                            <strong id="Subtation-lbl-kapasitas">..</strong> <strong> MVA</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row FilterKapasitasRUPTLPeriode">
                                    <div class="col-md-7 col-sm-7 col-xs-7 ">
                                        <div class="form-group">
                                            <strong> Progress s-curve </strong>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5 ">
                                        <div class="input-group">
                                            <strong id="Subtation-lbl-progress"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="portlet-body">
                                <ul class="nav nav-tabs">
                                    <li class="active tabSubtation-Region">
                                        <a href="#Subtation-Region" data-toggle="tab"> Region </a>
                                    </li>
                                    <li class="tabSubtation-Type">
                                        <a href="#Subtation-Type" data-toggle="tab"> Type </a>
                                    </li>
                                    <li class="FilterKapasitasRUPTLPeriode tabSubtation-Phase">
                                        <a href="#Subtation-Phase" data-toggle="tab"> Phase </a>
                                    </li>

                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Subtation-Region">
                                        <form id="form-main-substation-region" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Subtation-Region-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Subtation-Type">
                                        <form id="form-main-substation-type" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Subtation-Type-ChartContainer"></div>
                                    </div>
                                    <div class="tab-pane fade" id="Subtation-Phase">
                                        <form id="form-main-substation-phase" method="post" action="<c:url value="/dashboard-ruptl-capacity/detail"/>">
                                            <div class="form-group" align="right">
                                                <button class="btn-xs btn btn-danger  btn-submit" type="submit">
                                                    <span class="fa fa-list"></span> Detail
                                                </button>
                                            </div>
                                        </form>
                                        <div style="text-align: center" id="Subtation-Phase-ChartContainer"></div>
                                    </div>

                                </div>
                                <div class="clearfix margin-bottom-20"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="tab-pane fade" id="Elektrifikasi">
            <br>
            <div class="panel-group accordion" id="filter-elektrifikasi">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#filter-elektrifikasi" href="#content-elektrifikasi"> FILTER </a>
                        </h4>
                    </div>
                    <div id="content-elektrifikasi" class="panel-collapse collapse">
                        <div class="panel-body">
                            <!-- Content collapes -->
                            <div class="panel-body">
                                <form class="form-horizontal">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 -col-xs-12">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12" >
                                                        <div class="">
                                                            <div class="radio">
                                                                <label><input name="radio-status-elektrifikasi" class="type-filter-elektrifikasi" type="radio" value="1" > Rasio Elektrifikasi </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label><input name="radio-status-elektrifikasi" class="type-filter-elektrifikasi" type="radio" value="2"> Desa Berlistrik </label>
                                                            </div>
                                                            <div class="radio">
                                                                <label><input name="radio-status-elektrifikasi" class="type-filter-elektrifikasi" type="radio" value="3"> Rencana Mengaliri Desa </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-8 col-sm-12 -col-xs-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label class="status-elektrifikasi"> Status </label>
                                                        <label class="program-elektrifikasi"> Program </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <select id="status-elektrifikasi" class="form-control status-elektrifikasi">
                                                    <option value="1"> Berlistrik </option>
                                                    <option value="2"> Tidak Berlistrik </option>
                                                </select>
                                                <select id="program-elektrifikasi" class="form-control program-elektrifikasi">
                                                    <option value="0"> All Program </option>
                                                    <option value="1"> Lisdes Reguler </option>
                                                    <option value="2"> Program 2510 </option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <label> Tahun </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group date date-picker" data-date-minviewmode="years">
                                                    <input id="filter-elektrifikasi-year" type="text" class="form-control">
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4 hidden-sm hidden-xs">

                                                    <div class="btn-group">
                                                        <button id="submit-elektrifikasi" class="btn-sm btn btn-danger" type="button" title="Preview">
                                                            <span class="glyphicon glyphicon-search"></span> Submit</button>
                                                    </div>


                                                    <%--<div class="form-group">--%>
                                                        <%--<div class="col-sm-offset-2 col-sm-10">--%>
                                                            <%--<button type="submit" class="btn btn-default">Submit</button>--%>
                                                        <%--</div>--%>
                                                    <%--</div>--%>
                                            </div>

                                        </div>


                                    </div>



                                </div>

                                <div class="row">
                                    <div class="col-md-12 hidden-md hidden-lg">
                                        <div class="btn-group">
                                            <button id="submit-elektrifikasi-large" class="btn-sm btn btn-danger" type="button" title="Preview">
                                                <span class="glyphicon glyphicon-search"></span> Submit</button>
                                        </div>
                                    </div>
                                </div>
                                </form>

                            </div>
                            <!-- end Content collapes -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h4 id="label-map-rasio-elektrifikasi">Rasio Elektrifikasi 2017 Nasional 98.72 %</h4>
                            </div>
                        </div>
                        <div id="map-elektrifikasi"></div>
                    </div>
                </div>
            </div>






        </div>
    </div>
</div>


<%--modal--%>
<div id="modal-ruptl" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <h5>Tidak Ada Data Yang Tersedia</h5>
            </div>
            <div class="modal-footer">
                <button type="button" id="close-modal" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<!-- END CONTENT -->
