<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal modal-success fade" id="modal-welcome" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="margin: 0 !important; font-size: x-large">Selamat Datang di E-Catalog Pusharlis</h4>
            </div>
            <div class="modal-body">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <c:forEach var="produkSplashScreen" items="${listSpashScreen}" varStatus="number">
                            <li data-target="#carousel-example-generic" data-slide-to="${produkSplashScreen.URUTAN}"
                                    <c:if test="${produkSplashScreen.URUTAN == 1}">class="active"</c:if>>
                            </li>
                        </c:forEach>
                        <%--<li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>--%>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <c:forEach var="produkSplashScreen" items="${listSpashScreen}" varStatus="number">
                            <c:choose>
                                <c:when test="${produkSplashScreen.URUTAN == 1}"><div class="item active"></c:when>
                                <c:otherwise><div class="item"></c:otherwise>
                            </c:choose>
                                <a href="<c:url value="/product/product-detail?id=${produkSplashScreen.NOMOR_PRODUK}"/>">
                                <img src="<c:url value="/file/${produkSplashScreen.ID_FILE}"/>" alt="${produkSplashScreen.DESKRIPSI}" style="width: 100%;">
                                <div class="carousel-caption">
                                    <p> ${produkSplashScreen.DESKRIPSI}</p>
                                </div>
                                </a>
                            </div>
                        </c:forEach>
                        <%--<div class="item active">
                            <img src="<c:url value="/file/10"/>" alt="..." style="width: 100%;">
                            <div class="carousel-caption">
                                desk 01
                            </div>
                        </div>
                        <div class="item">
                            <img src="<c:url value="/file/11"/>" alt="...">
                            <div class="carousel-caption">
                                desk 02
                            </div>
                        </div>
                        <div class="item">
                            <img src="http://placehold.it/350x150" alt="...">
                            <div class="carousel-caption">
                                desk 03
                            </div>
                        </div>--%>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

