<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN SLIDER -->
<tiles:insertAttribute name="home-slider"/>
<!-- END SLIDER -->

<div class="main">
    <div class="container">

        <!-- BEGIN SERVICE BOX -->
        <tiles:insertAttribute name="home-service"/>
        <!-- END SERVICE BOX -->

        <!-- BEGIN STEPS -->
        <tiles:insertAttribute name="home-step"/>
        <!-- END STEPS -->

        <!-- BEGIN RECENT WORKS -->
        <tiles:insertAttribute name="home-recent-work"/>
        <!-- END RECENT WORKS -->

        <!-- BEGIN CLIENTS -->
        <tiles:insertAttribute name="home-client"/>
        <!-- END CLIENTS -->
    </div>
</div>