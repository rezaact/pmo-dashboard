<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row recent-work margin-bottom-40">
    <div class="col-md-3">
        <h2><a href="portfolio.html">Recent Works</a></h2>
        <p>Mendukung Pekerjaan pemeliharaan / perbaikan atas unit-unit pembangkit di Wilayah Sumatera Bagian Utara (Sektor di lingkungan PLN KITSBU) dan Jawa Bagian Barat (Suralaya, Muara Tawar, Cilegon, Teluk Naga, Labuan, Pelabuhan Ratu).</p>
    </div>
    <div class="col-md-9">
        <div class="owl-carousel owl-carousel3">

            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img2.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img2.jpg'/>" class="fancybox-button" title="Project Name #2" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Perbaikan Trafo Distribusi</strong>
                    <b>Agenda Perbaikan Trafo Distribusi</b>
                </a>
            </div>
            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img4.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img4.jpg'/>" class="fancybox-button" title="Project Name #4" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Pembuatan Unit Gardu Bergerak</strong>
                    <b>Agenda Agenda Pembuatan Unit Gardu Bergerak</b>
                </a>
            </div>
            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img5.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img5.jpg'/>" class="fancybox-button" title="Project Name #5" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Pembuatan VLCB</strong>
                    <b>Agenda Agenda Pembuatan VLCB</b>
                </a>
            </div>
            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img6.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img6.jpg'/>" class="fancybox-button" title="Project Name #6" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Submerged Scrapper Conveyor</strong>
                    <b>Agenda Submerged Scrapper Conveyor PLTU</b>
                </a>
            </div>
            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img1.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img1.jpg'/>" class="fancybox-button" title="Project Name #1" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Perbaikan Trafo Distribusi</strong>
                    <b>Agenda Perbaikan Trafo Distribusi</b>
                </a>
            </div>
            <div class="recent-work-item">
                <em>
                    <img src="<c:url value='/assets/frontend/pages/img/works/img3.jpg'/>" alt="Amazing Project" class="img-responsive">
                    <a href="portfolio-item.html"><i class="fa fa-link"></i></a>
                    <a href="<c:url value='/assets/frontend/pages/img/works/img3.jpg'/>" class="fancybox-button" title="Project Name #3" data-rel="fancybox-button"><i class="fa fa-search"></i></a>
                </em>
                <a class="recent-work-description" href="#">
                    <strong>Pembuatan Unit Gardu Bergerak</strong>
                    <b>Agenda Pembuatan Unit Gardu Bergerak</b>
                </a>
            </div>
        </div>
    </div>
</div>