<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN SIDEBAR -->
<div class="sidebar col-md-3 col-sm-3">
  <ul class="list-group margin-bottom-25 sidebar-menu">
    <%--<li class="list-group-item clearfix"><a href="<c:url value="/account/register"/> "><i class="fa fa-angle-right"></i> Pendaftaran</a></li>--%>
    <%--<li class="list-group-item clearfix"><a href="<c:url value="/account/login"/> "><i class="fa fa-angle-right"></i> Login</a></li>--%>
    <%--<li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Lupa Password</a></li>--%>
    <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
      <%--<li class="list-group-item clearfix"><a href="#"><i class="fa fa-angle-right"></i> Akun Saya</a></li>--%>
    </c:if>
  </ul>
</div>
<!-- END SIDEBAR -->