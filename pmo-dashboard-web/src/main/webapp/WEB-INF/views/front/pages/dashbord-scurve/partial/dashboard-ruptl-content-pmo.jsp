<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
<div class="col-md-12">


<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">GRAFIK RUPTL S-CURVE</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="">Periode</label>

                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker" data-date-minviewmode="months">
                                    <input id="periode-start" type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-12 col-xs-12 text-center">
                                    S.D.
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div class="input-group date date-picker" data-date-minviewmode="months">
                                    <input id="periode-end" type="text" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                </div>
                            </div>

                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="">Region</label>
                        <div class="col-md-4">
                            <select id="region" class="form-control">
                                <option value="0">--Choose Region--</option>
                                <c:forEach var="dataComboRegion" items="${listComboRegion}" varStatus="number">
                                    <option value="${dataComboRegion.ID}">${dataComboRegion.STRINGVALUE}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="">Program</label>
                        <div class="col-md-4">
                            <select id="program" class="form-control">
                                <option value="">--Choose Program--</option>
                                <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">

                                    <c:if test="${dataComboProgram.ASSETGROUPID == 19}">
                                        <option value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                                    </c:if>

                                    <c:if test="${dataComboProgram.ASSETGROUPID == 22}">
                                        <option selected value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                                    </c:if>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-4">
                            <button id="filter-all" class="btn btn-danger" ><span class="glyphicon glyphicon-search"></span> Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <hr/>
        <!-- Navigasi -->
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#generation">Generation</a></li>
            <li><a data-toggle="tab" href="#transmission">Transmission</a></li>
            <li><a data-toggle="tab" href="#sub-station">Sub Station</a></li>
        </ul>

        <div class="tab-content">
            <div id="generation" class="tab-pane fade in active">
                <div id="chartGeneration"></div>
            </div>
            <div id="transmission" class="tab-pane fade">
                <div id="chart-transmission"></div>
            </div>
            <div id="sub-station" class="tab-pane fade">
                <div id="chart-sub-station"></div>
            </div>
        </div>


    </div>
</div>


</div>
</div>

<!-- END CONTENT -->


<%--BEGIN MODAL TABLE DETAIL--%>
<!-- Modal -->
<div class="modal fade" id="modal-detail-scurve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="judul">Detailx</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">


                        <div class="table-responsive">
                            <table id="table-detail-scurve">
                                <thead>
                                <tr>
                                    <th>Asset Number</th>
                                    <th>Asset Name</th>
                                    <th>Type</th>
                                    <th>Region</th>
                                    <th>Kapasitas</th>
                                    <th>Milestone</th>
                                    <th>Progress Plan</th>
                                    <th>Progress Actual</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL --%>

