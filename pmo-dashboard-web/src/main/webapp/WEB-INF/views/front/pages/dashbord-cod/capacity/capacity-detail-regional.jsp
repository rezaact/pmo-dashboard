<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<script>
    jenis = '${jenis}';
    subJenis = '${subJenis}';
    tahunAkhir = '${tahunAkhir}';
    tahunAwal = '${tahunAwal}';
    detailType = '${detailType}';
    group = '${group}';
    AssetType = '${AssetType}';
    strChart_All = '${strChart_All}';

    <%--CmbFilterIsActual = '${CmbFilterIsActual}';--%>
</script>

<div class="row">
    <div class="col-md-12">

        <!-- Navigasi -->
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#generation">Pie Chart</a></li>
            <li><a data-toggle="tab" href="#transmission">Data Grid</a></li>
        </ul>

        <div class="tab-content">
            <div id="generation" class="tab-pane fade in active">
                <div class="table-responsive">

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="detail-title-header">Chart</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="text-align: center" id="chartContainer-regional"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div id="transmission" class="tab-pane fade in">
                <div class="table-responsive">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="detail-title">Generation Capacity Detail</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table id="table-capacity-detail" class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Regional</th>
                                                <th>No</th>
                                                <th>Asset Number</th>
                                                <th>Nama Pembangkit</th>
                                                <th>Type Pembangkit</th>
                                                <th width="5%">Kapasitas</th>
                                                <th style="text-align: center">COD</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <a href='<c:url value="/dashboard-cod-capacity"/>' class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>
        <%--<a href="#" id="back" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Back</a>--%>
    </div>
</div>
<!-- END CONTENT -->