<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
    <div class="col-md-3">
        <h2>PLN PMO DASHBOARD</h2>
        <p style="text-align:justify;text-justify:auto;">
            Program Management Office (PMO) merupakan sistem dan program pengelolaan proyek untuk mendapatkan gambaran yang terkonsolidasi  terhadap seluruh proyek ketenagalistrikan guna meningkatan kinerja proyek, optimasi penggunaan sumber daya, komunikasi dan pelaporan data yang lebih efektif.
            Halaman ini merupakan dashboard yang digunakan untuk memantau status proyek atau asset pada PMO.
        </p>

    </div>
  <div class="col-md-3">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">COD</h3>
        </div>
        <div class="panel-body" style="height: 450px">
            <img src="<c:url value='/assets/frontend/layout/img/logos/bar.png'/>" style="height: 200px; width: 100%; display: block;">
            <p style="margin-top: 10px;text-align:justify;text-justify:auto;">
                Menampilkan data dari asset yang akan commercial operation date (COD). Terdapat data total kapasitas asset dan banyak asset yang COD. Data ditampilkan dalam bentuk multi years atau single year.
            </p>
            <p>
<%--
                <a class="btn btn-default" href="<c:url value="/dashboard-cod/tahunan"/>" role="button">TAHUNAN</a>
                <a class="btn btn-default" href="<c:url value="/dashboard-cod/bulanan"/>" role="button">BULANAN</a>
--%>
                <a class="btn btn-default" href="<c:url value="/dashboard-cod/estimate"/>" role="button">VIEW DETAIL</a>

            </p>

        </div>
    </div>
  </div>
  <div class="col-md-3">
      <div class="panel panel-success">
          <div class="panel-heading">
              <h3 class="panel-title">S-CURVE</h3>
          </div>
          <div class="panel-body" style="height: 450px">
              <img src="<c:url value='/assets/frontend/layout/img/logos/s-curve.jpg'/>" style="height: 200px; width: 100%; display: block;">
              <p style="margin-top: 10px;text-align:justify;text-justify:auto;">
                  Dashboard ini menampilkan perbandingan data perencanaan dan aktual milestone asset dalam bentuk S-Curve. Data didapat dari pembobotan milestone asset yang telah disepakati oleh PLN dan Direktorat Jenderal Ketenagalistrikan (DJK)
              </p>
              <p>
                  <%--<a class="btn btn-default" href="<c:url value="/dashboard-scurve/ruptl"/>" role="button">RUPTL </a>--%>

                  <%--<a class="btn btn-default" href="<c:url value="/dashboard-scurve/supply"/>" role="button">VIEW DETAIL</a>--%>
                  &nbsp;

              </p>

          </div>
      </div>
  </div>
  <div class="col-md-3">
      <div class="panel panel-success">
          <div class="panel-heading">
              <h3 class="panel-title">MILESTONE </h3>

          </div>
          <div class="panel-body" style="height: 450px">
              <img src="<c:url value='/assets/frontend/layout/img/logos/donut.png'/>" style="height: 200px; width: 200px; display: block;margin-left: auto;margin-right: auto;">
              <p style="margin-top: 10px;text-align:justify;text-justify:auto;">
                  Menampilkan data milestone-milestone proyek yang sedang berlangsung. Data yang ditampilkan berbentuk donut chart.
              </p>
              <p>
                  &nbsp;
                  <%--<a class="btn btn-default" href="<c:url value="/dashboard-milestone"/>" role="button">VIEW DETAIL</a>--%>
              </p>

          </div>
      </div>
  </div>

</div>

<!-- END CONTENT -->

