<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PROFILE SIDEBAR -->
<div class="container">
  <div class="row">
  <!-- PORTLET MAIN -->
  <div class="col-md-6 col-sm-6 portlet light profile-sidebar-portlet">
    <!-- SIDEBAR USERPIC -->
    <div class="profile-userpic">
      <img src="<c:url value="/assets/admin/pages/media/profile/profile_user.jpg"/>" class="img-responsive" alt="">
    </div>
    <!-- END SIDEBAR USERPIC -->
    <!-- SIDEBAR USER TITLE -->
    <div class="profile-usertitle">
      <div class="profile-usertitle-name">
          ${authUser.getNamaLengkap()}

      </div>
      <div class="profile-usertitle-job">
        ${authUser.getNomorTelp()}
      </div>
    </div>
    <!-- END SIDEBAR USER TITLE -->
  </div>
  <!-- END PORTLET MAIN -->
  <!-- PORTLET MAIN -->
  <div class="col-md-6 col-sm-6 portlet light">
    <div>
      <h4 class="profile-desc-title">Profil ${authUser.getNamaLengkap()}</h4>
      <span class="profile-desc-text">Email : ${authUser.getEmail()}  </span>
      <div class="margin-top-20 profile-desc-link">
        <i class="fa fa-globe"></i>
        <a href="#"></a>
      </div>
      <div class="margin-top-20 profile-desc-link">
        <i class="fa fa-twitter"></i>
        <a href="#"></a>
      </div>
      <div class="margin-top-20 profile-desc-link">
        <i class="fa fa-facebook"></i>
        <a href="#"></a>
      </div>
    </div>
  </div>
  <!-- END PORTLET MAIN -->
</div>
</div>
<!-- END BEGIN PROFILE SIDEBAR -->