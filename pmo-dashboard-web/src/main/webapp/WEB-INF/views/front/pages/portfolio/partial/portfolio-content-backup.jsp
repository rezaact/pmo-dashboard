<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="content-page">
  <div class="filter-v1">
    <ul class="mix-filter">
      <li data-filter="all" class="filter active">All</li>
      <li data-filter="category_1" class="filter">Portfolio 1</li>
      <li data-filter="category_2" class="filter">Portfolio 2</li>
      <li data-filter="category_3" class="filter">Portfolio 3</li>
      <li data-filter="category_3 category_1" class="filter">Portfolio 4</li>
    </ul>
    <div class="row mix-grid thumbnails">
      <div class="col-md-3 col-sm-4 mix category_1 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_2 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_3 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_1 category_2 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img4.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img4.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_2 category_1 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img5.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img5.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_1 category_2 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img6.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img6.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_2 category_3 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_1 category_2 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_3 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img4.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img4.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-sm-4 mix category_1 mix_all" style="display: block; opacity: 1; ">
        <div class="mix-inner">
          <img alt="" src="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/>" class="img-responsive">
          <div class="mix-details">
            <h4>Lihat Gambar Secara Detail</h4>
            <a class="mix-link"><i class="fa fa-link"></i></a>
            <a data-rel="fancybox-button" title="Project Name" href="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/>" class="mix-preview fancybox-button"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>