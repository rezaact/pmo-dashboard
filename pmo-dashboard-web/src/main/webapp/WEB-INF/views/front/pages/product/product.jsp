<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="main">
  <div class="container">
        <%--BEGIN PRODUCT SIDEBAR LEFT--%>
        <tiles:insertAttribute name="product-sidebar-left"/>
        <%--END PRODUCT SIDEBAR LEFT--%>

        <%--BEGIN PRODUCT CONTENS--%>
            <div id="fillContainer">
        <tiles:insertAttribute name="product-content-list" ignore="true"/>
        <tiles:insertAttribute name="product-content-grid" ignore="true"/>
        <tiles:insertAttribute name="product-content-detail" ignore="true"/>
            </div>
        <%--END PRODUCT CONTENS--%>
  </div>
</div>