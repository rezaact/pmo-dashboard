<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
<div class="col-md-12">


<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">GRAFIK COD TAHUNAN</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Region</label>
                    <select class="CmbRegion form-control OnFilterAll">
                        <option value="0">--Choose Region--</option>
                        <c:forEach var="dataComboRegion" items="${listComboRegion}" varStatus="number">
                            <option value="${dataComboRegion.ID}">${dataComboRegion.STRINGVALUE}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="form-group">
                    <label>Ownership</label>
                    <select class="CmbOwnership form-control OnFilterAll">
                        <%--<option value="0">--Choose Ownership--</option>--%>
                        <%--<c:forEach var="dataComboOwnership" items="${listComboOwnership}" varStatus="number">--%>
                            <%--<option value="${dataComboOwnership.ID}">${dataComboOwnership.VALUESTRING}</option>--%>
                        <%--</c:forEach>--%>
                        <option value="0">--Choose Ownership--</option>
                        <option value="1">IPP</option>
                        <option value="2">PLN</option>

                    </select>
                </div>


            </div>

            <div class="col-md-3">

                <div class="form-group">
                    <label>Start Period</label>
                    <div class="input-group input-medium date date-picker" data-date-minviewmode="months">
                        <input type="text" class="starperiod_tahunan form-control" value="2016">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label>End Period</label>
                    <div class="input-group input-medium date date-picker" data-date-minviewmode="months">
                        <input type="text" class="endperiod_tahunan form-control" value="2017">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>

            </div>

            <div class="col-md-6">

                <div class="form-group">
                    <label>Program</label>
                    <select class="CmbProgram form-control OnFilterAll">
                        <option value="0">--Choose Program--</option>
                        <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">
                            <option value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                        </c:forEach>
                    </select>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>COD Plan</label>
                            <select class="CmbPlan form-control OnFilterAll">
                                <option value="1">RUPTL COD</option>
                                <option value="2">Estimated COD</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>COD Actual</label>
                            <select class="CmbActual form-control OnFilterAll">
                                <option value="1">Actual COD</option>
                                <option value="2">Contractual COD</option>
                            </select>
                        </div>
                    </div>
                </div>



<%--                <div class="form-group">
                    <label>Type</label>
                    <select class="CmbType form-control OnFilterAll">
                        <option value="0">--Choose Type--</option>
                        <c:forEach var="dataComboType" items="${listComboType}" varStatus="number">
                            <option value="${dataComboType.ID}">${dataComboType.STRINGVALUE}</option>
                        </c:forEach>
                    </select>
                </div>--%>



            </div>

        </div>

        <!-- Navigasi -->
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#Generation">Generation</a></li>
            <li><a data-toggle="tab" href="#Transmission">Transmission</a></li>
            <li><a data-toggle="tab" href="#Substation">Substation</a></li>
        </ul>

        <div class="tab-content">
            <div id="Generation" class="tab-pane fade in active">
                <p><div id="DivChartCODTahunanGeneration"></div></p>
            </div>
            <div id="Transmission" class="tab-pane fade">
                <p><div id="DivChartCODTahunanTransmission"></div></p>
            </div>
            <div id="Substation" class="tab-pane fade">
                <p><div id="DivChartCODTahunanSubstation"></div></p>
            </div>
        </div>

    </div>
</div>


</div>
</div>

<!-- END CONTENT -->

<%--BEGIN MODAL DETAIL COD--%>
<!-- Modal -->
<div class="modal fade" id="detail-cod-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detail</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <table>
                            <tr>
                                <td width="30%">Previous Plan Capacity</td>
                                <td width="10%"> : </td>
                                <td width="30%"><span id="plan-prev"></span></td>
                            </tr>
                            <tr>
                                <td>Current Plan Capacity</td>
                                <td> : </td>
                                <td><span id="plan-cur"></span></td>
                            </tr>
                            <tr>
                                <td>Total Plan Capacity</td>
                                <td> : </td>
                                <td><span id="plan-total"></span></td>
                            </tr>

                            <tr>
                                <td>Previous Actual Capacity</td>
                                <td> : </td>
                                <td><span id="act-prev"></span></td>
                            </tr>

                            <tr>
                                <td>Current Actual Capacity</td>
                                <td> : </td>
                                <td><span id="act-cur"></span></td>
                            </tr>

                            <tr>
                                <td>Total Actual Capacity</td>
                                <td> : </td>
                                <td><span id="act-total"></span></td>
                            </tr>


                        </table>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL --%>

<%--BEGIN MODAL TABLE DETAIL--%>
<!-- Modal -->
<div class="modal fade" id="detail-asset-capacity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="margin-top: 100px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" class="myModalLabel">Detail</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <table>
                            <thead>
                                <tr>
                                    <th>Asset ID</th>
                                    <th>RUPTL COD</th>
                                    <th>Asset Group Name</th>
                                    <th>Asset Ownership</th>
                                    <th>Region Code</th>
                                    <th>Plan Capacity</th>
                                    <th>Actual Capacity</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--END MODAL --%>



