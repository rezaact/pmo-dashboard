<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN CONTENT -->

<div class="row">
<div class="col-md-12">


<div class="panel panel-success">
    <div class="panel-heading">
        <h3 class="panel-title">GRAFIK GENERATION CAPACITY</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Program</label>

                    <select id="CmbProgram" class="CmbProgram form-control">
                        <c:forEach var="dataComboProgram" items="${listComboProgram}" varStatus="number">
                            <option ${(dataComboProgram.ASSETGROUPID =='13')? 'selected' : '' } value="${dataComboProgram.ASSETGROUPID}">${dataComboProgram.ASSETGROUPNAME}</option>
                        </c:forEach>
                    </select>


                </div>

                <div class="form-group">
                    <label>Ownership</label>
                    <select class="cmbOwnership form-control">
                        <option value="0">--Choose Ownership--</option>
                        <option value="1">IPP</option>
                        <option value="2">PLN</option>

                    </select>
                </div>

            </div>

            <div class="col-md-4">

                <div class="form-group">
                    <label>Region</label>
                    <select class="CmbRegion form-control">
                        <option value="0">--Choose Region--</option>
                        <c:forEach var="dataComboRegion" items="${listComboRegion}" varStatus="number">
                            <option value="${dataComboRegion.ID}">${dataComboRegion.STRINGVALUE}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label>Type Generation</label>
                    <select class="cmbTypePembangkit form-control">
                        <option value="0">--Choose Type--</option>
                        <c:forEach var="dataComboType" items="${listComboTypePembangkit}" varStatus="number">
                            <option value="${dataComboType.ID}">${dataComboType.STRINGVALUE}</option>
                        </c:forEach>
                    </select>
                </div>



            </div>

            <div class="col-md-4">

                <div class="form-group">
                    <label>Period</label>

                    <div class="input-group input-medium date date-picker" data-date-minviewmode="months">
                        <input id="from" type="text" class="starperiod form-control cmbDate" value="Jan 2016">
                        <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </span>
                    </div>
                </div>
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-group">
                            <button type="button" class="OnFilterAll btn btn-primary">Submit</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <!-- Chart -->
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#Generation">Generation</a></li>
<%--
            <li><a data-toggle="tab" href="#Transmission">Transmission</a></li>
            <li><a data-toggle="tab" href="#Substation">Substation</a></li>
--%>
        </ul>

        <div class="tab-content">
            <div id="Generation" class="tab-pane fade in active">
                <p>

                    <div class="panel panel-success">
                        <div class="panel-heading"> <h3 class="panel-title"></h3> </div>
                            <div class="panel-body">


                                <table class="table table-hover">
                                    <tbody>
                                        <tr> <th align="right" width="10%" scope="row"></th> <td width="50%">COD Pembangkit Bulan Sebelumnya</td> <td width="10%" align="right" id="tbl_sebelumnya"></td> <td>MW <a class="DivGet_Data_Detail_Bulan_Sebelumnya" style="color: #0e76a8" href="#"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a> </td> </tr>
                                        <tr> <th align="right" width="10%" scope="row"></th> <td width="50%">COD Pembangkit Bulan Berjalan</td> <td width="10%" align="right" id="tbl_berjalan"></td> <td>MW <a class="Div_Data_Detail_Bulan_Berjalan" style="color: #0e76a8" href="#"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a> </td> </tr>
                                        <tr> <th align="right" width="10%" scope="row"></th> <td width="50%"><b>Total</b></td> <td width="10%" align="right" id="tbl_total"><b></b></td> <td>MW </td> </tr>

                                    </tbody>
                                </table>

                                <table class="table table-hover">
                                    <tbody>
                                    <tr> <th align="right" width="10%" scope="row"></th> <td width="50%">Kapasitas Pembangkit saat ini</td> <td width="10%" align="right" id="tbl_saat_ini"></td> <td>MW <a class="DivGet_Data_Detail_Pembangkit_saat_ini" style="color: #0e76a8" href="#"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a></td> </tr>
                                    <tr> <th align="right" width="10%" scope="row"></th> <td width="50%">Beban Puncak</td> <td width="10%" align="right" id="tbl_beban_puncak"></td> <td>MW <a class="DivGet_Data_Detail_Beban_Puncak" style="color: #0e76a8" href="#"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></a></td> </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                </p>
            </div>
<%--
            <!-- Tabs -->
            <div id="Transmission" class="tab-pane fade">
                <p>
                <div id="DivTransmision"></div>
                </p>

            </div>
            <div id="Substation" class="tab-pane fade">
                <p>
                <div id="DivSubtation"></div>
                </p>

            </div>
--%>
        </div>
    </div>
</div>






</div>
</div>
<!-- END CONTENT -->


<%--BEGIN MODAL GEN CAPACITY DETAIL --%>
<tiles:insertAttribute name="dasboard-gen-capacity-modal-bulan-sebelumnya" ignore="true"/>
<%--<tiles:insertAttribute name="dasboard-gen-capacity-modal-bulan-berjalan" ignore="true"/>--%>
<%--<tiles:insertAttribute name="dasboard-gen-capacity-modal-pembangkit-saat-ini" ignore="true"/>--%>
<%--<tiles:insertAttribute name="dasboard-gen-capacity-modal-beban-puncak" ignore="true"/>--%>
