<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            <%--best sellers--%>Most Viewed
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="sidebar-products">
            <div class="scroller" style="height: 250px">
            <c:forEach var="produkBestSeller" items="${listProdukBestSeller}" varStatus="number">
                <div class="item">
                    <a href="<c:url value="/product/product-detail?id=${produkBestSeller.NOMOR_PRODUK}"/>">
                        <img src="<c:url value="/file/${produkBestSeller.ID_FILE}"/>" alt="best_seller">
                    </a>
                    <h3><a href="<c:url value="/product/product-detail?id=${produkBestSeller.NOMOR_PRODUK}"/>">${produkBestSeller.NAMA_PRODUK}</a></h3>
                    <%--<div class="price">Rp. ${produkBestSeller.HARGA_PRODUK}</div>--%>
                </div>
            </c:forEach>
            </div>
        </div>
    </div>
</div>