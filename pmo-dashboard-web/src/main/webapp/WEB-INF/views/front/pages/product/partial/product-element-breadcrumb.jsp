<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row list-view-sorting clearfix">
    <div class="col-md-2 col-sm-2 list-view">
        <label class="control-label">Lihat:</label>
        <a href="javascript:void(0);" onclick="clickTabGrid('<c:url value="/product/product-grid-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.currentPage}"/>');" class="btn"><i class="fa fa-th-large" style="font-size: 20px"></i></a>
        <a href="javascript:void(0);" onclick="clickTabList('<c:url value="/product/product-list-data?search=${searchUtils.searchText}&length=${searchUtils.searchLength}&paramdir=${searchUtils.searchDir}&page=${searchUtils.currentPage}"/>');" class="btn btn-active"><i class="fa fa-th-list" style="font-size: 20px"></i></a>
    </div>
    <div class="col-md-10 col-sm-10">
        <div class="pull-right">
            <label class="control-label">Tampilkan:</label>
            <select class="form-control input-sm" name="length" id="urutan" onchange="onChangeUrutan(this);">
                <option value="10" <c:if test="${searchUtils.searchLength eq 10}"> selected="selected" </c:if>>10</option>
                <option value="15"<c:if test="${searchUtils.searchLength eq 15}"> selected="selected" </c:if>>15</option>
                <option value="20"<c:if test="${searchUtils.searchLength eq 20}"> selected="selected" </c:if>>20</option>
                <option value="25"<c:if test="${searchUtils.searchLength eq 25}"> selected="selected" </c:if>>25</option>
                <option value="50"<c:if test="${searchUtils.searchLength eq 50}"> selected="selected" </c:if>>50</option>
            </select>
        </div>
        <div class="pull-right">
            <label class="control-label">Urutan berdasarkan:</label>    <%--id="product-order-by"--%>
            <select class="form-control input-sm" name="paramdir" id="paramdir" onchange="onChangeOrderBy(this);">
                <option value="default"<c:if test="${searchUtils.searchDir eq 'default'}"> selected="selected" </c:if>>Default</option>
                <option value="ASC"<c:if test="${searchUtils.searchDir eq 'ASC'}"> selected="selected" </c:if>>Name (A - Z)</option>
                <option value="DESC"<c:if test="${searchUtils.searchDir eq 'DESC'}"> selected="selected" </c:if>>Name (Z - A)</option>
            </select>
        </div>
    </div>
</div>

<input type="hidden" id="idKat" value="${searchUtils.idKategori}"/>
<input type="hidden" id="idJenis" value="${searchUtils.idJenisProduk}"/>
<input type="hidden" id="idKomponen" value="${searchUtils.idKomponen}"/>