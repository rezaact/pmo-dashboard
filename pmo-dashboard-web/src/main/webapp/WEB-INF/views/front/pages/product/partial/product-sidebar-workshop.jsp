<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
    <c:if test="${authUser.getRole().getAuthority() == 'ROLE_SECRET' || authUser.getRole().getAuthority() == 'ROLE_PRIVATE' || authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' }">
<div class="portlet light">
    <div class="portlet-title">
        <div class="caption">
            workshop
        </div>
        <div class="tools">
            <a href="javascript:void(0);" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="scroller" style="height:150px">
            <ul class="list-unstyled checkbox-list" id="sidebar-workshop">
            <c:forEach var="workshop" items="${listWorkshop}" varStatus="number">
                <li class="list-group-item">
                    <label class="checkbox-inline">
                        <input type="checkbox" id="id_workshop" value="${workshop.ID_WORKSHOP}">
                        <span class="text-workshop">${workshop.NAMA_WORKSHOP}</span>
                    </label>
                </li>
            </c:forEach>
            </ul>
        </div>
    </div>
</div>
    </c:if>
</c:if>