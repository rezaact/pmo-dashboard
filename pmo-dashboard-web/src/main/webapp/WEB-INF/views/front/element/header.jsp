<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN LOADING PAGE -->
<div class="modal_loading" style="display: none">
    <div class="center_loading">
        <img class="center_img_loading" src="<c:url value='/assets/global/img/loader.gif'/>">
    </div>
</div>
<!--- END LOADING PAGE -->

<!-- BEGIN TOP BAR -->
<div class="pre-header">
    <div class="container">
        <div class="row">
            <!-- BEGIN TOP BAR LEFT PART -->
            <div class="col-md-6 col-sm-6 additional-shop-info">
            </div>
            <!-- END TOP BAR LEFT PART -->
            <!-- BEGIN TOP BAR MENU -->
            <div class="col-md-6 col-sm-6 additional-nav">
                <ul class="list-unstyled list-inline pull-right">
                    <c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
                        <%--<li>Selamat Datang, <a href="<c:url value="../account/profile"/> "><strong class="text-capitalize"> ${authUser.getNamaLengkap()} </strong> </a></li>--%>
                        <li>Selamat Datang, <strong class="text-capitalize"> ${authUser.getNamaLengkap()} </strong></li>
                    </c:if>
                    <c:if test="${authUser.getClass().simpleName != 'AuthUser'}">

                        <%--<li><a href="<c:url value="/account/login"/>">Log In</a></li>--%>
                        <%--<li><a href="<c:url value="/account/register"/>">Pendaftaran</a></li>--%>
                        <%--<span class="username username-hide-on-mobile"> </span>--%>
                    </c:if>
                </ul>
            </div>
            <!-- END TOP BAR MENU -->
        </div>
    </div>
</div>
<!-- END TOP BAR -->

<!-- BEGIN HEADER -->
<div class="header">
    <div class="container">
        <a class="site-logo hidden-sm hidden-xs">
            <img src="<c:url value='/assets/frontend/layout/img/logos/logo.png'/>" alt="PMO" height="70px" width="auto">
        </a>
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <tiles:insertAttribute name="header-front-menu"/>
        <tiles:insertAttribute name="header-front-cart" ignore="true"/>
    </div>
</div>
<!-- Header END -->