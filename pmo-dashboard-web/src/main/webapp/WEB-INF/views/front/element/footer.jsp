<!-- BEGIN FOOTER -->
<div class="footer">
    <div class="container">
        <div class="row">
            <!-- BEGIN COPYRIGHT -->
            <div class="col-md-7 col-sm-7 padding-top-10">
                <span id="copyright-year"></span> &copy; PT PLN (Persero). ALL Rights Reserved. <%--<a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>--%>
            </div>
            <!-- END COPYRIGHT -->
            <!-- BEGIN PAYMENTS -->
            <div class="col-md-5 col-sm-5">
                <%--<ul class="social-footer list-unstyled list-inline pull-right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>--%>
            </div>
            <!-- END PAYMENTS -->
        </div>
    </div>
</div>
<!-- END FOOTER -->