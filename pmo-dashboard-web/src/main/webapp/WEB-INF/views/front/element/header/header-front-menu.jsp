<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN NAVIGATION -->
<c:if test="${authUser.getClass().simpleName == 'AuthUser'}">
    <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_OPERATOR'}">
        <div class="header-navigation font-transform-inherit pull-right">
            <ul>
                <li class="menu-dropdown mega-menu-dropdown ">
                    <%--<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="<c:url value="/dashboard-ruptl-capacity"/>" class="dropdown-toggle"> Dashboard COD--%>
                    <a href="<c:url value="/dashboard-ruptl-capacity"/>" class="dropdown-toggle"> Dashboard COD
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <%--<ul class="dropdown-menu" style="min-width: 150px">--%>
                        <%--<li>--%>
                            <%--<div class="mega-menu-content">--%>
                                <%--<div class="row">--%>
                                    <%--<div class="col-md-4">--%>
                                        <%--<ul class="mega-menu-submenu">--%>
                                                <%--&lt;%&ndash;--%>
                                                                                    <%--<li>--%>
                                                                                        <%--<a href="<c:url value="/dashboard-cod/tahunan"/>" class="iconify">--%>
                                                                                            <%--<i class="icon-paper-clip"></i> COD Tahunan </a>--%>
                                                                                    <%--</li>--%>

                                                                                    <%--<li>--%>
                                                                                        <%--<a href="<c:url value="/dashboard-cod/bulanan"/>" class="iconify">--%>
                                                                                            <%--<i class="icon-puzzle"></i> COD Bulanan </a>--%>
                                                                                    <%--</li>--%>
                                                <%--&ndash;%&gt;--%>


                                            <%--<li>--%>
                                                <%--<a href="<c:url value="/dashboard-ruptl-capacity"/>" class="iconify">--%>
                                                    <%--<i class="icon-puzzle"></i> RUPTL Capacity </a>--%>
                                            <%--</li>--%>
                                            <%--&lt;%&ndash;<li>&ndash;%&gt;--%>
                                                <%--&lt;%&ndash;<a href="<c:url value="/dashboard-cod/estimate"/>" class="iconify">&ndash;%&gt;--%>
                                                    <%--&lt;%&ndash;<i class="icon-puzzle"></i> COD Estimate </a>&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;</li>&ndash;%&gt;--%>

                                        <%--</ul>--%>
                                    <%--</div>--%>

                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</li>--%>
                    <%--</ul>--%>
                </li>


                <li class="menu-dropdown mega-menu-dropdown ">
                    <%--<a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="#" class="dropdown-toggle"> Dashboard S-Curve--%>
                    <a href="<c:url value="/dashboard-scurve/ruptl"/>" class="dropdown-toggle"> Dashboard S-Curve
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <%--<ul class="dropdown-menu" style="min-width: 150px">--%>
                        <%--<li>--%>
                            <%--<div class="mega-menu-content">--%>
                                <%--<div class="row">--%>
                                    <%--<div class="col-md-4">--%>
                                        <%--<ul class="mega-menu-submenu">--%>
                                                <%--<li>--%>
                                                    <%--<a href="<c:url value="/dashboard-scurve/ruptl"/>" class="iconify">--%>
                                                        <%--<i class="icon-puzzle"></i> RUPTL S-Curve </a>--%>
                                                <%--</li>--%>
                                                <%--&lt;%&ndash;<li>&ndash;%&gt;--%>
                                                    <%--&lt;%&ndash;<a href="<c:url value="/dashboard-scurve/ruptl_phase"/>" class="iconify">&ndash;%&gt;--%>
                                                        <%--&lt;%&ndash;<i class="icon-puzzle"></i> RUPTL S-Curve by Phase </a>&ndash;%&gt;--%>
                                                <%--&lt;%&ndash;</li>&ndash;%&gt;--%>

                                            <%--&lt;%&ndash;<li>&ndash;%&gt;--%>
                                                <%--&lt;%&ndash;<a href="<c:url value="/dashboard-scurve/supply"/>" class="iconify">&ndash;%&gt;--%>
                                                    <%--&lt;%&ndash;<i class="icon-paper-clip"></i> Supply vs Demand </a>&ndash;%&gt;--%>
                                            <%--&lt;%&ndash;</li>&ndash;%&gt;--%>

                                        <%--</ul>--%>
                                    <%--</div>--%>

                                <%--</div>--%>
                            <%--</div>--%>
                        <%--</li>--%>
                    <%--</ul>--%>
                </li>
                <%--<li class="<tiles:insertAttribute name="active-service" ignore="true"/>"><a href="<c:url value="/dashboard-milestone"/>">Dashboard Milestone</a></li>--%>
                <li class="menu-dropdown mega-menu-dropdown ">
                    <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown" href="#" class="dropdown-toggle">Account
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" style="min-width: 150px">
                        <li>
                            <div class="mega-menu-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <ul class="mega-menu-submenu">
                                            <%--<li>--%>
                                                <%--<a href="#" class="iconify">--%>
                                                    <%--<i class="icon-puzzle"></i> Manual </a>--%>
                                            <%--</li>--%>
                                            <%--<li>--%>
                                                <%--<a href="#" class="iconify">--%>
                                                    <%--<i class="icon-paper-clip"></i> Help </a>--%>
                                            <%--</li>--%>
                                            <li>
                                                <a href="<c:url value="/account/profile"/>" class="iconify">
                                                    <i class="icon-paper-clip"></i> Profile </a>
                                            </li>
                                            <li>
                                                <a href="<c:url value="/account/logout"/>" class="iconify" id="btn_logout">
                                                    <i class="icon-paper-clip"></i> Logout </a>
                                            </li>

                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </li>
                    </ul>
                </li>



            </ul>
        </div>
    </c:if>
</c:if>
<!-- END NAVIGATION -->