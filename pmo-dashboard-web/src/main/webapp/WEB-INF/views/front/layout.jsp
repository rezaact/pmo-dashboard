<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Head BEGIN -->
<head>
    <meta charset="utf-8">
    <%--<title><tiles:insertAttribute name="title"/>PMO Dashboard</title>--%>
    <title>PMO Dashboard</title>

    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="Metronic Shop UI description" name="description">
    <meta content="Metronic Shop UI keywords" name="keywords">
    <meta content="keenthemes" name="author">

    <meta property="og:site_name" content="-CUSTOMER VALUE-">
    <meta property="og:title" content="-CUSTOMER VALUE-">
    <meta property="og:description" content="-CUSTOMER VALUE-">
    <meta property="og:type" content="website">
    <meta property="og:image" content="-CUSTOMER VALUE-"><!-- link to image for socio -->
    <meta property="og:url" content="-CUSTOMER VALUE-">

    <link rel="shortcut icon" href="<c:url value="/assets/global/img/logo_pln.jpg"/>">

    <!-- Fonts START -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
    <!-- Fonts END -->

    <!-- Global styles START -->
    <link href="<c:url value="/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet">
    <!-- Global styles END -->
    <link href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css">

    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <tiles:importAttribute name="stylePlugin" ignore="true"/>
    <c:forEach var="stylePluginFiles" items="${stylePlugin}">
        <link href="<c:url value="/"/><c:url value="${stylePluginFiles}"/>" rel="stylesheet" type="text/css"/>
    </c:forEach>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <tiles:importAttribute name="stylePage" ignore="true"/>
    <c:forEach var="stylePageFiles" items="${stylePage}">
        <link href="<c:url value="/"/><c:url value="${stylePageFiles}"/>" rel="stylesheet" type="text/css"/>
    </c:forEach>
    <!-- END PAGE STYLES -->

    <!-- Theme styles START -->
    <link href="<c:url value="/assets/global/css/components.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/frontend/layout/css/style.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/frontend/layout/css/style-responsive.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/frontend/layout/css/themes/red.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/frontend/pages/css/theme/theme-1.css"/>" rel="stylesheet">
    <link href="<c:url value="/assets/global/css/app-front.css"/>" rel="stylesheet">
    <!-- Theme styles END -->

    <!--Setting base url-->
    <script>
        BASE_URL = '<c:url value="/"/>';
    </script>


</head>
<!-- Head END -->

<!-- Body BEGIN -->
<body class="corporate">

<tiles:insertAttribute name="header"/>
<tiles:insertAttribute name="body"/>
<tiles:insertAttribute name="footer"/>

<!-- Load javascripts at bottom, this will reduce page load time -->
<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
<!--[if lt IE 9]>
<script src="<c:url value="/assets/global/plugins/respond.min.js"/>"></script>
<![endif]-->
<script src="<c:url value="/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/frontend/layout/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/frontend/layout/scripts/back-to-top.js"/>" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<tiles:importAttribute name="cdnJS" ignore="true"/>
<c:forEach var="cdnFiles" items="${cdnJS}">
    <!-- BEGIN PAGE LEVEL CDN FILES -->
    <script src="<c:url value="${cdnFiles}"/>"></script>
    <!-- END PAGE LEVEL CDN FILES -->
</c:forEach>

<tiles:importAttribute name="scriptPlugin" ignore="true"/>
<c:forEach var="scriptPluginFiles" items="${scriptPlugin}">
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<c:url value="/"/><c:url value="${scriptPluginFiles}"/>"></script>
    <!-- END PAGE LEVEL PLUGINS -->
</c:forEach>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<tiles:importAttribute name="scriptPage" ignore="true"/>
<c:forEach var="scriptPageFiles" items="${scriptPage}">
    <script src="<c:url value="/"/><c:url value="${scriptPageFiles}"/>"></script>
</c:forEach>
<script src="<c:url value="/assets/global/scripts/app-front.js"/>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END PAGE LEVEL JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
