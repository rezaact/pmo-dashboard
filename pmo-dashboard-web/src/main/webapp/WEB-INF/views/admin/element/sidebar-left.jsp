<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_SECRET'}">
            <li class="<tiles:insertAttribute name="active-dashboard" ignore="true"/>">
            <a href="<c:url value="/dashboard"/>">
                <i class="icon-home"></i>
                <span class="title">Dashboard</span>
            </a>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
            <li class="<tiles:insertAttribute name="active-message" ignore="true"/>">
                <a href="<c:url value="/admin/message"/>">
                    <i class="icon-envelope-open"></i>
                    <span class="title">Pesan</span>
                </a>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_SECRET'}">
            <li class="<tiles:insertAttribute name="active-ecatalog" ignore="true"/>">
                <a href="<c:url value="/product/product-list"/>" target="_blank">
                    <i class="icon-screen-desktop"></i>
                    <span class="title">e-catalog</span>
                </a>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR' || authUser.getRole().getAuthority() == 'ROLE_SECRET'}">
            <li class="<tiles:insertAttribute name="active-management-product" ignore="true"/>">
            <a href="javascript:;">
                <i class="icon-basket"></i>
                <span class="title">Managemen Produk</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="<tiles:insertAttribute name="active-management-product-product" ignore="true"/>">
                    <a href="<c:url value="/admin/product"/>">
                        <i class="icon-book-open"></i>
                        Produk </a>
                </li>
                <li class="<tiles:insertAttribute name="active-management-product-history" ignore="true"/>">
                    <a href="<c:url value="/admin/history"/>">
                        <i class="icon-docs"></i>
                        Histori </a>
                </li>
                <%-- <li class="<tiles:insertAttribute name="active-management-product-order" ignore="true"/>">
                    <a href="<c:url value="/admin/splashcreen"/>">
                        <i class="icon-screen-desktop"></i>Splashscreen </a>
                </li>--%>
            </ul>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
            <li class="<tiles:insertAttribute name="active-management-user" ignore="true"/>">
            <a href="<c:url value="/admin/user"/>">
                <i class="icon-user"></i>
                <span class="title">Managemen User</span>
            </a>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
            <li class="<tiles:insertAttribute name="active-master-data" ignore="true"/>">
                <a href="javascript:;">
                    <i class="icon-briefcase"></i>
                    <span class="title">Master Data</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="<tiles:insertAttribute name="active-master-data-material" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/material"/>">
                            <i class="icon-energy"></i>
                            Material </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-workshop" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/workshop"/>">
                            <i class="icon-wrench"></i>
                            Workshop </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-group" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/group"/>">
                            <i class="icon-users"></i>
                            Groups </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-customer" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/customer"/>">
                            <i class="icon-user-follow"></i>
                            Customers </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-jenis-pekerjaan" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/jenis-pekerjaan"/>">
                            <i class="icon-support"></i>
                            Jenis Pekerjaan </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-lokasi-pemasangan" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/lokasi-pemasangan"/>">
                            <i class="icon-badge"></i>
                            Lokasi Pemasangan </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-news" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/news"/>">
                            <i class="glyphicon glyphicon-info-sign"></i>
                            News </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-link" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/link"/>">
                            <i class="glyphicon glyphicon-send"></i>
                            Link </a>
                    </li>
                    <li class="<tiles:insertAttribute name="active-master-data-splash" ignore="true"/>">
                        <a href="<c:url value="/admin/master-data/splash"/>">
                            <i class="glyphicon glyphicon-adjust"></i>
                            Splashscreen </a>
                    </li>
                </ul>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
            <li class="<tiles:insertAttribute name="active-reporting" ignore="true"/>">
            <a href="javascript:;">
                <i class="icon-graph"></i>
                <span class="title">Laporan</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="<tiles:insertAttribute name="active-reporting-historikal" ignore="true"/>">
                    <a href="<c:url value="/admin/reporting/historikal-produk"/>">
                        <i class="icon-book-open"></i>
                        Historikal produk </a>
                </li>
                <%--<li class="<tiles:insertAttribute name="active-reporting-product" ignore="true"/>">--%>
                <%--<a href="<c:url value="/admin/reporting/product"/>">--%>
                    <%--<i class="icon-handbag"></i>--%>
                    <%--Product </a>--%>
                <%--</li>--%>
                <%--<li class="<tiles:insertAttribute name="active-reporting-workshop" ignore="true"/>">--%>
                <%--<a href="<c:url value="/admin/reporting/workshop"/>">--%>
                    <%--<i class="icon-wrench"></i>--%>
                    <%--Workshop </a>--%>
                <%--</li>--%>
                <%--<li class="<tiles:insertAttribute name="active-reporting-user" ignore="true"/>">--%>
                <%--<a href="<c:url value="/admin/reporting/user"/>">--%>
                    <%--<i class="icon-user"></i>--%>
                    <%--User </a>--%>
                <%--</li>--%>
                <%--<li class="<tiles:insertAttribute name="active-reporting-customer" ignore="true"/>">--%>
                <%--<a href="<c:url value="/admin/reporting/customer"/>">--%>
                    <%--<i class="icon-users"></i>--%>
                    <%--Customers </a>--%>
                <%--</li>--%>
            </ul>
            </li>
            </c:if>
            <c:if test="${authUser.getRole().getAuthority() == 'ROLE_ADMINISTRATOR'}">
            <li class="<tiles:insertAttribute name="active-setting" ignore="true"/>">
            <a href="javascript:;">
                <i class="icon-settings"></i>
                <span class="title">Pengaturan</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="<tiles:insertAttribute name="active-setting-service" ignore="true"/>">
                    <a href="<c:url value="/admin/setting/service"/>">
                        <i class="icon-cup"></i>
                        Layanan Kami </a>
                </li>
                <li class="<tiles:insertAttribute name="active-setting-comment" ignore="true"/>">
                    <a href="<c:url value="/admin/setting/comment"/>">
                        <i class="icon-bubble"></i>
                        Komentar </a>
                </li>
                <li class="<tiles:insertAttribute name="active-setting-profile" ignore="true"/>">
                <a href="<c:url value="/admin/profile/update"/>">
                    <i class="icon-user"></i>
                    Profil </a>
                </li>
            </ul>
            </li>
            </c:if>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>