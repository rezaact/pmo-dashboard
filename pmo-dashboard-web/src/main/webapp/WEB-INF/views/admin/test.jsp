<%--
  Created by IntelliJ IDEA.
  User: LATIF
  Date: 5/5/2015
  Time: 11:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Testing</title>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript"
            src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>

</head>
<body>

<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>ID</th>
        <th>UNIT</th>
        <th>LOKASI</th>
    </tr>
    </thead>
    <tbody>
    <%-- Menampilkan data awal pada tabel --%>
    <c:forEach items="${data}" var="d">
        <tr>
            <td>${d.ID_WORKSHOP}</td>
            <td>${d.UNIT}</td>
            <td>${d.LOKASI}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<script>
    $(document).ready(function () {

        <%--$("#mySpriteSpin").spritespin({--%>
            <%--// path to the source images.--%>
            <%--source: [--%>
                <%--<c:forEach items="${listPic}" var="pic">--%>
                    <%--"<c:url value='/file/'/>${pic}",--%>
                <%--</c:forEach>--%>
            <%--]--%>
            <%--//dst--%>
        <%--})--%>

        /**
         * membuat DataTable
         */
        $('#example').DataTable({
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "deferLoading": ${recordsTotal}, //inisialisasi total records, berguna untuk pagination server side
            "pageLength": ${length}, //inisialisasi panjang halaman, berguna untuk pagination server side
            "lengthMenu": [ 5, 10, 20 ],
            "ajax": { //ajax untuk pagination server side
                "url": "<c:url value="/test/json"/>",
                "type": "GET"
            },
            "columns":[//inisialisasi kolom agar cocok dengan JSON diterima nanti
                {"data":"ID_WORKSHOP", "defaultContent": ""},//defaultContent -> Nilai default jika content pada field null.
                {"data":"UNIT", "defaultContent": ""},
                {"data":"LOKASI", "defaultContent": ""}
            ]
        });
    });
</script>
</body>
</html>
