<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title><tiles:insertAttribute name="title"/> | Pusharlis</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <%--<link rel="shortcut icon" href="<c:url value="/assets/global/img/favicon.png"/>">--%>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/plugins/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/plugins/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/plugins/uniform/css/uniform.default.css"/>" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <tiles:importAttribute name="stylePlugin" ignore="true"/>
    <c:forEach var="stylePluginFiles" items="${stylePlugin}">
        <link href="<c:url value="/"/><c:url value="${stylePluginFiles}"/>" rel="stylesheet" type="text/css"/>
    </c:forEach>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN PAGE STYLES -->
    <tiles:importAttribute name="stylePage" ignore="true"/>
    <c:forEach var="stylePageFiles" items="${stylePage}">
        <link href="<c:url value="/"/><c:url value="${stylePageFiles}"/>" rel="stylesheet" type="text/css"/>
    </c:forEach>
    <!-- END PAGE STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link href="<c:url value="/assets/global/css/components.css"/>" id="style_components" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/global/css/plugins.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/admin/layout/css/layout.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="/assets/admin/layout/css/themes/default.css"/>" rel="stylesheet" type="text/css" id="style_color"/>
    <link href="<c:url value="/assets/global/css/app-admin.css"/>" rel="stylesheet" type="text/css"/>
    <!-- END THEME STYLES -->

    <script>
        BASE_URL = '<c:url value="/"/>';
    </script>

</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content page-style-square">

<!-- BEGIN HEADER -->
<tiles:insertAttribute name="header"/>
<!-- END HEADER -->

<!-- BEGIN CONTAINER -->
<div class="page-container">

    <!-- BEGIN SIDEBAR -->
    <tiles:insertAttribute name="sidebar-left"/>
    <!-- END SIDEBAR -->

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <tiles:insertAttribute name="body"/>

        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<tiles:insertAttribute name="footer"/>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<c:url value="/assets/global/plugins/respond.min.js"/>"></script>
<script src="<c:url value="/assets/global/plugins/excanvas.min.js"/>"></script>
<![endif]-->
<!--[if (gte IE 8)&(lt IE 10)]>
<tiles:importAttribute name="supportIE" ignore="true"/>
<c:forEach var="IEFiles" items="${supportIE}">
    <script src="<c:url value="${IEFiles}"/>"></script>
</c:forEach>
<![endif]-->

<script src="<c:url value="/assets/global/plugins/jquery.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-migrate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap/js/bootstrap.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery.blockui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/jquery.cokie.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/uniform/jquery.uniform.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL CDN FILES -->
<tiles:importAttribute name="cdnJS" ignore="true"/>
<c:forEach var="cdnFiles" items="${cdnJS}">
    <script src="<c:url value="${cdnFiles}"/>"></script>
</c:forEach>
<!-- END PAGE LEVEL CDN FILES -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<tiles:importAttribute name="scriptPlugin" ignore="true"/>
<c:forEach var="scriptPluginFiles" items="${scriptPlugin}">
    <script src="<c:url value="/"/><c:url value="${scriptPluginFiles}"/>"></script>
</c:forEach>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<c:url value="/assets/global/scripts/metronic.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/admin/layout/scripts/layout.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/admin/layout/scripts/quick-sidebar.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/admin/layout/scripts/demo.js"/>" type="text/javascript"></script>
<script src="<c:url value="/assets/global/scripts/app-admin.js"/>" type="text/javascript"></script>
<tiles:importAttribute name="scriptPage" ignore="true"/>
<c:forEach var="scriptPageFiles" items="${scriptPage}">
    <script src="<c:url value="/"/><c:url value="${scriptPageFiles}"/>"></script>
</c:forEach>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->

</body>
<!-- END BODY -->
</html>