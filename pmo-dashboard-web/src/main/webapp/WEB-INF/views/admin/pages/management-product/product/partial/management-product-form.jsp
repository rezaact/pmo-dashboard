<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="row">
    <div class="col-md-12">

    <form id="form-product" action="<c:if test="${label == 'create'}"><c:url value="/admin/product"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/product?id=${NOMOR_PRODUK}"/></c:if>"  method="post" enctype="multipart/form-data" class="form-horizontal form">
    <div class="form-header padding-0">
        <c:if test="${success == true}">
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button>
                    <c:out value="${message}"/>
            </div>
        </c:if>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <c:choose>
                    <c:when test="${success == false}">
                        <c:out value="${message}"/>
                    </c:when>
                    <c:otherwise>
                        Pengisian form anda salah, mohon cek kembali.
                    </c:otherwise>
                </c:choose>
            </div>
    </div>
    <div class="form-body padding-0">
    <div class="portlet light bg-inverse form-fit">
        <div class="portlet-title">
            <div class="caption">
                Part Produk
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body form">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">
                            Nomor Produk <span aria-required="true" class="required"> * </span>
                        </label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail' || label == 'update'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${NOMOR_PRODUK}</p></c:if>
                            <c:if test="${label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control text-uppercase" name="PRM_NOMOR_PRODUK" value="${NOMOR_PRODUK}">
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                Spesifikasi Produk
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Nama Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${NAMA_PRODUK}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control text-capitalize" name="PRM_NAMA_PRODUK" value="${NAMA_PRODUK}">
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Tahun Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${TAHUN_PRODUK}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <c:set var="years" value="2010,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020" scope="application" />
                                    <select class="form-control select2me grade" name="PRM_TAHUN_PRODUK" data-placeholder="Pilih...">
                                        <option value=""></option>
                                        <c:forEach items="${fn:split(years, ',')}" var="year">
                                            <option value="${year}" ${TAHUN_PRODUK == year ? 'selected' : ''}>${year}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Dimensi Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${DIMENSI}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="PRM_DIMENSI" value="${DIMENSI}" data-placement="left">
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Flag Produk</label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:choose>
                                        <c:when test="${FLAG_PRODUK_BARU == 0}">
                                            Produk lama
                                        </c:when>
                                        <c:when test="${FLAG_PRODUK_BARU == 1}">
                                            Produk baru
                                        </c:when>
                                    </c:choose>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me" name="PRM_FLAG_PRODUK_BARU" data-placeholder="Pilih..." data-placement="left">
                                        <option value=""></option>
                                        <option value="0" ${FLAG_PRODUK_BARU == '0' ? 'selected' : ''}>Produk lama</option>
                                        <option value="1" ${FLAG_PRODUK_BARU == '1' ? 'selected' : ''}>Produk baru</option>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group margin-top-10">
                        <label class="col-md-3 control-label" style="width: 21%">Deskripsi Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-9" style="width: 79%">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${DESKRIPSI_PRODUK}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa" style="top: 175px"></i>
                                    <textarea class="ckeditor form-control" name="PRM_DESKRIPSI_PRODUK" data-placement="left">${DESKRIPSI_PRODUK}</textarea>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                Group Produk
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Pilih Manufaktur <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:forEach var="manufaktur" items="${listManufaktur}" varStatus="number">
                                        <c:if test="${ID_WORKSHOP == manufaktur.id}">
                                            ${manufaktur.text}
                                        </c:if>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me margin-bottom-15" name="PRM_ID_WORKSHOP" data-placeholder="Pilih...">
                                        <option value=""></option>
                                        <c:forEach var="manufaktur" items="${listManufaktur}" varStatus="number">
                                            <option value="${manufaktur.id}" ${ID_WORKSHOP == manufaktur.id ? 'selected' : ''}>${manufaktur.text}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Pilih Kategori Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:forEach var="kategoriProduk" items="${listKategoriProduk}" varStatus="number">
                                        <c:if test="${ID_KATEGORI == kategoriProduk.id}">
                                            ${kategoriProduk.text}
                                        </c:if>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me margin-bottom-15" name="PRM_ID_KATEGORI" data-placeholder="Pilih...">
                                        <option value=""></option>
                                        <c:forEach var="kategoriProduk" items="${listKategoriProduk}" varStatus="number">
                                            <option value="${kategoriProduk.id}" ${ID_KATEGORI == kategoriProduk.id ? 'selected' : ''}>${kategoriProduk.text}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Pilih Jenis Produk <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:forEach var="jenisProduk" items="${listJenisProduk}" varStatus="number">
                                        <c:if test="${ID_KATEGORI == jenisProduk.id}">
                                            ${kategoriProduk.text}
                                        </c:if>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me margin-bottom-15" name="PRM_ID_JENIS_PRODUK" data-placeholder="Pilih...">
                                        <option value=""></option>
                                        <c:forEach var="jenisProduk" items="${listJenisProduk}" varStatus="number">
                                            <option value="${jenisProduk.id}" ${ID_JENIS_PRODUK == jenisProduk.id ? 'selected' : ''}>${jenisProduk.text}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Pilih Jenis Komponen <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:forEach var="jenisKomponen" items="${listJenisKomponen}" varStatus="number">
                                        <c:if test="${ID_KATEGORI == jenisKomponen.id}">
                                            ${kategoriProduk.text}
                                        </c:if>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me margin-bottom-15" name="PRM_ID_KOMPONEN" data-placeholder="Pilih..." data-placement="left">
                                        <option value=""></option>
                                        <c:forEach var="jenisKomponen" items="${listJenisKomponen}" varStatus="number">
                                            <option value="${jenisKomponen.id}" ${ID_KOMPONEN == jenisKomponen.id ? 'selected' : ''}>${jenisKomponen.text}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Pilih Jenis Pekerjaan <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">:&nbsp;&nbsp;&nbsp;
                                    <c:forEach var="kategoriProduk" items="${listKategoriProduk}" varStatus="number">
                                        <c:if test="${ID_KATEGORI == kategoriProduk.id}">
                                            ${kategoriProduk.text}
                                        </c:if>
                                    </c:forEach>
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <select class="form-control select2me margin-bottom-15" name="PRM_ID_JENIS_PEKERJAAN" data-placeholder="Pilih..." data-placement="left">
                                        <option value=""></option>
                                        <c:forEach var="jenisPekerjaan" items="${listJenisPekerjaan}" varStatus="number">
                                            <option value="${jenisPekerjaan.id}" ${ID_JENIS_PEKERJAAN == jenisPekerjaan.id ? 'selected' : ''}>${jenisPekerjaan.text}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Kemampuan Produksi</label>
                        <div class="col-md-7">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <div class="row">
                                    <div class="col-md-6">
                                        <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${KEMAMPUAN_PRODUK_PERBULAN} / Perbulan</p></c:if>
                                        <c:if test="${label == 'update' || label == 'create'}">
                                            <div class="input-icon right input-number">
                                                <i class="fa"></i>
                                                <input class="form-control" placeholder="Perbulan" type="number" name="PRM_KEMAMPUAN_PRODUK_PERBULAN" value="${KEMAMPUAN_PRODUK_PERBULAN}">
                                            </div>
                                        </c:if>
                                    </div>
                                    <div class="col-md-6" style="padding-left: 0px; padding-right: 15px">
                                        <c:if test="${label == 'detail'}"><p class="form-control-static">${KEMAMPUAN_PRODUK_PERTAHUN} / Pertahun</p></c:if>
                                        <c:if test="${label == 'update' || label == 'create'}">
                                            <div class="input-icon right input-number">
                                                <i class="fa"></i>
                                                <input class="form-control" placeholder="Pertahun" type="number" name="PRM_KEMAMPUAN_PRODUK_PERTAHUN" value="${KEMAMPUAN_PRODUK_PERTAHUN}">
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                Detail Produk
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Harga Produk (Rupiah)</label>
                        <div class="col-md-7">
                            <div class="input-icon right">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${HARGA_PRODUK_IND}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control currency" name="PRM_HARGA_PRODUK" value="${HARGA_PRODUK}">
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Label produk</label>
                        <div class="col-md-7">
                            <div class="input-icon right">
                                <c:if test="${label == 'detail'}">
                                    <p class="form-control-static">
                                                <span class="label label-default">
                                                    <c:choose>
                                                        <c:when test="${JNS_HARGA_PRODUK == 0}">
                                                            Based on condition
                                                        </c:when>
                                                        <c:when test="${JNS_HARGA_PRODUK == 1}">
                                                            Estimate
                                                        </c:when>
                                                        <c:when test="${JNS_HARGA_PRODUK == 2}">
                                                            Fix
                                                        </c:when>
                                                    </c:choose>
                                                </span>
                                    </p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <select class="form-control select2me select2-small" name="PRM_JNS_HARGA_PRODUK" data-placeholder="Pilih...">
                                            <option value=""></option>
                                            <option <c:if test="${JNS_HARGA_PRODUK == 0}">selected="selected"</c:if> value="0">Based on condition</option>
                                            <option <c:if test="${JNS_HARGA_PRODUK == 1}">selected="selected"</c:if> value="1">Estimate</option>
                                            <option <c:if test="${JNS_HARGA_PRODUK == 2}">selected="selected"</c:if> value="2">Fix</option>
                                        </select>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Durasi Pekerjaan (Hari)</label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${DURASI_PEKERJAAN}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right input-number">
                                    <i class="fa"></i>
                                    <input type="number" class="form-control" name="PRM_DURASI_PEKERJAAN" value="${DURASI_PEKERJAAN}">
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Nomor Drawing <span aria-required="true" class="required"> * </span></label>
                        <div class="col-md-7">
                            <c:if test="${label == 'detail'}">
                                <p class="form-control-static">
                                    :&nbsp;&nbsp;&nbsp;${NOMOR_DRAWING}
                                </p>
                            </c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="PRM_NOMOR_DRAWING" value="${NOMOR_DRAWING}" >
                                </div>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Material Produk</label>
                        <div class="col-md-7" style="padding-right: 15px">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">:&nbsp;&nbsp;&nbsp;${DESKRIPSI_MATERIAL}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <textarea class="form-control" name="PRM_DESKRIPSI_MATERIAL">${DESKRIPSI_MATERIAL}</textarea>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="form-group margin-top-10">
                        <label class="col-md-5 control-label">Produk Unggulan</label>
                        <div class="col-md-7">
                        <div class="checkbox">
                            <label style="margin-left: -5px; margin-top: 8px">
                                <input type="checkbox" value="1" id="PRM_FLAG_PRODUK_UNGGULAN"
                                    <c:if test="${FLAG_PRODUK_UNGGULAN == 1}">checked</c:if>
                                    <c:if test="${label == 'detail'}">readonly disabled </c:if>
                                />
                                <input type="hidden" name="PRM_FLAG_PRODUK_UNGGULAN" id="VAL_PRODUK_UNGGULAN"   value="0"/>

                            </label>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="form-actions">
        <div class="pull-right">
            <a href="#kembali" class="btn btn-danger" data-toggle="modal">Kembali</a>
                <c:if test="${label == 'create'}">
                <button type="submit" class="btn btn-success">
                    Submit
                </button>
                </c:if>
                <c:if test="${label == 'update'}">
                <button type="submit" class="btn btn-success">
                    Update
                </button>
                </c:if>
        </div>
    </div>
    </form>
        <!-- MODAL KEMBALI -->
        <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="kembali">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title"></h4>
                    </div>
                    <div class="modal-body">
                        <p>Apakah Anda ingin kembali? </p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <a href="<c:url value="/admin/product"/>" class="btn btn-success">ya</a>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
 

