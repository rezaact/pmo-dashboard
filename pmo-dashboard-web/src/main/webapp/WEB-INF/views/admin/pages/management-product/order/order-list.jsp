<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Produk <small>daftar produk</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="index.html">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Management produk</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Produk</a>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                Actions <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li>
                    <a href="#">Action</a>
                </li>
                <li>
                    <a href="#">Another action</a>
                </li>
                <li>
                    <a href="#">Something else here</a>
                </li>
                <li class="divider">
                </li>
                <li>
                    <a href="#">Separated link</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
<!-- Begin: life time stats -->
<div class="portlet">
<div class="portlet-title">
    <div class="caption">
        <i class="fa fa-gift"></i>Products
    </div>
    <div class="actions">
        <div class="btn-group">
            <a href="#" class="btn default yellow-stripe" data-toggle="modal" data-target="#tambah-produk">
                <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Tambah Produk </span>
            </a>
            <a class="btn default yellow-stripe dropdown-toggle" href="#" data-toggle="dropdown">
                <i class="fa fa-share"></i> Tools <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu pull-right">
                <li>
                    <a href="#">
                        Export to Excel </a>
                </li>
                <li>
                    <a href="#">
                        Export to CSV </a>
                </li>
                <li>
                    <a href="#">
                        Export to XML </a>
                </li>
                <li class="divider">
                </li>
                <li>
                    <a href="#">
                        Print Invoices </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="portlet-body">
<div class="table-container">
<div class="table-actions-wrapper">
									<span>
									</span>
    <select class="table-group-action-input form-control input-inline input-small input-sm">
        <option value="">Pilih...</option>
        <option value="publish">Publish</option>
        <option value="unpublished">Un-publish</option>
        <option value="delete">Delete</option>
    </select>
    <button class="btn btn-sm yellow table-group-action-submit"><i class="fa fa-check"></i> Submit</button>
</div>
<!-- BEGIN MODAL EDIT-->
<div class="modal fade" data-backdrop="static" id="tambah-produk" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header red">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true"></button>
                <h4 class="modal-title">Produk Baru</h4>
            </div>
            <div class="modal-body no-padding">
                <form action="" class="form-horizontal form" method="post">
                    <input type="hidden" name="id_category" value=""/>
                    <div class="form-body">
                        <div class="form-group margin-top-10">
                            <label class="col-md-3 control-label">ID</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="category_name" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama Produk</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="description" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Kategori</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="description" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Harga</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="description" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Banyak</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="text" class="form-control" name="description" value=""/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tanggal di buat</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <i class="fa"></i>
                                    <input type="datetime" class="form-control" name="description" value=""/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions right">
                        <button type="button" class="btn default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn blue">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END MODAL EDIT-->
<table class="table table-striped table-bordered table-hover" id="datatable_products">
    <thead>
    <tr role="row" class="heading">
        <th width="1%">
            <input type="checkbox" class="group-checkable">
        </th>
        <th width="10%">
            ID
        </th>
        <th width="15%">
            Nama&nbsp;Produk
        </th>
        <th width="15%">
            Kategori
        </th>
        <th width="10%">
            Harga
        </th>
        <th width="10%">
            Banyak
        </th>
        <th width="15%">
            Dibuat&nbsp;Tanggal
        </th>
        <th width="10%">
            Status
        </th>
        <th width="10%">
            Pilihan
        </th>
    </tr>
    <tr role="row" class="filter">
        <td>
        </td>
        <td>
            <input type="text" class="form-control form-filter input-sm" name="product_id">
        </td>
        <td>
            <input type="text" class="form-control form-filter input-sm" name="product_name">
        </td>
        <td>
            <select name="product_category" class="form-control form-filter input-sm">
                <option value="">Pilih...</option>
                <option value="1">Mens</option>
                <option value="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Footwear</option>
                <option value="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Clothing</option>
                <option value="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Accessories</option>
                <option value="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fashion Outlet</option>
                <option value="6">Football Shirts</option>
                <option value="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Premier League</option>
                <option value="8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Football League</option>
                <option value="9">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Serie A</option>
                <option value="10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bundesliga</option>
                <option value="11">Brands</option>
                <option value="12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Adidas</option>
                <option value="13">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nike</option>
                <option value="14">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Airwalk</option>
                <option value="15">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;USA Pro</option>
                <option value="16">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kangol</option>
            </select>
        </td>
        <td>
            <div class="margin-bottom-5">
                <input type="text" class="form-control form-filter input-sm" name="product_price_from" placeholder="From"/>
            </div>
            <input type="text" class="form-control form-filter input-sm" name="product_price_to" placeholder="To"/>
        </td>
        <td>
            <div class="margin-bottom-5">
                <input type="text" class="form-control form-filter input-sm" name="product_quantity_from" placeholder="From"/>
            </div>
            <input type="text" class="form-control form-filter input-sm" name="product_quantity_to" placeholder="To"/>
        </td>
        <td>
            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                <input type="text" class="form-control form-filter input-sm" readonly name="product_created_from" placeholder="From">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
            </div>
            <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                <input type="text" class="form-control form-filter input-sm" readonly name="product_created_to " placeholder="To">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
            </div>
        </td>
        <td>
            <select name="product_status" class="form-control form-filter input-sm">
                <option value="">Pilih...</option>
                <option value="published">Diterbitkan</option>
                <option value="notpublished">Tidak diterbitkan</option>
                <option value="deleted">Hapus</option>
            </select>
        </td>
        <td>
            <div class="margin-bottom-5">
                <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Search</button>
            </div>
            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>1</td>
        <td>1</td>
        <td>Nama Produk 1</td>
        <td>Kategori</td>
        <td>Rp. 1000.000,-</td>
        <td>5</td>
        <td>10 Maret 2015 - 10 April 2015</td>
        <td>
            <span class="label label-success ">Diterbitkan</span>
        </td>
        <td><a href="" class="btn btn-xs default btn-editable"><i class="fa fa-eye"></i>Lihat</a></td>
    </tr>
    <tr>
        <td>2</td>
        <td>2</td>
        <td>Nama Produk 2</td>
        <td>Kategor1i</td>
        <td>Rp. 1000.000,-</td>
        <td>5</td>
        <td>10 Maret 2015 - 10 April 2015</td>
        <td>
            <span class="label label-warning ">Tidak Diterbitkan</span>
        </td>
        <td><a href="" class="btn btn-xs default btn-editable"><i class="fa fa-eye"></i>Lihat</a></td>
    </tr>
    <tr>
        <td>3</td>
        <td>3</td>
        <td>Nama Produk 3</td>
        <td>Kategori 3</td>
        <td>Rp. 1000.000,-</td>
        <td>5</td>
        <td>10 Maret 2015 - 10 April 2015</td>
        <td>
            <span class="label label-success ">Diterbitkan</span>
        </td>
        <td><a href="" class="btn btn-xs default btn-editable"><i class="fa fa-eye"></i>Lihat</a></td>
    </tr>
    <tr>
        <td>4</td>
        <td>4</td>
        <td>Nama Produk 4</td>
        <td>Kategori 4</td>
        <td>Rp. 1000.000,-</td>
        <td>5</td>
        <td>10 Maret 2015 - 10 April 2015</td>
        <td>
            <span class="label label-danger ">Dihapus</span>
        </td>
        <td><a href="" class="btn btn-xs default btn-editable"><i class="fa fa-eye"></i>Lihat</a></td>
    </tr>

    </tbody>
</table>
</div>
</div>
</div>
<!-- End: life time stats -->
</div>
</div>
<!-- END PAGE CONTENT-->