<div class="row ">
    <div class="col-md-12 col-sm-6">
        <!-- BEGIN MARKERS PORTLET-->
        <div class="portlet solid grey-cararra bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-wrench"></i>Daftar Workshop
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config">
                    </a>
                    <a href="javascript:;" class="reload">
                    </a>
                    <a href="javascript:;" class="remove">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div id="gmap_marker" class="gmaps" style="height: 400px">
                </div>
            </div>
        </div>
        <!-- END MARKERS PORTLET-->
    </div>
</div>
<div class="clearfix">
</div>