<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/product/json" />";
</script>


<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover table-number table-action table-datatable" id="product-list-table">
                    <thead>
                    <tr role="row" class="heading">
                        <th>
                            Gambar
                        </th>
                        <th style="width: 110px">
                            Nomor Produk
                        </th>
                        <th style="width: 130px">
                            Nama Produk
                        </th>
                        <th>
                            Deskripsi
                        </th>
                        <th style="width: 1% !important;">
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>

        </div>
    </div>
</div>