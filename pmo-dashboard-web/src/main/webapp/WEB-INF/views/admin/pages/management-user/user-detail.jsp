<!-- BEGIN PAGE CONTENT-->
<div class="row">
<div class="col-md-12">
<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar">
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet">
        <c:forEach var="masterUser" items="${getUsername}">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <%--<img src="<c:url value="/assets/img/profile_user.jpg"/>" class="img-responsive" alt="">--%>
                <img src="<c:url value='/user-management/get_image/'/>?prm_username=${masterUser.USERNAME}"  class="img-responsive" alt="">
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    ${masterUser.nama_lengkap}
                </div>
                <div class="profile-usertitle-job">
                    ${masterUser.nama_group}
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->
        </c:forEach>
    </div>
    <!-- END PORTLET MAIN -->
</div>
<!-- END BEGIN PROFILE SIDEBAR -->
<!-- BEGIN PROFILE CONTENT -->
<div class="profile-content">
<div class="row">
<div class="col-md-12">
<div class="portlet light">
<div class="portlet-title tabbable-line">
    <div class="caption caption-md">
        <i class="icon-globe theme-font hide"></i>
        <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
    </div>
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab-biodata" data-toggle="tab">Biodata</a>
        </li>
        <li>
            <a href="#tab-ubah-foto" data-toggle="tab">Ubah Foto</a>
        </li>
        <li>
            <a href="#tab-ubah-password" data-toggle="tab">Ubah Password</a>
        </li>
        <li>
            <a href="#tab-setting" data-toggle="tab">Settings</a>
        </li>
    </ul>
</div>
<div class="portlet-body">
<div class="tab-content">
<!-- PERSONAL INFO TAB -->
<div class="tab-pane active fade in" id="tab-biodata">
    <c:forEach var="masterUser" items="${getUsername}">
        <form id="form-biodata" action="#" class="form-horizontal form">
            <input type="hidden" name="pkg_name" value="p_master_user"/>
            <input type="hidden" name="func_name" value="update_user"/>
            <input type="hidden" name="prm_id_mitra" id="prm_id_mitra" value="${masterUser.ID_MITRA}" />

            <div class="form-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="alert alert-danger display-hide no-margin">
                            <button class="close" data-close="alert"></button>
                            Data belum lengkap, periksa kembali
                        </div>
                        <div class="alert alert-success display-hide no-margin">
                            <button class="close" data-close="alert"></button>
                            Data berhasil dikirim
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 col-md-3 col-sm-12 col-xs-12 control-label">Username
                        <span class="required" aria-required="true"> * </span>
                    </label>

                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <input type="text" name="prm_username" id="profile_username" placeholder="Username"
                                   value="${masterUser.username}" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Nama Lengkap
                        <span class="required" aria-required="true"> * </span>
                    </label>

                    <div class="col-md-9 ">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <input type="text" name="prm_nama_lengkap" placeholder="Nama Lengkap"
                                   value="${masterUser.nama_lengkap}" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="form-group margin-bottom-15">
                    <label class="col-md-3 control-label">No. Telp
                        <span class="required" aria-required="true"> * </span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <input type="text" name="prm_no_telpon" placeholder="08xxxxxxxx"
                                   value="${masterUser.no_telpon}" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Alamat Email
                        <span class="required" aria-required="true"> * </span>
                    </label>

                    <div class="col-md-9">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <input type="text" name="prm_alamat_email" placeholder="Alamat"
                                   value="${masterUser.alamat_email}" class="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions right">
                <button type="submit" id="biodata"  class="btn green">
                    Simpan
                </button>
            </div>
        </form>
    </c:forEach>
</div>
<!-- END PERSONAL INFO TAB -->
<!-- CHANGE AVATAR TAB -->
<div class="tab-pane fade" id="tab-ubah-foto">
    <c:forEach var="masterUser" items="${getUsername}">
        <form action="#" method="post" id="form-gambar" role="form" class="form-horizontal form" enctype="multipart/form-data">
            <input type="hidden" name="pkg_name" value="p_master_user"/>
            <input type="hidden" name="func_name" value="upload_gambar"/>
            <input type="hidden" name="prm_username" value="${userMitrausername}"/>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                Data belum lengkap, periksa kembali
            </div>
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button>
                Data berhasil dikirim
            </div>
            <input type="hidden" name="prm_username" value=""/>

            <div class="form-group" style="margin-left: 0px">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail"
                         style="width: 200px; height: 150px;">
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                             alt=""/>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail"
                         style="max-width: 200px; max-height: 150px;">
                    </div>
                    <div>
                                                                                                <span class="btn default btn-file">
                                                                                                <span class="fileinput-new">
                                                                                                Select image </span>
                                                                                                <span class="fileinput-exists">
                                                                                                Change </span>
                                                                                                <input type="file" name="prm_gambar">
                                                                                                </span>
                        <a href="#" class="btn default fileinput-exists"
                           data-dismiss="fileinput">
                            Remove </a>
                    </div>
                </div>

            </div>
            <div class="form-actions right">
                <button type="submit" id="gambar" class="btn green">
                    Simpan </button>

            </div>
        </form>
    </c:forEach>
</div>
<!-- END CHANGE AVATAR TAB -->
<!-- CHANGE PASSWORD TAB -->
<div class="tab-pane fade" id="tab-ubah-password">
    <c:forEach var="masterUser" items="${getUsername}">
        <form action="#" id="form-password" class="form-horizontal form">
            <input type="hidden" name="pkg_name" value="p_master_user"/>
            <input type="hidden" name="func_name" value="change_password"/>
            <input type="hidden" name="prm_username" value="${masterUser.username}"/>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                Data belum lengkap, periksa kembali
            </div>
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button>
                Data berhasil dikirim
            </div>
            <div class="form-group margin-bottom-15 ">
                <label class=" col-md-4 control-label">Password Lama
                    <span class="required" aria-required="true"> * </span> </label>

                <div class="col-md-8">
                    <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="password" name="prm_old_password" class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="form-group margin-bottom-15">
                <label class="col-md-4 control-label">Password Baru
                    <span class="required" aria-required="true"> * </span></label>

                <div class="col-md-8">
                    <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="password" name="prm_new_password" class="form-control" id="new_password"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Ketik ulang Password Baru
                    <span class="required" aria-required="true"> * </span>
                </label>

                <div class="col-md-8">
                    <div class="input-icon right">
                        <i class="fa"></i>
                        <input type="password"  name="prm_new_password_again" class="form-control"/>
                    </div>
                </div>
            </div>
            <div class="form-actions right">
                <button type="submit" id="password"  class="btn green">
                    Simpan
                </button>

            </div>
        </form>
    </c:forEach>
</div>
<!-- END CHANGE PASSWORD TAB -->
<!--CHANGE LANGUAGE TAB -->
<div class="tab-pane fade" id="tab-setting">
    <c:forEach var="masterUser" items="${getUsername}">
        <form id="form-bahasa" action="#" class="form-horizontal form" >
            <input type="hidden" name="pkg_name" value="p_master_user"/>
            <input type="hidden" name="func_name" value="ubah_bahasa"/>
            <input type="hidden" name="prm_username" value="${masterUser.username}"/>
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Bahasa</label>

                    <div class="col-md-9">

                        <select id="select2_sample4" class="form-control select2me" name="prm_bahasa">
                            <option value="id"
                            <c:if test="${masterUser.bahasa=='id'}">
                                selected="selected"
                            </c:if>>
                            Indonesia
                            </option>
                            <option value="US"

                            <c:if test="${masterUser.bahasa=='US'}">
                                selected="selected"
                            </c:if>>
                            English</option>
                        </select>

                    </div>
                </div>
            </div>

            <!--end profile-settings-->
            <div class="form-actions right">
                <button type="submit" id="bahasa" class="btn green">
                    Simpan
                </button>
            </div>
        </form>
    </c:forEach>
</div>
<!-- CHANGE LANGUAGE TAB  -->
</div>
</div>
</div>
</div>
</div>
</div>
<!-- END PROFILE CONTENT -->
</div>
</div>
<!-- END PAGE CONTENT-->