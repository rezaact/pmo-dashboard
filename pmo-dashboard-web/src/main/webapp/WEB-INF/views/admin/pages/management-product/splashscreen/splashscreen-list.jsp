<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/master-data/material/json" />";
</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Material
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="#">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Material</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<c:if test="${not empty ret_code}">
    <div class="row">
        <div class="col-md-12">
            <div class="note ${ret_class}">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>${ret_message}</p>
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-energy"></i>Master Material
                </div>
                <div class="actions">
                    <a href="<c:url value="/admin/master-data/material/create"/>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-number table-action table-strip table-condensed table-datatable table-box" id="material-table" style="margin: 0px">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="10%">
                                No
                            </th>
                            <th>
                                Nama Material
                            </th>
                            <th>
                                Deskripsi
                            </th>
                            <th class="text-center" style="width: 95px">
                                Action
                            </th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        <%--</div>--%>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
</div>
<!-- END PAGE CONTENT-->