<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Produk <small>daftar produk</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Managemen Produk</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Produk</a>
            <c:if test="${label == 'create' || label == 'update' || label == 'detail'}">
                <i class="fa fa-angle-right"></i>
            </c:if>
        </li>
        <c:if test="${label == 'create' || label == 'update' || label == 'detail'}">
            <li>
                <a href="#">
                    <c:choose>
                        <c:when test="${label == 'create'}">
                            Tambah
                        </c:when>
                        <c:when test="${label == 'update'}">
                            Edit
                        </c:when>
                        <c:when test="${label == 'detail'}">
                            Detail
                        </c:when>
                    </c:choose>
                    Produk
                </a>
            </li>
        </c:if>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-custom tabbable-full-width">
    <ul class="nav nav-tabs">
        <c:if test="${label == 'list'}">
            <li class="active" id="tab-product-list">
                <a href="<c:url value="/admin/product"/>">
                    Daftar Produk </a>
            </li>
            <li id="tab-product-form">
                <a href="<c:url value="/admin/product/create"/>" class="btn tooltips" data-original-title="Tambah Produk" data-placement="right">
                    <i class="fa fa-plus"></i>
                </a>
            </li>
        </c:if>
        <c:if test="${label == 'create'}">
            <li id="tab-product-list">
                <a href="<c:url value="/admin/product"/>">
                    Daftar Produk </a>
            </li>
            <li class="active" id="tab-product-form">
                <a href="#product-form" data-toggle="tab" class="btn tooltips" data-original-title="Tambah Produk" data-placement="right">
                    <i class="fa fa-plus"></i>
                </a>
            </li>
        </c:if>
        <c:if test="${label == 'update' || label == 'detail'}">
            <li id="tab-product-list">
                <a href="<c:url value="/admin/product"/>">
                    Daftar Produk </a>
            </li>
            <li class="active" id="tab-product-form">
                <a href="#product-form" data-toggle="tab" class="btn">
                    <c:if test="${label == 'update'}">Edit</c:if>
                    <c:if test="${label == 'detail'}">Detail</c:if>
                </a>
            </li>
            <li id="tab-product-file">
                <a href="#product-file" data-toggle="tab">
                    Upload File</a>
            </li>
            <li id="tab-product-image">
                <a href="#product-image" data-toggle="tab">
                    Upload Gambar 3D</a>
            </li>
        </c:if>
        <%--<li id="tab-product-group" class="display-hide">--%>
            <%--<a href="#product-group" data-toggle="tab">--%>
                <%--Group Produk </a>--%>
        <%--</li>--%>
        <%--<li id="tab-product-comment" class="display-hide">--%>
            <%--<a href="#product-comment" data-toggle="tab">--%>
                <%--Komentar</a>--%>
        <%--</li>--%>
    </ul>
    <div class="tab-content no-padding">

        <c:if test="${label == 'list'}">
            <div class="tab-pane fade active in" id="product-list">
                <tiles:insertAttribute name="product-list" ignore="true"/>
            </div>
            <div class="tab-pane fade"  id="product-group">
                <tiles:insertAttribute name="product-group" ignore="true"/>
            </div>
        </c:if>

        <c:if test="${label == 'update' || label == 'create' || label == 'detail'}">
            <div class="tab-pane fade active in"  id="product-form">
                <tiles:insertAttribute name="product-form" ignore="true"/>
            </div>
            <div class="tab-pane fade"  id="product-file">
                <tiles:insertAttribute name="product-file" ignore="true"/>
            </div>
            <div class="tab-pane fade"  id="product-image">
                <tiles:insertAttribute name="product-image" ignore="true"/>
            </div>
        </c:if>

        <div class="tab-pane fade"  id="product-comment">
            <tiles:insertAttribute name="product-comment" ignore="true"/>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
