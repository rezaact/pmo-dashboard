<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>
    RECORDS_TOTAL = 10;
    LENGTH = 10;
    URL = "<c:url value="/admin/product/json" />";
</script>

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-rocket"></i>
            </div>
            <div class="details">
                <div class="number">
                        <c:choose>
                            <c:when test="${not empty totalProject}">
                                ${totalProject}
                            </c:when>
                            <c:otherwise>
                                0
                            </c:otherwise>
                        </c:choose>
                </div>
                <div class="desc">
                    Jumlah Project
                </div>
            </div>
            <a class="more" href="<c:url value="/admin/product"/>">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-money"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalRAB}">
                            ${totalRAB}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Total Biaya (dalam Rupiah)
                </div>
            </div>
            <a class="more" href="#" data-toggle="modal" data-target="#modal-total-biaya">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
            <div class="modal fade" id="modal-total-biaya" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">
                                Total RAB : Rp.
                                <c:choose>
                                    <c:when test="${not empty totalRAB}">
                                        ${totalRAB}
                                    </c:when>
                                    <c:otherwise>
                                        0
                                    </c:otherwise>
                                </c:choose>
                            </h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover table-number table-action table-datatable" id="product-list-table">
                                    <thead>
                                    <tr role="row" class="heading">
                                        <th >
                                            NOMOR PRODUK
                                        </th>
                                        <th >
                                            NAMA PRODUK
                                        </th>
                                        <th >
                                            NOMOR PENUGASAN
                                        </th>
                                        <th>
                                            NILAI RAB (RP)
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-group"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalUserApproval}">
                            ${totalUserApproval}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Registered User
                </div>
            </div>
            <a class="more" href="<c:url value="/admin/user"/>">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow">
            <div class="visual">
                <i class="fa fa-user"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalUserPending}">
                            ${totalUserPending}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Pending User
                </div>
            </div>
            <a class="more" href="<c:url value="/admin/user"/>">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>