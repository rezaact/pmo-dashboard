<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Workshop
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Workshop</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Workshop</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
    <div class="portlet light bg-inverse form-fit">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Workshop
                </span>
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="<c:if test="${label == 'create'}"><c:url value="/admin/master-data/workshop"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/master-data/workshop?id=${ID_WORKSHOP}"/></c:if>" class="form-horizontal form-bordered" method="post">
                <input type="hidden" name="PRM_ID_WORKSHOP" value="${ID_WORKSHOP}"/>
                <input type="hidden" name="PRM_PIC" value="${PIC}"/>
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Nama Workshop</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_WORKSHOP}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_NAMA_WORKSHOP" value="${NAMA_WORKSHOP}"></c:if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Nomor Telephone</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${TELP}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_TELP" value="${TELP}"></c:if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Nomor FAX</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${FAKS}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_FAKS" value="${FAKS}"></c:if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${EMAIL_PIC}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_EMAIL_PIC" value="${EMAIL_PIC}"></c:if>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Alamat</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${ALAMAT}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><textarea class="form-control" name="PRM_ALAMAT">${ALAMAT}</textarea></c:if>
                        </div>
                    </div>
                    <div class="form-group last">
                        <label class="control-label col-md-3">Deskripsi</label>
                        <div class="col-md-9">
                            <c:if test="${label == 'detail'}"><p class="form-control-static">${DESKRIPSI}</p></c:if>
                            <c:if test="${label == 'update' || label == 'create'}"><textarea class="form-control" name="PRM_DESKRIPSI">${DESKRIPSI}</textarea></c:if>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <c:if test="${label == 'update' || label =='create'}">
                                <button type="submit" class="btn green"><i class="fa fa-check"></i><c:if test="${label == 'create'}">Submit</c:if><c:if test="${label == 'update'}">Update</c:if></button>
                                    <a href="#cancel" class="btn btn-danger" data-toggle="modal">Cancel</a>
                               </c:if>
                            <c:if test="${label == 'detail'}">
                                <a href="<c:url value="/admin/master-data/workshop"/>" class="btn btn-danger" data-toggle="modal">Kembali</a>
                            </c:if>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
            <!-- MODAL CANCEL -->
            <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="cancel">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-red">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"></h4>
                        </div>
                        <div class="modal-body">
                            <p>Apakah Anda ingin membatalkan? </p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                            <a href="<c:url value="/admin/master-data/workshop"/>" class="btn btn-success">ya</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<!-- END PAGE CONTENT-->