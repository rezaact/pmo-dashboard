<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Workshop
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Workshop</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<c:if test="${not empty ret_code}">
    <div class="row">
        <div class="col-md-12">
            <div class="note ${ret_class}">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>${ret_message}</p>
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-12">
        <!-- Setting alert untuk message -->
        <c:if test="${delete != null}">
            <div class="note note-success">
                <p>
                    Data berhasil dihapus !
                </p>
            </div>
        </c:if>
        <c:if test="${banned != null}">
            <div class="note note-success">
                <p>
                    Data berhasil dibanned !
                </p>
            </div>
        </c:if>
        <c:if test="${active != null}">
            <div class="note note-success">
                <p>
                    Data berhasil diaktifkan !
                </p>
            </div>
        </c:if>
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-wrench"></i>Master Workshop
                </div>
                <div class="actions">
                    <a href="<c:url value="/admin/master-data/workshop/create"/>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                   <table class="table table-hover table-bordered table-number table-action table-striped table-condensed" style="margin: 0px">
                        <thead>
                        <tr class="heading">
                            <th>
                                No.
                            </th>
                            <th>
                                Workshop
                            </th>
                            <th>
                                Deskripsi
                            </th>
                            <th>
                                Alamat
                            </th>
                            <th>
                                Email
                            </th>
                            <th class="text-right" style="width: 128px">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="workshop" items="${listWorkshop}" varStatus="number">
                            <tr <c:if test="${workshop.status == 0}">class="danger"</c:if>>
                                <td class="text-center">
                                    <c:out value="${number.count}."/>
                                </td>
                                <td>
                                    ${workshop.NAMA_WORKSHOP}
                                </td>
                                <td>
                                    ${workshop.DESKRIPSI}
                                </td>
                                <td>
                                     ${workshop.ALAMAT}
                                </td>
                                <td>
                                    <a href="mailto:${workshop.EMAIL_PIC}" target="_blank">${workshop.EMAIL_PIC}</a>
                                </td>
                                <td class="text-right">
                                    <a href="<c:url value="/admin/master-data/workshop/detail?id=${workshop.id_workshop}"/>" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>

                                    <a href="<c:url value="/admin/master-data/workshop/update?id=${workshop.id_workshop}"/> " class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>

                                    <a href="#workshop-delete-${workshop.id_workshop}" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>
                                    <div id="workshop-delete-${workshop.id_workshop}" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-red">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">${workshop.unit}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p style="margin: 0px">
                                                        Apakah anda yakin akan menghapus permanen ${workshop.unit} ?
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                                    <a href="<c:url value="/admin/master-data/workshop/delete?id=${workshop.id_workshop}"/>" class="btn btn-success">Ya</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->