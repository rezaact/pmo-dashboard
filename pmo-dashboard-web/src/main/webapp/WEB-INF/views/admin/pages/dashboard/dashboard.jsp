<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>

    visitors = new Array();
    <c:forEach var="top5ProductByRate" items="${listTop5ProductByRate}">
        var top5Data =  new Array();
        top5Data.push('${top5ProductByRate.NOMOR_PRODUK}');
        top5Data.push(${top5ProductByRate.RATING});
        visitors.push(top5Data);
    </c:forEach>

    console.log('visitors', visitors);
    counterHitArray = new Array();
    <c:forEach var="counterHit" items="${listCounterHit}">
        var counterHitData =  new Array();
        counterHitData.push('${counterHit.TANGGAL}');
        counterHitData.push(${counterHit.JML});
        counterHitArray.push(counterHitData);
    </c:forEach>

    console.log('counterHitArray', counterHitArray);
</script>

<tiles:insertAttribute name="dashboard-breadcrumb"/>
<tiles:insertAttribute name="dashboard-stat"/>
<tiles:insertAttribute name="dashboard-graph"/>
<%--<tiles:insertAttribute name="dashboard-workshop"/>--%>
