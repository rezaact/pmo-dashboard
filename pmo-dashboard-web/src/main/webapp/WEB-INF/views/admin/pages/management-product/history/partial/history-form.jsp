<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
<div class="col-md-12">

<form id="form-product" action="<c:if test="${label == 'create'}"><c:url value="/admin/history"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/history?id="/>${ID_HISTORY}</c:if>"  method="post" enctype="multipart/form-data" class="form-horizontal form">
<div class="form-header padding-0">
    <c:if test="${success == true}">
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
                Data berhasil <c:if test="${success_update == true}">Diupdate</c:if><c:if test="${success_create == true}">Ditambah</c:if>
        </div>
    </c:if>
    <div class="alert alert-danger display-hide">
        <button class="close" data-close="alert"></button>
        <c:choose>
            <c:when test="${success == false}">
                <c:out value="${message}"/>
            </c:when>
            <c:otherwise>
                Pengisian form anda salah, mohon cek kembali.
            </c:otherwise>
        </c:choose>
    </div>
</div>
<div class="form-body padding-0">
<div class="portlet light bg-inverse form-fit">
    <div class="portlet-title">
        <div class="caption">
            Detail Penggunaan Produk
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="row">
            <div class="col-md-6">
                <input type="hidden" class="form-control" name="PRM_ID_HISTORY" value="${ID_HISTORY}">
                <input type="hidden" class="form-control" name="PRM_NOMOR_PRODUK" value="${nomor_produk}">
                <div class="form-group margin-top-10">
                    <label class="col-md-7 control-label">
                        Lokasi Pemasangan <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-5">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_LOKASI}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select class="form-control select2me select2-small" name="PRM_ID_LOKASI_PEMASANGAN" data-placeholder="Pilih...">
                                    <option value=""></option>
                                    <c:forEach var="pemasangan" items="${lokasiPemasangan}">
                                        <option <c:if test="${pemasangan.ID_LOKASI_PEMASANGAN == ID_LOKASI_PEMASANGAN}"> selected </c:if> value="${pemasangan.ID_LOKASI_PEMASANGAN}">${pemasangan.NAMA_LOKASI}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-7 control-label">
                        Nomor Penugasan <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-5">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${NOMOR_PENUGASAN}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="PRM_NOMOR_PENUGASAN" value="${NOMOR_PENUGASAN}">
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-7 control-label">
                        Perihal <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-5">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${PERIHAL}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control" name="PRM_PERIHAL" value="${PERIHAL}">
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-7 control-label">
                        Tanggal Penugasan <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-5">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${TGL_PENUGASAN_IND}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control date-picker" name="PRM_TGL_PENUGASAN" value="${TGL_PENUGASAN}">
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-7 control-label">
                        Time Schedule Pelaksanaan Penugasan <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-5">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${JADWAL_PELAKSANAAN_PENUGASAN} Hari</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right input-number">
                                <i class="fa"></i>
                                <input type="number" class="form-control" name="PRM_TS_PELAKSANAAN_PENUGASAN" value="${JADWAL_PELAKSANAAN_PENUGASAN}">
                            </div>
                        </c:if>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group margin-top-10">
                    <label class="col-md-5 control-label">
                        Realisasi Waktu Pelaksanaan <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-6">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${REALISASI_PELAKSANAAN_PENUGASA} Hari</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right input-number">
                                <i class="fa"></i>
                                <input type="number" class="form-control" name="PRM_REALISASI_PP" value="${REALISASI_PELAKSANAAN_PENUGASA}" data-placement="left">
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-5 control-label">
                        Nilai Penugasan (Rupiah) <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-6">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">Rp. ${NILAI_PENUGASAN_IND}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <input type="text" class="form-control currency" name="PRM_NILAI_PENUGASAN" value="${NILAI_PENUGASAN}" data-placement="left">
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-5 control-label">
                        Pemberi Kerja <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-6">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_PEMBERI_PENUGASAN}</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                <select class="form-control select2me select2-small" name="PRM_ID_PEMBERI_PENUGASAN" data-placeholder="Pilih..." data-placement="left">
                                    <option value=""></option>
                                    <c:forEach var="penugasan" items="${pemberiPenugasan}">
                                        <option <c:if test="${penugasan.ID_PEMBERI_PENUGASAN== ID_PEMBERI_PENUGASAN}"> selected </c:if> value="${penugasan.ID_PEMBERI_PENUGASAN}">${penugasan.NAMA_PEMBERI_PENUGASAN}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </c:if>
                    </div>
                </div>
                <div class="form-group margin-top-10">
                    <label class="col-md-5 control-label">
                        Jumlah Items <span aria-required="true" class="required"> * </span>
                    </label>
                    <div class="col-md-6">
                        <c:if test="${label == 'detail'}"><p class="form-control-static">${JUMLAH_ITEM_PEKERJAAN} Hari</p></c:if>
                        <c:if test="${label == 'update' || label == 'create'}">
                            <div class="input-icon right input-number">
                                <i class="fa"></i>
                                <input type="number" class="form-control" name="PRM_ITEM_PEKERJAAAN" value="${JUMLAH_ITEM_PEKERJAAN}" data-placement="left">
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    <div class="form-body padding-0">
        <div class="portlet light bg-inverse form-fit">
            <div class="portlet-title">
                <div class="caption">
                    Biaya Penggunaan Produk
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse">
                    </a>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group margin-top-10">
                            <label class="col-md-5 control-label">
                                RAB  Produksi (Rupiah) <span aria-required="true" class="required"> * </span>
                            </label>
                            <div class="col-md-7">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">Rp. ${RAB_PRODUKSI_IND}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control currency" name="PRM_RAB_PRODUKSI" value="${RAB_PRODUKSI}">
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="form-group margin-top-10">
                            <label class="col-md-5 control-label">
                                Harga Pokok Produksi <span aria-required="true" class="required"> * </span>
                            </label>
                            <div class="col-md-7">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">Rp. ${HARGA_POKOK_PRODUKSI_IND}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control currency" name="PRM_HARGA_POKOK_PRODUKSI" value="${HARGA_POKOK_PRODUKSI}">
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group margin-top-10">
                            <label class="col-md-5 control-label">
                                Realisasi Biaya Produksi <span aria-required="true" class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">Rp. ${REALISASI_BIAYA_PRODUKSI_IND}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control currency" name="PRM_REALISASI_BIAYA_PRODUKSI" value="${REALISASI_BIAYA_PRODUKSI}" data-placement="left">
                                    </div>
                                </c:if>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="form-actions">
    <div class="pull-right">
        <%--<c:if test="${label == 'detail'}">
            <a href="<c:url value="/admin/history"/>" class="btn btn-info">Kembali</a>
        </c:if>--%>
        <c:if test="${label == 'update' || label == 'create'}">
            <a href="<c:url value="/admin/history"/>" class="btn btn-danger">Batal</a>
            <button type="submit" class="btn btn-success">Submit</button>
        </c:if>
    </div>
</div>
</form>

</div>
</div>
 

