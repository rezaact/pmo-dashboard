<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
  Layanan Kami
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa icon-home"></i>
      <a href="<c:url value="/dashboard"/>">Dashboard</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a>Pengaturan</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Layanan Kami</a>
    </li>
  </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
  <div class="col-md-12">
    <!-- Setting alert untuk message -->
    <c:if test="${delete != null}">
      <div class="note note-success">
        <p>
          Data berhasil dihapus !
        </p>
      </div>
    </c:if>
    <c:if test="${banned != null}">
      <div class="note note-success">
        <p>
          Data berhasil dibanned !
        </p>
      </div>
    </c:if>
    <c:if test="${active != null}">
      <div class="note note-success">
        <p>
          Data berhasil diaktifkan !
        </p>
      </div>
    </c:if>
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa icon-wrench"></i>Layanan Kami
        </div>
        <div class="actions">
          <a href="<c:url value="/admin/setting/service/create"/>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah </a>
        </div>
      </div>
      <div class="portlet-body">
        <div class="table-responsive">
          <table class="table table-hover table-strip table-condensed" style="margin: 0px">
            <thead>
            <tr>
                <th>
                    Icon
                </th>
              <th>
                Judul
              </th>
              <th>
                Deskripsi
              </th>
              <th class="text-right" style="width: 128px">
                Action
              </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="service" items="${listService}" varStatus="number">
              <tr <c:if test="${service.status == 0}">class="danger"</c:if>>
                  <td class="text-center">
                          <i class="${service.ICON} fa-2x" style="margin-top: 10px"></i>
                  </td>
                <td>
                    ${service.JUDUL}
                </td>
                <td>
                    ${service.DESKRIPSI}
                </td>
                <td class="text-right">
                    <a href="<c:url value="/admin/setting/service/update?id=${service.id_service}"/> " class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>
                  <a href="<c:url value="/admin/setting/service/detail?id=${service.id_service}"/>" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>
                  <a href="#workshop-delete-${service.id_service}" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>
                  <div id="workshop-delete-${service.id_service}" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-red">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title">${service.JUDUL}</h4>
                        </div>
                        <div class="modal-body">
                          <p style="margin: 0px">
                            Apakah anda yakin akan menghapus permanen ${service.JUDUL} ?
                          </p>
                        </div>
                        <div class="modal-footer">
                          <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                          <a href="<c:url value="/admin/setting/service/delete?id=${service.id_service}"/>" class="btn btn-success">Ya</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
              </tr>
            </c:forEach>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
  </div>
</div>
<!-- END PAGE CONTENT-->