<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Management User
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Management User</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> User</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<c:if test="${label == 'update' || label == 'detail'}">
    <tiles:insertAttribute name="form-edit"/>
</c:if>
<c:if test="${label == 'create'}">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bg-inverse form-fit">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> User
                </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form action="<c:if test="${label == 'create'}"><c:url value="/admin/user"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/master-data/workshop?id=${ID_WORKSHOP}"/></c:if>" class="form-horizontal form-bordered user-form" method="post">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <p>Mohon form layanan diisi</p>
                        </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button>
                            <p>Data berhasil disimpan</p>
                        </div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Nama Lengkap</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_LENGKAP}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="PRM_NAMA_LENGKAP" value="${NAMA_LENGKAP}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">NIP</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${NIP}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="PRM_NIP" value="${NIP}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Unit</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${UNIT}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="PRM_UNIT" value="${UNIT}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Email</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static"><a href="mailto:${ALAMAT_EMAIL}">${ALAMAT_EMAIL}</a></p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="email" class="form-control" name="PRM_EMAIL" value="${ALAMAT_EMAIL}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">No Telp</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${NO_TELPON}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="PRM_NO_TELP" value="${NO_TELPON}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Username</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${USERNAME}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" name="PRM_USERNAME" value="${USERNAME}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Password</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">xxxxx</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="password" class="form-control" name="PRM_PASSWORD" value="${LOKASI}" id="password">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Konfirmasi Password</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">xxxxx</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="password" class="form-control" name="PRM_KONF_PASSWORD" value="${LOKASI}">
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-4">Role</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_ROLE}</p></c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2me id_role" name="PRM_ID_ROLE">
                                                        <option value="0">Pilih...</option>
                                                        <c:forEach  var="role" items="${listRole}" varStatus="number">
                                                            <option  <c:if test="${ID_ROLE == role.ID_ROLE}"> selected="selected" </c:if>value="${role.ID_ROLE}">${role.NAMA_ROLE}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row pull-right">
                                <div class="col-md-12">
                                    <c:if test="${label == 'update' || label =='create'}">
                                        <button type="submit" class="btn green"><i class="fa fa-check"></i><c:if test="${label == 'create'}">Submit</c:if><c:if test="${label == 'update'}">Update</c:if></button>
                                        <a href="<c:url value="/admin/user"/>" class="btn btn-danger">Cancel</a>
                                    </c:if>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
        </div>
    </div>
</c:if>



<!-- END PAGE CONTENT-->