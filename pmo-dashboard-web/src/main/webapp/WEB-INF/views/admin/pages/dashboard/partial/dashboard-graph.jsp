<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet solid bordered grey-cararra">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-bullhorn"></i> Dashboard COD : Transmisi
                </div>
                <div class="actions">
                    <%--<div class="btn-group">
                        <a aria-expanded="false" class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
                            Filter By <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="top-project-filter-by">
                            <li>
                                <a href="javascript:;" data-value="customer"> Customer </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-value="material"> Material </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-value="workshop"> Workshop </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-value="group"> Group </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-value="jenis-pekerjaan"> Jenis Pekerjaan </a>
                            </li>
                        </ul>
                    </div>--%>
                    <%--<div class="btn-group">
                        <a aria-expanded="false" class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
                            Filter Range <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="top-project-filter-range">
                            <li>
                                <a href="javascript:;"> 2015 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2016 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2017 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2018 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2019 </a>
                            </li>
                            <li>
                                <a href="javascript:;"> 2020 </a>
                            </li>
                        </ul>
                    </div>--%>
                </div>
            </div>
            <div class="portlet-body">
                <div id="testFusionChart" style="width: 100%; height: 360px;"></div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet solid grey-cararra bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i> Milestone
                </div>
            </div>
            <div class="portlet-body">
                <div id="testFusionMap" style="width: 100%; height: 360px;"></div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<div class="clearfix"></div>