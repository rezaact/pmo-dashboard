<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<mvc:resources mapping="/assets/**" location="/assets/" />
<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/reporting/historikal-produk/json" />";
</script>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Historikal produk
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Reporting</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Historikal produk</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <form novalidate="novalidate" id="form-product" action="/e-catalog/admin/product" method="post" enctype="multipart/form-data" class="form-horizontal form">
            <div class="form-header padding-0">
                <c:if test="${success == true}">
                    <div class="alert alert-success display-hide">
                        <button class="close" data-close="alert"></button>
                        <c:out value="${message}"/>
                    </div>
                </c:if>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <c:choose>
                        <c:when test="${success == false}">
                            <c:out value="${message}"/>
                        </c:when>
                        <c:otherwise>
                            Pengisian form anda salah, mohon cek kembali.
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="form-body padding-0">
                <div class="portlet light bg-inverse">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group margin-top-10">
                                    <label class="col-md-5 control-label">Unit Workshop</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select class="form-control select2me" name="prm_id_workshop" id="id_workshop" data-placeholder="Pilih...">
                                                <option value=""></option>
                                                <c:forEach items="${listManufaktur}" var="workshop">
                                                    <option value="${workshop.id}">${workshop.text}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-top-10">
                                    <label class="col-md-5 control-label" style="margin-top: 20px">Tgl Penugasan</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control text-capitalize date-picker" name="XXX" style="margin-top: 20px" id="tgl_penugasan">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group margin-top-10">
                                    <label class="col-md-5 control-label">Kategori</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select class="form-control select2me" name="PRM_KATEGORI" data-placeholder="Pilih..." style="margin-top: 20px" id="id_kategori">
                                                <option value=""></option>
                                                <c:forEach items="${listKategoriProduk}" var="kategori">
                                                    <option value="${kategori.id}">${kategori.text}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group margin-top-10">
                                    <label class="col-md-5 control-label" style="margin-top: 20px">Jenis Produk</label>
                                    <div class="col-md-7" style="margin-top: 20px">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <select class="form-control select2me" name="PRM_KATEGORI" data-placeholder="Pilih..." id="id_jenis">
                                                <option value=""></option>
                                                <c:forEach items="${listJenisProduk}" var="jenis">
                                                    <option value="${jenis.id}">${jenis.text}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group margin-top-10">
                                    <label class="col-md-4 control-label">Tahun</label>
                                    <div class="col-md-8">
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                            <input type="text" class="form-control text-capitalize" name="XXX" id="tahun">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <a href="#" class="btn btn-primary" id="excel">Excel</a>
                                    <button type="button" class="btn btn-success" id="filter">
                                        Filter
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-book-open"></i>Historikal produk
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title="">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive" style="overflow-y: hidden;overflow-x: scroll">
                    <table class="table table-striped table-bordered table-hover table-number table-action table-datatable" id="historikal-list-table">
                        <thead>
                        <tr role="row" class="heading">
                            <th>
                                Nomor Produk
                            </th>
                            <th>
                                Lokasi pemasangan
                            </th>
                            <th>
                                Nomor penugasan
                            </th>
                            <th>
                                Perihal
                            </th>
                            <th>
                                Tanggal penugasan
                            </th>
                            <th>
                                Time schedule (Hari)
                            </th>
                            <th>
                                Realisasi waktu pelaksanaan (Hari)
                            </th>
                            <th>
                                Nilai penugasan (Rp.)
                            </th>
                            <th>
                                Pemberi kerja
                            </th>
                            <th>
                                Jumlah items
                            </th>
                            <th>
                                RAB Produksi (Rp.)
                            </th>
                            <th>
                                Harga pokok produk (Rp.)
                            </th>
                            <th>
                                Realisasi biaya produksi (Rp.)
                            </th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->