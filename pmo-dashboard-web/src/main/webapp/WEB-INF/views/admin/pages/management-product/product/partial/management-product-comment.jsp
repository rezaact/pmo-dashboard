<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-hover table-number table-action" id="comment-table">
            <thead>
            <tr role="row" class="heading">
                <th width="10%">
                    No
                </th>
                <th width="15%">
                    Tanggal
                </th>
                <th width="15%">
                    Email
                </th>
                <th width="10%">
                    Komentar
                </th>
                <th width="10%">
                    Action
                </th>
            </tr>
            </thead>
            <tbody>
            <tr role="row" class="filter">
                <td>
                    1
                </td>
                <td>
                    22-03-2015
                </td>
                <td>
                    testing@pln.com
                </td>
                <td>
                    Sangat bagus
                </td>
                <td>
                    <div class="margin-bottom-5">
                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                    </div>
                </td>
            </tr>
            <tr role="row" class="filter">
                <td>
                    2
                </td>
                <td>
                    22-03-2015
                </td>
                <td>
                    testing@pln.com
                </td>
                <td>
                    Wow awesome..
                </td>
                <td>
                    <div class="margin-bottom-5">
                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                    </div>
                </td>
            </tr>
            <tr role="row" class="filter">
                <td>
                    4
                </td>
                <td>
                    03-03-2015
                </td>
                <td>
                    testing@pln.com
                </td>
                <td>
                    Sangat menggumkan
                </td>
                <td>
                    <div class="margin-bottom-5">
                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>