<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<h3 class="page-title">
    Produk <small>daftar produk</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Managemen Produk</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Produk</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Readmore ${NAMA_PRODUK}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-12">

    <form id="form-product" action="<c:if test="${label == 'create'}"><c:url value="/admin/product"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/product?id=${NOMOR_PRODUK}"/></c:if><c:if test="${label == 'readmore' && DESKRIPSI == null}"><c:url value="/admin/product/readmore/create"/></c:if><c:if test="${label == 'readmore' && DESKRIPSI != null}"><c:url value="/admin/product/readmore/update?id=${NOMOR_PRODUK}"/></c:if>"  method="post" enctype="multipart/form-data" class="form-horizontal form">
        <input type="hidden" name="PRM_NOMOR_PRODUK" value="${NOMOR_PRODUK}"/>
    <div class="form-header padding-0">
        <c:if test="${success == true}">
            <div class="alert alert-success display-hide">
                <button class="close" data-close="alert"></button>
                    <c:out value="${message}"/>
            </div>
        </c:if>
            <div class="alert alert-danger display-hide">
                <button class="close" data-close="alert"></button>
                <c:choose>
                    <c:when test="${success == false}">
                        <c:out value="${message}"/>
                    </c:when>
                    <c:otherwise>
                        Pengisian form anda salah, mohon cek kembali.
                    </c:otherwise>
                </c:choose>
            </div>
    </div>
    <div class="form-body padding-0">
    <div class="portlet">
        <div class="portlet-body">
            <div class="media">
                <div class="media-left" style="float: left; margin-right: 15px">
                    <a href="#">
                        <img src="<c:url value="/file/${ID_FILE}"/>" class="img-responsive" alt="${NAMA_PRODUK}" width="130"/>
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">${NAMA_PRODUK}</h4>
                    ${DESKRIPSI_PRODUK}
                </div>
            </div>
        </div>
    </div>
    <div class="portlet light bg-inverse">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> readmore produk
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="input-icon right">
                <i class="fa" style="top: 175px"></i>
                <textarea class="ckeditor form-control" name="PRM_DESKRIPSI" data-placement="left">${DESKRIPSI}</textarea>
            </div>
        </div>
    </div>
    </div>
    <div class="form-actions">
        <div class="pull-right">
            <a href="<c:url value="/admin/product"/>" class="btn btn-danger">Kembali</a>
            <c:if test="${label == 'create' || label == 'readmore'}">
                <button type="submit" class="btn btn-success">
                    Submit
                </button>
            </c:if>
            <c:if test="${label == 'update'}">
                <button type="submit" class="btn btn-success">
                    Update
                </button>
            </c:if>
        </div>
    </div>
    </form>

    </div>
</div>
 

