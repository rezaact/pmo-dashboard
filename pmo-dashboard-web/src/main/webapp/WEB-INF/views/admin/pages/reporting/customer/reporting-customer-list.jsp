<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Customers
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<?= site_url('backend/dashboard'); ?>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Customers</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-user-follow"></i>Master Customers
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title="">
                    </a>
                    <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title="">
                    </a>
                    <a href="javascript:;" class="reload" data-original-title="" title="">
                    </a>
                    <a href="javascript:;" class="remove" data-original-title="" title="">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Nama
                            </th>
                            <th>
                                NO Telp
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Alamat
                            </th>
                            <th>
                                Download
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                1
                            </td>
                            <td>
                                Arif Hidayatullah
                            </td>
                            <td>
                                085642396294
                            </td>
                            <td>
                                arifdegozaru24@gmail.com
                            </td>
                            <td>
                                Pekalongan
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                2
                            </td>
                            <td>
                                Reki Susanto
                            </td>
                            <td>
                                085641826003
                            </td>
                            <td>
                                rekisusanto@gmail.com
                            </td>
                            <td>
                                kendal
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                3
                            </td>
                            <td>
                                Ngadiono
                            </td>
                            <td>
                                081903062181
                            </td>
                            <td>
                                ondoel@gmail.com
                            </td>
                            <td>
                                Cilacap
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a> </td>
                        </tr>
                        <tr>
                            <td>
                                4
                            </td>
                            <td>
                                Eko Prasetyo
                            </td>
                            <td>
                                085658207794
                            </td>
                            <td>
                                simon@gmail.com
                            </td>
                            <td>
                                magelang
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a></td>
                        </tr>
                        <tr>
                            <td>
                                5
                            </td>
                            <td>
                                Joko Susmiyanto
                            </td>
                            <td>
                                085664212381
                            </td>
                            <td>
                                djoko@gmail.com
                            </td>
                            <td>
                                lampung
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a></td>
                        </tr>
                        <tr>
                            <td>
                                6
                            </td>
                            <td>
                                Retno Dwi Wulandari
                            </td>
                            <td>
                                085739096497
                            </td>
                            <td>
                                wulan@gmail.com
                            </td>
                            <td>
                                solo
                            </td>
                            <td>
                                <a href=""><button type="button" class="btn btn-success">PDF</button></a>
                                <a href=""><button type="button" class="btn btn-success">Excel</button></a>
                                <a href=""><button type="button" class="btn btn-success">Word</button></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->