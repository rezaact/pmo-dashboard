<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script>
    //untuk mengantisipasi agar jquery plugin multi upload hanya di create satu kali
    INIT_PLUGING_MULTI_UPLOAD = 0;
    //RECORDS_TOTAL = ${recordsTotal};
    //LENGTH = ${length};
    URL = "<c:url value="/admin/product/json" />";
    PRM_ID_HISTORY = '${ID_HISTORY}';
</script>
<div class="row">
    <div class="col-md-12">

        <div class="tabbable tabbable-custom tabbable-full-width">
            <ul class="nav nav-tabs">
                <li class="active" id="tab-ls-daftar-file">
                    <a href="#ls-daftar-file" data-toggle="tab">
                        Daftar File</a>
                </li>
                <li id="tab-upload-file">
                    <a href="#upload-file" data-toggle="tab">
                        Upload File</a>
                </li>

            </ul>
            <div class="tab-content no-padding">

                <div class="tab-pane active fade in" id="ls-daftar-file">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover table-number table-action" id="file-list-table">
                                <thead>
                                <tr role="row" class="heading">
                                    <th>
                                        Nomor Produk
                                    </th>
                                    <th>
                                        Nomor Penugasan
                                    </th>
                                    <th>
                                        Nama File
                                    </th>
                                    <th>
                                        Jenis File
                                    </th>
                                    <th style="width: 130px">
                                        Content Type
                                    </th>
                                    <th style="width: 130px !important;">
                                        Action
                                    </th>
                                </tr>
                                </thead>

                                <tbody>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="upload-file">

                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Multi Upload File
                            </div>
                            <div class="col-md-4">
                                <%--<button class="btn red" data-toggle="modal" data-target="#modal-hapus-image-3D">Hapus Data Sebelumnya</button>--%>
                                <select class="form-control select2me select2-small" name="PRM_JNS_UPLOAD" id="select2-jns-upload" data-placeholder="Pilih Jenis File yg akan di upload">
                                    <option value=""></option>
                                    <option value="0">File Penugasan</option>
                                    <option value="1">File RAB Produksi</option>
                                    <option value="2">File HPP</option>
                                    <option value="3">File Realisasi Produksi</option>
                                </select>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="fileupload-history" action="<c:url value="/admin/history/simpanHistoryFile"/>" method="POST" enctype="multipart/form-data">
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <c:if test="${label == 'update'}">
                                    <input type="hidden" name="PRM_ID_HISTORY" value="${ID_HISTORY}">
                                </c:if>
                                <input type="hidden" name="PRM_JENIS_UPLOAD" value="">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-lg-7">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                    <span class="btn green fileinput-button">
                                    <i class="fa fa-plus"></i>
                                    <span>
                                    Tambah Data </span>
                                    <input type="file" name="PRM_FILE_HISTORY" multiple="">
                                    </span>
                                        <button type="submit" class="btn blue start">
                                            <i class="fa fa-upload"></i>
                                    <span>
                                    Mulai Mengupload</span>
                                        </button>
                                        <button type="reset" class="btn warning cancel">
                                            <i class="fa fa-ban-circle"></i>
                                    <span>
                                    Batal upload </span>
                                        </button>
                                        <button type="button" class="btn red delete display-none">
                                            <i class="fa fa-trash"></i>
                                    <span>
                                    Hapus </span>
                                        </button>
                                        <input type="checkbox" class="toggle" >
                                        <!-- The global file processing state -->
                                    <span class="fileupload-process">
                                    </span>
                                    </div>
                                    <!-- The global progress information -->
                                    <div class="col-lg-5 fileupload-progress fade">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;">
                                            </div>
                                        </div>
                                        <!-- The extended global progress information -->
                                        <div class="progress-extended">
                                            &nbsp;
                                        </div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped clearfix">
                                    <tbody class="files-pdf">
                                    </tbody>
                                </table>
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Catatan : </h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul>
                                            <%--<li>
                                                Ukuran file maksimum untuk upload <strong>5 MB</strong>
                                            </li>--%>
                                            <li>
                                                Hanya file <strong>PDF Dan EXCEL</strong> yang diperbolehkan.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
 

