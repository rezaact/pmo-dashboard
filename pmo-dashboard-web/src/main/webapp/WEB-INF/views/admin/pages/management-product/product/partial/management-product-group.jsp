<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
<div class="col-md-4">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-group"></i>Pilih Group
            </div>
            <div class="tools">
                <a href="javascript:;" class="collapse">
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <div id="product-group" class="tree-demo"></div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption col-md-7 col-sm-12">
                <i class="fa fa-list"></i>Daftar Produk
            </div>
            <div class="actions pull-right">
                <div class="btn-group">
                    <a href="#" class="btn btn-success" data-toggle="modal" data-target="#tambah-produk">
                        <i class="fa fa-plus"></i>
								<span class="hidden-480">
								Produk </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-number table-action" id="product-group-table">
                <thead>
                <tr role="row" class="heading">
                    <th width="10%">
                        No
                    </th>
                    <th width="15%">
                        Gambar
                    </th>
                    <th width="15%">
                        Nama Gambar
                    </th>
                    <th width="10%">
                        Produk
                    </th>
                    <th width="10%">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr role="row" class="filter">
                    <td>
                        1
                    </td>
                    <td>
                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/> "/>
                    </td>
                    <td>
                        Thumbnail
                    </td>
                    <td>
                        1
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                        </div>
                    </td>
                </tr>
                <tr role="row" class="filter">
                    <td>
                        2
                    </td>
                    <td>
                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/> "/>
                    </td>
                    <td>
                        Thumbnail
                    </td>
                    <td>
                        2
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                        </div>
                    </td>
                </tr>
                <tr role="row" class="filter">
                    <td>
                        3
                    </td>
                    <td>
                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/> "/>
                    </td>
                    <td>
                        Thumbnail
                    </td>
                    <td>
                        3
                    </td>
                    <td>
                        <div class="margin-bottom-5">
                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>
</div>
</div>