<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Splashscreen
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Splashscreen</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Splashscreen</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bg-inverse form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Splashscreen
                </span>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="<c:if test="${label == 'create'}"><c:url value="/admin/master-data/splash"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/master-data/splash?id=${ID_SPLASHSCREEN}"/></c:if>" class="form-horizontal form-bordered" method="post"  enctype="multipart/form-data">
                    <input type="hidden" name="PRM_ID_SPLASHSCREEN" value="${ID_SPLASHSCREEN}"/>

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Urutan</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">${URUTAN}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_URUTAN" value="${URUTAN}"></c:if>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Deskripsi</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">${DESKRIPSI}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_DESKRIPSI" value="${DESKRIPSI}"></c:if>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nomor Produk</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">${NOMOR_PRODUK}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_NOMOR_PRODUK" value="${NOMOR_PRODUK}"></c:if>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Gambar</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static"><img src="<c:url value="/file/${ID_FILE}"/>" alt="hot_info"></p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><input type="file" class="form-control" name="PRM_GAMBAR" value=""></c:if>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <c:if test="${label == 'update' || label =='create'}">
                                    <button type="submit" class="btn green"><i class="fa fa-check"></i><c:if test="${label == 'create'}">Submit</c:if><c:if test="${label == 'update'}">Update</c:if></button>
                                    <%--<a href="<c:url value="/admin/master-data/splash"/>" class="btn btn-danger">Cancel</a>--%>
                                    <a href="#cancel" class="btn btn-danger" data-toggle="modal">Cancel</a>
                                </c:if>
                                <c:if test="${label == 'detail'}">
                                    <a href="<c:url value="/admin/master-data/splash"/>" class="btn btn-danger" data-toggle="modal">Kembali</a>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
                <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="cancel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-red">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <p>Apakah Anda ingin Membatalkan ? </p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                <a href="<c:url value="/admin/master-data/splash"/>" class="btn btn-success">ya</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->