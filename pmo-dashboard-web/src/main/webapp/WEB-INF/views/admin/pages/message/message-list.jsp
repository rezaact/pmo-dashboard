<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
  RECORDS_TOTAL = ${recordsTotal};
  LENGTH = ${length};
  URL = "<c:url value="/admin/message/json" />";
</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
  Pesan
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa icon-home"></i>
      <a href="<c:url value="/dashboard"/>">Dashboard</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a>Pesan</a>
      <i class="fa fa-angle-right"></i>
    </li>
  </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
  <div class="col-md-12">
    <!-- Setting alert untuk message -->
    <c:if test="${delete != null}">
      <div class="note note-success">
        <p>
          Data berhasil dihapus !
        </p>
      </div>
    </c:if>
    <c:if test="${banned != null}">
      <div class="note note-success">
        <p>
          Data berhasil dibanned !
        </p>
      </div>
    </c:if>
    <c:if test="${active != null}">
      <div class="note note-success">
        <p>
          Data berhasil diaktifkan !
        </p>
      </div>
    </c:if>
    <!-- BEGIN SAMPLE TABLE PORTLET-->
    <div class="portlet box red">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa icon-wrench"></i>Pesan
        </div>
      </div>
      <div class="portlet-body">
        <div class="table-responsive">
          <table class="table table-hover table-strip table-condensed table-datatable table-box" id="message-table" style="margin: 0px">
            <thead>
            <tr role="row" class="heading">
              <th>
                No.
              </th>
              <th>
                Nomor Produk
              </th>
              <th>
                Nama
              </th>
              <th>
                Email
              </th>
                <th>
                No Telp
              </th>
              <th>
                Pesan
              </th>
              <th>
                Tanggal pesan
              </th>
              <th>
                Status
              </th>
              <th class="text-center" style="width: 150px">
                Action
              </th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <!-- END SAMPLE TABLE PORTLET-->
  </div>
</div>
<!-- END PAGE CONTENT-->