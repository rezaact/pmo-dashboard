<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/product/json" />";
</script>
<%--<div class="row">
    <div class="col-md-12">
        <c:if test="${success != null && success == true}">
            <div class="note note-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>
                    <strong>Sukses !</strong> Data produk berhasil Disimpan
                    &lt;%&ndash;<c:if test="${success_create == true}">ditambahkan</c:if>
                    <c:if test="${success_delete == true}">dihapus</c:if>
                    <c:if test="${success_update == true}">dirubah</c:if>.&ndash;%&gt;
                </p>
            </div>
        </c:if>
    </div>
</div>--%>

<c:if test="${not empty ret_code}">
    <div class="row">
        <div class="col-md-12">
            <div class="note ${ret_class}">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>${ret_message}</p>
            </div>
        </div>
    </div>
</c:if>

<div class="row">
<div class="col-md-12">
    <div class="portlet">
        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-number table-action table-datatable" id="product-list-table">
                <thead>
                    <tr role="row" class="heading">
                        <th>
                            Gambar
                        </th>
                        <th style="width: 110px">
                            Nomor Produk
                        </th>
                        <th style="width: 130px">
                            Nama Produk
                        </th>
                        <th>
                            Deskripsi
                        </th>
                        <th style="width: 165px !important;">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

    </div>
</div>
</div>