<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/user/json" />";
</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Management User <small>daftar member</small>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Managemen User</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-check-square"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalUserApproval}">
                            ${totalUserApproval}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Registered User
                </div>
            </div>
            <a class="more" href="#">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat yellow">
            <div class="visual">
                <i class="fa fa-plus-square"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalUserPending}">
                            ${totalUserPending}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Pending User
                </div>
            </div>
            <a class="more" href="#">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-minus-square"></i>
            </div>
            <div class="details">
                <div class="number">
                    <c:choose>
                        <c:when test="${not empty totalUserBanned}">
                            ${totalUserBanned}
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="desc">
                    Banned User
                </div>
            </div>
            <a class="more" href="#">
                Detail <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <!-- Setting alert untuk message -->
        <c:if test="${save == true}">
            <div class="note note-success">
                <p>
                    Simpan data berhasil  !
                </p>
            </div>
        </c:if>
        <c:if test="${approve == true}">
            <div class="note note-success">
                <p>
                    Approve data berhasil !
                </p>
            </div>
        </c:if>
        <c:if test="${banned == true}">
            <div class="note note-success">
                <p>
                    Banned data berhasil !
                </p>
            </div>
        </c:if>
        <c:if test="${disapprove == true }">
            <div class="note note-success">
                <p>
                    Disapprove Data berhasil !
                </p>
            </div>
        </c:if>
        <c:if test="${delete == true}">
            <div class="note note-success">
                <p>
                    Hapus data berhasil !
                </p>
            </div>
        </c:if>

        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-user"></i>Management User
                </div>
                <div class="actions">
                    <a href="<c:url value="/admin/user/create"/>" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Tambah </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-action table-datatable" id="user-table">
                        <thead>
                        <tr role="row" class="heading">
                            <th class="text-center">
                                Username
                            </th>
                            <th class="text-center">
                                Email
                            </th>
                            <th class="text-center">
                                No Telp
                            </th>
                            <th class="text-center">
                                Status
                            </th>
                            <th class="text-center" style="width: managpx">
                                Tgl Register
                            </th>
                            <th class="text-center" style="width: 150px">
                                Hak Akses
                            </th>
                            <th class="text-center" style="width: 150px">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${data}" var="d">
                                <tr <c:if test="${d.AKTIF ==2}">class="warning"</c:if> <c:if test="${d.AKTIF ==3}">class="danger"</c:if>>
                                    <td>${d.USERNAME}</td>
                                    <td>
                                        <a href="mailto:${d.ALAMAT_EMAIL}">
                                             ${d.ALAMAT_EMAIL}
                                        </a>
                                    </td>
                                    <td>${d.NO_TELPON}</td>
                                    <td>
                                            <span class="label label-sm ${d.LABEL_CLASS}"> ${d.STATUS}</span>
                                    </td>
                                    <td>${d.TGL_REGISTER}</td>
                                    <td>
                                        <c:if test="${d.AKTIF ==1}">
                                        <select class="form-control select2me id_role" name="id_role" data-id="${d.ID_USER}">
                                            <option value="0">Pilih...</option>
                                            <c:forEach  var="role" items="${listRole}" varStatus="number">
                                            <option  <c:if test="${d.ID_ROLE == role.ID_ROLE}"> selected="selected" </c:if>value="${role.ID_ROLE}">${role.NAMA_ROLE}</option>
                                            </c:forEach>
                                        </select>
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${d.AKTIF == 2}">
                                        <div class="actions">
                                            <div class="btn-group">
                                                <a class="btn btn-sm btn-success tooltips" href="javascript:;" data-toggle="dropdown" data-container="body" data-placement="top"  data-original-title="Approve" >
                                                    <i class="fa fa-check"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <c:forEach  var="role" items="${listRole}" varStatus="number">
                                                        <li>
                                                            <a href="<c:url value="/admin/user/approve?id="/>${d.ID_USER}&role=${role.ID_ROLE}" data-val="${role.ID_ROLE}" data-id="${d.ID_USER}" class="approve">
                                                                <i class="fa fa-user"></i> ${role.NAMA_ROLE}</a>
                                                        </li>
                                                    </c:forEach>
                                                </ul>
                                            </div>
                                            <a href="<c:url value="/admin/user/detail?id=${d.id_user}"/>" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail"><i class="fa fa-eye"></i></a>
                                            <a href="#user-reject-${d.ID_USER}" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Disapprove" data-toggle="modal"><i class="fa fa-close"></i></a>
                                            <div id="user-reject-${d.ID_USER}" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-red">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                            <h4 class="modal-title">Disapprove</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p style="margin: 0px">
                                                                Apakah anda yakin akan menghapus permanen akun ${d.USERNAME} ?
                                                            </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                                            <a href="<c:url value="/admin/user/disapprove?id=${d.ID_USER}"/>" class="btn btn-success">Ya</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </c:if>
                                        <c:if test="${d.AKTIF !=2}">
                                        <a href="<c:url value="/admin/user/update?id=${d.id_user}"/> " class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>
                                        <a href="<c:url value="/admin/user/detail?id=${d.id_user}"/>" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" ><i class="fa fa-eye"></i></a>
                                        <c:if test="${d.AKTIF ==0 || d.AKTIF ==3}">
                                        <a href="#user-delete-${d.ID_USER}" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>
                                        <div id="user-delete-${d.ID_USER}" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-red">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title">Hapus Akun</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p style="margin: 0px">
                                                            Apakah anda yakin akan menghapus permanen ${d.USERNAME} ?
                                                        </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                                        <a href="<c:url value="/admin/user/delete?id=${d.ID_USER}"/>" class="btn btn-success">Ya</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </c:if>
                                            <c:if test="${d.AKTIF ==1}">
                                                <a href="#user-ban-${d.ID_USER}" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Banned" data-toggle="modal"><i class="fa fa-trash"></i></a>
                                                <div id="user-ban-${d.ID_USER}" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header bg-red">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                <h4 class="modal-title">Banned</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p style="margin: 0px">
                                                                    Apakah anda yakin akan banned akun ${d.USERNAME} ?
                                                                </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                                                <a href="<c:url value="/admin/user/banned?id=${d.ID_USER}"/>" class="btn btn-success">Ya</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
            <%--</div>--%>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
<!-- END PAGE CONTENT-->