<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="row">
    <div class="col-md-12">

        <%--<p>
						<span class="label label-danger">
						Catatan : </span>
            &nbsp; Plugin ini bekerja hanya pada Chrome terbaru , Firefox , Safari , Opera dan Internet Explorer 10
        </p>
        <form action="<c:url value="/admin/product/saveProductImage3D/"/>" class="dropzone" id="my-dropzone">
            <c:if test="${label == 'update'}">
                <input type="hidden" name="PRM_NOMOR_PRODUK" value="${NOMOR_PRODUK}">
            </c:if>
            <input type="hidden" name="PRM_JENIS_UPLOAD" value="">
        </form>--%>

        <div class="tabbable tabbable-custom tabbable-full-width">

            <ul class="nav nav-tabs">
                <li class="active" id="tab-ls-gambar">
                    <a href="#ls-gambar" data-toggle="tab">
                        Upload Gambar </a>
                </li>
                <%--<li id="tab-upload">
                    <a href="#upload-gambar" data-toggle="tab">
                        Upload Gambar</a>
                </li>--%>

            </ul>

            <div class="tab-content no-padding">


                <%--<div class="tab-pane active fade in" id="ls-gambar">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped table-bordered table-hover table-number table-action" id="image-table">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="10%">
                                        No
                                    </th>
                                    <th width="15%">
                                        Gambar
                                    </th>
                                    <th width="15%">
                                        Nama Gambar
                                    </th>
                                    <th width="10%">
                                        Ukuran File
                                    </th>
                                    <th width="10%">
                                        Urutan
                                    </th>
                                    <th width="10%">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="filter">
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img1.jpg"/> "/>
                                    </td>
                                    <td>
                                        Thumbnail
                                    </td>
                                    <td>
                                        10 Kb
                                    </td>
                                    <td>
                                        1
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr role="row" class="filter">
                                    <td>
                                        2
                                    </td>
                                    <td>
                                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img2.jpg"/> "/>
                                    </td>
                                    <td>
                                        Thumbnail
                                    </td>
                                    <td>
                                        11 Kb
                                    </td>
                                    <td>
                                        2
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr role="row" class="filter">
                                    <td>
                                        3
                                    </td>
                                    <td>
                                        <img alt="" class="img-list" src="<c:url value="/assets/frontend/pages/img/works/img3.jpg"/> "/>
                                    </td>
                                    <td>
                                        Thumbnail
                                    </td>
                                    <td>
                                        12 Kb
                                    </td>
                                    <td>
                                        3
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i class="fa fa-search"></i> Detail</button>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>--%>
                    <div class="tab-pane active fade in" id="ls-gambar">
                <%--<div class="tab-pane fade" id="upload-gambar">--%>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col-md-6">
                                <select class="form-control select2me select2-small" name="PRM_JNS_UPLOAD" id="select2-jns-upload-3D" data-placeholder="Pilih Jenis File 3D yg akan di upload">
                                    <option value=""></option>
                                    <option value="001">Gambar 3D 1</option>
                                    <option value="002">Gambar 3D 2</option>
                                </select>

                            </div>

                            <div class="col-md-2">
                                <button class="btn red" data-toggle="modal" data-target="#modal-hapus-image-3D">Hapus Data Sebelumnya</button>
                            </div>


                        </div>
                        <div id="modal-hapus-image-3D" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-red">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Menghapus Gambar 3D</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p style="margin: 0px">
                                            Apakah anda yakin akan Menghapus Gambar 3D ?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                        <button class="btn btn-success" data-dismiss="modal" aria-hidden="true" id="btn-hapus-image-3D">Ya</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-10">
                        <div class="col-md-12">
                            <p>
						<span class="label label-danger">
						Catatan : </span>
                                &nbsp; Plugin ini bekerja hanya pada Chrome terbaru , Firefox , Safari , Opera dan Internet Explorer 10
                            </p>
                            <form action="<c:url value="/admin/product/saveProductImage3D/"/>" class="dropzone" id="my-dropzone">
                                <c:if test="${label == 'update'}">
                                    <input type="hidden" name="PRM_NOMOR_PRODUK" value="${NOMOR_PRODUK}">
                                </c:if>
                                <input type="hidden" name="PRM_JENIS_UPLOAD" value="">
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>