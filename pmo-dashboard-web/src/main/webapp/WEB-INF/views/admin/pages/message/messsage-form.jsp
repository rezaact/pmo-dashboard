<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
  Pesan
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa icon-home"></i>
      <a href="<c:url value="/dashboard"/>">Dashboard</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#"><c:if test="${label == 'reply'}">Reply</c:if>Pesan</a>
    </li>
  </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
  <div class="col-md-12">
    <div class="portlet light bg-inverse form-fit">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'reply'}">Reply</c:if> Pesan
                </span>
        </div>
      </div>
      <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="<c:if test="${label == 'reply'}"><c:url value="/admin/message?id=${ID_KONTAK}"/></c:if>" class="form-horizontal form-bordered" method="post">
          <input type="hidden" name="PRM_ID_KONTAK" value="${ID_KONTAK}"/>

          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">User</label>
              <div class="col-md-9">
                <c:if test="${label == 'reply'}"><input type="text" class="form-control" name="PRM_USER_REPLY" value="${authUser.getNamaLengkap()}" readonly></c:if>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Tanggal Reply</label>
              <div class="col-md-9">
                <c:if test="${label == 'reply'}"><input type="text" class="form-control" id="prm_tgl_reply" name="PRM_TGL_REPLY" value="" readonly></c:if>
              </div>
            </div>
            <div class="form-group last">
              <label class="control-label col-md-3">Keterangan</label>
              <div class="col-md-9">
                <c:if test="${label == 'reply'}"><textarea class="form-control" name="PRM_DESKRIPSI_REPLY">${DESKRIPSI_REPLY}</textarea></c:if>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <c:if test="${label =='reply'}">
                  <button type="submit" class="btn green"><i class="fa fa-check"></i><c:if test="${label == 'reply'}">Reply</c:if></button>
                  <a href="#cancel" class="btn btn-danger" data-toggle="modal">Cancel</a>
                </c:if>
                <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="cancel">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header bg-red">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title"></h4>
                      </div>
                      <div class="modal-body">
                        <p>Apakah Anda ingin membatalkan? </p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                        <a href="<c:url value="/admin/message"/>" class="btn btn-success">ya</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>

      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->