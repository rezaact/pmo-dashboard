<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PROFILE SIDEBAR -->
<div class="container">
    <div class="row">
    <div class="col-md-11">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="profile-sidebar">
        <!-- PORTLET MAIN -->
        <div class="portlet light profile-sidebar-portlet">
            <!-- SIDEBAR USERPIC -->
            <div class="profile-userpic">
                <c:choose>
                    <c:when test="${not empty ID_GAMBAR}">
                        <img src="<c:url value="/file/${ID_GAMBAR}"/>" class="img-responsive" alt="">
                    </c:when>
                    <c:otherwise>
                        <img src="<c:url value="/assets/global/img/default.png"/>" class="img-responsive" alt="">
                    </c:otherwise>
                </c:choose>
            </div>
            <!-- END SIDEBAR USERPIC -->
            <!-- SIDEBAR USER TITLE -->
            <div class="profile-usertitle">
                <div class="profile-usertitle-name">
                    ${NAMA_LENGKAP}
                </div>
                <div class="profile-usertitle-job">
                    ${UNIT}
                </div>
                <div class="profile-usertitle-job">
                    <a href="mailto:${ALAMAT_EMAIL}">${ALAMAT_EMAIL}</a>
                </div>
            </div>
            <!-- END SIDEBAR USER TITLE -->

            <!-- END SIDEBAR BUTTONS -->

        </div>
        <!-- END PORTLET MAIN -->

    </div>
    <!-- END BEGIN PROFILE SIDEBAR -->
    <!-- BEGIN PROFILE CONTENT -->
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Profile Pengguna</span>
                        </div>
                        <ul class="nav nav-tabs">
                        <c:if test="${label == 'update'}">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab">Info Pengguna</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab">Ubah Foto</a>
                            </li>
                            <li>
                                <a href="#tab_1_3" data-toggle="tab">Ganti  Password</a>
                            </li>
                        </c:if>
                            <c:if test="${label == 'detail'}">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Info Pengguna</a>
                                </li>
                            </c:if>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">
                                <form class="update-form" role="form" action="<c:if test="${label == 'update'}"><c:url value="/admin/profile?id=${ID_USER}"/></c:if>" method="post">
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <p>Mohon form layanan diisi</p>
                                    </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <p>Data berhasil disimpan</p>
                                    </div>
                                    <div class="form-group margin-top-10">
                                        <label class="control-label col-md-4">Nama Lengkap</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="PRM_NAMA_LENGKAP" class="form-control" value="${NAMA_LENGKAP}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group margin-top-10">
                                        <label class="control-label col-md-4">NIP</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="PRM_NIP" class="form-control" value="${NIP}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group margin-top-10">
                                        <label class="control-label col-md-4">Unit</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="PRM_UNIT" class="form-control" value="${UNIT}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group margin-top-10">
                                        <label class="control-label col-md-4">Email</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="PRM_EMAIL" class="form-control" value="${ALAMAT_EMAIL}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group margin-top-10">
                                        <label class="control-label col-md-4">No Telp</label>
                                        <div class="col-md-8">
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" name="PRM_NO_TELP" class="form-control" value="${NO_TELPON}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Role</label>
                                        <div class="col-md-8">
                                            <c:if test="${label == 'detail'}">
                                                <input type="text" name="PRM_EMAIL" class="form-control" value="${NAMA_ROLE}"/>
                                            </c:if>
                                            <c:if test="${label == 'update' || label == 'create'}">
                                                <input type="hidden" value="${ID_ROLE}" name="PRM_ID_ROLE"/>
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control select2me id_role" name="PRM_ID_ROLE" disabled>
                                                        <option value="0">Pilih...</option>
                                                        <c:forEach  var="role" items="${listRole}" varStatus="number">
                                                            <option  <c:if test="${ID_ROLE == role.ID_ROLE}"> selected="selected" </c:if>value="${role.ID_ROLE}">${role.NAMA_ROLE}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </c:if>
                                        </div>
                                    </div>
                                    <c:if test="${label == 'update'}">
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze"> Simpan </button>
                                        <a href="<c:url value="/dashboard"/>" class="btn default">
                                            Batal </a>
                                    </div>
                                    </c:if>
                                </form>
                            </div>
                            <!-- END PERSONAL INFO TAB -->
                            <!-- CHANGE AVATAR TAB -->
                            <div class="tab-pane" id="tab_1_2">
                                <form action="<c:url value="/admin/profile/simpanPhoto"/>" role="form" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="PRM_ID_USER" value="${ID_USER}"/>
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="PRM_PHOTO">
																</span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze">
                                            Simpan </button>
                                        <a href="<c:url value="/dashboard"/>" class="btn default">
                                            Batal </a>
                                    </div>
                                </form>
                            </div>
                            <!-- END CHANGE AVATAR TAB -->
                            <!-- CHANGE PASSWORD TAB -->
                            <div class="tab-pane" id="tab_1_3">
                                <form class="pass-form" action="<c:url value="/admin/profile/password"/>" method="post">
                                    <input type="hidden" name="prm_id_user" value="${ID_USER}"/>
                                    <div class="alert alert-danger display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <p>Mohon form layanan diisi</p>
                                    </div>
                                    <div class="alert alert-success display-hide">
                                        <button class="close" data-close="alert"></button>
                                        <p>Data berhasil disimpan</p>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Password Baru</label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                        <input type="password" class="form-control" name="prm_password" id="password"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ketik ulang password</label>
                                        <div class="input-icon right">
                                            <i class="fa"></i>
                                        <input type="password" class="form-control" name="prm_password_again"/>
                                       </div>
                                    </div>
                                    <div class="margin-top-10">
                                        <button type="submit" class="btn green-haze">
                                            Simpan </button>
                                        <a href="<c:url value="/dashboard"/>"  class="btn default">
                                            Batal </a>
                                    </div>
                                </form>
                            </div>
                            <!-- END CHANGE PASSWORD TAB -->
                            <!-- PRIVACY SETTINGS TAB -->

                            <!-- END PRIVACY SETTINGS TAB -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PROFILE CONTENT -->
    </div>

    </div>
</div>
<!-- END BEGIN PROFILE SIDEBAR -->