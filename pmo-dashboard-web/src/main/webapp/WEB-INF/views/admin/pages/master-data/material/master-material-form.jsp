<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Master Workshop
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Master Data</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Material</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Material</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light bg-inverse form-fit">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Material
                </span>
                </div>
            </div>
            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form id="form-material" action="<c:if test="${label == 'create'}"><c:url value="/admin/master-data/material"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/master-data/material?id=${ID_MATERIAL}"/></c:if>" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="PRM_ID_MATERIAL" value="${ID_MATERIAL}"/>
                    <div class="form-header padding-0">
                        <c:if test="${success == true}">
                            <div class="alert alert-success">
                                <button class="close" data-close="alert"></button>
                                Data berhasil <c:if test="${success_update == true}">Diupdate</c:if><c:if test="${success_create == true}">Ditambah</c:if>
                            </div>
                        </c:if>
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button>
                            <c:choose>
                                <c:when test="${success == false}">
                                    <c:out value="${message}"/>
                                </c:when>
                                <c:otherwise>
                                    Pengisian form anda salah, mohon cek kembali.
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Material</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">${NAMA_MATERIAL}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><input type="text" class="form-control" name="PRM_NAMA_MATERIAL" value="${NAMA_MATERIAL}"></c:if>
                            </div>
                        </div>
                        <div class="form-group last">
                            <label class="control-label col-md-3">Deskripsi</label>
                            <div class="col-md-9">
                                <c:if test="${label == 'detail'}"><p class="form-control-static">${DESKRIPSI}</p></c:if>
                                <c:if test="${label == 'update' || label == 'create'}"><textarea class="form-control" name="PRM_DESKRIPSI">${DESKRIPSI}</textarea></c:if>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <c:if test="${label == 'update' || label =='create'}">
                                    <button type="submit" class="btn green"><i class="fa fa-check"></i><c:if test="${label == 'create'}">Submit</c:if><c:if test="${label == 'update'}">Update</c:if></button>
                                    <a href="#cancel" class="btn btn-danger" data-toggle="modal">Cancel</a>
                                </c:if>
                                <c:if test="${label == 'detail'}">
                                    <a href="<c:url value="/admin/master-data/material"/>" class="btn btn-danger" data-toggle="modal">Kembali</a>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
                <!-- MODAL -->
                <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="cancel">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-red">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <p>Apakah Anda ingin kembali? </p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                                <a href="<c:url value="/admin/master-data/material"/>" class="btn btn-success">ya</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->