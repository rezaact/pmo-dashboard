<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/setting/comment/json" />";
</script>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Komentar Pelanggan
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa icon-home"></i>
            <a href="<c:url value="/dashboard"/>">Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a>Pengaturan</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Komentar</a>
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box red">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa icon-users"></i>Komentar Pelanggan
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" data-original-title="" title="">
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-hover table-strip table-condensed table-datatable table-box" id="comment-table" style="margin: 0px">
                        <thead>
                        <tr role="row" class="heading">
                            <th>
                                No
                            </th>
                            <th style="width: 110px">
                                Nama
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Nomor Produk
                            </th>
                            <th>
                                Komentar
                            </th>
                            <th>
                                Rate
                            </th>
                            <th>
                                Status
                            </th>
                            <th class="text-center" style="width: 150px">
                                Action
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<!-- END PAGE CONTENT-->