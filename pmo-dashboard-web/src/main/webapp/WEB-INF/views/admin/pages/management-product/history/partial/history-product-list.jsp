<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script>
    RECORDS_TOTAL = ${recordsTotal};
    LENGTH = ${length};
    URL = "<c:url value="/admin/history/json" />";
</script>

<c:if test="${not empty ret_code}">
    <div class="row">
        <div class="col-md-12">
            <div class="note ${ret_class}">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>${ret_message}</p>
            </div>
        </div>
    </div>
</c:if>

<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-body">
                <input type="hidden" id="nomor_produk" value="${nomor_produk}"/>
                <table class="table table-striped table-bordered table-hover table-number table-action table-datatable" id="history-list-table">
                    <thead>
                    <tr role="row" class="heading">
                        <th style="width: 130px !important;">
                            Nomor Produk
                        </th>
                        <th style="width: 110px">
                            Nama Produk
                        </th>
                        <th style="width: 130px">
                            Nomor Penugasan
                        </th>
                        <th>
                            Lokasi Pemasangan
                        </th>
                        <th style="width: 130px !important;">
                            Action
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>