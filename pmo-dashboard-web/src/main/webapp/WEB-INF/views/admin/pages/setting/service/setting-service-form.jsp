<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
  Layanan Kami
</h3>
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <i class="fa icon-home"></i>
      <a href="<c:url value="/dashboard"/>">Dashboard</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a>Pengaturan</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#">Layanan kami</a>
      <i class="fa fa-angle-right"></i>
    </li>
    <li>
      <a href="#"><c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Layanan Kami</a>
    </li>
  </ul>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN PAGE CONTENT-->
<div class="row">
  <div class="col-md-12">
    <div class="portlet light bg-inverse form-fit">
      <div class="portlet-title">
        <div class="caption">
          <i class="icon-plus font-blue-hoki"></i>
                <span class="caption-subject font-blue-hoki bold uppercase">
                   <c:if test="${label == 'create'}">Tambah</c:if><c:if test="${label == 'update'}">Update</c:if><c:if test="${label == 'detail'}">Detail</c:if> Layanan Kami
                </span>
        </div>
      </div>
      <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form action="<c:if test="${label == 'create'}"><c:url value="/admin/setting/service"/></c:if><c:if test="${label == 'update'}"><c:url value="/admin/setting/service?id=${ID_SERVICE}"/></c:if>" class="form-horizontal form-bordered service-form" method="post">
          <div class="form-header">
            <input type="hidden" name="PRM_ID_SERVICE" value="${ID_SERVICE}"/>
            <div class="alert alert-danger display-hide">
              <button class="close" data-close="alert"></button>
              <p>Mohon form layanan diisi</p>
            </div>
            <div class="alert alert-success display-hide">
              <button class="close" data-close="alert"></button>
              <p>Data berhasil disimpan</p>
            </div>
          </div>
          <div class="form-body">
              <div class="form-group">
                  <label class="control-label col-md-3">Icon</label>
                  <div class="col-md-9">
                      <c:if test="${label == 'detail'}"><p class="form-control-static"><i class="${ICON} fa-2x"></i></p></c:if>
                      <c:if test="${label == 'update' || label == 'create'}">
                          <div class="input-icon right">
                              <i class="fa"></i>
                              <select class="form-control select-icon" name="PRM_ICON" data-placeholder="Pilih..." data-placement="left">
                                  <option value=""></option>
                                  <option <c:if test="${ICON == 'fa fa-building'}">selected</c:if> value="fa fa-building" data-icon="fa fa-building"> Bangunan</option>
                                  <option <c:if test="${ICON == 'fa fa-gear'}">selected</c:if> value="fa fa-gear" data-icon="fa fa-gear"> Engineering</option>
                                  <option <c:if test="${ICON == 'fa fa-cube'}">selected</c:if> value="fa fa-cube" data-icon="fa fa-cube"> Komponen</option>
                                  <option <c:if test="${ICON == 'fa fa-wrench'}">selected</c:if> value="fa fa-wrench" data-icon="fa fa-wrench"> Perbaikan</option>
                                  <option <c:if test="${ICON == 'fa fa-bolt'}">selected</c:if> value="fa fa-bolt" data-icon="fa fa-bolt"> Listrik</option>
                                  <option <c:if test="${ICON == 'fa fa-truck'}">selected</c:if> value="fa fa-truck" data-icon="fa fa-truck"> Angkutan</option>
                                  <option <c:if test="${ICON == 'fa fa-database'}">selected</c:if> value="fa fa-database" data-icon="fa fa-database"> Database</option>
                                  <option <c:if test="${ICON == 'fa fa-medkit'}">selected</c:if> value="fa fa-medkit" data-icon="fa fa-medkit"> Kesehatan</option>
                                  <option <c:if test="${ICON == 'fa fa-tree'}">selected</c:if> value="fa fa-tree" data-icon="fa fa-tree"> Fresh</option>
                                  <option <c:if test="${ICON == 'fa fa-users'}">selected</c:if> value="fa fa-users" data-icon="fa fa-users"> Group</option>
                                  <option <c:if test="${ICON == 'fa fa-warning'}">selected</c:if> value="fa fa-warning" data-icon="fa fa-warning"> Warning</option>
                              </select>
                          </div>
                      </c:if>
                  </div>
              </div>
            <div class="form-group">
              <label class="control-label col-md-3">Judul</label>
              <div class="col-md-9">
                <c:if test="${label == 'detail'}"><p class="form-control-static">${JUDUL}</p></c:if>
                <c:if test="${label == 'update' || label == 'create'}">
                  <div class="input-icon right">
                    <i class="fa"></i>
                    <input type="text" class="form-control" id="prm_judul" name="prm_judul" value="${JUDUL}">
                  </div>
                  </c:if>
              </div>
            </div>
            <div class="form-group last">
              <label class="control-label col-md-3">Deskripsi</label>
              <div class="col-md-9">
                <c:if test="${label == 'detail'}"><p class="form-control-static">${DESKRIPSI}</p></c:if>
                <c:if test="${label == 'update' || label == 'create'}">
                  <div class="input-icon right">
                    <i class="fa"></i>
                    <textarea class="form-control" id="prm_deskripsi" name="prm_deskripsi">${DESKRIPSI}</textarea>
                  </div>
                </c:if>
              </div>
            </div>
          </div>
          <div class="form-actions">
            <div class="row">
              <div class="col-md-offset-3 col-md-9">
                <c:if test="${label == 'update' || label =='create'}">
                  <button type="submit" class="btn green"><c:if test="${label == 'create'}">Submit</c:if><c:if test="${label == 'update'}">Update</c:if></button>
                  <a href="#cancel" class="btn btn-danger" data-toggle="modal">Cancel</a>
                </c:if>
                  <c:if test="${label == 'detail'}">
                      <a href="<c:url value="/admin/setting/service"/>" class="btn btn-danger" data-toggle="modal">Kembali</a>
                  </c:if>
              </div>
            </div>
          </div>
        </form>
        <!-- END FORM-->
          <!-- MODAL CANCEL -->
          <div class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true" id="cancel">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header bg-red">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                          <h4 class="modal-title"></h4>
                      </div>
                      <div class="modal-body">
                          <p>Apakah Anda ingin membatalkan? </p>
                      </div>
                      <div class="modal-footer">
                          <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>
                          <a href="<c:url value="/admin/setting/service"/>" class="btn btn-success">ya</a>

                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>
<!-- END PAGE CONTENT-->