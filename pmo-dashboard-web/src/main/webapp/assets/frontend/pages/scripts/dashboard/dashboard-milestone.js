var DashboardMileStone = function () {
    var donatChart = null;
    var revenuePreQualification = null;
    var revenueRequestForProposal = null;
    var revenuePPASigning = null;
    var revenueFinancialClosing = null;
    var revenueLaterOfIntent= null;
    var revenueKonstruksi = null;
    var revenueCOD = null;
    var mapChart = null;
    var detailProjectModal = null;
    var milestoneDatatableIPP = null;
    var milestoneDatatableIPPModal = null;
    var milestoneDatatablePLN = null;
    var milestoneDatatablePLNModal = null;
    var isIPP = 1;

    //PLN
    var revenuefeasibility_study = null;
    var revenueBidDoc = null;
    var revenueProcProcess = null;
    var revenueConstructionProcess = null;
    var revenueCommisioning = null;
    var revenueFAC = null;

    var MilestoneDatatable = function () {
        var cDataTable = null;
        var dataTableId = null;
        //var dan fungsi bantu
        var defaultContent = "<i>N/A</i>";
        var renderEPCColorStyle = function(data){
            var ret = "";
            var color = "";
            var type = "fa-square";
            switch(data){
                case "GreenStyle" :
                    ret = "Good";
                    color = "#4DB848";
                    break;
                case "YellowStyle" :
                    ret = "Caution";
                    color = "#F6F06A";
                    break;
                case "RedStyle" :
                    ret = "Warning";
                    color = "#F27789";
                    break;

                default ://N/A
                    ret = "N/A";
                    color = "yellow";
                    type = "fa-exclamation-triangle"
            }
            return '<p style="display:inline-table"><i class="fa '+type+'" aria-hidden="true" style="color:'+color+'"></i> '+ret + '</p>';
        };

        var renderDate = function(data){
            if(data != undefined){
                return data.substr(0,10);
            }
            return defaultContent;
        };

        var renderColorStyle =  function(data){
            var ret = "";
            var color = "";
            switch(data){
                case "GreenStyle" :
                    ret = "Good";
                    color = "#4DB848";
                    break;
                case "YellowStyle" :
                    ret = "Caution";
                    color = "#F6F06A";
                    break;
                case "RedStyle" :
                    ret = "Warning";
                    color = "#F27789";
                    break;
                case "PurpleStyle" :
                    ret = "Completed";
                    color = "#A349A4";
                    break;
                default ://grayStyle
                    ret = "N/A";
                    color = "#E4E4E4";
            }
            return '<p style="display:inline-table"><i class="fa fa-square" aria-hidden="true" style="color:'+color+'"></i> '+ret + '</p>';
        };

        detailProjectModal = $("#detail-project-modal");





        return {

            init : function(id, type){
                dataTableId = id;
                var settings = {};
                if(type === MilestoneDatatable.TYPE_IPP){
                    settings.fixedLeftColumns = 2;
                    settings.order = 2;
                    settings.columns = [ //jumlah coulums 32
                        { data: null , "render" : function(data, type, row){
                            return "<a href='#' id='detail-project-button' data-row='"+JSON.stringify(row)+"'>detail</a>";
                        }},
                        { data: "PROJECTNAME" ,"defaultContent": defaultContent},
                        { data: "PROJECTOWNERSHIP" ,"defaultContent": defaultContent},
                        { data: "POWERPRICE" ,"defaultContent": defaultContent},
                        { data: "PQPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PQACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PQDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "RFPPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "RFPACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "RFPDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "LOIPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "LOIACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "LOIDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "PPASIGNPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PPASIGNACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PPASIGNDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "FCPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FCACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FCDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CONSTRUCTIONPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONPROCESSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CODPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "ENGINEERINGDEVIATION" ,"defaultContent": defaultContent},
                        { data: "ENGINEERINGDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent},
                        { data: "PROCUREMENTDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent},
                        { data: "CONSTRUCTIONDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PLANPROGRESS" ,"defaultContent": defaultContent},
                        { data: "ACTUALPROGRESS" ,"defaultContent": defaultContent}
                    ];
                }
                else if(type === MilestoneDatatable.TYPE_PLN){
                    settings.fixedLeftColumns = 2;
                    settings.order = 2;
                    settings.columns = [ //jumlah coulums 32
                        { data: null , "render" : function(data, type, row){
                            return "<a href='#' id='detail-project-button' data-row='"+JSON.stringify(row)+"'>detail</a>";
                        }},
                        { data: "PROJECTNAME" ,"defaultContent": defaultContent},
                        { data: "PROJECTOWNERSHIP" ,"defaultContent": defaultContent},
                        { data: "POWERPRICE" ,"defaultContent": defaultContent},
                        { data: "FSPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FSACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "BIDDOCPPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "BIDDOCPACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "BIDDOCPDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "PROCUREMENTPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PROCUREMENTACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CONSTRUCTIONPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "COMMISSIONINGPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "COMMISSIONINGACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "COMMISSIONINGDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CODPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODPROCESSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "FACPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FACACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FACDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "ENGINEERINGDEVIATION" ,"defaultContent": defaultContent},
                        { data: "ENGINEERINGDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent},
                        { data: "PROCUREMENTDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent},
                        { data: "CONSTRUCTIONDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PLANPROGRESS" ,"defaultContent": defaultContent},
                        { data: "ACTUALPROGRESS" ,"defaultContent": defaultContent}
                    ];
                }
                else if (type === MilestoneDatatable.TYPE_IPP_MODAL){
                    settings.fixedLeftColumns = 1;
                    settings.order = 1;
                    settings.columns = [ //jumlah coulums 31
                        { data: "PROJECTNAME" ,"defaultContent": defaultContent},
                        { data: "PROJECTOWNERSHIP" ,"defaultContent": defaultContent},
                        { data: "POWERPRICE" ,"defaultContent": defaultContent},
                        { data: "PQPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PQACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PQDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "RFPPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "RFPACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "RFPDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "LOIPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "LOIACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "LOIDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "PPASIGNPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PPASIGNACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PPASIGNDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "FCPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FCACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FCDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CONSTRUCTIONPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONPROCESSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CODPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "ENGINEERINGDEVIATION" ,"defaultContent": defaultContent},
                        { data: "ENGINEERINGDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent},
                        { data: "PROCUREMENTDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent},
                        { data: "CONSTRUCTIONDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PLANPROGRESS" ,"defaultContent": defaultContent},
                        { data: "ACTUALPROGRESS" ,"defaultContent": defaultContent}
                    ];
                }
                else if(type === MilestoneDatatable.TYPE_PLN_MODAL){
                    settings.fixedLeftColumns = 1;
                    settings.order = 1;
                    settings.columns = [ //jumlah coulums 31
                        { data: "PROJECTNAME" ,"defaultContent": defaultContent},
                        { data: "PROJECTOWNERSHIP" ,"defaultContent": defaultContent},
                        { data: "POWERPRICE" ,"defaultContent": defaultContent},
                        { data: "FSPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FSACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "BIDDOCPPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "BIDDOCPACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "BIDDOCPDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "PROCUREMENTPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PROCUREMENTACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CONSTRUCTIONPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "COMMISSIONINGPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "COMMISSIONINGACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "COMMISSIONINGDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "CODPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "CODPROCESSDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "FACPLAN" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FACACTUAL" ,"defaultContent": defaultContent, "render" : renderDate},
                        { data: "FACDEVIATION" ,"defaultContent": defaultContent, "render" : renderColorStyle},
                        { data: "ENGINEERINGDEVIATION" ,"defaultContent": defaultContent},
                        { data: "ENGINEERINGDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PROCUREMENTDEVIATION" ,"defaultContent": defaultContent},
                        { data: "PROCUREMENTDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "CONSTRUCTIONDEVIATION" ,"defaultContent": defaultContent},
                        { data: "CONSTRUCTIONDEVIATIONCOLOR" ,"defaultContent": defaultContent, "render" : renderEPCColorStyle},
                        { data: "PLANPROGRESS" ,"defaultContent": defaultContent},
                        { data: "ACTUALPROGRESS" ,"defaultContent": defaultContent}
                    ];
                }

                //add detail project listener
                $('#'+id).on('click', '#detail-project-button', function(e){
                    e.preventDefault();
                    var row = $(this).data("row");
                    detailProjectModal.find("#PROJECTNAME").text(row.PROJECTNAME);
                    detailProjectModal.find("#current-status").text(row.CURRENTSTATUS);

                    dataPost = {
                        p_projectid : row.PROJECTID
                    };
                    var url = BASE_URL+'dashboard-milestone/getdata-asset-project';
                    $(".modal_loading").fadeIn(1000);
                    $.ajax({
                        type : "POST",
                        url: url,
                        data : dataPost
                    }).done(function(response){
                        console.log("get detail");
                        console.log(response);
                        //update detail
                        var tbody = "";

                        response.data.forEach(function(item){
                            var defaultNull = "-";
                            var planOutput = item.UNITCAPACITY || item.CIRCUITLENGTH || item.OUTPUT || defaultNull;
                            var actualOutput = item.ACTUALUNITCAPACITY || item.ACTUALCIRCUITLENGTH || item.ACTUALOUTPUT || defaultNull;
                            console.log(planOutput);
                            tbody += "<tr>" +

                                    "<td>"+item.ASSETNAME+"</td>"+
                                    "<td>"+planOutput+"</td>"+
                                    "<td>"+actualOutput+"</td>"+
                                    "<td>"+item.ESTIMATECOD+"</td>"+

                                "</tr>";
                        });
                        detailProjectModal.find("#table-asset tbody").html(tbody);
                        $(".modal_loading").fadeOut(1000);
                    detailProjectModal.modal("show");
                    });
                });

                cDataTable = $('#'+id).DataTable( {
                    "searching": true,
                    "iDisplayLength": 5,
                    "order": [[ settings.order, "desc" ]],
                    "bAutoWidth": true,
                    //data: milestoneDatatableSource,
                    columns: settings.columns,
                    scrollX:true,
                    fixedColumns:   {
                        leftColumns: settings.fixedLeftColumns
                    }
                });
            },
            fetchData : function(data, callback){
                var url = BASE_URL+'dashboard-milestone/get-grid-milestone';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data
                }).done(function(response){
                    cDataTable.clear().rows.add( response.data ).draw();
                    callback();
                });
            },
            adjustColumns : function(){
                cDataTable.columns.adjust().draw();
            },
            hide : function(){
                $('#'+dataTableId).closest(".milestone-datatable-div").hide();
            },
            show : function(){
                $('#'+dataTableId).closest(".milestone-datatable-div").show();
            }
        }
    }

    MilestoneDatatable.TYPE_IPP = 1;
    MilestoneDatatable.TYPE_PLN = 2;
    MilestoneDatatable.TYPE_IPP_MODAL = 3;
    MilestoneDatatable.TYPE_PLN_MODAL = 4;

    //helper function
    var toggleDatatable = function(){

    }

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================

        init: function () {
            var data_chart1 = null;
            //DashboardMileStone.Default_Value();
            //DashboardMileStone.testFusionMap();

            milestoneDatatableIPP = new MilestoneDatatable();
            milestoneDatatableIPP.init('milestone-datatable', MilestoneDatatable.TYPE_IPP);

            milestoneDatatablePLN = new MilestoneDatatable();
            milestoneDatatablePLN.init('milestone-datatable-pln', MilestoneDatatable.TYPE_PLN);

            milestoneDatatableIPPModal = new MilestoneDatatable();
            milestoneDatatableIPPModal.init('milestone-datatable-modal', MilestoneDatatable.TYPE_IPP_MODAL);

            milestoneDatatablePLNModal = new MilestoneDatatable();
            milestoneDatatablePLNModal.init('milestone-datatable-pln-modal', MilestoneDatatable.TYPE_IPP_MODAL);


            //DashboardMileStone.dataGrid();
            //DashboardMileStone.dataGridModal();
            DashboardMileStone.Default_Value();
            //DashboardMileStone.testFusionMap();

            DashboardMileStone.onChangeCmbRegion();
            //DashboardMileStone.onChangeBox();
            DashboardMileStone.onChangeOwnership();
            //DashboardMileStone.openModal();

            //tricky adjust table coulumn width
            $("#project-tab").on("click", function(){
                var temp = null;
                if (isIPP == 1){
                    milestoneDatatableIPP.show();
                    milestoneDatatablePLN.hide();
                    temp = milestoneDatatableIPP;
                }
                else{
                    milestoneDatatableIPP.hide();
                    milestoneDatatablePLN.show();
                    temp = milestoneDatatablePLN;
                }
                setTimeout(function(){
                    temp.adjustColumns();
                }, 300);
            });


        },
        onChangeOwnership:function (){
            $("#radioIPP0").click(function() {
                $(this).attr('checked', true);
                $("#radioPLN0").prop("checked", true)
                $("#radioPLN1").prop("checked", false)
                $("#radioPLN1").removeAttr('checked');

            });
            $("#radioPLN1").click(function() {
                $(this).attr('checked', true);
                $("#radioPLN1").prop("checked", true)
                $("#radioPLN0").prop("checked", false)
                $("#radioIPP0").removeAttr('checked');
            });
        },
        onChangeBox:function (){
            $( ".box-milestone" ).click(function() {

            });
        },
        onChangeCmbRegion:function (){
            $( ".OnFilterAll" ).change(function() {
                console.log("filter all");
                $(".modal_loading").fadeIn(1000);

                var cmbRegion = $(".CmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();
                var cmbType = $(".CmbType").val();
                var cmbPhase = $(".CmbPhase").val();
                var cmbOwnership = $("input[name='OWH']:checked").val();
                cmbOwnership = 1- cmbOwnership;
                var tempIsIPP = isIPP;
                isIPP = cmbOwnership;

                var data = {
                    cmbRegion : cmbRegion,
                    cmbProgram : cmbProgram,
                    cmbType: cmbType,
                    cmbPhase: cmbPhase,
                    cmbOwnership: cmbOwnership
                }

                //------------------------------------------------
                //Validasi IPP or PLN Show and off Div Chart donat
                //by deni
                //Bugs --
                if (cmbOwnership == 1){
                    $('.divIPP').fadeIn(100);
                    $('.divPLN').fadeOut(100);
                }else{
                    $('.divIPP').fadeIn(100);
                    $('.divPLN').fadeOut(100);
                }
                //End Validasi
                //------------------------------------------------

                var url = BASE_URL+'dashboard-milestone/getdata-box-milestone';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data,
                    success: function(response) {
                        if("box" in response){

                            $("#PRE-QUALIFICATION").text(response.box[0]['HASIL']);
                            $("#PRE-QUALIFICATION-NAME").text(response.box[0]['BOXSTATUS']);

                            $("#REQUEST-FOR-PROPOSAL").text(response.box[1]['HASIL']);
                            $("#REQUEST-FOR-PROPOSAL-NAME").text(response.box[1]['BOXSTATUS']);


                            var LATTER1 = response.box[2]['BOXSTATUS'].substr(0,11);
                            var LATTER2 = response.box[2]['BOXSTATUS'].substr(11,18);
                            $("#LATTER-OF-INTENT").text(response.box[2]['HASIL']);
                            $("#LATTER-OF-INTENT-NAME").text(LATTER1);
                            $("#LATTER-OF-INTENT-NAME2").text(LATTER2);

                            var PPA1 = response.box[3]['BOXSTATUS'].substr(0,12);
                            var PPA2 = response.box[3]['BOXSTATUS'].substr(12,19);
                            $("#PPA-SIGNING").text(response.box[3]['HASIL']);
                            $("#PPA-SIGNING-NAME").text(PPA1);
                            $("#PPA-SIGNING-NAME2").text(PPA2);

                            $("#FINANCIAL-CLOSING").text(response.box[4]['HASIL']);
                            $("#FINANCIAL-CLOSING-NAME").text(response.box[4]['BOXSTATUS']);

                            $("#KONSTRUKSI").text(response.box[5]['HASIL']);
                            $("#KONSTRUKSI-NAME").text(response.box[5]['BOXSTATUS']);

                            $("#COD").text(response.box[6]['HASIL']);
                            $("#COD-NAME").text(response.box[6]['BOXSTATUS']);



                        }


                        //generate chart overall
                        DashboardMileStone.ChartOverAll(JSON.parse(response.strOverAllMileStone));
                        DashboardMileStone.ChartCOD(JSON.parse(response.strCOD));


                        //------------------------------------------------
                        //Validasi IPP or PLN Show and off Div Chart donat
                        //by deni
                        //Bugs --
                        if (cmbOwnership == 1){ //IPP
                            $('.divIPP').fadeIn(100);
                            $('.divPLN').fadeOut(100);
                            DashboardMileStone.ChartPreQualification(JSON.parse(response.strPreQualification));
                            DashboardMileStone.ChartRequestForProposal(JSON.parse(response.strRequestForProposal));
                            DashboardMileStone.ChartLaterOfIntent(JSON.parse(response.strLaterOfIntent));
                            DashboardMileStone.ChartPPASigning(JSON.parse(response.strPPASigning));
                            DashboardMileStone.ChartFinancialClosing(JSON.parse(response.strFinancialClosing));
                            DashboardMileStone.ChartKonstruksi(JSON.parse(response.strKonstruksi));

                        }else{ //PLN
                            $('.divIPP').fadeOut(100);
                            $('.divPLN').fadeIn(100);
                            DashboardMileStone.ChartFeasibilityStudy(JSON.parse(response.strDataFS));
                            DashboardMileStone.ChartBidDoc(JSON.parse(response.strDataBidDoc));
                            DashboardMileStone.ChartProcProcess(JSON.parse(response.strDataProcProcess));
                            DashboardMileStone.ChartConstructionProcess(JSON.parse(response.strDataConstructionProcess));
                            DashboardMileStone.ChartCommisioning(JSON.parse(response.strCommisioning));
                            DashboardMileStone.ChartFAC(JSON.parse(response.strFAC));


                        }
                        //End Validasi
                        //------------------------------------------------



                        //BAR CHART
                        //DashboardMileStone.ChartBar(JSON.parse(response.strOverAllMileStone));

                        //map chart
                        //DashboardMileStone.testFusionMap(JSON.parse(response.strMap));
                        $(".modal_loading").fadeOut(1000);

                    }
                });

                ////for datagrid
                //var url = BASE_URL+'dashboard-milestone/get-grid-milestone';
                //$.ajax({
                //    type : "POST",
                //    url: url,
                //    data : data
                //}).done(function(response){
                //    milestoneDatatableSource = response.data;
                //    milestoneDatatable.clear().rows.add( milestoneDatatableSource ).draw();
                //});
                var tempDatatable = null;
                var doAdjust = (tempIsIPP != isIPP)?1:0;

                if(isIPP == 1){
                    milestoneDatatableIPP.fetchData(data, function(){
                        milestoneDatatablePLN.hide();
                        milestoneDatatableIPP.show();
                        if(doAdjust == 1){
                            setTimeout(function(){
                                milestoneDatatableIPP.adjustColumns();
                            }, 300);
                        }
                    });

                }
                else{
                    milestoneDatatablePLN.fetchData(data, function(){
                        milestoneDatatableIPP.hide();
                        milestoneDatatablePLN.show();
                        if(doAdjust == 1){
                            setTimeout(function(){
                                milestoneDatatablePLN.adjustColumns();
                            }, 300);
                        }
                    });
                }



            });
        },
        Default_Value:function (){
            $(".modal_loading").fadeIn(1000);


            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbType = $(".CmbType").val();
            var cmbPhase = $(".CmbPhase").val();
            var cmbOwnership = $("input[name='OWH']:checked").val();
            cmbOwnership = 1- cmbOwnership;
            isIPP = cmbOwnership;
            var data = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbType: cmbType,
                cmbPhase: cmbPhase,
                cmbOwnership: cmbOwnership
            }

            var url = BASE_URL+'dashboard-milestone/getdata-box-milestone';
            $.ajax({
                type : "POST",
                url: url,
                data : data,
                success: function(response) {

                    $("#PRE-QUALIFICATION").text(response.box[0]['HASIL']);
                    $("#PRE-QUALIFICATION-NAME").text(response.box[0]['BOXSTATUS']);

                    $("#REQUEST-FOR-PROPOSAL").text(response.box[1]['HASIL']);
                    $("#REQUEST-FOR-PROPOSAL-NAME").text(response.box[1]['BOXSTATUS']);


                    var LATTER1 = response.box[2]['BOXSTATUS'].substr(0,11);
                    var LATTER2 = response.box[2]['BOXSTATUS'].substr(11,18);
                    $("#LATTER-OF-INTENT").text(response.box[2]['HASIL']);
                    $("#LATTER-OF-INTENT-NAME").text(LATTER1);
                    $("#LATTER-OF-INTENT-NAME2").text(LATTER2);

                    var PPA1 = response.box[3]['BOXSTATUS'].substr(0,12);
                    var PPA2 = response.box[3]['BOXSTATUS'].substr(12,19);
                    $("#PPA-SIGNING").text(response.box[3]['HASIL']);
                    $("#PPA-SIGNING-NAME").text(PPA1);
                    $("#PPA-SIGNING-NAME2").text(PPA2);

                    $("#FINANCIAL-CLOSING").text(response.box[4]['HASIL']);
                    $("#FINANCIAL-CLOSING-NAME").text(response.box[4]['BOXSTATUS']);

                    $("#KONSTRUKSI").text(response.box[5]['HASIL']);
                    $("#KONSTRUKSI-NAME").text(response.box[5]['BOXSTATUS']);

                    $("#COD").text(response.box[6]['HASIL']);
                    $("#COD-NAME").text(response.box[6]['BOXSTATUS']);



                    //generate chart overall
                    DashboardMileStone.ChartOverAll(JSON.parse(response.strOverAllMileStone));
                    DashboardMileStone.ChartCOD(JSON.parse(response.strCOD));


                    //------------------------------------------------
                    //Validasi IPP or PLN Show and off Div Chart donat
                    //by deni
                    //Bugs --
                    if (cmbOwnership == 1){ //IPP
                        $('.divIPP').fadeIn(100);
                        $('.divPLN').fadeOut(100);
                        DashboardMileStone.ChartPreQualification(JSON.parse(response.strPreQualification));
                        DashboardMileStone.ChartRequestForProposal(JSON.parse(response.strRequestForProposal));
                        DashboardMileStone.ChartLaterOfIntent(JSON.parse(response.strLaterOfIntent));
                        DashboardMileStone.ChartPPASigning(JSON.parse(response.strPPASigning));
                        DashboardMileStone.ChartFinancialClosing(JSON.parse(response.strFinancialClosing));
                        DashboardMileStone.ChartKonstruksi(JSON.parse(response.strKonstruksi));

                    }else{ //PLN
                        $('.divIPP').fadeOut(100);
                        $('.divPLN').fadeIn(100);
                        DashboardMileStone.ChartFeasibilityStudy(JSON.parse(response.strDataFS));
                        DashboardMileStone.ChartBidDoc(JSON.parse(response.strDataBidDoc));
                        DashboardMileStone.ChartProcProcess(JSON.parse(response.strDataProcProcess));
                        DashboardMileStone.ChartConstructionProcess(JSON.parse(response.strDataConstructionProcess));
                        DashboardMileStone.ChartCommisioning(JSON.parse(response.strCommisioning));
                        DashboardMileStone.ChartFAC(JSON.parse(response.strFAC));


                    }
                    //End Validasi
                    //------------------------------------------------



                    //BAR CHART
                    //DashboardMileStone.ChartBar(JSON.parse(response.strOverAllMileStone));

                    //map chart
                    DashboardMileStone.testFusionMap(JSON.parse(response.strMap));

                    $(".modal_loading").fadeOut(1000);

                }
            });

            milestoneDatatableIPP.fetchData(data, function(){
                $(".modal_loading").fadeOut(1000);
            });

        },

        ChartOverAll: function (data_chart1) {
            //Script umtuk menghapus chart existing
            try {
                donatChart.dispose();
                console.log('---> data_chart1', data_chart1);
            }catch(err) {
                console.log(data_chart1);
                console.error('error handler on chart dispose. dont worry be happy :)) ');
            }

            FusionCharts.ready(function(){
                donatChart = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "chartGeneration",
                    "width": "90%",
                    "height": "300",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":data_chart1
                });
                donatChart.render();
            })

        },
        ChartPreQualification: function (dataPreQualification) {
            //Script umtuk menghapus chart existing
            try {
                revenuePreQualification.dispose();
                console.log('---> ChartPreQualification', ChartPreQualification);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenuePreQualification= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartPreQualification",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataPreQualification
                });

                revenuePreQualification.render();
            })
        },
        ChartRequestForProposal: function (dataRequestForProposal) {
            //Script umtuk menghapus chart existing
            try {
                revenueRequestForProposal.dispose();
                console.log('---> data_RequestForProposal', dataRequestForProposal);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                var revenueRequestForProposal= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartRequestForProposal",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource": dataRequestForProposal
                });

                revenueRequestForProposal.render();
            })

        },
        ChartLaterOfIntent: function (dataLaterOfIntent) {
            // //Script umtuk menghapus chart existing
            try {
                revenueLaterOfIntent.dispose();
                console.log('---> strLaterOfIntent', dataLaterOfIntent);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                var revenueLaterOfIntent = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "DivLaterOfIntent",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource": dataLaterOfIntent
                });

                revenueLaterOfIntent.render();
            })

        },
        ChartPPASigning: function (DataPPASigning) {
            //Script umtuk menghapus chart existing
            try {
                revenuePPASigning.dispose();
                console.log('---> DataPPASigning', DataPPASigning);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                var revenuePPASigning = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "DivPPASigning",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":DataPPASigning
                });

                revenuePPASigning.render();
            })

        },
        ChartFinancialClosing: function (DataFinancialClosing) {
            //Script umtuk menghapus chart existing
            try {
                revenueFinancialClosing.dispose();
                console.log('---> DataFinancialClosing', DataFinancialClosing);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueFinancialClosing = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "DivFinancialClosing",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource": DataFinancialClosing
                });

                revenueFinancialClosing.render();
            })

        },
        ChartKonstruksi: function (DataKonstruksi) {
            //Script umtuk menghapus chart existing
            try {
                revenueKonstruksi.dispose();
                console.log('---> DataKonstruksi', DataKonstruksi);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                var revenueKonstruksi = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "DivKonstruksi",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource": DataKonstruksi
                });

                revenueKonstruksi.render();
            })

        },
        ChartCOD: function (DataCOD) {
            //Script umtuk menghapus chart existing
            try {
                revenueCOD.dispose();
                console.log('---> DataCOD', DataCOD);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueCOD = new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "DivCOD",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource": DataCOD
                });

                revenueCOD.render();
            })

        },
        //------------------------------------------------------
        // Donat Chart chart_feasibility_study
        // by denis
        ChartFeasibilityStudy: function (datafeasibility_study) {
            //Script umtuk menghapus chart existing
            try {
                revenuefeasibility_study.dispose();
                console.log('---> Chart ', datafeasibility_study);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenuefeasibility_study= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartFeasibilityStudy",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":datafeasibility_study
                });

                revenuefeasibility_study.render();
            })
        },
        ChartBidDoc: function (dataBidDoc) {
            //Script umtuk menghapus chart existing
            try {
                revenueBidDoc.dispose();
                console.log('---> Chart ', dataBidDoc);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueBidDoc= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartBiddoc",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataBidDoc
                });

                revenueBidDoc.render();
            })
        },
        // gen_chart_proc_process
        ChartProcProcess: function (dataProcProcess) {
            //Script umtuk menghapus chart existing
            try {
                revenueProcProcess.dispose();
                console.log('---> Chart ', dataProcProcess);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueProcProcess= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartProcurementProcess",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataProcProcess
                });

                revenueProcProcess.render();
            })
        },
        //gen_chart_construction_process
        ChartConstructionProcess: function (dataConstructionProcess) {
            //Script umtuk menghapus chart existing
            try {
                revenueConstructionProcess.dispose();
                console.log('---> Chart ', dataConstructionProcess);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueConstructionProcess= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartConstructionProcess",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataConstructionProcess
                });

                revenueConstructionProcess.render();
            })
        },
        //Commisioning
        ChartCommisioning: function (dataCommisioning) {
            //Script umtuk menghapus chart existing
            try {
                revenueCommisioning.dispose();
                console.log('---> Chart ', dataCommisioning);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueCommisioning= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartCOmmisioning",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataCommisioning
                });

                revenueCommisioning.render();
            })
        },
        //gen_chart_construction_process
        ChartFAC: function (dataFAC) {
            //Script umtuk menghapus chart existing
            try {
                revenueFAC.dispose();
                console.log('---> Chart ', dataFAC);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                var revenueFAC= new FusionCharts({
                    "type": "doughnut2d",
                    "renderAt": "divChartFAC",
                    "width": "100%",
                    "height": "300",
                    "dataFormat": "json",
                    "dataSource":dataFAC
                });

                revenueFAC.render();
            })
        },


        //MAP
        testFusionMap : function(strMap){

            try {
                mapChart.dispose();
                //console.log('---> strMap', strMap);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            mapChart = new FusionCharts({
                'type': 'maps/indonesia',
                'renderAt': 'testFusionMap',
                'width': '100%',
                'height': '50%',
                'bgAlpha': '30%',
                'dataFormat': 'json',
                'dataSource': strMap,
                "events": {
                    "markerClick": function(evt, data) {
                        $(".modal_loading").fadeIn(1000);
                        var modalChartModal = $('#detail-progress-sum-modal');
                        var detailMilestoneModal = $("#detail-milestone-modal");
                        modalChartModal.find("#myModalLabel").text(data.label);

                        var cmbRegion = data.id;
                        var cmbProgram = $(".CmbProgram").val();
                        var cmbType = $(".CmbType").val();
                        var cmbPhase = $(".CmbPhase").val();
                        var cmbOwnership = $("input[name='OWH']:checked").val();
                        cmbOwnership = 1-cmbOwnership;
                        isIPP = cmbOwnership;

                        var dataPost = {
                            cmbRegion : cmbRegion,
                            cmbProgram : cmbProgram,
                            cmbType: cmbType,
                            cmbPhase: cmbPhase,
                            cmbOwnership: cmbOwnership
                        }


                        var url = BASE_URL+'dashboard-milestone/getdata-chart-milestone';
                        ////getdata-chart-milestone
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: dataPost,
                            success: function (response) {
                                //override
                                var  chartData = JSON.parse(response.strOverAllMileStone);
                                chartData.chart.caption = data.label;
                                chartData.chart.defaultCenterLabel = "Detail";
                                chartData.chart.centerLabelBold = "1";
                                chartData.chart.centerLabel = "Detail";
                                donatChart = new FusionCharts({
                                    "type": "doughnut2d",
                                    "renderAt": "detail-donut",
                                    "width": "90%",
                                    "height": "300",
                                    'bgAlpha': '40%',
                                    "dataFormat": "json",
                                    "dataSource":JSON.stringify(chartData),
                                    "events" : {
                                        "centerLabelClick" : function(eventObj, dataObj){
                                            modalChartModal.modal('hide');
                                            $(".modal_loading").fadeIn(1000);
                                            //for datagrid
                                            milestoneDatatableIPPModal.hide();
                                            milestoneDatatablePLNModal.hide();
                                            var tempDatatable = (isIPP == 1)?milestoneDatatableIPPModal:milestoneDatatablePLNModal;
                                            tempDatatable.show();
                                            tempDatatable.fetchData(dataPost, function(){
                                                $(".modal_loading").fadeOut(1000);
                                                detailMilestoneModal.modal('show');
                                                //tricky adjust columns width
                                                setTimeout(function(){
                                                    console.log("adjust");
                                                    tempDatatable.adjustColumns();
                                                }, 300);
                                            });

                                        }
                                    }
                                });
                                donatChart.render();
                                modalChartModal.modal('show');
                                $(".modal_loading").fadeOut(1000);
                            }
                        });

                    },
                }
            });
            mapChart.render();

        },

    }

}();

// Call init
DashboardMileStone.init();



