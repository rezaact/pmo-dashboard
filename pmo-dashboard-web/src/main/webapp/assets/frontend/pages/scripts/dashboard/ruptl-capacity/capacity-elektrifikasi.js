var CapacityElektrifikasi = function () {

    var mapElektrifikasi = null;
    var params = null;
    var dataMapElektrifikasi = null;
    var dataKeterangan = null;
    var datePickerFilterElektrifikasi = null;



    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            //ration elektrifikasi


            CapacityElektrifikasi.initSelector();
            CapacityElektrifikasi.registerEvent();

            CapacityElektrifikasi.initDatePickerValue();
            CapacityElektrifikasi.initChart(CapacityElektrifikasi.initCookieParams);//use callback





        },
        initSelector : function(){
            //Load pertama
            if($("input[name='Kapasitas']:checked").length == 0){
                $("input[name='Kapasitas'][value='3']").prop('checked', true);
            }
        },
        initChart : function (callback){
            // Chart Generation
            FusionCharts.ready(function(){
                mapElektrifikasi = new FusionCharts({
                    'type': 'maps/indonesia',
                    'renderAt': 'map-elektrifikasi',
                    'width': '100%',
                    'height': '75%',
                    'bgAlpha': '30%',
                    'dataFormat': 'json',
                    'dataSource': null
                });
                mapElektrifikasi.render();

                callback();
            });
        },
        initDatePickerValue : function(){
            var today = new Date();
            var yyyy = today.getFullYear();

            datePickerFilterElektrifikasi = $('#filter-elektrifikasi-year');
            datePickerFilterElektrifikasi.datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years"
            });

            if(datePickerFilterElektrifikasi.val()==''){
                //init date
                datePickerFilterElektrifikasi.val(yyyy.toString());
            }

        },
        getParams : function(){
            var typeFilter = $("input[name='radio-status-elektrifikasi']:checked").val();
            var tahun = $("#filter-elektrifikasi-year").val();
            var status = $("#status-elektrifikasi").val();
            var program = $("#program-elektrifikasi").val();



            var data = {
                typeFilter : typeFilter,
                tahun : tahun,
                status : status,
                program : program
            }

            params = data;
            return data;
        },
        getData : function () {
                var url = BASE_URL+'dashboard-ruptl-capacity/get-data-electrifikasi';
                var data = CapacityElektrifikasi.getParams();
              return $.ajax({
                  type: "POST",
                  url: url,
                  data: data
              });
        },
        registerEvent : function(){
            //rasio elektrifikasi

            var typeFilterElektrifikasi = $('.type-filter-elektrifikasi');
            typeFilterElektrifikasi.change(function(){
                var type = $("input[name='radio-status-elektrifikasi']:checked").val();
                CapacityElektrifikasi.showHideTypeFilter(type);


            });

            $("#submit-elektrifikasi, #submit-elektrifikasi-large").on("click", function(){
                dataKeterangan = null;

                CapacityElektrifikasi.getData().done(function(response){

                    dataMapElektrifikasi = response.map;
                    mapElektrifikasi.setChartData(dataMapElektrifikasi, "json");
                    var persen = null;
                    try{
                        persen = response.keterangan[0].PERSEN || "N/A";
                    }
                    catch(error){
                        persen = "N/A";
                    }
                    var jumlah = null;
                    try{
                        jumlah = response.keterangan[0].JUMLAH || "N/A";
                    }
                    catch(error){
                        jumlah = "N/A";
                    }


                    if(persen != "N/A"){
                        persen = parseFloat(persen).toFixed(2);
                    }

                    switch (params.typeFilter){
                        case "1" :
                            dataKeterangan = "Rasio Elektrifikasi "+params.tahun+" Nasional "+persen+" %";
                            break;
                        case "2" :
                            var diff = (params.status == "1")?"":" (Tidak)";
                            if(jumlah != "N/A"){
                                dataKeterangan = "Jumlah Desa"+diff+" Berlistrik "+params.tahun+" Nasional "+jumlah+" Desa ("+persen+" %)";
                            }
                            else{
                                dataKeterangan = "Rasio Desa"+diff+" Berlistrik "+params.tahun+" Nasional "+persen+" %";
                            }

                            break;
                        case "3" :
                            dataKeterangan = "Rencana Melistriki Desa "+params.tahun+" Nasional "+jumlah+" desa";
                            break;
                    }


                    $("#label-map-rasio-elektrifikasi").html(dataKeterangan);
                    CapacityElektrifikasi.setCookieParams();

                });




            });
        },
        showHideTypeFilter : function(type){
            var statusElektrifikasi = $(".status-elektrifikasi");
            var programElektrifikasi = $(".program-elektrifikasi");

            if(type == 2){
                statusElektrifikasi.show();
                programElektrifikasi.hide();
            }
            else if(type == 3){
                statusElektrifikasi.hide();
                programElektrifikasi.show();
            }
            else{
                statusElektrifikasi.hide();
                programElektrifikasi.hide();
            }
        },
        initCookieParams : function(){

            var typeFIlterElektrifikasi = Cookie.get('typeFIlterElektrifikasi');

            if(typeFIlterElektrifikasi!=''){
                var jenis = $("input[name='radio-status-elektrifikasi'][value='"+typeFIlterElektrifikasi+"']");
                jenis.prop( "checked", true );
                CapacityElektrifikasi.showHideTypeFilter(typeFIlterElektrifikasi);
            }
            else{
                var jenis = $("input[name='radio-status-elektrifikasi'][value='1']");
                jenis.prop( "checked", true );
                CapacityElektrifikasi.showHideTypeFilter(1);
            }


            var tahun = Cookie.get('tahun');
            if(tahun != ""){
                datePickerFilterElektrifikasi.val(tahun);
            }

            var status = Cookie.get('status');

            if(status!=''){
                $("#status-elektrifikasi").val(status);
            }
            else{
                $("#status-elektrifikasi").val(1);
            }

            var program = Cookie.get('program');
            if(program!=''){
                $("#program-elektrifikasi").val(program);
            }
            else{
                $("#program-elektrifikasi").val(0);
            }


            dataMapElektrifikasi = Cookie.get('dataMapElektrifikasi');
            if(dataMapElektrifikasi != ""){
                mapElektrifikasi.setChartData(dataMapElektrifikasi, "json");
            }
            else{
                $('#submit-elektrifikasi').trigger("click");
            }

            dataKeterangan = Cookie.get('dataKeterangan');
            if(dataKeterangan != ""){
                $("#label-map-rasio-elektrifikasi").html(dataKeterangan);
            }


        },
        setCookieParams : function () {
            var typeFIlterElektrifikasi = $("input[name='radio-status-elektrifikasi']:checked");



            var time = 30;//5 menit
            Cookie.set('typeFIlterElektrifikasi', typeFIlterElektrifikasi.val(), time);
            Cookie.set('tahun', params.tahun, time);
            Cookie.set('status', params.status, time);
            Cookie.set('program', params.program, time);

            Cookie.set('dataMapElektrifikasi', dataMapElektrifikasi, time);
            Cookie.set('dataKeterangan', dataKeterangan, time);



        }

    }

}();

// Call init
CapacityElektrifikasi.init();