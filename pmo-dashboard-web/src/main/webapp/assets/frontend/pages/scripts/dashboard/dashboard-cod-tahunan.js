var DashboardCodTahunan = function () {
    var revenueChart = null; //Generation
    var revenueChartTransmision = null; //Transmision
    var revenueChartSubtation = null; //Subtation
    var codModal = null;
    var detailAssetCapacity = null;

    //var generationDataTemp = null;
    //var transmisionDataTemp = null;
    //var substationDataTemp = null;



    var restructureData = function(data){
        var newData = [];
        data.categories[0].category.forEach(function(item){
            newData[item.value] = [];
        });

        data.dataset.forEach(function(item){
            item.dataset.forEach(function(i){
                newData.forEach(function(d){
                    d[i.seriesname] = [];
                });
            });
        });

        console.log(newData);

    }

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            DashboardCodTahunan.Datepicker();
            DashboardCodTahunan.Default_Value();
            DashboardCodTahunan.onChangeFilterAll();
            codModal = $("#detail-cod-modal");
            detailAssetCapacity = $("#detail-asset-capacity");
        },
        Datepicker: function (){
        $('.starperiod_tahunan').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
           }).on("hide", function() {
                // `e` here contains the extra attributes
                //$(".datepicker").fadeOut();
                DashboardCodTahunan.Default_Value();
        });

        $('.endperiod_tahunan').datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
            }).on("hide", function() {
                // `e` here contains the extra attributes
                //$(".datepicker").fadeOut();
                DashboardCodTahunan.Default_Value();
        });
        },
        onChangeFilterAll: function (){
            $( ".OnFilterAll" ).change(function() {

                var cmbRegion = $(".CmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();
                var cmbOwnership = $(".CmbOwnership").val();
                var cmbThnAwal = $(".starperiod_tahunan").val();
                var cmbThnAkhir = $(".endperiod_tahunan").val();
                var cmbPlan = $(".CmbPlan").val();
                var cmbActual= $(".CmbActual").val();

                if (cmbThnAwal > cmbThnAkhir){
                    alert ("Filter tidak bisa di lakukan karena \n Start Period > End Period ");
                    return
                }else {

                    var data = {
                        cmbRegion: cmbRegion,
                        cmbProgram: cmbProgram,
                        cmbOwnership: cmbOwnership,
                        cmbThnAwal: cmbThnAwal,
                        cmbThnAkhir: cmbThnAkhir,
                        cmbPlan: cmbPlan,
                        cmbActual: cmbActual
                    }


                    var url = BASE_URL + 'dashboard-cod/tahunan/getdata-cod-tahunan';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        beforeSend: function () {
                            $(".datepicker").fadeOut();
                            $(".modal_loading").fadeIn(1000);
                        },
                        success: function (response) {

                            //generate chart
                            DashboardCodTahunan.ChartTahunanGeneration(JSON.parse(response.strGetGeneration_gen));
                            //console.log("change tahunan");
                            //console.log(JSON.parse(response.strGetGeneration_gen));
                            DashboardCodTahunan.ChartTahunanTransmision(JSON.parse(response.strGetGeneration_tran));
                            DashboardCodTahunan.ChartTahunanSubtation(JSON.parse(response.strGetSubtation_sub));
                            $(".modal_loading").fadeOut(1000);

                        }
                    });
                }
            });

        },
        Default_Value:function (){


            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbOwnership = $(".CmbOwnership").val();
            var cmbThnAwal = $(".starperiod_tahunan").val();
            var cmbThnAkhir = $(".endperiod_tahunan").val();
            var cmbPlan = $(".CmbPlan").val();
            var cmbActual= $(".CmbActual").val();

            if (cmbThnAwal > cmbThnAkhir){
                alert ("Filter tidak bisa di lakukan karena \n Start Period > End Period ");
                return
            }else {

                var data = {
                    cmbRegion: cmbRegion,
                    cmbProgram: cmbProgram,
                    cmbOwnership: cmbOwnership,
                    cmbThnAwal: cmbThnAwal,
                    cmbThnAkhir: cmbThnAkhir,
                    cmbPlan: cmbPlan,
                    cmbActual: cmbActual
                }

                var url = BASE_URL + 'dashboard-cod/tahunan/getdata-cod-tahunan';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    beforeSend: function () {
                        $(".modal_loading").fadeIn(1000);
                    },
                    success: function (response) {
                        //generationDataTemp = JSON.parse(response.strGetGeneration_gen);
                        //transmisionDataTemp = JSON.parse(response.strGetGeneration_tran);
                        //substationDataTemp = JSON.parse(response.strGetSubtation_sub);
                        //
                        //console.log("restructure data");
                        // restructureData(generationDataTemp);
                        //console.log(generationDataTemp);
                        //console.log(transmisionDataTemp);
                        //console.log(substationDataTemp);

                        //generate chart
                        DashboardCodTahunan.ChartTahunanGeneration(JSON.parse(response.strGetGeneration_gen));
                        //console.log("default tahunan");
                        //console.log(JSON.parse(response.strGetGeneration_gen));
                        DashboardCodTahunan.ChartTahunanTransmision(JSON.parse(response.strGetGeneration_tran));
                        DashboardCodTahunan.ChartTahunanSubtation(JSON.parse(response.strGetSubtation_sub));


                        $(".modal_loading").fadeOut(1000);

                    }
                });
            }
        },
        ChartTahunanGeneration: function (data) {
            //Script umtuk menghapus chart existing
            try {
                revenueChart.dispose();
                console.log('---> data', data);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChart = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivChartCODTahunanGeneration",
                    "width": "90%",
                    "height": "300",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":data,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {

                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            //console.log("dataModal generation");
                            //console.log(dataModal);
                            var satuan = " MW";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);


                            codModal.modal("show");
                        }
                    }
                });
                revenueChart.render();

            })
        },
        ChartTahunanTransmision: function (data) {
            //Script umtuk menghapus chart existing
            try {
                revenueChartTransmision.dispose();
                console.log('---> data', data);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                revenueChartTransmision = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivChartCODTahunanTransmission",
                    "width": "90%",
                    "height": "300",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":data,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();

                            var satuan = " KMS";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        }
                    }
                });
                revenueChartTransmision.render();
            })
        },
        ChartTahunanSubtation: function (data) {
            //Script umtuk menghapus chart existing
            try {
                revenueChartSubtation.dispose();
                console.log('---> data', data);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }
            FusionCharts.ready(function(){
                revenueChartSubtation = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivChartCODTahunanSubstation",
                    "width": "90%",
                    "height": "300",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":data,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " MVA";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        }
                    }
                });
                revenueChartSubtation.render();
            })
        },
    }

}();

// Call init
DashboardCodTahunan.init();