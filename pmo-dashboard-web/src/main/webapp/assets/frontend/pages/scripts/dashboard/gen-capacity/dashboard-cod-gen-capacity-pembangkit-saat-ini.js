var DashboardCodGenCapacityPembangkitSaatIni = function () {
    var modal = null;
    var datatableActual = null;
    var datatableKapasitasTerpasang = null;
    var url = BASE_URL+'dashboard-cod/get-data-detail-cod-v2';

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            modal = $("#detail-asset-capacity-pembangkit-saat-ini");
            datatableActual = $('#table-actual');
            datatableKapasitasTerpasang = $('#table-kapasitas-terpasang');


            datatableActual.DataTable({
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "TAHUN", "defaultContent": "-"},
                    { data: "BULAN", "defaultContent": "-"},
                    { data: "CAPACITY", "defaultContent": "-"},
                    { data: "REGION_NAME", "defaultContent": "-"}
                ]
            });

            datatableKapasitasTerpasang.DataTable({
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "TAHUN", "defaultContent": "-"},
                    { data: "BULAN", "defaultContent": "-"},
                    { data: "CAPACITY", "defaultContent": "-"},
                    { data: "REGION_NAME", "defaultContent": "-"}
                ]
            });



            registerEvent();
        },
        registerEvent: function(){
            $(".DivGet_Data_Detail_Pembangkit_saat_ini").on("click", function(e){
                e.preventDefault();
                fetchData().done(function(response){

                });
                modal.modal('show');
            });
        },
        getParams: function(){
            var data = {
                program : $('#CmbProgram').val(),
                ownership : $('.Ownership').val(),
                region : $('.CmbRegion').val(),
                type : $('.CmbType').val(),
                period : $('#from').val()
            }
            return data;
        },
        fetchData: function(){
            var dataPost = getParams();
            return $.ajax({
                type: "POST",
                url: url,
                data: dataPost
            });
        }
    }

}();

// Call init
DashboardCodGenCapacityPembangkitSaatIni.init();