var DashboardCodGenCapacity = function () {
    var codModal = null;
    var detailAssetCapacityModal = null;
    var detailAssetCapacityDatatable = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            DashboardCodGenCapacity.onChangeFilterAll();
            DashboardCodGenCapacity.Datepicker();
            DashboardCodGenCapacity.Default_Value();

        },
        Datepicker: function (){
            //Link : Plugin
            // http://jsfiddle.net/hLe0yhz5/1/
            // https://bootstrap-datepicker.readthedocs.io/en/latest/
            $("#from").trigger( "change" );

            $('#from').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
                }).on('changeDate', function(selected){
                    startDate =  $("#from").val();
                    $('#to').datepicker('setStartDate', startDate);
            });
        },
        Default_Value:function (){
            //$(".modal_loading").fadeIn(1000);

            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbOwnership = $(".cmbOwnership").val();
            var cmbTypePembangkit = $(".cmbTypePembangkit").val();
            var startPeriod = ConvertMonth.MyyyyToyyyymm($("#from").val());

            var data = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbOwnership: cmbOwnership,
                cmbType: cmbTypePembangkit,
                startPeriod: startPeriod
            }

            var url = BASE_URL+'dashboard-cod/get-data-detail-cod-gen-capacity-filter';
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    $("#tbl_sebelumnya").html(response.listGenCapacity[0]['V_CODLALU']);
                    $("#tbl_berjalan").html(response.listGenCapacity[0]['V_CODKINI']);
                    $("#tbl_total").html(response.listGenCapacity[0]['V_TOTAL']);

                    $("#tbl_saat_ini").html(response.listGenCapacity[0]['V_KAPASITAS_KINI']);
                    $("#tbl_beban_puncak").html(response.listGenCapacity[0]['V_BEBANPUNCAK']);
                }
            });
        },
        onChangeFilterAll: function (){
            $( ".OnFilterAll" ).click(function() {
                DashboardCodGenCapacity.Default_Value();
            });
        },
    }

}();

// Call init
DashboardCodGenCapacity.init();