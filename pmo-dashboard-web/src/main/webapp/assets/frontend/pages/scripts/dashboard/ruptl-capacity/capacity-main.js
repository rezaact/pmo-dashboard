var CapacityMain = function () {
    var advanceFilterView = null;


    //TODO : ubah char dan data chart jadi array.
    //Var untuk setcookies
    var tabGenerationRegion = null, tabGenerationType = null, tabGenerationPhase = null;
    var tabTransmissionRegion = null, tabTransmissionType = null, tabTransmissionPhase = null;
    var tabSubtationRegion = null, tabSubtationType = null, tabSubtationPhase = null;

    //variable chart
    var dataChartGenerationRegion = null;
    var dataChartGenerationType = null;
    var dataChartGenerationPhase = null;

    var dataChartTransmissionRegion = null;
    var dataChartTransmissionType = null;
    var dataChartTransmissionPhase = null;

    var dataChartSubstationRegion = null;
    var dataChartSubstationType = null;
    var dataChartSubstationPhase = null;


    var dataHeaderLabel = null;


    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            dataHeaderLabel = {};
            //$(".FilterKapasitasRUPTL").hide();
            /*Kapasitas sampai dengan*/
            $('#DateNowStart').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });

            /*Kapasitas berjalan berdasarkan*/
            $('#DateRunningPeriodeMonth').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });
            $('#DateRunningPeriodeYear').datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years"
            });


            /*Kapasitas periode tertentu*/
            $('#DateStartPeriode').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            }).on('changeDate', function(selected){
                startDate =  $("#DateStartPeriode").val();
                $('#DateEndPeriode').datepicker('setStartDate', startDate);
            });
            $('#DateEndPeriode').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });

            CapacityMain.initSelector();
            CapacityMain.registerEvent();

            CapacityMain.initDatePickerValue();
            CapacityMain.initChart(CapacityMain.initCookieParams);//use callback


            CapacityMain.hiddenElement();
            CapacityMain.disableEnable();

            //$('#Btn_FilterAll').trigger("click");


        },
        initSelector : function(){
            //Load pertama
            if($("input[name='Kapasitas']:checked").length == 0){
                $("input[name='Kapasitas'][value='3']").prop('checked', true);
            }
        },
        initChart : function (callback){
            // Chart Generation
            FusionCharts.ready(function(){
                chartGenerationRegion = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Generation-Region-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartGenerationType = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Generation-Type-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartGenerationPhase = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Generation-Phase-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null,
                });

                chartTransmissionRegion = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Transmission-Region-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartTransmissionType = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Transmission-Type-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartTransmissionPhase = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Transmission-Phase-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartSubstationRegion = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Subtation-Region-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null
                });

                chartSubstationType = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Subtation-Type-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null,
                });

                chartSubstationPhase = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "Subtation-Phase-ChartContainer",
                    "width": "330",
                    "height": "450",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": null,
                });




                chartGenerationRegion.render();
                chartGenerationType.render();
                chartGenerationPhase.render();

                chartTransmissionRegion.render();
                chartTransmissionType.render();
                chartTransmissionPhase.render();

                chartSubstationRegion.render();
                chartSubstationType.render();
                chartSubstationPhase.render();

                callback();
            });
        },
        initDatePickerValue : function(){
            var today = new Date();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            var monthYear = ConvertMonth.halfNumToHalfStr(mm)+' '+yyyy;
            //type 1
            var datePickerType1 = $('#DateNowStart');
            if(datePickerType1.val()==''){
                //init date
                datePickerType1.val(monthYear);
            }
            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            if(datePickerType2Month.val()==''){
                //init date
                datePickerType2Month.val(monthYear);
            }
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            if(datePickerType2Year.val()==''){
                //init date
                datePickerType2Year.val(yyyy.toString());
            }
            //type 3
            var firstMonthInYear = 'Jan '+yyyy.toString();
            var lastMonthInYear = 'Dec '+yyyy.toString();
            var datePickerType3Start = $('#DateStartPeriode');
            if(datePickerType3Start.val()==''){
                //init date
                datePickerType3Start.val(firstMonthInYear);
            }
            var datePickerType3End = $('#DateEndPeriode');
            if(datePickerType3End.val()==''){
                //init date
                datePickerType3End.val(lastMonthInYear);
            }



        },
        showHideProgamSebelumnya : function(type){
            /*            rowProgramSebelumnya = $('.row-program-sebelumnya');
             if(type == 3){
             rowProgramSebelumnya.show();
             }
             else{
             rowProgramSebelumnya.hide();
             }*/
        },
        getParams : function(){
            var P_Jenis = $("input[name='Kapasitas']:checked").val();
            var CmbFilterIsActual = $("#CmbFilterIsActual").val();
            var cmbGroup = $("#cmbGroup").val();

            if(P_Jenis == 3){
                var cmbJenis = P_Jenis;
                var cmbSubJenis = 0;
                var cmbThnAwal = ConvertMonth.MyyyyToyyyymm($("#DateNowStart").val());
                var cmbThnAkhir = 0;

            }else if(P_Jenis == 2){

                var BulanORTahun = $("#CmbMonths").val();

                if (BulanORTahun == 1){//Bulan
                    var ThnAwal_conver = ConvertMonth.MyyyyToyyyymm($("#DateRunningPeriodeMonth").val());
                    var cmbSubJenis = BulanORTahun;
                }else{
                    var ThnAwal_conver = $("#DateRunningPeriodeYear").val();
                    var cmbSubJenis = BulanORTahun;
                }
                var cmbJenis = P_Jenis;
                var cmbThnAwal = ThnAwal_conver;
                var cmbThnAkhir = 0;

            }else{
                var cmbJenis = P_Jenis;
                var cmbSubJenis = 0;
                var cmbThnAwal = ConvertMonth.MyyyyToyyyymm($("#DateStartPeriode").val());
                var cmbThnAkhir = ConvertMonth.MyyyyToyyyymm($("#DateEndPeriode").val());
            }

            var data = {
                cmbJenis : cmbJenis,
                cmbSubJenis : cmbSubJenis,
                cmbThnAwal: cmbThnAwal,
                cmbThnAkhir: cmbThnAkhir,
                CmbFilterIsActual: CmbFilterIsActual,
                cmbGroup : cmbGroup
            }


            return data;
        },
        registerEvent : function(){
            /*------------------------
             @type or @detailType
             1=Detail by Region
             2=Detail by Asset
             3=Detail by Phase

             @group
             19=7GW
             22=35GW
             ------------------------ */


            //form submit
            var CmbFilterIsActual = $("#CmbFilterIsActual").val();

            // Filter
            $('.btn-submit').on('click', function(event){

                var group = null;
                var detailType = null;

                var type = $(this).attr('value');
                var group = $("#cmbGroup").val();

                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = type;

                /*
                 Update kebutuhan tabs : by den
                 @params.AssetTypeGeneration = "17";
                 @params.AssetTypeTransmission = "18";
                 @params.AssetTypeSubStation = "19";
                 */
                //params.CmbFilterIsActual = CmbFilterIsActual;


            });

            //Generation Region
            var formGenerationRegion = $('#form-main-generation-region');
            formGenerationRegion.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 1;
                params.AssetType = 17;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formGenerationRegion);
                });

                return true;
            });
            var formGenerationType = $('#form-main-generation-type');
            formGenerationType.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 2;
                params.AssetType = 17;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formGenerationType);
                });

                return true;
            });

            var formGenerationPhase = $('#form-main-generation-phase');
            formGenerationPhase.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 3;
                params.AssetType = 17;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formGenerationPhase);
                });

                return true;
            });
            //end generation

            //transmission
            var formTransmissionRegion = $('#form-main-transmission-region');
            formTransmissionRegion.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 1;
                params.AssetType = 18;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formTransmissionRegion);
                });

                return true;
            });
            var formTransmissionType = $('#form-main-transmission-type');
            formTransmissionType.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 2;
                params.AssetType = 18;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formTransmissionType);
                });

                return true;
            });

            var formTransmissionPhase = $('#form-main-transmission-phase');
            formTransmissionPhase.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 3;
                params.AssetType = 18;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formTransmissionPhase);
                });

                return true;
            });
            //end transmission

            //substation
            var formSubstationRegion = $('#form-main-substation-region');
            formSubstationRegion.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 1;
                params.AssetType = 19;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formSubstationRegion);
                });

                return true;
            });
            var formSubstationType = $('#form-main-substation-type');
            formSubstationType.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 2;
                params.AssetType = 19;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formSubstationType);
                });

                return true;
            });

            var formSubstationPhase = $('#form-main-substation-phase');
            formSubstationPhase.submit(function(){
                var group = $("#cmbGroup").val();
                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = 3;
                params.AssetType = 19;
                //setCookie('AssetType', 17, 5);
                $.each(params, function(i,param){
                    $('<input />').attr('type', 'hidden')
                        .attr('name', i)
                        .attr('value', param)
                        .appendTo(formSubstationPhase);
                });

                return true;
            });
            //end substation

            //Load awal hidden "Kapasitas berjalan berdasarkan"
            $('#CmbMonths').change(function() {
                CapacityMain.hiddenElement();
            });

            //Filter Advanced
            $('.AdvancedFilter').click(function(e) {
                e.preventDefault();
                //$(".FilterKapasitasRUPTL").toggle("slow");

                if(advanceFilterView == 1){
                    $(".FilterKapasitasRUPTL").hide();
                    advanceFilterView = 0;
                }
                else{
                    $(".FilterKapasitasRUPTL").show();
                    advanceFilterView = 1;
                }

            });

            /*
             Set Value Tabs Cookies
             */
            //Generation
            $('.tabGeneration-Region').click(function() {
                tabGenerationRegion = 1;
                tabGenerationType = 0;
                tabGenerationPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabGeneration-Type').click(function() {
                tabGenerationRegion = 0;
                tabGenerationType = 1;
                tabGenerationPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabGeneration-Phase').click(function() {
                tabGenerationRegion = 0;
                tabGenerationType = 0;
                tabGenerationPhase = 1;
                CapacityMain.setCookieParams();
            });

            //Transmission
            $('.tabTransmission-Region').click(function() {
                tabTransmissionRegion = 1;
                tabTransmissionType = 0;
                tabTransmissionPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabTransmission-Type').click(function() {
                tabTransmissionRegion = 0;
                tabTransmissionType = 1;
                tabTransmissionPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabTransmission-Phase').click(function() {
                tabTransmissionRegion = 0;
                tabTransmissionType = 0;
                tabTransmissionPhase = 1;
                CapacityMain.setCookieParams();
            });

            //Subtation
            $('.tabSubtation-Region').click(function() {
                tabSubtationRegion = 1;
                tabSubtationType = 0;
                tabSubtationPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabSubtation-Type').click(function() {
                tabSubtationRegion = 0;
                tabSubtationType = 1;
                tabSubtationPhase = 0;
                CapacityMain.setCookieParams();
            });
            $('.tabSubtation-Phase').click(function() {
                tabSubtationRegion = 0;
                tabSubtationType = 0;
                tabSubtationPhase = 1;
                CapacityMain.setCookieParams();
            });
            /*
             end Set Value Tabs Cookies
             */


            //Button Filter ALL
            $('#Btn_FilterAll').click(function() {
                $(".modal_loading").fadeIn(10);


                if ($("input[name='Kapasitas']:checked").val()) {

                    var data = CapacityMain.getParams();

                    var url = BASE_URL+'dashboard-ruptl-capacity/get-data-capacity';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            $(".modal_loading").fadeOut(10);

                            dataHeaderLabel = {
                                "TOTAL7GW": response.listGenCapcity[0]['TOTAL7GW'],
                                "TOTALBOBOT7GW" : response.listGenCapcity[0]['TOTALBOBOT7GW'],
                                "TOTALBOBOT7GW_TRANSMISI" : response.listGenCapcity[0]['TOTALBOBOT7GW_TRANSMISI'],
                                "TOTAL7GW_TRANSMISI" : response.listGenCapcity[0]['TOTAL7GW_TRANSMISI'],
                                "TOTALBOBOT7GW_GI" : response.listGenCapcity[0]['TOTALBOBOT7GW_GI'],
                                "TOTAL7GW_GI" : response.listGenCapcity[0]['TOTAL7GW_GI'],

                                "TOTAL35GW" : response.listGenCapcity[0]['TOTAL35GW'],
                                "TOTALBOBOT35GW" : response.listGenCapcity[0]['TOTALBOBOT35GW'],
                                "TOTAL35GW_TRANSMISI" : response.listGenCapcity[0]['TOTAL35GW_TRANSMISI'],
                                "TOTALBOBOT35GW_TRANSMISI" : response.listGenCapcity[0]['TOTALBOBOT35GW_TRANSMISI'],
                                "TOTAL35GW_GI" : response.listGenCapcity[0]['TOTAL35GW_GI'],
                                "TOTALBOBOT35GW_GI" : response.listGenCapcity[0]['TOTALBOBOT35GW_GI'],

                            };

                            CapacityMain.setHeaderLabel(data.cmbGroup);

                            //update chart
                            dataChartGenerationRegion = response.chartAllGenerationRegion;
                            chartGenerationRegion.setChartData(dataChartGenerationRegion, 'json');
                            dataChartGenerationType = response.chartAllGenerationType;
                            chartGenerationType.setChartData(dataChartGenerationType, 'json');
                            dataChartGenerationPhase = response.chartAllGenerationPhase;
                            chartGenerationPhase.setChartData(dataChartGenerationPhase, 'json');

                            dataChartTransmissionRegion = response.chartAllTransmissionRegion;
                            chartTransmissionRegion.setChartData(dataChartTransmissionRegion, 'json');
                            dataChartTransmissionType = response.chartAllTransmissionType;
                            chartTransmissionType.setChartData(dataChartTransmissionType, 'json');
                            dataChartTransmissionPhase = response.chartAllTransmissionPhase;
                            chartTransmissionPhase.setChartData(dataChartTransmissionPhase, 'json');

                            dataChartSubstationRegion = response.chartAllSubstationRegion;
                            chartSubstationRegion.setChartData(dataChartSubstationRegion, 'json');
                            dataChartSubstationType = response.chartAllSubstationType;
                            chartSubstationType.setChartData(dataChartSubstationType, 'json');
                            dataChartSubstationPhase = response.chartAllSubstationPhase;
                            chartSubstationPhase.setChartData(dataChartSubstationPhase, 'json');

                            CapacityMain.setCookieParams();
                            $(".modal_loading").fadeOut(10);

                        }
                    });

                    return false;
                }
                else {
                    alert('Pilih salah satu Filter');
                }

            });

            //Pilih Salah satu Kapasitas
            $('.panel-body input').on('change', function(e) {
                CapacityMain.disableEnable();
            });




        },
        disableEnable : function () {
            $('input[name=Kapasitas]:checked', '.panel-body').val();
            var progres = $(".progres");
            var valKapasitas = $("input[name='Kapasitas']:checked").val();

            if (valKapasitas == 1){

                $(".KapasitasSD").prop('disabled', true);
                $(".KapasitasBD").prop('disabled', true);
                $(".KapasitasPT").prop('disabled', false);
                progres.css('opacity','0');

                //Update Req by deni - 23-01-2017:
                //$(".Row_Kapasitas_Label").hide();
                //$(".progres").hide();
                //$(".btn-detail-by-phase").hide();

                //Update Req by deni - 08-02-2017:
                $(".FilterKapasitasRUPTLPeriode").hide();


            }else if(valKapasitas == 2) {
                $(".KapasitasSD").prop('disabled', true);
                $(".KapasitasBD").prop('disabled', false);
                $(".KapasitasPT").prop('disabled', true);
                progres.css('opacity','0');

                //Update Req by deni - 23-01-2017:
                //$(".Row_Kapasitas_Label").hide();
                //$(".progres").hide();
                //$(".btn-detail-by-phase").hide();

                //Update Req by deni - 08-02-2017:
                $(".FilterKapasitasRUPTLPeriode").hide();

            }else{
                $(".KapasitasSD").prop('disabled', false);
                $(".KapasitasBD").prop('disabled', true);
                $(".KapasitasPT").prop('disabled', true);
                progres.css('opacity','1');

                //Update Req by deni - 23-01-2017:
                //$(".Row_Kapasitas_Label").show();
                //$(".progres").show();
                //$(".btn-detail-by-phase").show();

                //Update Req by deni - 08-02-2017:
                $(".FilterKapasitasRUPTLPeriode").show();
            }
        },
        hiddenElement : function(){
            var CmbMonths = $("#CmbMonths").val();
            if (CmbMonths == 1){
                $('#DateRunningPeriodeMonth').show();
                $('.DateRunningPeriodeMonth_img').show();

                $('#DateRunningPeriodeYear').hide();
                $('.DateRunningPeriodeYear_img').hide();

            }else{
                $('#DateRunningPeriodeMonth').hide();
                $('.DateRunningPeriodeMonth_img').hide();

                $('#DateRunningPeriodeYear').show();
                $('.DateRunningPeriodeYear_img').show();

            }
        },
        setHeaderLabel : function(group){
            var satuanPercen = ' %';
            //Update Label Generation
            var Generation_lbl_kapasitas = $('#Generation-lbl-kapasitas');
            var Generation_lbl_progress = $('#Generation-lbl-progress');

            var Transmission_lbl_kapasitas = $('#Transmission-lbl-kapasitas');
            var Transmission_lbl_progress = $('#Transmission-lbl-progress');

            var Subtation_lbl_kapasitas = $('#Subtation-lbl-kapasitas');
            var Subtation_lbl_progress = $('#Subtation-lbl-progress');

            if(group == "19"){


                //Generation
                Generation_lbl_kapasitas.html(dataHeaderLabel['TOTAL7GW'] || 'N/A');
                Generation_lbl_progress.html(dataHeaderLabel['TOTALBOBOT7GW'] || 'N/A');
                Generation_lbl_progress.html(Generation_lbl_progress.html().trim());
                Generation_lbl_progress.html((Generation_lbl_progress.html()!='.00')? addNol(Generation_lbl_progress.html())+satuanPercen:'N/A');

                //Transmission
                Transmission_lbl_kapasitas.html(dataHeaderLabel['TOTAL7GW_TRANSMISI'] || 'N/A');
                Transmission_lbl_progress.html(dataHeaderLabel['TOTALBOBOT7GW_TRANSMISI'] || 'N/A');
                Transmission_lbl_progress.html(Transmission_lbl_progress.html().trim());
                Transmission_lbl_progress.html((Transmission_lbl_progress.html()!='.00')? addNol(Transmission_lbl_progress.html())+satuanPercen:'N/A');

                //Subtation
                Subtation_lbl_kapasitas.html(dataHeaderLabel['TOTAL7GW_GI'] || 'N/A');
                Subtation_lbl_progress.html(dataHeaderLabel['TOTALBOBOT7GW_GI'] || 'N/A');
                Subtation_lbl_progress.html(Subtation_lbl_progress.html().trim());
                Subtation_lbl_progress.html((Subtation_lbl_progress.html()!='.00')? addNol(Subtation_lbl_progress.html())+satuanPercen:'N/A');

            }else{

                //Generation
                Generation_lbl_kapasitas.html(dataHeaderLabel['TOTAL35GW'] || 'N/A');
                Generation_lbl_progress.html(dataHeaderLabel['TOTALBOBOT35GW'] || 'N/A');
                Generation_lbl_progress.html(Generation_lbl_progress.html().trim());
                Generation_lbl_progress.html((Generation_lbl_progress.html()!='.00')? addNol(Generation_lbl_progress.html())+satuanPercen:'N/A');

                //Transmission
                Transmission_lbl_kapasitas.html(dataHeaderLabel['TOTAL35GW_TRANSMISI'] || 'N/A');
                Transmission_lbl_progress.html(dataHeaderLabel['TOTALBOBOT35GW_TRANSMISI'] || 'N/A');
                Transmission_lbl_progress.html(Transmission_lbl_progress.html().trim());
                Transmission_lbl_progress.html((Transmission_lbl_progress.html()!='.00')? addNol(Transmission_lbl_progress.html())+satuanPercen:'N/A');

                //Subtation
                Subtation_lbl_kapasitas.html(dataHeaderLabel['TOTAL35GW_GI'] || 'N/A');
                Subtation_lbl_progress.html(dataHeaderLabel['TOTALBOBOT35GW_GI'] || 'N/A');
                Subtation_lbl_progress.html(Subtation_lbl_progress.html().trim());
                Subtation_lbl_progress.html((Subtation_lbl_progress.html()!='.00')? addNol(Subtation_lbl_progress.html())+satuanPercen:'N/A');

            }
        },
        initCookieParams : function(){

            var jenisCookie = Cookie.get('jenis');

            if(jenisCookie!=''){
                var jenis = $("input[name='Kapasitas'][value='"+jenisCookie+"']");
                jenis.prop( "checked", true );
            }

            //group
            var groupCookie = Cookie.get('group');

            if(groupCookie!=''){
                var group = $("#cmbGroup");
                group.val(groupCookie);
            }


            //type 1
            var datePickerType1 = $('#DateNowStart');
            var datePickerType1Cookie = Cookie.get('datePickerType1');
            if(datePickerType1Cookie!=''){
                //init date
                datePickerType1.val(datePickerType1Cookie);
            }

            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            var datePickerType2MonthCookie = Cookie.get('datePickerType2Month');
            if(datePickerType2MonthCookie!=''){
                //init date
                datePickerType2Month.val(datePickerType2MonthCookie);
            }
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            var datePickerType2YearCookie = Cookie.get('datePickerType2Year');
            if(datePickerType2YearCookie!=''){
                //init date
                datePickerType2Year.val(datePickerType2YearCookie);
            }

            var comboSubtype = $('#CmbMonths');
            var comboSubtypeCookie = Cookie.get('comboSubtype');
            if(comboSubtypeCookie!=''){
                comboSubtype.val(comboSubtypeCookie);
            }
            //type 3
            var datePickerType3Start = $('#DateStartPeriode');
            var datePickerType3StartCookie = Cookie.get('datePickerType3Start');
            if(datePickerType3StartCookie!=''){
                //init date
                datePickerType3Start.val(datePickerType3StartCookie);
            }
            var datePickerType3End = $('#DateEndPeriode');
            var datePickerType3EndCookie = Cookie.get('datePickerType3End');
            if(datePickerType3EndCookie!=''){
                //init date
                datePickerType3End.val(datePickerType3EndCookie);
            }


            //tab
            var assetType = Cookie.get('AssetType');
            if(typeof assetType != 'undefined'){

                switch (assetType){

                    case '17':
                        $('#generation').addClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').addClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                        break;
                    case '18':
                        $('#generation').removeClass('active');
                        $('#transmission').addClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').removeClass('active');
                        $('#transmission-tab-header').addClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                        break;
                    case '19':
                        $('#generation').removeClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').addClass('active');
                        $('#generation-tab-header').removeClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').addClass('active');
                        break;
                    default :
                        $('#generation').addClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').addClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                }
            }
            else{
                $('.tab-content #generation').addClass('active');
                $('.tab-content #transmission').removeClass('active');
                $('.tab-content #sub-station').removeClass('active');
            }

            //advance filter view
            advanceFilterView = Cookie.get('advanceFilterView');
            //1 : show
            //0 : hide
            if(advanceFilterView == 1){
                $(".FilterKapasitasRUPTL").show();
            }
            else{
                $(".FilterKapasitasRUPTL").hide();
                advanceFilterView = 0;
            }


            /*
             Set Tabs Cookies
             -----------------
             */
            tabGenerationRegion = Cookie.get('tabGenerationRegion');
            if (tabGenerationRegion == 1){
                //show

                $(".tabGeneration-Region").addClass("active");
                $("#Generation-Region").addClass("active in");

                $(".tabGeneration-Type").removeClass("active");
                $("#Generation-Type").removeClass("active in");

                $(".tabGeneration-Phase").removeClass("active");
                $("#Generation-Phase").removeClass("active in");
            }

            tabGenerationType = Cookie.get('tabGenerationType');
            if (tabGenerationType == 1){
                //show

                $(".tabGeneration-Region").removeClass("active");
                $("#Generation-Region").removeClass("active in");

                $(".tabGeneration-Type").addClass("active");
                $("#Generation-Type").addClass("active in");

                $(".tabGeneration-Phase").removeClass("active");
                $("#Generation-Phase").removeClass("active in");

            }
            tabGenerationPhase = Cookie.get('tabGenerationPhase');
            if (tabGenerationPhase == 1){
                //show

                $(".tabGeneration-Region").removeClass("active");
                $("#Generation-Region").removeClass("active in");

                $(".tabGeneration-Type").removeClass("active");
                $("#Generation-Type").removeClass("active in");

                $(".tabGeneration-Phase").addClass("active");
                $("#Generation-Phase").addClass("active in");
            }




            //Transmission
            tabTransmissionRegion = Cookie.get('tabTransmissionRegion');
            if (tabTransmissionRegion == 1){
                //show

                $(".tabTransmission-Region").addClass("active");
                $("#Transmission-Region").addClass("active in");

                $(".tabTransmission-Type").removeClass("active");
                $("#Transmission-Type").removeClass("active in");

                $(".tabTransmission-Phase").removeClass("active");
                $("#Transmission-Phase").removeClass("active in");
            }

            tabTransmissionType = Cookie.get('tabTransmissionType');
            if (tabTransmissionType == 1){
                //show

                $(".tabTransmission-Region").removeClass("active");
                $("#Transmission-Region").removeClass("active in");

                $(".tabTransmission-Type").addClass("active");
                $("#Transmission-Type").addClass("active in");

                $(".tabTransmission-Phase").removeClass("active");
                $("#Transmission-Phase").removeClass("active in");

            }

            tabTransmissionPhase = Cookie.get('tabTransmissionPhase');
            if (tabTransmissionPhase == 1){
                //show

                $(".tabTransmission-Region").removeClass("active");
                $("#Transmission-Region").removeClass("active in");

                $(".tabTransmission-Type").removeClass("active");
                $("#Transmission-Type").removeClass("active in");

                $(".tabTransmission-Phase").addClass("active");
                $("#Transmission-Phase").addClass("active in");
            }

            //Subtation
            tabSubtationRegion = Cookie.get('tabSubtationRegion');
            if (tabSubtationRegion == 1){
                //show

                $(".tabSubtation-Region").addClass("active");
                $("#Subtation-Region").addClass("active in");

                $(".tabSubtation-Type").removeClass("active");
                $("#Subtation-Type").removeClass("active in");

                $(".tabSubtation-Phase").removeClass("active");
                $("#Subtation-Phase").removeClass("active in");
            }

            tabSubtationType = Cookie.get('tabSubtationType');
            if (tabSubtationType == 1){
                //show

                $(".tabSubtation-Region").removeClass("active");
                $("#Subtation-Region").removeClass("active in");

                $(".tabSubtation-Type").addClass("active");
                $("#Subtation-Type").addClass("active in");

                $(".tabSubtation-Phase").removeClass("active");
                $("#Subtation-Phase").removeClass("active in");

            }

            tabSubtationPhase = Cookie.get('tabSubtationPhase');
            if (tabSubtationPhase == 1){
                //show

                $(".tabSubtation-Region").removeClass("active");
                $("#Subtation-Region").removeClass("active in");

                $(".tabSubtation-Type").removeClass("active");
                $("#Subtation-Type").removeClass("active in");

                $(".tabSubtation-Phase").addClass("active");
                $("#Subtation-Phase").addClass("active in");
            }

            /*
             end set tabs Cookies
             */

            //init cookie data chart
            dataChartGenerationRegion = Cookie.get('dataChartGenerationRegion');
            if(dataChartGenerationRegion != ""){
                chartGenerationRegion.setChartData(dataChartGenerationRegion, 'json');
            }
            dataChartGenerationType = Cookie.get('dataChartGenerationType');
            if(dataChartGenerationType != ""){
                chartGenerationType.setChartData(dataChartGenerationType, 'json');
            }
            dataChartGenerationPhase = Cookie.get('dataChartGenerationPhase');
            if(dataChartGenerationPhase != ""){
                chartGenerationPhase.setChartData(dataChartGenerationPhase, 'json');
            }

            dataChartTransmissionRegion = Cookie.get('dataChartTransmissionRegion');
            if(dataChartTransmissionRegion != ""){
                chartTransmissionRegion.setChartData(dataChartTransmissionRegion, 'json');
            }
            dataChartTransmissionType = Cookie.get('dataChartTransmissionType');
            if(dataChartTransmissionType != ""){
                chartTransmissionType.setChartData(dataChartTransmissionType, 'json');
            }
            dataChartTransmissionPhase = Cookie.get('dataChartTransmissionPhase');
            if(dataChartTransmissionPhase != ""){
                chartTransmissionPhase.setChartData(dataChartTransmissionPhase, 'json');
            }

            dataChartSubstationRegion = Cookie.get('dataChartSubstationRegion');
            if(dataChartSubstationRegion != ""){
                chartSubstationRegion.setChartData(dataChartSubstationRegion, 'json');
            }
            dataChartSubstationType = Cookie.get('dataChartSubstationType');
            if(dataChartSubstationType != ""){
                chartSubstationType.setChartData(dataChartSubstationType, 'json');
            }
            dataChartSubstationPhase = Cookie.get('dataChartSubstationPhase');
            if(dataChartSubstationPhase != ""){
                chartSubstationPhase.setChartData(dataChartSubstationPhase, 'json');
            }

            //end init data cookie

            //init header label
            dataHeaderLabel = Cookie.get('dataHeaderLabel');
            if(dataHeaderLabel != ""){
                dataHeaderLabel = JSON.parse(dataHeaderLabel);
                CapacityMain.setHeaderLabel(groupCookie);
            }
            else{
                $('#Btn_FilterAll').trigger("click");
            }


        },
        setCookieParams : function () {
            var jenis = $("input[name='Kapasitas']:checked");
            var group = $("#cmbGroup");

            //type 1
            var datePickerType1 = $('#DateNowStart');
            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            var comboSubtype = $('#CmbMonths');
            //type 3
            var datePickerType3Start = $('#DateStartPeriode');
            var datePickerType3End = $('#DateEndPeriode');


            var time = 30;//5 menit
            Cookie.set('jenis', jenis.val(), time);
            Cookie.set('group', group.val(), time);
            Cookie.set('datePickerType1', datePickerType1.val(), time);
            Cookie.set('datePickerType2Month', datePickerType2Month.val(), time);
            Cookie.set('datePickerType2Year', datePickerType2Year.val(), time);
            Cookie.set('datePickerType3Start', datePickerType3Start.val(), time);
            Cookie.set('datePickerType3End', datePickerType3End.val(), time);
            Cookie.set('comboSubtype', comboSubtype.val(), time);

            Cookie.set('advanceFilterView', advanceFilterView, time);

            Cookie.set('tabGenerationRegion', tabGenerationRegion, time);
            Cookie.set('tabGenerationType', tabGenerationType, time);
            Cookie.set('tabGenerationPhase', tabGenerationPhase, time);

            Cookie.set('tabTransmissionRegion', tabTransmissionRegion, time);
            Cookie.set('tabTransmissionType', tabTransmissionType, time);
            Cookie.set('tabTransmissionPhase', tabTransmissionPhase, time);

            Cookie.set('tabSubtationRegion', tabSubtationRegion, time);
            Cookie.set('tabSubtationType', tabSubtationType, time);
            Cookie.set('tabSubtationPhase', tabSubtationPhase, time);

            //set data chart to cookie
            Cookie.set('dataChartGenerationRegion', dataChartGenerationRegion, time);
            Cookie.set('dataChartGenerationType', dataChartGenerationType, time);
            Cookie.set('dataChartGenerationPhase', dataChartGenerationPhase, time);

            Cookie.set('dataChartTransmissionRegion', dataChartTransmissionRegion, time);
            Cookie.set('dataChartTransmissionType', dataChartTransmissionType, time);
            Cookie.set('dataChartTransmissionPhase', dataChartTransmissionPhase, time);

            Cookie.set('dataChartSubstationRegion', dataChartSubstationRegion, time);
            Cookie.set('dataChartSubstationType', dataChartSubstationType, time);
            Cookie.set('dataChartSubstationPhase', dataChartSubstationPhase, time);

            //set data header label
            Cookie.set('dataHeaderLabel', JSON.stringify(dataHeaderLabel), time);

        }

    }

}();

// Call init
CapacityMain.init();

function addNol(data){
    var firstChar = data.toString().substring(0,1);
    var newData = (firstChar == '.')?'0'+data:data;
    return newData;
}