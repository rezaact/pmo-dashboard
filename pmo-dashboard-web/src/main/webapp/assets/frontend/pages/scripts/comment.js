var Comment = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Comment.formValidation();
            Comment.formValidationReply();
//
        },
        // =========================================================================
        // HANDLE SERVICE VALIDATION
        // =========================================================================
        formValidation: function () {
            form = $('.comment-form');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_NAME: {
                        required: true
                    },
                    PRM_EMAIL: {
                        required: true
                    },
                    PRM_KOMENTAR: {
                        required: true
                    }
                },
                messages: {
                    PRM_NAME: "nama harus diisi",
                    PRM_EMAIL: "Email harus diisi",
                    PRM_KOMENTAR: "Pesan harus diisi"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (data) {
//                    error.hide();
//                    form[0].submit();
                  $.ajax({
                    method: "POST",
                    url: BASE_URL +"product/comment",
                    data:{
                        PRM_NOMOR_PRODUK : form.find("input[name='PRM_NOMOR_PRODUK']").val(),
                        PRM_NAMA : form.find("input[name='PRM_NAME']").val(),
                        PRM_EMAIL : form.find("input[name='PRM_EMAIL']").val(),
                        PRM_KOMENTAR : form.find("textarea[name='PRM_KOMENTAR']").val(),
                        PRM_PARENT : form.find("input[name='PRM_PARENT']").val(),
                        PRM_RATE : $("#rate_com  > div").attr('aria-valuenow')
                    },
                      beforeSend: function () {
                          // loading pada product container
                          Metronic.blockUI({
                              target: $('#tab-comment'),
                              boxed: true
                          });
                      },
                    success: function (response) {
                        //$.each(response.detailKomentar.data, function (idx, value) {
                        //    console.log(value.RATE)
                        //});

                        Metronic.unblockUI($('#tab-comment'));
                        $.ajax({
                            method: "POST",
                            url: BASE_URL + "product/total-comment",
                            data: {
                                PRM_NOMOR_PRODUK: form.find("input[name='PRM_NOMOR_PRODUK']").val(),
                            },
                            success: function (value) {
                                console.log(value);
                                $('#rateProduk').find('.rateit').remove();
                                $.each(value, function (idx, v) {
                                    console.log(v.RATE);
                                    console.log(v.TOTAL_KOMEN);
                                    $('#total-komentar').text(v.TOTAL_KOMEN);
                                    $('#rateProduk').append('' +
                                        '<div class="rateit" data-rateit-value="'+v.RATE+'" data-rateit-ispreset="true" data-rateit-readonly="true"></div></div>');
                                    var sel="#rateProduk";
                                    $(sel).find('.rateit').rateit({ max: 5, step: 1});
                                });
                            }
                        });
                        $("#isi-comment").html(response);
                        $("#name_com").val("");
                        $("#email_com").val("");
                        $("#review").val("");
                        Comment.setStar($("#no-prd").val());
                        Comment.formValidationReply();


                        /*$.each(response.detailKomentar, function (idx, value) {
                                console.dir(value.KOMENTAR);
                            $("#isi-comment").append('' +
                            '<div class="row-review-item">'+
                            '<div class="review-item clearfix">'+
                            '<div class="review-avatar">'+
                            '<img class="img-circle" src="'+BASE_URL+'/assets/frontend/pages/img/comment/avatar.png" alt="..."/>'+
                            '</div>'+
                            '<div class="review-item-submitted">'+
                            '<strong class="text-capitalize">'+value.CREATED_BY+'</strong>'+
                            '<em>'+value.CREATED_DATE+
                             //'<a href="#collapse-${komentar.ID_KOMENTAR}" aria-controls="collapse-${komentar.ID_KOMENTAR}" style="margin-left: 10px" data-toggle="collapse" aria-expanded="false">Reply</a>'+
                            '</em>'+
                            '<div class="rateit" data-rateit-value="5" data-rateit-ispreset="true" data-rateit-readonly="true" id="rate-'+value.ID_KOMENTAR+'"></div>'+
                            '</div>'+
                             '<div class="review-item-content">'+
                              '<p>'+value.KOMENTAR+'</p>'+
                              '</div>'+
                              '</div>'
                            );
                            var sel="#rate-"+value.ID_KOMENTAR;
                                $(sel).rateit({max: value.RATE, min: 0, step:1});
                                //$(sel).attr( 'data-rateit-value',value.RATE);
                                //$(sel).rateit('value', (value.RATE));
                        });*/

                    }
                });

                }
            });

        },

        // =========================================================================
        // HANDLE SERVICE VALIDATION REPLY
        // =========================================================================
        formValidationReply: function () {
            $('.btn-action-reply').on('click', function(){
                var me = $(this);
                var rate='#rate_com'+me.closest('.comment-reply').find("input[name='PRM_PARENT']").val()
                $.ajax({
                    method: "POST",
                    url: BASE_URL +"product/comment",
                    data:{
                        PRM_NOMOR_PRODUK : me.closest('.comment-reply').find("input[name='PRM_NOMOR_PRODUK']").val(),
                        PRM_NAMA : me.closest('.comment-reply').find("input[name='PRM_NAME']").val(),
                        PRM_EMAIL : me.closest('.comment-reply').find("input[name='PRM_EMAIL']").val(),
                        PRM_KOMENTAR : me.closest('.comment-reply').find("textarea[name='PRM_KOMENTAR']").val(),
                        PRM_PARENT : me.closest('.comment-reply').find("input[name='PRM_PARENT']").val(),
                        PRM_RATE : $(rate+"  > div").attr('aria-valuenow')
                    },
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#tab-comment'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        Metronic.unblockUI($('#tab-comment'));
                        $("#isi-comment").html(response);
                        $("#name_com").val("");
                        $("#email_com").val("");
                        $("#review").val("");
                        Comment.setStar(me.closest('.comment-reply').find("input[name='PRM_NOMOR_PRODUK']").val());
                        Comment.formValidationReply();


                        /*$.each(response.detailKomentar, function (idx, value) {
                         console.dir(value.KOMENTAR);
                         $("#isi-comment").append('' +
                         '<div class="row-review-item">'+
                         '<div class="review-item clearfix">'+
                         '<div class="review-avatar">'+
                         '<img class="img-circle" src="'+BASE_URL+'/assets/frontend/pages/img/comment/avatar.png" alt="..."/>'+
                         '</div>'+
                         '<div class="review-item-submitted">'+
                         '<strong class="text-capitalize">'+value.CREATED_BY+'</strong>'+
                         '<em>'+value.CREATED_DATE+
                         //'<a href="#collapse-${komentar.ID_KOMENTAR}" aria-controls="collapse-${komentar.ID_KOMENTAR}" style="margin-left: 10px" data-toggle="collapse" aria-expanded="false">Reply</a>'+
                         '</em>'+
                         '<div class="rateit" data-rateit-value="5" data-rateit-ispreset="true" data-rateit-readonly="true" id="rate-'+value.ID_KOMENTAR+'"></div>'+
                         '</div>'+
                         '<div class="review-item-content">'+
                         '<p>'+value.KOMENTAR+'</p>'+
                         '</div>'+
                         '</div>'
                         );
                         var sel="#rate-"+value.ID_KOMENTAR;
                         $(sel).rateit({max: value.RATE, min: 0, step:1});
                         //$(sel).attr( 'data-rateit-value',value.RATE);
                         //$(sel).rateit('value', (value.RATE));
                         });*/
                    }
                });
            });
        },
        setStar :function(nomorProduk){
            $.ajax({
                method: "POST",
                url: BASE_URL +"product/star",
                data:{
                    PRM_NOMOR_PRODUK : nomorProduk
                },
                success: function (response) {
                    console.dir(response)

                    $.each(response.detailKomentar.data, function (idx, value) {
                        var sel="#rate-"+value.ID_KOMENTAR;
                        $(sel).rateit({max: value.RATE, min: 0, step:1});
                        $('#rate_com'+value.ID_KOMENTAR).rateit({ max: 5, step: 1});
                    });
                    $.each(response.commentChild, function (idx, v) {
                        console.log(v.ID_KOMENTAR);
                        var sel="#rate-"+v.ID_KOMENTAR;
                        $(sel).rateit({max: v.RATE, min: 0, step:1});
                        console.log(sel);
                    });

                }
            });
        },

    };
}();

Comment.init();


