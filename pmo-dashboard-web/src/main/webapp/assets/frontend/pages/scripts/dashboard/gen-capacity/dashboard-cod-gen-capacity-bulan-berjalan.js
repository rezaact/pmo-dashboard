var DashboardCodGenCapacityBulanBerjalan = function () {
    var codModal = null;
    var detailAssetCapacityModal = null;
    var detailAssetCapacityDatatable = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            DashboardCodGenCapacityBulanBerjalan.DataPopUP();
            codModal = $("#detail-cod-modal");

            detailAssetCapacityDatatable = $('#table-detail-gen-capacity-bulan-sebelumnya').DataTable( {
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "THBL", "defaultContent": "-", "render" : function (data, type, row) {
                        return ConvertMonth.numToStr(row.THBL.substring(4,6));
                    }},
                    { data: "THBL", "defaultContent": "-", "render" : function (data, type, row) {
                        return row.THBL.substring(0,4);
                    }},
                    { data: "CAPACITY", "defaultContent": "-"},
                    { data: "REGION_NAME", "defaultContent": "-"}
                ],
            });

        },
        DataPopUP: function () {
            $(".Div_Data_Detail_Bulan_Berjalan").click(function(e){
                e.preventDefault();
                alert ("Load Bulan Berjalan");

                $('#detail-asset-capacity-bulan-berjalan').modal('show');
                //URL Load
                //var URL = "";
                //$('#modal-data-detail-pmo-ku').load(URL);
                    detailAssetCapacityModal = $("#detail-asset-capacity-bulan-berjalan");
                    detailAssetCapacityDatatable = $('#table-detail-gen-capacity-bulan-berjalan').DataTable( {
                        "searching": true,
                        "bLengthChange" : false,
                        "iDisplayLength": 5,
                        "bAutoWidth": true,
                        columns: [
                            { data: "TAHUN", "defaultContent": "-"},
                            { data: "BULAN", "defaultContent": "-"},
                            { data: "CAPACITY", "defaultContent": "-"},
                            { data: "REGION_NAME", "defaultContent": "-"}
                        ],
                    });
            });
        },
        dataPlotClick : function(eventObj, dataObj){
            console.log(dataObj);

            var categoryLabel = dataObj.categoryLabel;
            var datasetName = dataObj.datasetName;

            detailAssetCapacityModal.find("#judul").text(categoryLabel);

            var cmbRegion = "0";//$(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbType = $(".CmbType").val();
            var cmbOwnership = $(".Ownership").val();
            var startPeriod = "201609";//$(".starperiod").val();

            console.log(categoryLabel);
            console.log(categoryLabel.substring(0,3));

            switch (categoryLabel.substring(0,3)){
                case 'JAN': category = '01'; break;
                case 'FEB': category = '02'; break;
                case 'MAR': category = '03'; break;
                case 'APR': category = '04'; break;
                case 'MAY': category = '05'; break;
                case 'JUN': category = '06'; break;
                case 'JUL': category = '07'; break;
                case 'AUG': category = '08'; break;
                case 'SEP': category = '09'; break;
                case 'OCT': category = '10'; break;
                case 'NOV': category = '11'; break;
                case 'DEC': category = '12'; break;
                default : category = '01';
            }
            category = categoryLabel.substring(4,8)+category;

            var dataPost = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbOwnership: cmbOwnership,
                cmbType: cmbType,
                startPeriod : startPeriod
            }

            //fetch data

            console.log('dataPost');
            console.log(dataPost);


            var url = BASE_URL+'dashboard-cod/get-data-detail-cod-gen-capacity';
            ////getdata-chart-milestone
            $.ajax({
                type: "POST",
                url: url,
                data: dataPost,
                success: function (response) {
                    console.log(response);
                    detailAssetCapacityDatatable.clear().rows.add( response.data ).draw();
                    detailAssetCapacityModal.modal('show');
                }
            });
        },
        onChangeFilterAll: function (){
            $( ".OnFilterAll" ).change(function() {

                var cmbRegion = $(".CmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();//Group
                var cmbType = $(".CmbType").val();
                var cmbOwnership = $(".Ownership").val();
                var cmbStartDate = $(".starperiod").val();
                var cmbEndDate = $(".endperiod").val();
                var cmbKapasitas= $(".cmbKapasitasAwal").val();

                var data = {
                    cmbRegion : cmbRegion,
                    cmbProgram : cmbProgram,
                    cmbType: cmbType,
                    cmbOwnership: cmbOwnership,
                    cmbStartDate: cmbStartDate,
                    cmbEndDate: cmbEndDate,
                    cmbKapasitas: cmbKapasitas
                }


                var url = BASE_URL+'dashboard-cod/getdata-cod-estimate';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data,
                    beforeSend: function () {
                        $(".modal_loading").fadeIn(1000);
                        $(".datepicker").fadeOut();
                    },
                    success: function(response) {
                        //DashboardCodEstimate.ChartBulananGeneration(JSON.parse(response.strGetGeneration_gen));
                        $(".modal_loading").fadeOut(1000);

                    }
                });

            });

        },
    }

}();

// Call init
DashboardCodGenCapacityBulanBerjalan.init();