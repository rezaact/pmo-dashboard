
var CapacityDetail = function () {
    $(".panel_region").hide();
    var detailCapacityDatatable = null;
    var satuan = null;
    var header = null;
    var headerName = null;
    var headerType = null;
    var detailTitle = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            CapacityDetail.piechart();

            switch (AssetType){
                case '17' : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
                    break;
                case '18' : satuan = ' KMS';
                    headerName = 'Jalur Transmisi';
                    headerType = 'Type Transmisi';
                    detailTitle = 'Transmission Capacity Detail';
                    break;
                case '19' : satuan = ' MVA';
                    headerName = 'Nama GI';
                    headerType = 'Type GI';
                    detailTitle = 'Substation Capacity Detail';
                    break;
                default : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
            }
            detailTitle = detailTitle + " Periode " + ConvertMonth.yyyymmToStr(tahunAwal);

            if(tahunAkhir != '0'){
                detailTitle = detailTitle + " - "+ ConvertMonth.yyyymmToStr(tahunAkhir);
            }

            $('#detail-title-header').html("Chart "+detailTitle);
            $('#detail-title').html(detailTitle);


            CapacityDetail.initDataTable();

            //if(!((tahunAwal == '201701') && (jenis == '3'))){
            //    //hide fase coulumn
            //    detailCapacityDatatable.column(7).visible( false );
            //}
            //Insert title to Captions Chart
            json_strChart_All = jQuery.parseJSON(strChart_All);
            json_strChart_All.chart.caption = json_strChart_All.chart.caption +" ("+satuan + " )";

            $('#back').on('click', function(){
                history.back();
            });

        },
        piechart:function(){

            FusionCharts.ready(function(){
                var revenueChart = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "chartContainer",
                    "width": "400",
                    "height": "400",
                    //'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":json_strChart_All,
                });
                revenueChart.render();

                //Chart Region 1
                FusionCharts.ready(function(){

                    //Parsing
                    var json_strChart_Sumatera = jQuery.parseJSON(strChart_Sumatera),
                        caption_sumatera = json_strChart_Sumatera.chart.caption,
                        data_sumatera = json_strChart_Sumatera.data.length;

                    var json_strChart_JBB = jQuery.parseJSON(strChart_JBB),
                        caption_JBB = json_strChart_JBB.chart.caption,
                        data_JBB = json_strChart_JBB.data.length;

                    var json_strChart_JBT = jQuery.parseJSON(strChart_JBT),
                        caption_JBT = json_strChart_JBT.chart.caption,
                        data_JBT = json_strChart_JBT.data.length;

                    var json_strChart_JBTB = jQuery.parseJSON(strChart_JBTB),
                        caption_JBTB = json_strChart_JBTB.chart.caption,
                        data_JBTB = json_strChart_JBTB.data.length;

                    var json_strChart_KAL = jQuery.parseJSON(strChart_KAL),
                        caption_KAL = json_strChart_KAL.chart.caption,
                        data_KAL = json_strChart_KAL.data.length;

                    var json_strChart_SNT= jQuery.parseJSON(strChart_SNT),
                        caption_SNT = json_strChart_SNT.chart.caption,
                        data_SNT = json_strChart_SNT.data.length;

                    var json_strChart_MP = jQuery.parseJSON(strChart_MP),
                        caption_MP = json_strChart_MP.chart.caption,
                        data_MP = json_strChart_MP.data.length;





                    //Hide Div Chart dengan jika data kosong
                    if (data_sumatera == '0'){
                        $(".chartContainer-regional-1").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_sumatera+'</div>');
                        $(".panel_region").show();
                    }
                    if (data_JBB == '0'){
                        $(".chartContainer-regional-2").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBB+'</span>');
                        $(".panel_region").show();
                    }
                    if (data_JBT == '0'){
                        $(".chartContainer-regional-3").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBT+'</span>');
                        $(".panel_region").show();
                    }
                    if (data_JBTB == '0'){
                        $(".chartContainer-regional-4").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBTB+'</span>');
                        $(".panel_region").show();
                    }
                    if (data_KAL == '0'){
                        $(".chartContainer-regional-5").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_KAL+'</span>');
                        $(".panel_region").show();
                    }
                    if (data_SNT == '0'){
                        $(".chartContainer-regional-6").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_SNT+'</span>');
                        $(".panel_region").show();
                    }
                    if (data_MP == '0'){
                        $(".chartContainer-regional-7").hide();
                        $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_MP+'</span>');
                        $(".panel_region").show();
                    }



                    //For Region 1
                    var revenueChartRegion1 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-1",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_Sumatera
                    });
                    revenueChartRegion1.render();
                    //End For Region 1

                    //For Region 2
                    var revenueChartRegion2 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-2",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_JBB
                    });
                    revenueChartRegion2.render();
                    //End Region 2

                    //For Region 3
                    var revenueChartRegion3 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-3",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_JBT
                    });
                    revenueChartRegion3.render();
                    //End Region 3


                    //For Region 4
                    var revenueChartRegion4 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-4",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_JBTB
                    });
                    revenueChartRegion4.render();
                    //End Region 4

                    //For Region 5
                    var revenueChartRegion5 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-5",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_KAL
                    });
                    revenueChartRegion5.render();
                    //End Region 5


                    //For Region 6
                    var revenueChartRegion6 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-6",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_SNT
                    });
                    revenueChartRegion6.render();
                    //End Region 6

                    //For Region 7
                    var revenueChartRegion7 = new FusionCharts({
                        "dataLoadStartMessage" : "Fetching data",
                        "dataEmptyMessage" : "Data Not Available",
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-7",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_MP
                    });
                    revenueChartRegion7.render();
                    //End Region 7

                });
            })
        },
        initDataTable : function(){

            function renderSatuan(data, type, row){
                var firstChar = data.toString().substring(0,1);
                var newData = (firstChar == '.')?'0'+data:data;

                return newData+satuan;
            }

            detailCapacityDatatable = $('#table-capacity-detail').DataTable( {
                "searching": false,
                "responsive" : true,
                "bLengthChange" : false,
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "processing": true,
                "serverSide": true,
                "columnDefs": [
                    { "visible": false, "targets": 0 },
                    { className: "dt-body-right", "targets": [ 5 ] },
                    { className: "dt-body-center", "targets": [ 6 ] }
                ],
                "ajax": {
                    "url": BASE_URL + "dashboard-cod-capacity/grid",
                    "type": "POST",
                    "data": {
                        p_jenis : jenis,
                        p_subjenis : subJenis,
                        p_Group : group,
                        p_detail_tipe : detailType,
                        p_thblawal : tahunAwal,
                        p_thblakhir : tahunAkhir,
                        p_asset_type : AssetType
                    }
                },
                columns: [
                    { data: "TIPE_PEMBANGKIT", "defaultContent": "-"},
                    { data: "RN", "defaultContent": "-"},
                    { data: "ASSETNUMBER", "defaultContent": "-", "visible": false},
                    { data: "ASSETNAME", "render": function(data, type, row){
                        return "<p data-toggle='tooltip' title='"+row.ASSETNUMBER+"'>"+data+"</p>";

                    }, "title" : headerName},
                    { data: "REGION", "defaultContent": "-"},
                    { data: "UNITCAPACITY", "defaultContent": "-", "render" : renderSatuan},
                    { data: "COD", "defaultContent": "-"},
                    { data: "PHASE", "defaultContent": "-", "visible": false},
                    { data: "STATUS", "defaultContent": "-"}
                ],

                "drawCallback": function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(0, {page:'current'} ).data().each( function ( group, i ) {

                        //if(group != 'N/A'){
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group" ><td colspan="7" style="background-color: #dff0d8!important;">'+headerType+' : ' + group + '</td></tr>'
                            );
                            last = group;
                        }
                        //}
                        //else{
                        //    //remove row
                        //    $(rows).eq(i).remove();
                        //}



                    } );
                }
            });




        },
        getParams: function(){

        }
    }

}();

// Call init
CapacityDetail.init();