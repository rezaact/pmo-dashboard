var Home = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Home.sliderRevolution();
            Home.sliderOWL();
        },

        // =========================================================================
        // SLIDER IMAGE (REVOLUTION SLIDER)
        // =========================================================================
        sliderRevolution: function () {
            jQuery('.fullwidthabnner').show().revolution({
                delay:2000,
                startheight:417,
                startwidth:1150,

                hideThumbs:10,

                thumbWidth:100,                         // Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
                thumbHeight:50,
                thumbAmount:5,

                navigationType:"bullet",                // bullet, thumb, none
                navigationArrows:"solo",                // nexttobullets, solo (old name verticalcentered), none

                navigationStyle:"round",                // round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom

                navigationHAlign:"center",              // Vertical Align top,center,bottom
                navigationVAlign:"bottom",              // Horizontal Align left,center,right
                navigationHOffset:0,
                navigationVOffset:20,

                soloArrowLeftHalign:"left",
                soloArrowLeftValign:"center",
                soloArrowLeftHOffset:20,
                soloArrowLeftVOffset:0,

                soloArrowRightHalign:"right",
                soloArrowRightValign:"center",
                soloArrowRightHOffset:20,
                soloArrowRightVOffset:0,

                touchenabled:"on",                      // Enable Swipe Function : on/off
                onHoverStop:"on",                       // Stop Banner Timet at Hover on Slide on/off

                stopAtSlide:-1,
                stopAfterLoops:-1,

                hideCaptionAtLimit:0,         // It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
                hideAllCaptionAtLilmit:0,       // Hide all The Captions if Width of Browser is less then this value
                hideSliderAtLimit:0,          // Hide the whole slider, and stop also functions if Width of Browser is less than this value

                shadow:1,                               //0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
                fullWidth:"on"                          // Turns On or Off the Fullwidth Image Centering in FullWidth Modus
            });
        },

        // =========================================================================
        // SLIDER IMAGE (OWL SLIDER)
        // =========================================================================
        sliderOWL: function () {
            $(".owl-carousel6-brands").owlCarousel({
                pagination: false,
                navigation: true,
                items: 6,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 5],
                    [1200, 6],
                    [1400, 6],
                    [1600, 6]
                ],
            });

            $(".owl-carousel5").owlCarousel({
                pagination: false,
                navigation: true,
                items: 5,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [660, 2],
                    [700, 3],
                    [768, 3],
                    [992, 4],
                    [1024, 4],
                    [1200, 5],
                    [1400, 5],
                    [1600, 5]
                ],
            });

            $(".owl-carousel4").owlCarousel({
                pagination: false,
                navigation: true,
                items: 4,
                addClassActive: true,
            });

            $(".owl-carousel3").owlCarousel({
                pagination: false,
                navigation: true,
                items: 3,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [768, 2],
                    [1024, 3],
                    [1200, 3],
                    [1400, 3],
                    [1600, 3]
                ],
            });

            $(".owl-carousel2").owlCarousel({
                pagination: false,
                navigation: true,
                items: 2,
                addClassActive: true,
                itemsCustom : [
                    [0, 1],
                    [320, 1],
                    [480, 2],
                    [700, 3],
                    [975, 2],
                    [1200, 2],
                    [1400, 2],
                    [1600, 2]
                ],
            });
        }

    };

}();

// Call init
Home.init();