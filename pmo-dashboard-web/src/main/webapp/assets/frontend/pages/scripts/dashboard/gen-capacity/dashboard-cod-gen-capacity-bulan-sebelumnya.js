  var DashboardCodGenCapacityBulanSebelumnya = function () {
    var codModal = null;
    var detailAssetCapacityModal = null;
    var detailAssetCapacityDatatable = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            DashboardCodGenCapacityBulanSebelumnya.DataPopUP();
            detailAssetCapacityModal = $("#detail-asset-capacity-bulan-sebelumnya");
            codModal = $("#detail-cod-modal");
            detailAssetCapacityDatatable = $('#table-detail-gen-capacity-bulan-sebelumnya').DataTable( {
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "THBL", "defaultContent": "-", "render" : function (data, type, row) {
                        return ConvertMonth.numToStr(row.THBL.substring(4,6));
                    }},
                    { data: "THBL", "defaultContent": "-", "render" : function (data, type, row) {
                        return row.THBL.substring(0,4);
                    }},
                    { data: "CAPACITY", "defaultContent": "-"},
                    { data: "REGION_NAME", "defaultContent": "-"}
                ],
            });


        },
        DataPopUP: function () {
            $(".DivGet_Data_Detail_Bulan_Sebelumnya").click(function(e){
                e.preventDefault();

                $('#detail-asset-capacity-bulan-sebelumnya').modal('show');
                //URL Load
                //var URL = "";
                //$('#modal-data-detail-pmo-ku').load(URL);

                var cmbRegion = $(".CmbRegion").val();
                var startPeriod = ConvertMonth.MyyyyToyyyymm($("#from").val());

                var dataPost = {
                    cmbRegion : cmbRegion,
                    startPeriod: startPeriod
                }

                var url = BASE_URL+'dashboard-cod/get-data-detail-cod-gen-capacity';
                ////getdata-chart-milestone
                $.ajax({
                    type: "POST",
                    url: url,
                    data: dataPost,
                    success: function (response) {
                        detailAssetCapacityDatatable.clear().rows.add( response.data ).draw();
                        detailAssetCapacityModal.modal('show');
                        console.log(response);
                    }
                });

            });
        },
        dataPlotClick : function(eventObj, dataObj){

            var categoryLabel = dataObj.categoryLabel;
            var datasetName = dataObj.datasetName;


        },
    }

}();

// Call init
DashboardCodGenCapacityBulanSebelumnya.init();