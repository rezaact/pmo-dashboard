var listCheck = [];
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

var ProductList = function(){
    return{
        init : function () {
            ProductList.callModal();
            ProductList.disabledScroll();
            ProductList.ajaxWorkshop();
            //ProductList.actionPagination();
            ProductList.productOrderBy();
            ProductList.productView();
            ProductList.actionKategori();
        },

        disabledScroll:function(){

            if (window.addEventListener) // older FF
                window.addEventListener('DOMMouseScroll', ProductList.preventDefault, false);

            window.onwheel = ProductList.preventDefault; // modern standard
            window.onmousewheel = document.onmousewheel = ProductList.preventDefault; // older browsers, IE
            window.ontouchmove  = ProductList.preventDefault; // mobile
            document.onkeydown  = ProductList.preventDefaultForScrollKeys;
        },

        preventDefault : function(e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        },

        preventDefaultForScrollKeys: function(e) {
            if (keys[e.keyCode]) {
                ProductList.preventDefault(e);
                return false;
            }
        },

        callModal: function () {
            //modal closed event
            $('#modal-welcome').on('hidden.bs.modal', function () {
                console.log('modal-welcome closeed');
                if (window.removeEventListener)
                    window.removeEventListener('DOMMouseScroll', ProductList.preventDefault, false);

                window.onmousewheel = document.onmousewheel = null;
                window.onwheel = null;
                window.ontouchmove = null;
                document.onkeydown = null;
            });

            $('#modal-welcome').modal(
                {
                    show: true,
                    backdrop: 'static',
                    keyboard: false
                }
            );
        },

        ajaxWorkshop: function () {
            $('#sidebar-workshop input[type=checkbox]').live('change', function() {
                listCheck = [];
                $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
                    listCheck.push($(this).val());
                });
                if(listCheck==''){
                    //alert('jalan');
                    $('#sidebar-workshop input[type=checkbox]').each(function() {
                        listCheck.push($(this).val());
                    });
                }
                var idKat=$('#idKat').val();
                var idJenis=$('#idJenis').val();
                var idKomponen=$('#idKomponen').val();
                var dir=$('#paramdir').val();
                var urutan=$('#urutan').val();
                var search=$('#search').val();
                var linkUrl=$('#linkUrl').val();

               // alert(linkUrl);
                $.ajax({
                    type: 'GET',
                    url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir+"&length="+urutan+"&search="+search,
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#product-container'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        // menghilangkan loading jika data telah sukses
                        Metronic.unblockUI($('#product-container'));
                        //console.table(response);
                        $('#fillContainer').html(response);
                    }
                });
            });
        },
        //tidak dipakai
        productOrderBy: function () {
            $('#product-order-by').change(function () {
                var dir = $(this).val();
                //alert(dir);
                listCheck = [];
                $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
                    listCheck.push($(this).val());
                });
                if(listCheck==''){
                    //alert('jalan');
                    $('#sidebar-workshop input[type=checkbox]').each(function() {
                        listCheck.push($(this).val());
                    });
                }
                var idKat=$('#idKat').val();
                var idJenis=$('#idJenis').val();
                var idKomponen=$('#idKomponen').val();
                var linkUrl=$('#linkUrl').val();
                $.ajax({
                    type: 'GET',
                    url: BASE_URL + 'product/product-list-data?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir,
                    beforeSend: function () {
                        Metronic.blockUI({
                            target: $('#product-container'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        Metronic.unblockUI($('#product-container'));
                        //console.table(response);
                        $('#fillContainer').html(response);
                    }
                });
            });
        },

        productView: function () {
            $('#product-view').on('change', function () {
                $.ajax({
                    type: 'POST',
                    url: 'xxx',
                    beforeSend: function () {
                        Metronic.blockUI({
                            target: $('#product-container'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        Metronic.unblockUI($('#product-container'));
                        console.table(response);
                    }
                });
            });
        },

       /* actionPagination: function () {
            function onPaginationClick(form,page){
                alert("jalan");
                form.action=BASE_URL+'/product/product-list?page='+page;
                alert(form.action);
                form.onsubmit();
            }
        },*/

        actionKategori: function () {
            /*function onKategoriClick(kat,jenis,kom){
                alert("jalan "+kat+", "+jenis+", "+kom);
            }*/
        }
    }
}();

ProductList.init();


function onKategoriClick(kat,jenis,kom){
    //alert("jalan "+kat+", "+jenis+", "+kom);
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var dir=$('#paramdir').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    var linkUrl=$('#linkUrl').val();
    $.ajax({
        type: 'GET',
        url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + kat+"&idjenis="+jenis+"&idkom="+kom+"&paramdir="+dir+"&length="+urutan+"&search="+search,
        beforeSend: function () {
            // loading pada product container
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            // menghilangkan loading jika data telah sukses
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });
}

function paginationClick(url){
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var dir=$('#paramdir').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    $.ajax({
        type: 'GET',
        url: url+'&idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen,//+"&paramdir="+dir+"&length="+urutan+"&search="+search
        beforeSend: function () {
            // loading pada product container
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            // menghilangkan loading jika data telah sukses
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });
}

function onChangeOrderBy(dir){
    //var dir = $(this).val();
    //alert(dir);
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    var linkUrl=$('#linkUrl').val();
    $.ajax({
        type: 'GET',
        url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir.value+"&length="+urutan+"&search="+search,
        beforeSend: function () {
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });

}

function onChangeUrutan(urutan){
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var dir=$('#paramdir').val();
    var search=$('#search').val();
    var linkUrl=$('#linkUrl').val();
    $.ajax({
        type: 'GET',
        url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir+"&length="+urutan.value+"&search="+search,
        beforeSend: function () {
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });
}

function onClickPencarian(){
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var dir=$('#paramdir').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    var linkUrl=$('#linkUrl').val();
    $.ajax({
        type: 'GET',
        url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir+"&length="+urutan+"&search="+search,
        beforeSend: function () {
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });
}



function onClickButtonSearch(){
        listCheck = [];
        $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
            listCheck.push($(this).val());
        });
        if(listCheck==''){
            //alert('jalan');
            $('#sidebar-workshop input[type=checkbox]').each(function() {
                listCheck.push($(this).val());
            });
        }
        var idKat=$('#idKat').val();
        var idJenis=$('#idJenis').val();
        var idKomponen=$('#idKomponen').val();
        var dir=$('#paramdir').val();
        var urutan=$('#urutan').val();
        var search=$('#search').val();
        var linkUrl=$('#linkUrl').val();
        $.ajax({
            type: 'GET',
            url: linkUrl + '?idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen+"&paramdir="+dir+"&length="+urutan+"&search="+search,
            beforeSend: function () {
                Metronic.blockUI({
                    target: $('#product-container'),
                    boxed: true
                });
            },
            success: function (response) {
                Metronic.unblockUI($('#product-container'));
                //console.table(response);
                $('#fillContainer').html(response);
            }
        });
}


function clickTabList(link){
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var dir=$('#paramdir').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    $.ajax({
        type: 'GET',
        url: link+'&idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen,//+"&paramdir="+dir+"&length="+urutan+"&search="+search
        beforeSend: function () {
            // loading pada product container
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            // menghilangkan loading jika data telah sukses
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });
}

function clickTabGrid(link){
    listCheck = [];
    $('#sidebar-workshop input[type=checkbox]:checked').each(function() {
        listCheck.push($(this).val());
    });
    if(listCheck==''){
        //alert('jalan');
        $('#sidebar-workshop input[type=checkbox]').each(function() {
            listCheck.push($(this).val());
        });
    }
    var idKat=$('#idKat').val();
    var idJenis=$('#idJenis').val();
    var idKomponen=$('#idKomponen').val();
    var dir=$('#paramdir').val();
    var urutan=$('#urutan').val();
    var search=$('#search').val();
    $.ajax({
        type: 'GET',
        url: link+'&idworkshop=' + listCheck+'&idkat=' + idKat+"&idjenis="+idJenis+"&idkom="+idKomponen,//+"&paramdir="+dir+"&length="+urutan+"&search="+search
        beforeSend: function () {
            // loading pada product container
            Metronic.blockUI({
                target: $('#product-container'),
                boxed: true
            });
        },
        success: function (response) {
            // menghilangkan loading jika data telah sukses
            Metronic.unblockUI($('#product-container'));
            //console.table(response);
            $('#fillContainer').html(response);
        }
    });

}