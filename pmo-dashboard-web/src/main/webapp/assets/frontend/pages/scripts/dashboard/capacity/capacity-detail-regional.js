var CapacityDetailRegion = function () {

    var detailCapacityDatatable = null;
    var revenueChartRegion1 = null;
    var revenueChartRegion2 = null;
    var satuan = null;
    var headerName = null;
    var headerType = null;
    var detailTitle = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            CapacityDetailRegion.piechart();

            switch (AssetType){
                case '17' : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
                    break;
                case '18' : satuan = ' KMS';
                    headerName = 'Jalur Transmisi';
                    headerType = 'Type Transmisi';
                    detailTitle = 'Transmission Capacity Detail';
                    break;
                case '19' : satuan = ' MVA';
                    headerName = 'Nama GI';
                    headerType = 'Type GI';
                    detailTitle = 'Substation Capacity Detail';
                    break;
                default : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
            }
            detailTitle = detailTitle + " Periode " + ConvertMonth.yyyymmToStr(tahunAwal);

            if(tahunAkhir != '0'){
                detailTitle = detailTitle + " - "+ ConvertMonth.yyyymmToStr(tahunAkhir);
            }

            $('#detail-title-header').html("Chart "+detailTitle);
            $('#detail-title').html(detailTitle);

            CapacityDetailRegion.initDataTable();

/*            if(!((tahunAwal == '201701') && (jenis == '3'))){
                //hide fase coulumn
                detailCapacityDatatable.column(7).visible( false );
            }*/

            //Insert title to Captions Chart
            json_strChart_All = jQuery.parseJSON(strChart_All);
            json_strChart_All.chart.caption = json_strChart_All.chart.caption +" ("+satuan + " )";


            $('#back').on('click', function(){
                history.back();
            });
        },
        piechart:function(){

            FusionCharts.ready(function(){
                //For All
                var revenueChart = new FusionCharts({
                    "type": "pie2d",
                    "renderAt": "chartContainer-regional",
                    "width": "600",
                    "height": "400",
                    "dataFormat": "json",
                    "dataSource":json_strChart_All
                   // "dataSource":{
                   //     "chart": {
                   //         "exportenabled": "1",
                   //         "exportatclient": "1",
                   //         "caption": "Marketing Expenses for 2013",
                   //         "numberprefix": "$",
                   //         "plotgradientcolor": "",
                   //         "bgcolor": "FFFFFF",
                   //         "showalternatehgridcolor": "0",
                   //         "showplotborder": "0",
                   //         "divlinecolor": "CCCCCC",
                   //         "showvalues": "1",
                   //         "showcanvasborder": "0",
                   //         "canvasbordercolor": "CCCCCC",
                   //         "canvasborderthickness": "1",
                   //         "yaxismaxvalue": "30000",
                   //         "captionpadding": "30",
                   //         "linethickness": "3",
                   //         "sshowanchors": "0",
                   //         "yaxisvaluespadding": "15",
                   //         "showlegend": "1",
                   //         "use3dlighting": "0",
                   //         "showshadow": "0",
                   //         "legendshadow": "0",
                   //         "legendborderalpha": "0",
                   //         "showborder": "0",
                   //         "palettecolors": "#EED17F,#97CBE7,#074868,#B0D67A,#2C560A,#DD9D82"
                   //     },
                   //     "data": [
                   //         {
                   //             "label": "Online Ads",
                   //             "value": "125000",
                   //             "issliced": "1"
                   //         },
                   //         {
                   //             "label": "Print Ads",
                   //             "value": "110000"
                   //         },
                   //         {
                   //             "label": "Content Marketing",
                   //             "value": "95000"
                   //         },
                   //         {
                   //             "label": "Tradeshows",
                   //             "value": "60000"
                   //         }
                   //     ]}

                }); //End For All
                revenueChart.render();



            });

    },
        initDataTable : function(){

            function renderSatuan(data, type, row){
                var firstChar = data.toString().substring(0,1);
                var newData = (firstChar == '.')?'0'+data:data;

                return newData+satuan;
            }

            detailCapacityDatatable = $('#table-capacity-detail').DataTable( {
                responsive: true,
                "searching": false,
                "bLengthChange" : false,
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "processing": true,
                "serverSide": true,
                "columnDefs": [
                    { "visible": false, "targets": 0 },
                    { className: "dt-body-right", "targets": [ 5 ] },
                    { className: "dt-body-center", "targets": [ 6 ] }
                ],
                "ajax": {
                    "url": BASE_URL + "dashboard-cod-capacity/grid",
                    "type": "POST",
                    "data": {
                        p_jenis : jenis,
                        p_subjenis : subJenis,
                        p_Group : group,
                        p_detail_tipe : detailType,
                        p_thblawal : tahunAwal,
                        p_thblakhir : tahunAkhir,
                        p_asset_type : AssetType
                    }
                },
                columns: [
                    { data: "REGION", "defaultContent": "-"},
                    { data: "RN", "defaultContent": "-"},
                    { data: "ASSETNUMBER", "defaultContent": "-", "visible": false},
                    { data: "ASSETNAME", "defaultContent": "-", "render" : function(data, type, row){
                        return "<p data-toggle='tooltip' title='"+row.ASSETNUMBER+"'>"+data+"</p>";
                    }, "title" : headerName},
                    { data: "TIPE_PEMBANGKIT", "defaultContent": "-", "title" : headerType},
                    { data: "UNITCAPACITY", "defaultContent": "-", "render": renderSatuan},
                    { data: "COD", "defaultContent": "-"},
                    { data: "STATUS", "defaultContent": "-"}
                    //{ data: "PHASE", "defaultContent": "-"}
                ],

                "drawCallback": function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="7" style="background-color: #dff0d8!important;">Region : ' + group + '</td></tr>'
                            );
                            last = group;
                        }
                    } );
                }
            });
        },
        getParams: function(){

        }
    }

}();

// Call init
CapacityDetailRegion.init();