/*
    Create by Deni : 12-12-2016
    Bugs : ?
    Update by Ruly : 12-12-2016
*/
var DashboardScurveSupply= function () {
    var revenueChart = null;
    var ValPenyusutan = 0.1;
    var ValPertumbuhan = 0.75;
    var tableSupplyDemand = null;
    var dataSupplyDemand = null;

    var rangePYT = $("#rangePenyusutan");
    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            var DataScuve = null;
            tableSupplyDemand = $('#table-supply-demand');
            DashboardScurveSupply.Datepicker();
            DashboardScurveSupply.Default_Value();
            DashboardScurveSupply.FuncScurveSupply();
            DashboardScurveSupply.onChangeCmb();
            DashboardScurveSupply.gos_meter();
        },
        gos_meter:function () {
                $("#rangePenyusutan").ionRangeSlider({
                    type: "single",
                    min: 0.10,
                    max: 100,
                    from: 0.1,
                    step: 0.1,
                    keyboard: true,
                    onChange: function (data) {
                        ValPertumbuhan = data.from;
                        DashboardScurveSupply.Default_Value();
                    }

                });


            $("#rangePertumbuhan").ionRangeSlider({
                type:"single",
                min: 0.00,
                max: 100,
                from: 0.75,
                step:0.75,
                keyboard: true,
                onChange: function (data) {
                    ValPertumbuhan = data.from;
                    DashboardScurveSupply.Default_Value();

                }
            });



            // $(".OnFilterAll_Click").click(function () {
            //     DashboardScurveSupply.Default_Value();
            // });
            var timer = null;
            $('.OnFilterAll_Click').change(function(){
                clearTimeout(timer);
                DashboardScurveSupply.Default_Value();
                timer = setTimeout(100)
            });

        },
        Datepicker: function (){
            $('.starperiod').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $('.endperiod').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });


        },
        Default_Value:function (){
            $(".modal_loading").fadeIn(1000);

            var cmbRegion = $(".cmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbThnAwal = $(".starperiod").val();
            var cmbThnAkhir = $(".endperiod").val();
            var cmbKapasitas = $(".kapasitas").val();
            var cmbDemand = $(".demand").val();
            var cmbSusut = ValPenyusutan;
            var cmbTumbuh = ValPertumbuhan;

            var data = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbThnAwal: cmbThnAwal,
                cmbThnAkhir: cmbThnAkhir,
                cmbKapasitas: cmbKapasitas,
                cmbDemand: cmbDemand,
                cmbSusut: cmbSusut,
                cmbTumbuh: cmbTumbuh
            }

            var url = BASE_URL+'dashboard-scurve/getdata-supply-demand';
            $.ajax({
                type : "POST",
                url: url,
                data : data,
                success: function(response) {
                    console.log("JSON.parse(response.strGetDataSupplay_gen)");
                    console.log(JSON.parse(response.strGetDataSupplay_gen));
                    //generate chart
                    dataSupplyDemand = JSON.parse(response.strGetDataSupplay_gen);

                    DashboardScurveSupply.FuncScurveSupply(dataSupplyDemand);
                    DashboardScurveSupply.TableSupplyDemand(dataSupplyDemand);
                    $(".modal_loading").fadeOut(1000);

                }
            });

        },
        onChangeCmb:function () {
            $(".OnFilterAll").change(function () {
                $(".modal_loading").fadeIn(1000);

                var cmbRegion = $(".cmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();
                var cmbThnAwal = $(".starperiod").val();
                var cmbThnAkhir = $(".endperiod").val();
                var cmbKapasitas = $(".kapasitas").val();
                var cmbDemand = $(".demand").val();
                var cmbSusut = ValPenyusutan;
                var cmbTumbuh = ValPertumbuhan;

                var data = {
                    cmbRegion : cmbRegion,
                    cmbProgram : cmbProgram,
                    cmbThnAwal: cmbThnAwal,
                    cmbThnAkhir: cmbThnAkhir,
                    cmbKapasitas: cmbKapasitas,
                    cmbDemand: cmbDemand,
                    cmbSusut: cmbSusut,
                    cmbTumbuh: cmbTumbuh

                }
                $(".datepicker").fadeOut();

                var url = BASE_URL+'dashboard-scurve/getdata-supply-demand';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data,
                    success: function(response) {

                        //generate chart
                        dataSupplyDemand = JSON.parse(response.strGetDataSupplay_gen);
                        DashboardScurveSupply.FuncScurveSupply(dataSupplyDemand);
                        DashboardScurveSupply.TableSupplyDemand(dataSupplyDemand);
                        $(".modal_loading").fadeOut(1000);

                    }
                });

            });
        },

        TableSupplyDemand: function(data){
            console.log("generate datatable");
            console.log(dataSupplyDemand);
            var demand = dataSupplyDemand.dataset[0];
            var existing = dataSupplyDemand.dataset[1];
            var project = dataSupplyDemand.dataset[2];
            var total = dataSupplyDemand.dataset[3];

            var row = [];

            for(var i=0;i<demand.data.length;i++){

                //console.log("demand.data[i]");
                //console.log(demand.data[i]);
                row.push({
                    demand : parseFloat(demand.data[i].value).toFixed(2),
                    existing : parseFloat(existing.data[i].value).toFixed(2),
                    project : parseFloat(project.data[i].value).toFixed(2),
                    total : parseFloat(total.data[i].value).toFixed(2),
                });
            }

            //console.log(row);

            var rowHTML = "";
            var startYear = $('.starperiod').val();
            var startMonth = 1;
            var nf = new Intl.NumberFormat();

            row.forEach(function(item){
                //console.log("item");
                //console.log(item);
                //console.log(item.demand);

               rowHTML += "<tr>" +
                       "<td>"+startYear+"</td>"+
                       "<td>"+startMonth+"</td>"+
                       "<td>"+nf.format(item.demand)+"</td>"+
                       "<td>"+nf.format(item.existing)+"</td>"+
                       "<td>"+nf.format(item.project)+"</td>"+
                       "<td>"+nf.format(item.total)+"</td>"+
                   "</tr>"

                if(startMonth < 12){
                    startMonth++;
                }
                else{
                    startMonth = 1;
                    startYear = parseInt(startYear)+1;
                }
            });

            tableSupplyDemand.find("tbody").html(rowHTML);

        },
        FuncScurveSupply: function (DataScuve) {
            //Script umtuk menghapus chart existing
            try {
                revenueChart.dispose();
                console.log('---> data ', DataScuve);
            } catch (err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function () {
                var revenueChart = new FusionCharts({
                    "type": "msline",
                    "renderAt": "chartScurveSupplay",
                    "width": "100%",
                    "height": "400",
                    "dataFormat": "json",
                    "dataSource": DataScuve

                });

                revenueChart.render();
            });
        }

    }
}();

// Call init
DashboardScurveSupply.init();