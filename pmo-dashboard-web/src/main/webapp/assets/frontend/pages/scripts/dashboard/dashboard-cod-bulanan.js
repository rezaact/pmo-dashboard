

var DashboardCodBulanan = function () {
    var revenueChart = null; //Generation
    var revenueChartTransmision = null; //Transmision
    var revenueChartSubtation = null; //Subtation
    var codModal = null;
    var detailAssetCapacityModal = null;
    var detailAssetCapacityDatatable = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            DashboardCodBulanan.onChangeFilterAll();
            DashboardCodBulanan.Datepicker();
            DashboardCodBulanan.Default_Value();
            DashboardCodBulanan.detailCapacity();
            codModal = $("#detail-cod-modal");

        },
        detailCapacity: function(){
            detailAssetCapacityModal = $("#detail-asset-capacity");
            detailAssetCapacityDatatable = $('#table-detail-asset-capacity').DataTable( {
                "searching": true,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "ASSETID", "defaultContent": "-"},
                    { data: "RUPTLCOD", "defaultContent": "-"},
                    { data: "ASSETGROUPNAME", "defaultContent": "-"},
                    { data: "ASSETOWNERSHIP", "defaultContent": "-"},
                    { data: "REGIONCODE", "defaultContent": "-"},
                    { data: "PLANCAPACITY", "defaultContent": "-"},
                    { data: "ACTUALCAPACITY", "defaultContent": "-"}
                ],
            });
        },
        dataPlotClick : function(eventObj, dataObj){


            var categoryLabel = dataObj.categoryLabel;
            var datasetName = dataObj.datasetName;

            detailAssetCapacityModal.find("#judul").text(datasetName+' '+categoryLabel);

            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbType = $(".CmbType").val();
            var cmbOwnership = $(".Ownership").val();
            var cmbDate = $(".cmbDate").val();
            var cmbPlan = $(".CmbPlan").val();
            var cmbActual= $(".CmbActual").val();
            var category = 0;
            var type = null;


            switch (eventObj.sender.args.renderAt){
                case "DivChartCODBulananGeneration" : type = 17; break;
                case "DivTransmision" : type = 18; break;
                case "DivSubtation" : type = 19; break;
            }


            switch (categoryLabel.substring(0,3)){
                case 'JAN': category = 1; break;
                case 'FEB': category = 2; break;
                case 'MAR': category = 3; break;
                case 'APR': category = 4; break;
                case 'MAY': category = 5; break;
                case 'JUN': category = 6; break;
                case 'JUL': category = 7; break;
                case 'AUG': category = 8; break;
                case 'SEP': category = 9; break;
                case 'OCT': category = 10; break;
                case 'NOV': category = 11; break;
                case 'DES': category = 12; break;
                default : category = 1;
            }

            var dataPost = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbType: cmbType,
                cmbOwnership: cmbOwnership,
                cmbDate: cmbDate,
                cmbPlan: cmbPlan,
                cmbActual: cmbActual,
                categoryLabel : category,
                datasetName : datasetName,
                type : type
            }

            //fetch data


            var url = BASE_URL+'dashboard-cod/bulanan/getdata-asset-capacity';
            ////getdata-chart-milestone
            $.ajax({
                type: "POST",
                url: url,
                data: dataPost,
                success: function (response) {
                    detailAssetCapacityDatatable.clear().rows.add( response.data ).draw();
                    detailAssetCapacityModal.modal('show');
                }
            });
        },
        Datepicker: function (){
            $('.starperiod').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            }).on("hide", function() {
                // `e` here contains the extra attributes
                DashboardCodBulanan.Default_Value();
            });
        },
        Default_Value:function (){

            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbType = $(".CmbType").val();
            var cmbOwnership = $(".Ownership").val();
            var cmbDate = $(".cmbDate").val();
            var cmbPlan = $(".CmbPlan").val();
            var cmbActual= $(".CmbActual").val();

            var data = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbType: cmbType,
                cmbOwnership: cmbOwnership,
                cmbDate: cmbDate,
                cmbPlan: cmbPlan,
                cmbActual: cmbActual
            }
            $(".modal_loading").fadeIn(1000);
            var url = BASE_URL+'dashboard-cod/bulanan/getdata-cod-bulanan';
            $.ajax({
                type : "POST",
                url: url,
                data : data,
                success: function(response) {

                    //generate chart overall
                    DashboardCodBulanan.ChartBulananGeneration(JSON.parse(response.strGetGeneration_gen));
                    DashboardCodBulanan.ChartBulananTransmision(JSON.parse(response.strGetGeneration_tran));
                    DashboardCodBulanan.ChartBulananSubtation(JSON.parse(response.strGetSubtation_sub));
                    $(".modal_loading").fadeOut(1000);

                }
            });
        },

        onChangeFilterAll: function (){

            $( ".OnFilterAll" ).change(function() {


                var cmbRegion = $(".CmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();
                var cmbType = $(".CmbType").val();
                var cmbOwnership = $(".Ownership").val();
                var cmbDate = $(".cmbDate").val();
                var cmbPlan = $(".CmbPlan").val();
                var cmbActual= $(".CmbActual").val();

                var data = {
                    cmbRegion : cmbRegion,
                    cmbProgram : cmbProgram,
                    cmbType: cmbType,
                    cmbOwnership: cmbOwnership,
                    cmbDate: cmbDate,
                    cmbPlan: cmbPlan,
                    cmbActual: cmbActual

                }


                var url = BASE_URL+'dashboard-cod/bulanan/getdata-cod-bulanan';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data,
                    beforeSend: function () {
                        $(".modal_loading").fadeIn(1000);
                        $(".datepicker").fadeOut();
                    },
                    success: function(response) {
                        //generate chart overall
                        DashboardCodBulanan.ChartBulananGeneration(JSON.parse(response.strGetGeneration_gen));
                        DashboardCodBulanan.ChartBulananTransmision(JSON.parse(response.strGetGeneration_tran));
                        DashboardCodBulanan.ChartBulananSubtation(JSON.parse(response.strGetSubtation_sub));
                        $(".modal_loading").fadeOut(1000);

                    }
                });

            });

        },
        ChartBulananGeneration: function (dataBulanan) {
            //Script umtuk menghapus chart existing
            try {
                revenueChart.dispose();
                console.log('---> dataBulanan', dataBulanan);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChart = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivChartCODBulananGeneration",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":dataBulanan,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " MW";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        },
                        "dataPlotClick" : DashboardCodBulanan.dataPlotClick
                    }
                });
                revenueChart.render();
            })
        },
        ChartBulananTransmision: function (dataTransmision) {
            //Script umtuk menghapus chart existing
            try {
                revenueChartTransmision.dispose();
                console.log('---> dataBulanan', dataTransmision);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChartTransmision = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivTransmision",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":dataTransmision,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " KMS";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        },
                        "dataPlotClick" : DashboardCodBulanan.dataPlotClick
                    }
                });
                revenueChartTransmision.render();
            })
        },
        ChartBulananSubtation: function (dataBulanan) {
            //Script umtuk menghapus chart existing
            try {
                revenueChartSubtation.dispose();
                console.log('---> dataBulanan', dataBulanan);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChartSubtation = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivSubtation",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":dataBulanan,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " MVA";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        },
                        "dataPlotClick" : DashboardCodBulanan.dataPlotClick
                    }
                });
                revenueChartSubtation.render();
            })
        },
    }

}();

// Call init
DashboardCodBulanan.init();