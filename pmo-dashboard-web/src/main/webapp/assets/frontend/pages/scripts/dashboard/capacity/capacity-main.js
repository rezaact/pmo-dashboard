var CapacityMain = function () {

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            /*Kapasitas sampai dengan*/
            $('#DateNowStart').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });

            /*Kapasitas berjalan berdasarkan*/
            $('#DateRunningPeriodeMonth').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });
            $('#DateRunningPeriodeYear').datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years"
            });


            /*Kapasitas periode tertentu*/
            $('#DateStartPeriode').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            }).on('changeDate', function(selected){
                startDate =  $("#DateStartPeriode").val();
                $('#DateEndPeriode').datepicker('setStartDate', startDate);
            });
            $('#DateEndPeriode').datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });


            //$('#DateRunningPeriodeMonth').show();
            //$('.DateRunningPeriodeMonth_img').show();

            //$(".KapasitasBD").prop('disabled', true);
            //$(".KapasitasPT").prop('disabled', true);
            CapacityMain.initSelector();
            CapacityMain.registerEvent();

            CapacityMain.initDatePickerValue();
            CapacityMain.initCookieParams();
            CapacityMain.hiddenElement();
            CapacityMain.disableEnable();
            $('#Btn_FilterAll').trigger("click");
        },
        initSelector : function(){
            //Load pertama
            if($("input[name='Kapasitas']:checked").length == 0){
                $("input[name='Kapasitas'][value='3']").prop('checked', true);
            }
        },
        initDatePickerValue : function(){
            var today = new Date();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
            var monthYear = ConvertMonth.halfNumToHalfStr(mm)+' '+yyyy;
            //type 1
            var datePickerType1 = $('#DateNowStart');
            if(datePickerType1.val()==''){
                //init date
                datePickerType1.val(monthYear);
            }
            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            if(datePickerType2Month.val()==''){
                //init date
                datePickerType2Month.val(monthYear);
            }
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            if(datePickerType2Year.val()==''){
                //init date
                datePickerType2Year.val(yyyy.toString());
            }
            //type 3
            var firstMonthInYear = 'Jan '+yyyy.toString();
            var lastMonthInYear = 'Dec '+yyyy.toString();
            var datePickerType3Start = $('#DateStartPeriode');
            if(datePickerType3Start.val()==''){
                //init date
                datePickerType3Start.val(firstMonthInYear);
            }
            var datePickerType3End = $('#DateEndPeriode');
            if(datePickerType3End.val()==''){
                //init date
                datePickerType3End.val(lastMonthInYear);
            }
        },
        showHideProgamSebelumnya : function(type){
            rowProgramSebelumnya = $('.row-program-sebelumnya');
            if(type == 3){
                rowProgramSebelumnya.show();
            }
            else{
                rowProgramSebelumnya.hide();
            }
        },
        getParams : function(){
            var P_Jenis = $("input[name='Kapasitas']:checked").val();
            var CmbFilterIsActual = $("#CmbFilterIsActual").val();
            if(P_Jenis == 3){
                var cmbJenis = P_Jenis;
                var cmbSubJenis = 0;
                var cmbThnAwal = ConvertMonth.MyyyyToyyyymm($("#DateNowStart").val());
                var cmbThnAkhir = 0;

            }else if(P_Jenis == 2){

                var BulanORTahun = $("#CmbMonths").val();

                if (BulanORTahun == 1){//Bulan
                    var ThnAwal_conver = ConvertMonth.MyyyyToyyyymm($("#DateRunningPeriodeMonth").val());
                    var cmbSubJenis = BulanORTahun;
                }else{
                    var ThnAwal_conver = $("#DateRunningPeriodeYear").val();
                    var cmbSubJenis = BulanORTahun;
                }
                var cmbJenis = P_Jenis;
                var cmbThnAwal = ThnAwal_conver;
                var cmbThnAkhir = 0;

            }else{
                var cmbJenis = P_Jenis;
                var cmbSubJenis = 0;
                var cmbThnAwal = ConvertMonth.MyyyyToyyyymm($("#DateStartPeriode").val());
                var cmbThnAkhir = ConvertMonth.MyyyyToyyyymm($("#DateEndPeriode").val());
            }

            var data = {
                cmbJenis : cmbJenis,
                cmbSubJenis : cmbSubJenis,
                cmbThnAwal: cmbThnAwal,
                cmbThnAkhir: cmbThnAkhir,
                CmbFilterIsActual: CmbFilterIsActual
            }


            return data;
        },
        registerEvent : function(){
            /*------------------------
             @type or @detailType
             1=Detail by Region
             2=Detail by Asset
             3=Detail by Phase

             @group
             19=7GW
             22=35GW
             ------------------------ */


            //form submit
            var CmbFilterIsActual = $("#CmbFilterIsActual").val();

            //Load data untuk tabs Transmision & SubTation
            $('.load-data-tabs').click(function(){
                //
            });

            // Filter
            $('.btn-submit').on('click', function(event){



                var group = null;
                var detailType = null;

                var type = $(this).attr('value');
                var group = $(this).attr('data-column');
                //if((type == 1)||(type == 2)){
                //    group = 19;
                //}
                //else{
                //    group = 22;
                //}

                //if((type == 1)||(type == 2)){
                //    detailType = 1;
                //}
                //else{
                //    detailType = 2;
                //}

                var params = CapacityMain.getParams();
                params.group = group;
                params.detailType = type;

                /*
                 Update kebutuhan tabs : by den
                 @params.AssetTypeGeneration = "17";
                 @params.AssetTypeTransmission = "18";
                 @params.AssetTypeSubStation = "19";
                 */
                //params.CmbFilterIsActual = CmbFilterIsActual;


                //Generation
                $('#form-main-Generation').submit(function(){
                    setCookie('AssetType', 17, 5);
                    $.each(params, function(i,param){
                        $('<input />').attr('type', 'hidden')
                            .attr('name', i)
                            .attr('value', param)
                            .appendTo('#form-main-Generation');
                    });
                    $("<input type='hidden' name='AssetType' value='17' />").appendTo('#form-main-Generation');
                    return true;
                });

                //Transmission
                $('#form-main-Transmission').submit(function(){
                    setCookie('AssetType', 18, 5);
                    $.each(params, function(i,param){
                        $('<input />').attr('type', 'hidden')
                            .attr('name', i)
                            .attr('value', param)
                            .appendTo('#form-main-Transmission');
                    });
                    $("<input type='hidden' name='AssetType' value='18' />").appendTo('#form-main-Transmission');
                    return true;
                });

                //SubStation
                $('#form-main-SubStation').submit(function(){
                    setCookie('AssetType', 19, 5);
                    $.each(params, function(i,param){
                        $('<input />').attr('type', 'hidden')
                            .attr('name', i)
                            .attr('value', param)
                            .appendTo('#form-main-SubStation');
                    });
                    $("<input type='hidden' name='AssetType' value='19' />").appendTo('#form-main-SubStation');
                    return true;
                });

            });

            //Load awal hidden "Kapasitas berjalan berdasarkan"
            $('#CmbMonths').change(function() {
                CapacityMain.hiddenElement();
            });


            //Button Filter ALL
            $('#Btn_FilterAll').click(function() {

                function addNol(data){
                    var firstChar = data.toString().substring(0,1);
                    var newData = (firstChar == '.')?'0'+data:data;
                    return newData;
                }

                if ($("input[name='Kapasitas']:checked").val()) {

                    $(".modal_loading").fadeIn(10);
                    var data = CapacityMain.getParams();

                    var url = BASE_URL+'dashboard-cod-capacity/get-data-capacity';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: data,
                        success: function (response) {
                            $(".modal_loading").fadeOut(10);

                            //change total header
                            var totalHeader = "TOTAL ";
                            totalHeader = totalHeader + ConvertMonth.yyyymmToHalfStr(data.cmbThnAwal);

                            if(data.cmbThnAkhir != '0'){
                                totalHeader = totalHeader +' - '+ ConvertMonth.yyyymmToHalfStr(data.cmbThnAkhir);
                            }

                            $(".total-header").html(totalHeader);

                            //GENERATION
                            var program7 = $("#program-7");
                            var program35 = $("#program-35");
                            var percenProgram7 = $("#percen-program-7");
                            var percenProgram35 = $("#percen-program-35");
                            var total = $("#span_total");
                            total.html(response.listGenCapcity[0]['TOTAL'] || 'N/A');
                            total.html( addNol(total.html().trim()));


                            program7.html(response.listGenCapcity[0]['TOTAL7GW'] || 'N/A');
                            percenProgram7.html(response.listGenCapcity[0]['TOTALBOBOT7GW'] || 'N/A');
                            program35.html(response.listGenCapcity[0]['TOTAL35GW'] || 'N/A');
                            percenProgram35.html(response.listGenCapcity[0]['TOTALBOBOT35GW'] || 'N/A');

                            program7.html(program7.html().trim());
                            program7.html((program7.html()!='.00')? addNol(program7.html()) :'N/A');
                            var satuanPercen = ' %';
                            percenProgram7.html(percenProgram7.html().trim());
                            percenProgram7.html((percenProgram7.html()!='.00')? addNol(percenProgram7.html())+satuanPercen:'N/A');

                            program35.html((program35.html().trim()!='.00')?program35.html():'N/A');
                            percenProgram35.html(percenProgram35.html().trim());
                            percenProgram35.html((percenProgram35.html()!='.00')? addNol(percenProgram35.html())+satuanPercen:'N/A');
                            $('.btn-program-7-generation').attr('disabled', (program7.html() == 'N/A')?true:false);
                            $('.btn-program-35-generation').attr('disabled', (program35.html() == 'N/A')?true:false);

                            //TRANSMISION
                            var transmission = $("#transmission");
                            var program7 = transmission.find(".program-7");
                            var program35 = transmission.find(".program-35");
                            var percenProgram7 = transmission.find(".percen-program-7");
                            var percenProgram35 = transmission.find(".percen-program-35");
                            var total = transmission.find(".span_total");
                            total.html(response.listGenCapcity[0]['TOTAL_TRANSMISI'] || 'N/A');
                            total.html( addNol(total.html().trim()));

                            program7.html(response.listGenCapcity[0]['TOTAL7GW_TRANSMISI'] || 'N/A');
                            percenProgram7.html(response.listGenCapcity[0]['TOTALBOBOT7GW_TRANSMISI'] || 'N/A');
                            program35.html(response.listGenCapcity[0]['TOTAL35GW_TRANSMISI'] || 'N/A');
                            percenProgram35.html(response.listGenCapcity[0]['TOTALBOBOT35GW_TRANSMISI'] || 'N/A');

                            program7.html((program7.html().trim()!='.00')?program7.html():'N/A');
                            var satuanPercen = ' %';
                            percenProgram7.html(percenProgram7.html().trim());
                            percenProgram7.html((percenProgram7.html()!='.00')? addNol(percenProgram7.html())+satuanPercen:'N/A');
                            program35.html((program35.html().trim()!='.00')?program35.html():'N/A');
                            percenProgram35.html(percenProgram35.html().trim());
                            percenProgram35.html((percenProgram35.html()!='.00')? addNol(percenProgram35.html())+satuanPercen:'N/A');
                            $('.btn-program-7-transmission').attr('disabled', (program7.html() == 'N/A')?true:false);
                            $('.btn-program-35-transmission').attr('disabled', (program35.html() == 'N/A')?true:false);

                            //GI
                            var subStation = $("#sub-station");
                            var program7 = subStation.find(".program-7");
                            var program35 = subStation.find(".program-35");
                            var percenProgram7 = subStation.find(".percen-program-7");
                            var percenProgram35 = subStation.find(".percen-program-35");
                            var total = subStation.find(".span_total");
                            total.html(response.listGenCapcity[0]['TOTAL_GI'] || 'N/A');
                            total.html( addNol(total.html().trim()));

                            program7.html(response.listGenCapcity[0]['TOTAL7GW_GI'] || 'N/A');
                            percenProgram7.html(response.listGenCapcity[0]['TOTALBOBOT7GW_GI'] || 'N/A');
                            program35.html(response.listGenCapcity[0]['TOTAL35GW_GI'] || 'N/A');
                            percenProgram35.html(response.listGenCapcity[0]['TOTALBOBOT35GW_GI'] || 'N/A');

                            program7.html((program7.html().trim()!='.00')?program7.html():'N/A');
                            var satuanPercen = ' %';
                            percenProgram7.html(percenProgram7.html().trim());
                            percenProgram7.html((percenProgram7.html()!='.00')? addNol(percenProgram7.html())+satuanPercen:'N/A');
                            program35.html((program35.html().trim()!='.00')?program35.html():'N/A');
                            percenProgram35.html(percenProgram35.html().trim());
                            percenProgram35.html((percenProgram35.html()!='.00')? addNol(percenProgram35.html())+satuanPercen:'N/A');
                            $('.btn-program-7-gi').attr('disabled', (program7.html() == 'N/A')?true:false);
                            $('.btn-program-35-gi').attr('disabled', (program35.html() == 'N/A')?true:false);

                            $("#program-sebelumnya").html(response.listGenCapcity[0]['TOTAL_INSTAL'] || 'N/A');
                            //add disable


                            CapacityMain.showHideProgamSebelumnya(data.cmbJenis);
                            CapacityMain.setCookieParams();

                        }
                    });

                    return false;
                }
                else {
                    alert('Pilih salah satu Filter');
                }

            });

            //Pilih Salah satu Kapasitas
            $('.panel-body input').on('change', function(e) {
                CapacityMain.disableEnable();
            });


        },
        disableEnable : function () {
            $('input[name=Kapasitas]:checked', '.panel-body').val();
            var progres = $(".progres");
            var valKapasitas = $("input[name='Kapasitas']:checked").val();

            if (valKapasitas == 1){
                $(".KapasitasSD").prop('disabled', true);
                $(".KapasitasBD").prop('disabled', true);
                $(".KapasitasPT").prop('disabled', false);
                progres.css('opacity','0');

                //Update Req by deni - 23-01-2017:
                $(".Row_Kapasitas_Label").hide();
                $(".progres").hide();
                $(".btn-detail-by-phase").hide();


            }else if(valKapasitas == 2) {
                $(".KapasitasSD").prop('disabled', true);
                $(".KapasitasBD").prop('disabled', false);
                $(".KapasitasPT").prop('disabled', true);
                progres.css('opacity','0');

                //Update Req by deni - 23-01-2017:
                $(".Row_Kapasitas_Label").hide();
                $(".progres").hide();
                $(".btn-detail-by-phase").hide();


            }else{
                $(".KapasitasSD").prop('disabled', false);
                $(".KapasitasBD").prop('disabled', true);
                $(".KapasitasPT").prop('disabled', true);
                progres.css('opacity','1');

                //Update Req by deni - 23-01-2017:
                $(".Row_Kapasitas_Label").show();
                $(".progres").show();
                $(".btn-detail-by-phase").show();

            }
        },
        hiddenElement : function(){
            var CmbMonths = $("#CmbMonths").val();
            if (CmbMonths == 1){
                $('#DateRunningPeriodeMonth').show();
                $('.DateRunningPeriodeMonth_img').show();

                $('#DateRunningPeriodeYear').hide();
                $('.DateRunningPeriodeYear_img').hide();

            }else{
                $('#DateRunningPeriodeMonth').hide();
                $('.DateRunningPeriodeMonth_img').hide();

                $('#DateRunningPeriodeYear').show();
                $('.DateRunningPeriodeYear_img').show();

            }
        },
        initCookieParams : function(){

            var jenisCookie = getCookie('jenis');

            if(jenisCookie!=''){
                var jenis = $("input[name='Kapasitas'][value='"+jenisCookie+"']");
                jenis.prop( "checked", true );
            }

            //type 1
            var datePickerType1 = $('#DateNowStart');
            var datePickerType1Cookie = getCookie('datePickerType1');
            if(datePickerType1Cookie!=''){
                //init date
                datePickerType1.val(datePickerType1Cookie);
            }

            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            var datePickerType2MonthCookie = getCookie('datePickerType2Month');
            if(datePickerType2MonthCookie!=''){
                //init date
                datePickerType2Month.val(datePickerType2MonthCookie);
            }
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            var datePickerType2YearCookie = getCookie('datePickerType2Year');
            if(datePickerType2YearCookie!=''){
                //init date
                datePickerType2Year.val(datePickerType2YearCookie);
            }

            var comboSubtype = $('#CmbMonths');
            var comboSubtypeCookie = getCookie('comboSubtype');
            if(comboSubtypeCookie!=''){
                comboSubtype.val(comboSubtypeCookie);
            }
            //type 3
            var datePickerType3Start = $('#DateStartPeriode');
            var datePickerType3StartCookie = getCookie('datePickerType3Start');
            if(datePickerType3StartCookie!=''){
                //init date
                datePickerType3Start.val(datePickerType3StartCookie);
            }
            var datePickerType3End = $('#DateEndPeriode');
            var datePickerType3EndCookie = getCookie('datePickerType3End');
            if(datePickerType3EndCookie!=''){
                //init date
                datePickerType3End.val(datePickerType3EndCookie);
            }


            //tab
            var assetType = getCookie('AssetType');
            if(typeof assetType != 'undefined'){

                switch (assetType){

                    case '17':
                        $('#generation').addClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').addClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                        break;
                    case '18':
                        $('#generation').removeClass('active');
                        $('#transmission').addClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').removeClass('active');
                        $('#transmission-tab-header').addClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                        break;
                    case '19':
                        $('#generation').removeClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').addClass('active');
                        $('#generation-tab-header').removeClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').addClass('active');
                        break;
                    default :
                        $('#generation').addClass('active');
                        $('#transmission').removeClass('active');
                        $('#sub-station').removeClass('active');
                        $('#generation-tab-header').addClass('active');
                        $('#transmission-tab-header').removeClass('active');
                        $('#sub-station-tab-header').removeClass('active');
                }
            }
            else{
                $('.tab-content #generation').addClass('active');
                $('.tab-content #transmission').removeClass('active');
                $('.tab-content #sub-station').removeClass('active');
            }

        },
        setCookieParams : function () {
            var jenis = $("input[name='Kapasitas']:checked");

            //type 1
            var datePickerType1 = $('#DateNowStart');
            //type 2
            var datePickerType2Month = $('#DateRunningPeriodeMonth');
            var datePickerType2Year = $('#DateRunningPeriodeYear');
            var comboSubtype = $('#CmbMonths');
            //type 3
            var datePickerType3Start = $('#DateStartPeriode');
            var datePickerType3End = $('#DateEndPeriode');


            var time = 5;//5 menit
            setCookie('jenis', jenis.val(), time);
            setCookie('datePickerType1', datePickerType1.val(), time);
            setCookie('datePickerType2Month', datePickerType2Month.val(), time);
            setCookie('datePickerType2Year', datePickerType2Year.val(), time);
            setCookie('datePickerType3Start', datePickerType3Start.val(), time);
            setCookie('datePickerType3End', datePickerType3End.val(), time);
            setCookie('comboSubtype', comboSubtype.val(), time);

        }

    }

}();

// Call init
CapacityMain.init();

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}