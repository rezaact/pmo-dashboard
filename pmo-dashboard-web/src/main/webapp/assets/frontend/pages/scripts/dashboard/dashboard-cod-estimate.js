

var DashboardCodEstimate = function () {
    var generationChart = null; //Generation
    var dataGenerationChart = null;
    var dataOld = null;

    var revenueChartTransmision = null; //Transmision
    var revenueChartSubtation = null; //Subtation
    var codModal = null;
    var detailAssetCapacityModal = null;
    var detailAssetCapacityDatatable = null;
    var detailAssetCapacityDatatableActual = null;
    var tableTooltip = null;

    var MIN_Y_AXIS = "37000";

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            DashboardCodEstimate.initChart();
            DashboardCodEstimate.onChangeFilterAll();
            DashboardCodEstimate.Datepicker();
            DashboardCodEstimate.Default_Value();

            DashboardCodEstimate.detailCapacity();
            codModal = $("#detail-cod-modal");


        },
        initChart : function(){
            FusionCharts.ready(function(){
                generationChart = new FusionCharts({
                    "type": "stackedcolumn2dline",
                    "renderAt": "DivChartCODBulananGeneration",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource": dataGenerationChart,
                });

                generationChart.addEventListener("dataLabelClick", function (eventObj, dataObj) {
                    codModal.find("#myModalLabel").text(dataObj.text);
                    var satuan = " MW";
                    var planPrev = dataGenerationChart.dataset[3].data[dataObj.dataIndex].value;
                    codModal.find("#plan-prev").text(planPrev + satuan);
                    var planCur = dataGenerationChart.dataset[4].data[dataObj.dataIndex].value;
                    codModal.find("#plan-cur").text(planCur+ satuan);
                    codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                    var actPrev = dataGenerationChart.dataset[1].data[dataObj.dataIndex].value;
                    codModal.find("#act-prev").text(actPrev+ satuan);
                    var actCur = dataGenerationChart.dataset[2].data[dataObj.dataIndex].value;
                    codModal.find("#act-cur").text(actCur+ satuan);
                    codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                    codModal.modal("show");
                });
                generationChart.addEventListener("dataPlotClick", DashboardCodEstimate.dataPlotClick);
                generationChart.addEventListener("legendItemClicked", function (eventObj, dataObj) {
                    //first data
                    dataOld = generationChart.getChartData('json');

                    var dataGenerationChartLimitY = $.extend(true, {} ,dataGenerationChart);
                    var dataGenerationChartHiddenLegend = $.extend(true, {} ,dataGenerationChart);
                    //no replace data need to handle ymin but current data < , ymax but current data >
                    //type 1 : all legend,  y_min 37000, no replace
                    var dataGenerationType1 = $.extend(true, {} ,dataOld);
                    dataGenerationType1.chart.yAxisMinValue = MIN_Y_AXIS;
                    dataGenerationType1.dataset[0].initiallyHidden = "0";
                    dataGenerationType1.dataset[5].initiallyHidden = "0";

                    //type 2 : minus kapasitas awal legend. y_min 0, no replace
                    var dataGenerationType2 = $.extend(true, {} ,dataOld);
                    dataGenerationType2.dataset[0].initiallyHidden = "1";
                    dataGenerationType2.dataset[5].initiallyHidden = "0";
                    for(var i=0;i<dataGenerationType2.dataset[5].data.length;i++){
                        dataGenerationType2.dataset[0].data[i].value = dataGenerationChart.dataset[0].data[i].value;
                        dataGenerationType2.dataset[5].data[i].value = dataGenerationChart.dataset[5].data[i].value;
                    }
                    dataGenerationType2.chart.yAxisMinValue = "0";

                    //type 3: no kapasitas awal & demand
                    var dataGenerationType3 = $.extend(true, {} ,dataOld);
                    dataGenerationType3.dataset[0].initiallyHidden = "1";
                    dataGenerationType3.dataset[5].initiallyHidden = "1";
                    for(var i=0;i<dataGenerationType3.dataset[5].data.length;i++){
                        dataGenerationType3.dataset[0].data[i].value = 0;
                        dataGenerationType3.dataset[5].data[i].value = 0;
                    }
                    dataGenerationType3.chart.yAxisMinValue = "0";
                    dataGenerationType3.chart.yAxisMaxValue = "10000";

                    //type 4 : no demand only
                    var dataGenerationType4 = $.extend(true, {} ,dataOld);
                    dataGenerationType4.dataset[5].initiallyHidden = "1";
                    dataGenerationType4.dataset[0].initiallyHidden = "0";
                    for(var i=0;i<dataGenerationType4.dataset[0].data.length;i++){
                        dataGenerationType4.dataset[5].data[i].value = dataGenerationChart.dataset[5].data[i].value;
                        dataGenerationType4.dataset[0].data[i].value = dataGenerationChart.dataset[0].data[i].value;
                    }
                    dataGenerationType4.chart.yAxisMinValue = MIN_Y_AXIS;





                    if(dataObj.datasetIndex == 0){

                        if((dataObj.visible == true)&&((dataOld.dataset[5].initiallyhidden == '1'))){
                            generationChart.setChartData(dataGenerationType3, "json");
                        }
                        else if((dataObj.visible == true)&&((dataOld.dataset[5].initiallyhidden == '0')||(dataOld.dataset[5].initiallyhidden == undefined))){
                            generationChart.setChartData(dataGenerationType2, 'json');
                        }
                        else if((dataObj.visible == false)&&((dataOld.dataset[5].initiallyhidden == '0')||(dataOld.dataset[5].initiallyhidden == undefined))){
                            //dataGenerationChartLimitY.chart.yAxisMinValue = "37000";
                            //generationChart.setChartData(dataGenerationChartLimitY, "json");
                            generationChart.setChartData(dataGenerationType1, 'json');
                        }
                        else if((dataObj.visible == false)&&(dataOld.dataset[5].initiallyhidden == '1')){
                            generationChart.setChartData(dataGenerationType4, 'json');
                        }
                    }
                    else if(dataObj.datasetIndex == 1){
                        if(dataObj.visible == true){
                            dataOld.dataset[1].initiallyHidden = "1";
                        }
                        else{
                            dataOld.dataset[1].initiallyHidden = "0";
                        }
                    }
                    else if(dataObj.datasetIndex == 2){
                        if(dataObj.visible == true){
                            dataOld.dataset[2].initiallyHidden = "1";
                        }
                        else{
                            dataOld.dataset[2].initiallyHidden = "0";
                        }
                    }
                    else if(dataObj.datasetIndex == 3){
                        if(dataObj.visible == true){
                            dataOld.dataset[3].initiallyHidden = "1";
                        }
                        else{
                            dataOld.dataset[3].initiallyHidden = "0";
                        }
                    }
                    else if(dataObj.datasetIndex == 4){
                        if(dataObj.visible == true){
                            dataOld.dataset[4].initiallyHidden = "1";
                        }
                        else{
                            dataOld.dataset[4].initiallyHidden = "0";
                        }
                    }
                    else
                    if(dataObj.datasetIndex == 5){

                        //resize jika kapasitas awal di hilangkan
                        if((dataOld.dataset[0].initiallyhidden == '1')&&(dataObj.visible == true)){
                            generationChart.setChartData(dataGenerationType3, 'json');
                        }
                        //munculin lagi
                        else if((dataOld.dataset[0].initiallyhidden == '1')&&(dataObj.visible == false)){
                            generationChart.setChartData(dataGenerationType2, "json");
                        }
                        else if(((dataOld.dataset[0].initiallyhidden == '0')||(dataOld.dataset[0].initiallyhidden == undefined))&&(dataObj.visible == true)){
                            generationChart.setChartData(dataGenerationType4, "json");
                        }
                        else if(((dataOld.dataset[0].initiallyhidden == '0')||(dataOld.dataset[0].initiallyhidden == undefined))&&(dataObj.visible == false)){
                            generationChart.setChartData(dataGenerationType1, 'json');
                        }

                    }
                });

                generationChart.addEventListener("dataPlotRollOver", function(eventObj, dataObj){
                    var satuan = " MW";
                    tableTooltip = $(".table-tooltip");


                    var kapasitasAwal = dataGenerationChart.dataset[0].data[dataObj.dataIndex].value;
                    tableTooltip.find(".kapasitas-awal").text(kapasitasAwal+satuan);

                    var cumulativeActualCapacity = dataGenerationChart.dataset[1].data[dataObj.dataIndex].value;
                    tableTooltip.find(".cumulative-actual-capacity").text(cumulativeActualCapacity+satuan);

                    var actualCapacity = dataGenerationChart.dataset[2].data[dataObj.dataIndex].value;
                    tableTooltip.find(".actual-capacity").text(actualCapacity+satuan);

                    var cumulativePlanCapacity = dataGenerationChart.dataset[3].data[dataObj.dataIndex].value;
                    tableTooltip.find(".cumulative-plan-capacity").text(cumulativePlanCapacity+satuan);

                    var planCapacity = dataGenerationChart.dataset[4].data[dataObj.dataIndex].value;
                    tableTooltip.find(".plan-capacity").text(planCapacity+satuan);

                    var demand = dataGenerationChart.dataset[5].data[dataObj.dataIndex].value;
                    tableTooltip.find(".demand").text(demand+satuan);

                });

                generationChart.render();
            });
        },
        detailCapacity: function(){
            function renderBulan(data, type, row){
                return ConvertMonth.yyyymmToStr(row.THBL);
            }

            detailAssetCapacityModal = $("#detail-asset-capacity");
            detailAssetCapacityDatatable = $('#table-detail-asset-capacity').DataTable( {
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "THBL", "defaultContent": "-", "render" : renderBulan},
                    { data: "ASSETNUMBER", "defaultContent": "-"},
                    { data: "ASSETNAME", "defaultContent": "-"},
                    { data: "UNITCAPACITY", "defaultContent": "-"},
                    { data: "ESTIMATECOD", "defaultContent": "-"},
                    { data: "PROPINSI", "defaultContent": "-"}
                ],
            });
            detailAssetCapacityDatatableActual = $('#table-detail-asset-capacity-actual').DataTable( {
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 5,
                "bAutoWidth": true,
                columns: [
                    { data: "THBL", "defaultContent": "-", "render" : renderBulan},
                    { data: "ASSETNUMBER", "defaultContent": "-"},
                    { data: "ASSETNAME", "defaultContent": "-"},
                    { data: "UNITCAPACITY", "defaultContent": "-"},
                    { data: "ACTUALCOD", "defaultContent": "-"},
                    { data: "PROPINSI", "defaultContent": "-"}
                ],
            });
        },
        dataPlotClick : function(eventObj, dataObj){
            $(".modal_loading").fadeIn(1000);

            var categoryLabel = dataObj.categoryLabel;
            var datasetName = dataObj.datasetName;

            detailAssetCapacityModal.find("#judul").text(categoryLabel);

            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();
            var cmbType = $(".CmbType").val();
            var cmbOwnership = $(".Ownership").val();
            var cmbDate = $(".cmbDate").val();
            var category = null;
            var cmbKapasitasAwal = $(".cmbKapasitasAwal").val();
            var startPeriod = $(".starperiod").val();
            var endperiod = $(".endperiod").val();
            var type = 17;
            var group = 0;

            category = categoryLabel.substring(4,8)+ConvertMonth.HalfStrToNum(categoryLabel.substring(0,3));

            var dataPost = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbKapasitasAwal : cmbKapasitasAwal,
                cmbOwnership: cmbOwnership,
                cmbDate: cmbDate,
                categoryLabel : category,
                datasetName : datasetName,
                startPeriod : startPeriod,
                endPeriod : endperiod,
                type : type,
                group : group
            }

            //fetch data

            var url = BASE_URL+'dashboard-cod/get-data-detail-cod-v2';
            ////getdata-chart-milestone
            $.ajax({
                type: "POST",
                url: url,
                data: dataPost,
                success: function (response) {
                    var dataPlan = response.data.filter(function(item){
                        return item.ISACTUAL == 0;
                    });
                    var dataActual = response.data.filter(function(item){
                        return item.ISACTUAL == 1;
                    });

                    detailAssetCapacityDatatable.clear().rows.add( dataPlan ).draw();
                    detailAssetCapacityDatatableActual.clear().rows.add( dataActual ).draw();
                    $(".modal_loading").fadeOut(1000);
                    detailAssetCapacityModal.modal('show');
                }
            });
        },
        Datepicker: function (){
            //Link : Plugin
            // http://jsfiddle.net/hLe0yhz5/1/
            // https://bootstrap-datepicker.readthedocs.io/en/latest/
            $("#from").trigger( "change" );

            $('#from').datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years"
                }).on('changeDate', function(selected){
                startDate =  $("#from").val();
                $('#to').datepicker('setStartDate', startDate);
            });


            $('#to').datepicker({
                format: "yyyy",
                autoclose: true,
                minViewMode: "years"
                }).on("hide", function() {
                // `e` here contains the extra attributes

                var starDates = $("#from").val();
                var endDates = $(this).val();
                if (starDates > endDates ){
                    $('#to').css('border-color', 'red');
                }else{
                    $('#to').css('border-color', '');
                    DashboardCodEstimate.Default_Value();
                }

            });

/*
            $('.starperiod').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            }).on("hide", function() {
                // `e` here contains the extra attributes
                //DashboardCodEstimate.Default_Value();
            });
            $('.endperiod').datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            }).on("hide", function() {
                // `e` here contains the extra attributes
                //DashboardCodEstimate.Default_Value();
            });*/
        },
        Default_Value:function (){

            var cmbRegion = $(".CmbRegion").val();
            var cmbProgram = $(".CmbProgram").val();//Group
            var cmbType = $(".CmbType").val();
            var cmbOwnership = $(".Ownership").val();
            var cmbStartDate = $(".starperiod").val();
            var cmbEndDate = $(".endperiod").val();
            var cmbKapasitas= $(".cmbKapasitasAwal").val();

            var data = {
                cmbRegion : cmbRegion,
                cmbProgram : cmbProgram,
                cmbType: cmbType,
                cmbOwnership: cmbOwnership,
                cmbStartDate: cmbStartDate,
                cmbEndDate: cmbEndDate,
                cmbKapasitas: cmbKapasitas
            }
            $(".modal_loading").fadeIn(1000);
            var url = BASE_URL+'dashboard-cod/getdata-cod-estimate';
            //var url = '';
            $.ajax({
                type : "POST",
                url: url,
                data : data,
                success: function(response) {

                    //generate chart overall
                    DashboardCodEstimate.ChartBulananGeneration(JSON.parse(response.strGetGeneration_gen));

                    //DashboardCodEstimate.ChartBulananTransmision(JSON.parse(response.strGetGeneration_tran));
                    //DashboardCodEstimate.ChartBulananSubtation(JSON.parse(response.strGetSubtation_sub));
                    $(".modal_loading").fadeOut(1000);

                }
            });
        },

        onChangeFilterAll: function (){
            $( ".OnFilterAll" ).change(function() {

                var cmbRegion = $(".CmbRegion").val();
                var cmbProgram = $(".CmbProgram").val();//Group
                var cmbType = $(".CmbType").val();
                var cmbOwnership = $(".Ownership").val();
                var cmbStartDate = $(".starperiod").val();
                var cmbEndDate = $(".endperiod").val();
                var cmbKapasitas= $(".cmbKapasitasAwal").val();

                var data = {
                    cmbRegion : cmbRegion,
                    cmbProgram : cmbProgram,
                    cmbType: cmbType,
                    cmbOwnership: cmbOwnership,
                    cmbStartDate: cmbStartDate,
                    cmbEndDate: cmbEndDate,
                    cmbKapasitas: cmbKapasitas
                }


                var url = BASE_URL+'dashboard-cod/getdata-cod-estimate';
                $.ajax({
                    type : "POST",
                    url: url,
                    data : data,
                    beforeSend: function () {
                        $(".modal_loading").fadeIn(1000);
                        $(".datepicker").fadeOut();
                    },
                    success: function(response) {
                        //generate chart overall
                        DashboardCodEstimate.ChartBulananGeneration(JSON.parse(response.strGetGeneration_gen));
                        //DashboardCodEstimate.ChartBulananTransmision(JSON.parse(response.strGetGeneration_tran));
                        //DashboardCodEstimate.ChartBulananSubtation(JSON.parse(response.strGetSubtation_sub));
                        $(".modal_loading").fadeOut(1000);

                    }
                });

            });

        },
        ChartBulananGeneration: function (dataBulanan) {

            dataBulanan.chart.plotToolText = "<div id='headerdiv'>$label</div>" +
                    "<div>" +
                    "<table class='table table-striped table-tooltip'>" +
                    "<tr>" +
                    "<td>Demand</td>" +
                    "<td class='demand'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Plan Capacity</td>" +
                    "<td class='plan-capacity'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td >Cumulative Plan Capacity</td>" +
                    "<td class='cumulative-plan-capacity'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td >Actual Capacity</td>" +
                    "<td class='actual-capacity'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td >Cumulative Actual Capacity</td>" +
                    "<td class='cumulative-actual-capacity'></td>" +
                    "</tr>" +
                    "<tr>" +
                    "<td>Kapasitas Awal</td>" +
                    "<td class='kapasitas-awal'></td>" +
                    "</tr>" +
                    "</table>" +
                    "</div>";

            dataGenerationChart = $.extend(true, {}, dataBulanan);
            var dataChart = $.extend(true, {}, dataBulanan);
            dataChart.chart.yAxisMinValue = MIN_Y_AXIS;
            generationChart.setChartData(dataChart, 'json');
        },
        ChartBulananTransmision: function (dataTransmision) {
            //Script umtuk menghapus chart existing
            try {
                revenueChartTransmision.dispose();
                console.log('---> dataBulanan', dataTransmision);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChartTransmision = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivTransmision",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":dataTransmision,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " KMS";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        },
                        "dataPlotClick" : DashboardCodEstimate.dataPlotClick
                    }
                });
                revenueChartTransmision.render();
            })
        },
        ChartBulananSubtation: function (dataBulanan) {

            //Script umtuk menghapus chart existing
            try {
                revenueChartSubtation.dispose();
                console.log('---> dataBulanan', dataBulanan);
            }catch(err) {
                console.error('error handler on chart dispose. dont worry be happy :)');
            }

            FusionCharts.ready(function(){
                revenueChartSubtation = new FusionCharts({
                    "type": "msstackedcolumn2dlinedy",
                    "renderAt": "DivSubtation",
                    "width": "100%",
                    "height": "350",
                    'bgAlpha': '40%',
                    "dataFormat": "json",
                    "dataSource":dataBulanan,
                    "events" : {
                        "dataLabelClick": function (eventObj, dataObj) {
                            codModal.find("#myModalLabel").text(dataObj.text);
                            dataModal = this.getJSONData();
                            var satuan = " MVA";
                            var planPrev = dataModal.dataset[0].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#plan-prev").text(planPrev + satuan);
                            var planCur = dataModal.dataset[0].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#plan-cur").text(planCur+ satuan);
                            codModal.find("#plan-total").text(parseInt(planPrev)+parseInt(planCur)+ satuan);

                            var actPrev = dataModal.dataset[1].dataset[0].data[dataObj.dataIndex].value;
                            codModal.find("#act-prev").text(actPrev+ satuan);
                            var actCur = dataModal.dataset[1].dataset[1].data[dataObj.dataIndex].value;
                            codModal.find("#act-cur").text(actCur+ satuan);
                            codModal.find("#act-total").text(parseInt(actPrev)+parseInt(actCur)+ satuan);
                            codModal.modal("show");
                        },
                        "dataPlotClick" : DashboardCodEstimate.dataPlotClick
                    }
                });
                revenueChartSubtation.render();
            })
        },
    }

}();

// Call init
DashboardCodEstimate.init();