var ProductDetail = function(){
    return{
        init : function () {
            ProductDetail.imageSpritespin();
            ProductDetail.imageZoom();
            ProductDetail.fancybox();
            ProductDetail.getReadmore();
        },

        imageSpritespin:function(){
            var url = BASE_URL + "product/get_image_sprint?prm_nomor_produk=" + NOMOR_PRODUK +
                "&prm_dok_type=" + DOC_TYPE +
                "&prm_urutan_file=";
            $(".spritespin").spritespin({
                source: SpriteSpin.sourceArray(url + "{frame}", { frame: [1,JML_FILE], digits: 2 }),
                width: 382,
                height: 300,
                sense: -1,
                mods: ["move", "zoom", "360"],
                behavior: null,
                module: null
            });
        },

        imageZoom: function () {
            $('.product-main-image-zoom').zoom({url: $('.product-main-image img').attr('data-BigImgSrc')});
        },
        fancybox: function(){
                $(".manual").click(function() {
                var img=$(this).attr('src');
//                console.log(BASE_URL+$(this).data('url'));
                $.fancybox({
                    //'orig'			: $(this),
                    'padding'		: 0,
                    'href'			: img+'&.jpg',
                    fitToView: true,
                    width: 452,
                    height: 252,
                    autoSize: true,
                    closeClick: false,
                    openEffect: 'none',
                    closeEffect: 'none'
                });
            });

        },
        getReadmore: function () {
            $('#readmore-'+NOMOR_PRODUK).on('shown.bs.modal', function (e) {
                $.ajax({
                    type: 'GET',
                    url: BASE_URL + 'product/product-readmore',
                    data : {
                        id : NOMOR_PRODUK
                    },
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#readmore-'+NOMOR_PRODUK).find('.modal-content'),
                            boxed: true
                        });
                        //return false;
                    },
                    success: function (response) {
                        Metronic.unblockUI($('#readmore-'+NOMOR_PRODUK).find('.modal-content'));
                        $('#readmore-'+NOMOR_PRODUK).find('.modal-body').html(response.DESKRIPSI);
                    }
                });
            });
        }

    }
}();

ProductDetail.init();

function paginationClickFirst(nomorProduk){
    console.log('test');
    var page =  1;
    $("#history_page").text(page);
    getPagingCustomHistoryProduct(page, nomorProduk);
};

function paginationClickPrev(nomorProduk){
    var currentPage = $("#history_page").text();
    var page = parseInt(currentPage) - 1;
    if(page<1)( page=1);
    console.log('page = ' + page);
    $("#history_page").text(page);

    getPagingCustomHistoryProduct(page, nomorProduk);
};

function paginationClickNext(nomorProduk){
    var currentPage = $("#history_page").text();
    var totalPage = parseInt($("#history_total_page").text());

    var page = parseInt(currentPage) + 1;
    if(page>totalPage)( page=totalPage);

    console.log('page = ' + page);
    $("#history_page").text(page);

    getPagingCustomHistoryProduct(page, nomorProduk);

};

function paginationClickLast(nomorProduk){
    var page =  parseInt($("#history_total_page").text());
    $("#history_page").text(page);
    getPagingCustomHistoryProduct(page, nomorProduk);
};


function getPagingCustomHistoryProduct(page, nomorProduk){
    $.ajax({
        type: 'POST',
        url: BASE_URL + 'history-produk/paging-custom',
        data : {
            page : page,
            nomor_produk : nomorProduk
        },
        beforeSend: function () {
            // loading pada product container
            /*Metronic.blockUI({
             target: $('#tab-historical-product table'),
             boxed: true
             });*/
        },
        success: function (response) {
            console.log(response);
            fillHistoryProduct(response);
            // menghilangkan loading jika data telah sukses
            //Metronic.unblockUI($('#tab-historical-product table'));
            //console.table(response);
            //$('#fillContainer').html(response);
        }
    });

};

function fillHistoryProduct(content){

    $('#tab-historical-product .NAMA_LOKASI').text(content.historyProduct.NAMA_LOKASI);
    $('#tab-historical-product .NOMOR_PENUGASAN').text(content.historyProduct.NOMOR_PENUGASAN);
    $('#tab-historical-product .PERIHAL').text(content.historyProduct.PERIHAL);
    $('#tab-historical-product .TANGGAL_PENUGASAN').text(content.historyProduct.TANGGAL_PENUGASAN);
    $('#tab-historical-product .JADWAL_PELAKSANAAN_PENUGASAN').text(content.historyProduct.JADWAL_PELAKSANAAN_PENUGASAN + ' Hari');
    $('#tab-historical-product .REALISASI_PELAKSANAAN_PENUGASA').text(content.historyProduct.REALISASI_PELAKSANAAN_PENUGASA + ' Hari');
    $('#tab-historical-product .PEMBERI_PENUGASAN').text(content.historyProduct.NAMA_PEMBERI_PENUGASAN);
    $('#tab-historical-product .JUMLAH_ITEM_PEKERJAAN').text(content.historyProduct.JUMLAH_ITEM_PEKERJAAN);
    $('#tab-historical-product .RAB_PRODUKSI').text('Rp. ' + content.historyProduct.RAB_PRODUKSI);
    $('#tab-historical-product .HARGA_POKOK_PRODUKSI').text('Rp. ' +content.historyProduct.HARGA_POKOK_PRODUKSI);
    $('#tab-historical-product .REALISASI_BIAYA_PRODUKSI').text('Rp. ' +content.historyProduct.REALISASI_BIAYA_PRODUKSI);

    //File Penugasan
    $('#tab-historical-product .FILE_PENUGASAN').empty();
    $.each(content.docHistoryNew, function (idx, value) {
        if(value.DOC_TYPE == '0'){
            //var str = '<a href="' + BASE_URL + '/file/download/' + value.ID_FILE + '"/>">value.NAME</a> </br>';
            var str = '<a href="' + BASE_URL + 'file/download/' + value.ID_FILE + '">' + value.NAME + '</a></br>'
            $('#tab-historical-product .FILE_PENUGASAN').append(str);
        }
    });

    //File RAB Produksi
    $('#tab-historical-product .FILE_RAB_PRODUKSI').empty();
    $.each(content.docHistoryNew, function (idx, value) {
        if(value.DOC_TYPE == '1'){
            //var str = '<a href="' + BASE_URL + '/file/download/' + value.ID_FILE + '"/>">value.NAME</a> </br>';
            var str = '<a href="' + BASE_URL + 'file/download/' + value.ID_FILE + '">' + value.NAME + '</a></br>'
            $('#tab-historical-product .FILE_RAB_PRODUKSI').append(str);
        }
    });

    //File HPP
    $('#tab-historical-product .FILE_HPP').empty();
    $.each(content.docHistoryNew, function (idx, value) {
        if(value.DOC_TYPE == '2'){
            //var str = '<a href="' + BASE_URL + '/file/download/' + value.ID_FILE + '"/>">value.NAME</a> </br>';
            var str = '<a href="' + BASE_URL + 'file/download/' + value.ID_FILE + '">' + value.NAME + '</a></br>'
            $('#tab-historical-product .FILE_HPP').append(str);
        }
    });

    //File Penugasan
    $('#tab-historical-product .FILE_REALISASI').empty();
    $.each(content.docHistoryNew, function (idx, value) {
        if(value.DOC_TYPE == '3'){
            //var str = '<a href="' + BASE_URL + '/file/download/' + value.ID_FILE + '"/>">value.NAME</a> </br>';
            var str = '<a href="' + BASE_URL + 'file/download/' + value.ID_FILE + '">' + value.NAME + '</a></br>'
            $('#tab-historical-product .FILE_REALISASI').append(str);
        }
    });


}

//saifulbahri@20150607 start
function paginationDetailClick(path){
         //alert("masuk "+path);
    $.ajax({
        type: 'GET',
        url: path,
        beforeSend: function () {
            // loading pada product container
            Metronic.blockUI({
                target: $('#tab-comment'),
                boxed: true
            });
        },
        success: function (response) {
            // menghilangkan loading jika data telah sukses
            //alert("masuk");
            Metronic.unblockUI($('#tab-comment'));
            //console.table(response);
            $('#isi-comment').html(response);
        }
    });
}

//saifulbahri@20150607 end