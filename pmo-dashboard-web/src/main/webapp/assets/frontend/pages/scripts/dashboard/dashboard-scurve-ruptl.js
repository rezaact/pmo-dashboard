var DashboardScurve = function () {
    var scurveRuptlChart = null;
    var transmissionChart = null;
    var subStationChart = null;
    var startPeriod = null;
    var endPeriod = null;
    var program = null;
    var region = null;
    var tableDetail = null;
    var modalDetail = null;
    var satuan = null; //MW, KMS, MVA

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            startPeriod = $("#periode-start");
            endPeriod = $("#periode-end");
            program = $("#program");
            region = $("#region");
            modalDetail = $('#modal-detail-scurve')
            DashboardScurve.initChart();
            DashboardScurve.initDataTable();
            DashboardScurve.initListener();
            DashboardScurve.initDatepicker();
            DashboardScurve.Default_Value();

        },
        initDataTable : function(){

            function renderSatuan(data, type, row){
                return data+satuan;
            }

            function renderPersen(data, type, row){
                return data+' %';
            }

            tableDetail = $('#table-detail-scurve').DataTable( {
                "searching": true,
                "bLengthChange" : false,
                "iDisplayLength": 10,
                "bAutoWidth": true,
                columns: [
                    { data: "ASSETNUMBER", "defaultContent": "-", "visible": false},
                    { data: "ASSETNAME", "defaultContent": "-", "render" : function(data, type, row){
                        return "<p data-toggle='tooltip' title='"+row.ASSETNUMBER+"'>"+data+"</p>";
                    }},
                    { data: "TIPE", "defaultContent": "-"},
                    { data: "REGION", "defaultContent": "-"},
                    { data: "UNITCAPACITY", "defaultContent": "-", "render" : renderSatuan},
                    { data: "ACTIVITY", "defaultContent": "-"},
                    { data: "BOBOT_PLAN", "defaultContent": "-", "render" : renderPersen},
                    { data: "BOBOT_ACTUAL", "defaultContent": "-", "render" : renderPersen}
                ],
            });
        },
        initDatepicker: function (){


            startPeriod.datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            }).on('changeDate', function(selected){
                endPeriod.datepicker('setStartDate', startPeriod.val());
            });
            endPeriod.datepicker({
                format: "M yyyy",
                autoclose: true,
                minViewMode: "months"
            });

            var today = new Date();
            var yyyy = today.getFullYear();

            //startPeriod.val('Jan '+(yyyy-1));
            //endPeriod.val('Dec '+(yyyy));

            startPeriod.val('Jan 2015');
            endPeriod.val('Dec 2019');
        },
        getParams: function(){
            var data = {
                region : region.val(),
                program : program.val(),
                startPeriod: ConvertMonth.MyyyyToyyyymm(startPeriod.val()),
                endPeriod: ConvertMonth.MyyyyToyyyymm(endPeriod.val())
            }

            return data;
        },
        getChartData : function(){
            data = DashboardScurve.getParams();
            var url = BASE_URL+'dashboard-scurve/getdata-scurve-ruptl';
            return $.ajax({
                type : "POST",
                url: url,
                data : data,
            });
        },
        getDetailChartData : function(thbl, assettype, planactual){
            data = DashboardScurve.getParams();
            data.thbl = thbl.substring(4,8)+ConvertMonth.HalfStrToNum(thbl.substring(0,3));
            data.assettype = assettype;
            data.planactual = planactual;

            //kondisional plan/actual
            if(planactual == 'A'){
                tableDetail.column(6).visible( false );
                tableDetail.column(7).visible( true );
            }
            else{
                tableDetail.column(6).visible( true );
                tableDetail.column(7).visible( false );
            }

            //kondisional satuan
            switch(assettype){
                case 17: satuan = ' MW'; break;
                case 18: satuan = ' KMS';break;
                case 19: satuan = ' KVA';break;
                default : satuan = ' MW';
            }



            var url = BASE_URL+'dashboard-scurve/getdata-detail-scurve-ruptl';
            return $.ajax({
                type : "POST",
                url: url,
                data : data,
            });
        },
        Default_Value:function (){
            $('#filter-all').trigger("click");
        },

        initListener: function (){
            $( "#filter-all" ).on('click', function(event) {
                event.preventDefault();
                $(".modal_loading").fadeIn(10);
                DashboardScurve.getChartData().done(function(response){
                    var dataChart = JSON.parse(response.strGetDataSupplay_gen);
                    var dataChartTransmission = JSON.parse(response.dataTransmission);
                    var dataChartSubStation = JSON.parse(response.dataGI);

                    var counter = 0;
                    var today = new Date();
                    var mm = today.getMonth()+1; //January is 0!
                    var yyyy = today.getFullYear();
                    var monthYear = ConvertMonth.halfNumToHalfStr(mm)+' '+yyyy;

                    for (var i=0;i<dataChart.categories[0].category.length ; i++){
                        if(dataChart.categories[0].category[i].label.trim().toUpperCase() == monthYear.trim().toUpperCase()){
                            break;
                        }
                        else{
                            counter++;
                        }
                    }

                    var newDataActual = [];
                    var newDataActualTransmission = [];
                    var newDataActualSubStation = [];
                    for(var i=0;i<=counter;i++){
                        newDataActual.push(dataChart.dataset[0].data[i]);
                        newDataActualTransmission.push(dataChartTransmission.dataset[0].data[i]);
                        newDataActualSubStation.push(dataChartSubStation.dataset[0].data[i]);
                    }

                    dataChart.dataset[0].data = newDataActual;
                    dataChartTransmission.dataset[0].data = newDataActualTransmission;
                    dataChartSubStation.dataset[0].data = newDataActualSubStation;

                    scurveRuptlChart.setChartData(JSON.stringify(dataChart), 'json');
                    transmissionChart.setChartData(JSON.stringify(dataChartTransmission), 'json');
                    subStationChart.setChartData(JSON.stringify(dataChartSubStation), 'json');



                    $(".modal_loading").fadeOut(10);
                });
            });

        },
        initChart: function () {

            FusionCharts.ready(function(){
                scurveRuptlChart = new FusionCharts({
                    "type": "msline",
                    "renderAt": "chartGeneration",
                    "width": "100%",
                    "height": "400",
                    "dataFormat": "json",
                    "dataSource": null,
                    "events" : {
                        "dataPlotClick" : function(eventObj, dataObj){

                            console.log('eventObj');
                            console.log(eventObj);
                            console.log('dataObj');
                            console.log(dataObj);
                            var planactual = (dataObj.datasetIndex == 0)?'A':'P';

                            $(".modal_loading").fadeIn(10);
                            modalDetail.find('#judul').html(dataObj.categoryLabel);
                            DashboardScurve.getDetailChartData(dataObj.categoryLabel, 17, planactual).done(function(response){
                                tableDetail.clear().rows.add( response.data ).draw();
                                $(".modal_loading").fadeOut(1000);
                                modalDetail.modal('show');
                            });


                        }
                    }
                });

                transmissionChart = new FusionCharts({
                    "type": "msline",
                    "renderAt": "chart-transmission",
                    "width": "100%",
                    "height": "400",
                    "dataFormat": "json",
                    "dataSource": null,
                    "events" : {
                        "dataPlotClick" : function(eventObj, dataObj){
                            $(".modal_loading").fadeIn(10);
                            modalDetail.find('#judul').html(dataObj.categoryLabel);
                            var planactual = (dataObj.datasetIndex == 0)?'A':'P';
                            DashboardScurve.getDetailChartData(dataObj.categoryLabel, 18, planactual).done(function(response){
                                tableDetail.clear().rows.add( response.data ).draw();
                                $(".modal_loading").fadeOut(1000);
                                modalDetail.modal('show');
                            });


                        }
                    }
                });

                subStationChart = new FusionCharts({
                    "type": "msline",
                    "renderAt": "chart-sub-station",
                    "width": "100%",
                    "height": "400",
                    "dataFormat": "json",
                    "dataSource": null,
                    "events" : {
                        "dataPlotClick" : function(eventObj, dataObj){
                            $(".modal_loading").fadeIn(10);
                            modalDetail.find('#judul').html(dataObj.categoryLabel);
                            var planactual = (dataObj.datasetIndex == 0)?'A':'P';
                            DashboardScurve.getDetailChartData(dataObj.categoryLabel, 19, planactual).done(function(response){
                                tableDetail.clear().rows.add( response.data ).draw();
                                $(".modal_loading").fadeOut(10);
                                modalDetail.modal('show');
                            });


                        }
                    }
                });



                scurveRuptlChart.render();
                transmissionChart.render();
                subStationChart.render();
            })

        }
    }

}();

// Call init
DashboardScurve.init();