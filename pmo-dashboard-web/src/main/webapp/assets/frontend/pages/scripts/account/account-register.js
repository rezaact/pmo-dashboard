// Get param from URL
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

var AccountRegister = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            //AccountRegister.formValidation();
            AccountRegister.formValidationUpdateForm();

        },

        // =========================================================================
        // HANDLE GROUP PRODUCT
        // =========================================================================
        formValidationUpdateForm: function () {


            //If update Password
            //----------------------------------------------
            var update_pass = getUrlParameter("status_update_pass");
            form = $('.pass-form');
            error_pass = $('.alert-pass-invalid', form);
            success_pass = $('.alert-pass-success', form);
            if (update_pass == "true"){
                success_pass.show();
                setTimeout(function() {
                    success_pass.hide();
                }, 2400);
                $( "#Ganti_Password" ).trigger( "click" );
            }
            if(update_pass == "false"){
                error_pass.show();
                setTimeout(function() {
                    error_pass.hide();
                }, 2400);
                $( "#Ganti_Password" ).trigger( "click" );
            }

            //----------------------------------------------

            //If update Info Pengguna
            //----------------------------------------------
            var update_pribadi = getUrlParameter("save");
            form = $('.update-form');
            error = $('.alert-pass-invalid', form);
            success = $('.alert-pass-success', form);
            if (update_pribadi == "true"){
                success.show();
               setTimeout(function() {
                    success.hide();
                }, 2400);
            }
            if(update_pribadi == "false"){
                error.show();
               setTimeout(function() {
                    error.hide();
                }, 2400);
            }
            //----------------------------------------------
            $('.pass-form').validate({
                rules: {
                    prm_old_password: {required: true,},
                    prm_password: {required: true, minlength:6, pwcheck: true},
                    prm_password_again: {required: true, equalTo: "#prm_password",},
                },
                messages: {
                    prm_old_password: {required: 'Password lama harus di isi'},
                    prm_password: {
                        required: 'Password baru harus di isi',
                        minlength: 'Password baru minimal 6 digit',
                        pwcheck: "Password baru mohon digabungkan antara huruf dan angka"
                    },
                    prm_password_again: {
                        required: 'Ketik ulang password harus di isi',
                        equalTo: 'Tidak sama dengan Password baru'},
                },
                submitHandler: function(form) {
                    form.submit();
                }

            });
            //Validasi Password Harus Angkat dan Huruf
            $.validator.addMethod("pwcheck", function(value) {
                return  /([a-zA-Z])/.test(value) //Huruf
                    && /\d/.test(value) // has a digit Angka
                    //&& /[a-z]/.test(value) // has a lowercase letter
                    // consists of only these == /^[A-Za-z0-9\d=!\-@._*]*$/.test(value)
            });



            $('.update-form').validate({
                rules: {
                    PRM_NAMA_LENGKAP: { required: true, },
                    PRM_NIP: { required: true, },
                    PRM_UNIT: { required: true, },
                    PRM_EMAIL: {
                        required: true,
                        email: true
                    },
                    PRM_NO_TELP: {
                        required: true,
                        number: true
                    }
                },
                messages:{
                    PRM_NAMA_LENGKAP:{required:'Nama harus di isi'},
                    PRM_NIP:{required:'NIP harus di isi'},
                    PRM_UNIT:{required:'Unit harus di isi'},
                    PRM_EMAIL:{
                        required:'Email harus di isi',
                        email:'Pengisian email salah'},
                    PRM_NO_TELP:{
                        required:'NoTelp harus di isi',
                        number:'NoTelp harus Number',
                    },
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

        },
        formValidation: function () {
                form = $('.register-form');
                error = $('.alert-empty', form);
                success = $('.alert-success', form);
                form.validate({
                    errorElement: 'span', //default input error message container
                    errorClass: 'help-block help-block-error', // default input error message class
                    focusInvalid: false, // do not focus the last invalid input
                    ignore: "",  // validate all fields including form hidden input
                    rules: {
                        prm_username: {
                            required: true,
                            minlength: 3,
                            maxlength: 20,
                            remote: {
                                url: BASE_URL+"account/checker",
                                data: {
                                    'prm_field':'username'
                                },
                                type: "post"
                            }

                        },
                        prm_nama_lengkap: {
                            required: true,
                            minlength: 2,
                            maxlength: 50
                        },
                        prm_email: {
                            required: true,
                            email: true,
                            maxlength: 225,
                            remote: {
                                url: BASE_URL+"account/checker",
                                data: {
                                    'prm_field':'email'
                                },
                                type: "post"
                            }
                        },

                        prm_password: {
                            minlength: 2,
                            required: true,
                            maxlength: 225
                        },
                        prm_no_telp: {
                            minlength: 2,
                            required: true,
                            digits:true,
                            maxlength: 20
                        },
                        prm_unit: {
                            required: true
                        },
                        prm_nip: {
                            minlength: 2,
                            maxlength: 20,
                            remote: {
                                url: BASE_URL+"account/checker",
                                data: {
                                    'prm_field':'nip'
                                },
                                type: "post"
                            }
                        },
                        prm_repassword: {
                            minlength: 2,
                            required: true,
                            equalTo:"#password",
                            maxlength: 225
                        },
                        "hiddenRecaptcha": {
                            required: function() {
                                if(grecaptcha.getResponse() == '') {
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        }
                    },
                    messages: {
                        prm_username: {
                            required: "Username harus diisi!",
                            //remote: "Username sudah digunakan!"
                        },
                        prm_nama_lengkap: {
                            required: "Nama Depan harus diisi",
                        },
                        prm_email: {
                            required: "Username harus diisi!",
                            email: "Email tidak valid!",
                            //remote: "Email sudah digunakan!"
                        },
                        prm_password: {
                            required: "Password harus diisi",
                        },
                        prm_repassword:{
                            required: "Konfirmasi Password harus diisi",
                        },
                        prm_no_telp:{
                            required: "No Telp Password harus diisi",
                        },
                        //prm_unit: "Pengisian unit kurang sesuai",
                        prm_nip: {
                            //remote: "NIP sudah digunakan!"
                        },
                        prm_unit: {
                            //remote: "NIP sudah digunakan!"
                        },
                        hiddenRecaptcha: {
                            required: "Captcha harus diisi",
                        }
                    },

                    invalidHandler: function (event, validator) { //display error alert on form submit
                        success.hide();
                        error.show();
                        Metronic.scrollTo(error, -200);
                    },

                    errorPlacement: function (error, element) { // render error placement for each input type
                        var icon = $(element).parent('.input-icon').children('i');
                        icon.removeClass('fa-check').addClass("fa-warning");
                        icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                    },

                    highlight: function (element) { // hightlight error inputs
                        $(element)
                            .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                    },

                    unhighlight: function (element) { // revert the change done by hightlight

                    },

                    success: function (label, element) {
                        var icon = $(element).parent('.input-icon').children('i');
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                        icon.removeClass("fa-warning").addClass("fa-check");
                    },

                    submitHandler: function (data) {
                        error.hide();
                        form[0].submit();

                    }
                });

        }

    };
}();

AccountRegister.init();


