var CapacityDetailPhase = function () {
    $(".panel_region").hide();
    var detailCapacityDatatable = null;
    var satuan = null;
    var headerName = null;
    var headerType = null;

    var revenueChart = null;
    var revenueChartRegion1 = null;
    var revenueChartRegion2 = null;
    var detailTitle = null;

    return {

        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            CapacityDetailPhase.piechart();

            switch (AssetType){
                case '17' : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
                    break;
                case '18' : satuan = ' KMS';
                    headerName = 'Jalur Transmisi';
                    headerType = 'Type Transmisi';
                    detailTitle = 'Transmission Capacity Detail';
                    break;
                case '19' : satuan = ' MVA';
                    headerName = 'Nama GI';
                    headerType = 'Type GI';
                    detailTitle = 'Substation Capacity Detail';
                    break;
                default : satuan = ' MW';
                    headerName = 'Nama Pembangkit';
                    headerType = 'Type Pembangkit';
                    detailTitle = 'Generation Capacity Detail';
            }

            detailTitle = detailTitle + " Periode " + ConvertMonth.yyyymmToStr(tahunAwal);

            if(tahunAkhir != '0'){
                detailTitle = detailTitle + " - "+ ConvertMonth.yyyymmToStr(tahunAkhir);
            }

            $('#detail-title-header').html("Chart "+detailTitle);
            $('#detail-title').html(detailTitle);


            CapacityDetailPhase.initDataTable();

            //if(!((tahunAwal == '201701') && (jenis == '3'))){
            //    //hide fase coulumn
            //    detailCapacityDatatable.column(7).visible( false );
            //}

            //Insert title to Captions Chart
            json_strChart_All = jQuery.parseJSON(strChart_All);
            json_strChart_All.chart.caption = json_strChart_All.chart.caption +" ("+satuan + " )";



            $('#back').on('click', function(){
                history.back();
            });

        },

        exportPdf : function(){

            //FusionCharts.batchExport({
            //    "charts":[
            //        {"id":"chartContainer"},
            //        {"id":"chartContainer-regional-1"}
            //    ],
            //    "imageWidth": 450,
            //    "imageHeight": 525,
            //    "exportFileName":"selectiveBatchExport",
            //    "exportFormats":"pdf",
            //    "exportAtClientSide":"1",
            //});


        },

        piechart:function(){

            ///*
            // Fungsi untuk export to pdf
            // */
            //var form = $('.export_pdf'),
            //    cache_width = form.width(),
            //    a4  =[ 595.28,  841.89];  // for a4 size paper width and height
            //
            //batchExportConfig1 = function() {
            //    alert ("Exprot 2");
            //    FusionCharts.batchExport({
            //        "charts": [{
            //            "id": "chart-1",
            //        }, {
            //            "id": "chart-2",
            //        }, {
            //            "id": "chart-3",
            //        }, {
            //            "id": "chart-4",
            //        }],
            //        "exportFileName": "batchExport",
            //        "exportFormats": "jpg",
            //        "exportAtClientSide": "1"
            //    })
            //}

            //$('#create_pdf').on('click',function(){
            //    //$('body').scrollTop(0);
            //    //console.log("crate pdf");
            //    //createPDF();
            //    //CapacityDetailPhase.exportPdf();
            //    alert ("Exprot 1");
            //    FusionCharts.batchExport({
            //        "charts": [{
            //            "id": "id-chartContainer",
            //        }, {
            //            "id": "id-chartContainer-regional-1",
            //        }, {
            //            "id": "id-chartContainer-regional-2",
            //        }, {
            //            "id": "id-chartContainer-regional-3",
            //        }, {
            //            "id": "id-chartContainer-regional-4",
            //        }, {
            //            "id": "id-chartContainer-regional-5",
            //        }, {
            //            "id": "id-chartContainer-regional-6",
            //        }, {
            //            "id": "id-chartContainer-regional-7",
            //        }],
            //        "exportFileName": "batchExport",
            //        "exportFormats": "jpg",
            //        "exportAtClientSide": "1"
            //    })
            //
            //});
            ////create pdf
            //function createPDF(){
            //    getCanvas().then(function(canvas){
            //
            //        var
            //            img = canvas.toDataURL("image/png"),
            //
            //            doc = new jsPDF({
            //                unit:'px',
            //                format:'a4'
            //            });
            //
            //
            //        doc.addImage(img, 'JPEG', 20, 20);
            //        //doc.addImage(img, 'PNG', 10, 10, 500, 800);
            //        doc.save('Capacity-Detail-by-Phase.pdf');
            //        form.width(cache_width);
            //    });
            //}
            //
            //// create canvas object
            //function getCanvas(){
            //    form.width((a4[0]*1.33333) -80).css('max-width','none');
            //    return html2canvas(form,{
            //        imageTimeout:2000,
            //        removeContainer:true
            //    });
            //}
            // -------------------------------------- End Export to pdf



            //Parsing

            var json_strChart_Sumatera = jQuery.parseJSON(strChart_Sumatera),
                caption_sumatera = json_strChart_Sumatera.chart.caption,
                data_sumatera = json_strChart_Sumatera.data.length;

            //json_strChart_Sumatera.chart.caption = json_strChart_Sumatera.chart.caption + "DEDE";
            //chart.coolness  = "34.33";
            //console.log(json_strChart_Sumatera.chart);
            //console.log('chart : ', chart);

            var json_strChart_JBB = jQuery.parseJSON(strChart_JBB),
                caption_JBB = json_strChart_JBB.chart.caption,
                data_JBB = json_strChart_JBB.data.length;

            var json_strChart_JBT = jQuery.parseJSON(strChart_JBT),
                caption_JBT = json_strChart_JBT.chart.caption,
                data_JBT = json_strChart_JBT.data.length;

            var json_strChart_JBTB = jQuery.parseJSON(strChart_JBTB),
                caption_JBTB = json_strChart_JBTB.chart.caption,
                data_JBTB = json_strChart_JBTB.data.length;

            var json_strChart_KAL = jQuery.parseJSON(strChart_KAL),
                caption_KAL = json_strChart_KAL.chart.caption,
                data_KAL = json_strChart_KAL.data.length;

            var json_strChart_SNT= jQuery.parseJSON(strChart_SNT),
                caption_SNT = json_strChart_SNT.chart.caption,
                data_SNT = json_strChart_SNT.data.length;

            var json_strChart_MP = jQuery.parseJSON(strChart_MP),
                caption_MP = json_strChart_MP.chart.caption,
                data_MP = json_strChart_MP.data.length;


            //Hide Div Chart dengan jika data kosong
            if (data_sumatera == '0'){
                $(".chartContainer-regional-1").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_sumatera+'</div>');
                $(".panel_region").show();
            }
            if (data_JBB == '0'){
                $(".chartContainer-regional-2").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBB+'</span>');
                $(".panel_region").show();
            }
            if (data_JBT == '0'){
                $(".chartContainer-regional-3").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBT+'</span>');
                $(".panel_region").show();
            }
            if (data_JBTB == '0'){
                $(".chartContainer-regional-4").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_JBTB+'</span>');
                $(".panel_region").show();
            }
            if (data_KAL == '0'){
                $(".chartContainer-regional-5").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_KAL+'</span>');
                $(".panel_region").show();
            }
            if (data_SNT == '0'){
                $(".chartContainer-regional-6").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_SNT+'</span>');
                $(".panel_region").show();
            }
            if (data_MP == '0'){
                $(".chartContainer-regional-7").hide();
                $(".data_region").append('<span class="alert alert-warning" role="alert"> '+caption_MP+'</span>');
                $(".panel_region").show();
            }


            FusionCharts.ready(function(){

                var revenueChart = new FusionCharts({
                    id: 'id-chartContainer',
                    "type": "pie2d",
                    "renderAt": "chartContainer",
                    "width": "420",
                    "height": "380",
                    "dataFormat": "json",
                    "dataSource":json_strChart_All
                });

                revenueChart.render();

                //console.log("_cek_"+strChart_All);
                /*var revenueChart = new FusionCharts({
                    "id" : 'id-chartContainer',
                    "type": "pie2d",
                    "renderAt": "chartContainer",
                    "width": "420",
                    "height": "380",
                    "dataFormat": "json",
                    "dataSource":
                    {
                        "chart":{
                        "caption" : "DETAIL BY PHASE",
                        "numberPrefix" : "",
                        "exportenabled" : "1",
                        "exportatclient" : "1",
                        "defaultCenterLabel" : "",
                        "centerLabel" : "Nilai $label: $value",
                        "useDataPlotColorForLabels" : "1",
                        "theme" : "fint",
                        "pieRadius" : "85",
                        "legendPosition" : "down",
                        "showLegend" : "1",
                        "showLabels" : "0",
                        "bgColor" : "#ffffff",
                        "bgAlpha" : "40%",
                        "legendItemFontSize" : "9",
                        "legendItemFontColor" : "#000000",
                        "labelFontSize" : "9"
                    },
                        "data":[
                            { "label" : "Kontrak Belum Konstruksi", "value" : 10, "color" : "#4CAF50"},
                            { "label" : "Kontrak Belum Konstruksi", "value" : 10, "color" : "#800000"},
                            {  "label" : "Kontrak Konstruksi",  "value" : 1899.24,  "color" : "#191970"},
                            {  "label" : "Kontrak Konstruksi",  "value" : 1899.24,  "color" : "#FFEB3B"},
                            {  "label" : "Perencanaan",  "value" : 20,  "color" : "#778899"},
                            {  "label" : "Perencanaan",  "value" : 20,  "color" : "#F44336"},
                            {  "label" : "SLO",  "value" : 5558.89,  "color" : "#BC8F8F"},
                            {  "label" : "SLO",  "value" : 5558.89,  "color" : "#E91E63"}
                        ]}
                }).render();
*/

                //Chart Region 1
                FusionCharts.ready(function(){
                    //For Region 1
                    var revenueChartRegion1 = new FusionCharts({
                        id: 'id-chartContainer-regional-1',
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-1",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_Sumatera
                        });
                    revenueChartRegion1.render();
                });

                //End For Region 1

                //For Region 2
                FusionCharts.ready(function(){
                    var revenueChartRegion2 = new FusionCharts({
                        id: 'id-chartContainer-regional-2',
                        "type": "pie2d",
                        "renderAt": "chartContainer-regional-2",
                        "width": "300",
                        "height": "450",
                        "dataFormat": "json",
                        "dataSource":strChart_JBB
                        });
                        revenueChartRegion2.render();
                });
                    //End Region 2

                FusionCharts.ready(function(){
                        //For Region 3
                        var revenueChartRegion3 = new FusionCharts({
                            id: 'id-chartContainer-regional-3',
                            "type": "pie2d",
                            "renderAt": "chartContainer-regional-3",
                            "width": "300",
                            "height": "450",
                            "dataFormat": "json",
                            "dataSource":strChart_JBT
                        });
                revenueChartRegion3.render();
                });
                //End Region 3


                FusionCharts.ready(function(){

                    //For Region 4
                        var revenueChartRegion4 = new FusionCharts({
                            id: 'id-chartContainer-regional-4',
                            "type": "pie2d",
                            "renderAt": "chartContainer-regional-4",
                            "width": "300",
                            "height": "450",
                            "dataFormat": "json",
                            "dataSource":strChart_JBTB
                        });
                        revenueChartRegion4.render();
                        //End Region 4

                        //For Region 5
                        var revenueChartRegion5 = new FusionCharts({
                            id: 'id-chartContainer-regional-5',
                            "type": "pie2d",
                            "renderAt": "chartContainer-regional-5",
                            "width": "300",
                            "height": "450",
                            "dataFormat": "json",
                            "dataSource":strChart_KAL
                        });
                        revenueChartRegion5.render();
                        //End Region 5


                        //For Region 6
                        var revenueChartRegion6 = new FusionCharts({
                            id: 'id-chartContainer-regional-6',
                            "type": "pie2d",
                            "renderAt": "chartContainer-regional-6",
                            "width": "300",
                            "height": "450",
                            "dataFormat": "json",
                            "dataSource":strChart_SNT
                        });
                        revenueChartRegion6.render();
                        //End Region 6

                        //For Region 7
                        var revenueChartRegion7 = new FusionCharts({
                            id: 'id-chartContainer-regional-7',
                            "type": "pie2d",
                            "renderAt": "chartContainer-regional-7",
                            "width": "300",
                            "height": "450",
                            "dataFormat": "json",
                            "dataSource":strChart_MP
                        });
                        revenueChartRegion7.render();
                        //End Region 7


                    });

            })
        },
        initDataTable : function(){

            function renderSatuan(data, type, row){
                var firstChar = data.toString().substring(0,1);
                var newData = (firstChar == '.')?'0'+data:data;

                return newData+satuan;
            }

            detailCapacityDatatable = $('#table-capacity-detail').DataTable( {
                "responsive" : true,
                "searching": false,
                "bLengthChange" : false,
                "iDisplayLength": 10,
                "bAutoWidth": true,
                "processing": true,
                "serverSide": true,
                "columnDefs": [
                    { "visible": false, "targets": 0 },
                    { className: "dt-body-right", "targets": [ 5 ] }
                ],
                "ajax": {
                    "url": BASE_URL + "dashboard-cod-capacity/grid",
                    "type": "POST",
                    "data": {
                        p_jenis : jenis,
                        p_subjenis : subJenis,
                        p_Group : group,
                        p_detail_tipe : detailType,
                        p_thblawal : tahunAwal,
                        p_thblakhir : tahunAkhir,
                        p_asset_type : AssetType
                    }
                },
                columns: [
                    { data: "PHASE", "defaultContent": "-"},
                    { data: "RN", "defaultContent": "-"},
                    //{ data: "ASSETNUMBER", "defaultContent": "-"},
                    { data: "ASSETNAME", "defaultContent": "-", "render" : function(data, type, row){
                        return "<p data-toggle='tooltip' title='"+row.ASSETNUMBER+"'>"+data+"</p>";
                    }, "title" : headerName},
                    { data: "TIPE", "defaultContent": "-", "title" : headerType},
                    { data: "REGIONNAME", "defaultContent": "-"},
                    { data: "UNITCAPACITY", "defaultContent": "-", "render": renderSatuan},
                    //{ data: "STATUS", "defaultContent": "-"}
                    { data: "MILLESTONE", "defaultContent": "-"},
                    { data: "BOBOT", "defaultContent": "-", "render": function(data){
                        return data+' %';
                    }}
                ],

                "drawCallback": function ( settings ) {
                    var api = this.api();
                    var rows = api.rows( {page:'current'} ).nodes();
                    var last=null;

                    api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                        //console.log('apakah rows ?', rows[i]);
                        if ( last !== group ) {
                            $(rows).eq( i ).before(
                                '<tr class="group"><td colspan="7" style="background-color: #dff0d8!important;">Phase : ' + group + '</td></tr>'
                            );
                            last = group;
                        }
                    } );
                }
            });


        },

        getParams: function(){

        }
    }

}();

// Call init
CapacityDetailPhase.init();