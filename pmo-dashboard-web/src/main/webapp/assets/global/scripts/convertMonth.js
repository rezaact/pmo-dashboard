/**
 * Created by datuk on 12/31/2016.
 */

var ConvertMonth = function () {
    /*
    * istilah
    * MonthStr : Januari
    * MonthHalfStr : Jan
    * MonthNum : 01
    * MonthHalfNum : 1
     */
    return {

        yyyymmToStr : function (yyyymm) {
            var year = yyyymm.substring(0,4);
            var month = ConvertMonth.halfNumToStr(yyyymm.substring(4, 6));
            return month +' '+year;
        },
        yyyymmToHalfStr : function (yyyymm) {
            var year = yyyymm.substring(0,4);
            var month = null;
            if(yyyymm.substring(4, 6) != ""){
                month = ConvertMonth.halfNumToHalfStr(yyyymm.substring(4, 6));
                return month +' '+year;
            }
            return year;
        },
        MyyyyToyyyymm : function(Myyyy){
            var year = Myyyy.substring(4,8);
            var month = ConvertMonth.HalfStrToNum(Myyyy.substring(0,3));
            return year+month;
        },
        halfNumToStr : function (month) {

            var monthStr = null;
            month = parseInt(month);
            switch (month){
                case 1 : monthStr = "Januari"; break;
                case 2 : monthStr = "Februari"; break;
                case 3 : monthStr = "Maret"; break;
                case 4 : monthStr = "April"; break;
                case 5 : monthStr = "Mei"; break;
                case 6 : monthStr = "Juni"; break;
                case 7 : monthStr = "Juli"; break;
                case 8 : monthStr = "Agustus"; break;
                case 9 : monthStr = "September"; break;
                case 10 : monthStr = "Oktober"; break;
                case 11 : monthStr = "November"; break;
                case 12 : monthStr = "Desember"; break;
                default : monthStr = "-";
            }

            return monthStr;
        },
        numToStr : function (month) {
            var monthStr = null;
            switch (month){
                case '01' : monthStr = "Januari"; break;
                case '02': monthStr = "Februari"; break;
                case '03': monthStr = "Maret"; break;
                case '04': monthStr = "April"; break;
                case '05': monthStr = "Mei"; break;
                case '06': monthStr = "Juni"; break;
                case '07': monthStr = "Juli"; break;
                case '08': monthStr = "Agustus"; break;
                case '09': monthStr = "September"; break;
                case '10' : monthStr = "Oktober"; break;
                case '11' : monthStr = "November"; break;
                case '12' : monthStr = "Desember"; break;
                default : monthStr = "-";break;
            }
            return monthStr;
        },
        HalfStrToNum : function(month){
            var monthNum = null;
            month = month.toUpperCase();
            switch (month){
                case 'JAN': monthNum = '01'; break;
                case 'FEB': monthNum = '02'; break;
                case 'MAR': monthNum = '03'; break;
                case 'APR': monthNum = '04'; break;
                case 'MAY': monthNum = '05'; break;
                case 'JUN': monthNum = '06'; break;
                case 'JUL': monthNum = '07'; break;
                case 'AUG': monthNum = '08'; break;
                case 'SEP': monthNum = '09'; break;
                case 'OCT': monthNum = '10'; break;
                case 'NOV': monthNum = '11'; break;
                case 'DEC': monthNum = '12'; break;
                default : monthNum = '01';
            }
            return monthNum;
        },
        halfNumToHalfStr : function (month) {
            var monthStr = null;
            month = parseInt(month);
            switch (month){
                case 1 : monthStr = "Jan"; break;
                case 2 : monthStr = "Feb"; break;
                case 3 : monthStr = "Mar"; break;
                case 4 : monthStr = "Apr"; break;
                case 5 : monthStr = "May"; break;
                case 6 : monthStr = "Jun"; break;
                case 7 : monthStr = "Jul"; break;
                case 8 : monthStr = "Aug"; break;
                case 9 : monthStr = "Sep"; break;
                case 10 : monthStr = "Oct"; break;
                case 11 : monthStr = "Nov"; break;
                case 12 : monthStr = "Dec"; break;
                default : monthStr = "-";
            }

            return monthStr;
        }



    }

}();
