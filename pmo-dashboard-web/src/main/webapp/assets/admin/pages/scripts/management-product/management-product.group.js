var ManagementProductGroup = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementProductGroup.productGroup();
            ManagementProductGroup.productTable();
        },

        // =========================================================================
        // HANDLE GROUP PRODUCT
        // =========================================================================
        productGroup: function () {

            $('#product-group').jstree({
                "core" : {
                    //"animation" : 0,
                    "themes" : {
                        "responsive": false
                    },
                    "check_callback" : true,
                    'data' : {
                        "url" : BASE_URL+'misc/product/group/json',
                        "dataType": "json",
                        "dataFilter":function(data, type){
                            var d = data.replace(/"NAMA"/ig,"\"text\"").replace(/"ID_PRODUCT_GROUP"/ig,"\"id\"");
                            return d;
                        }
                    }
                    //dataType: 'json'

                },
                "types" : {
                    "default" : {
                        "icon" : "fa fa-folder icon-state-warning icon-lg"
                    },
                    "file" : {
                        "icon" : "fa fa-file icon-state-warning icon-lg"
                    }
                },
                "state" : { "key" : "demo3" },
                "plugins" : [ "dnd", "state", "types" ]
            });

            // handle link clicks in tree nodes(support target="_blank" as well)
            $('#product-group').on('select_node.jstree', function(e,data) {
                var link = $('#' + data.selected).find('a');
                if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
                    if (link.attr("target") == "_blank") {
                        link.attr("href").target = "_blank";
                    }
                    document.location.href = link.attr("href");
                    return false;
                }
            });
        },

        // =========================================================================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================
        productTable: function () {
            var table = $('#product-group-table');

            var oTable = table.dataTable({

                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1, 2, 3, 4 ] } ]

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

            // Remove table tool
            $('.tabletools-dropdown-on-portlet').closest('.row').remove();
        }
    };
}();

ManagementProductGroup.init();


