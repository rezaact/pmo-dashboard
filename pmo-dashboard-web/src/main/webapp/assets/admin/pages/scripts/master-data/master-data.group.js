var MasterDataGroup = function() {

    return {

        //main function to initiate the module
        init: function() {
            MasterDataGroup.listGroup();
        },

        listGroup : function() {
            jQuery('#list-group').gtreetable({
                'draggable': true,
                'source': function(id) {
                    return {
                        type: 'GET',
                        url: BASE_URL+'admin/master-data/group/construct-tree',
                        data: {
                            'id': id
                        },
                        dataType: 'json',
                        error: function(XMLHttpRequest) {
                            alert(XMLHttpRequest.status + ': ' + XMLHttpRequest.responseText);
                        }
                    }
                },
                'onSave':function (oNode) {
                    return {
                        type: 'GET',
                        url: !oNode.isSaved() ? 'group/nodeCreate' : 'group/nodeUpdate?id=' + oNode.getId(),
                        data: {
                            parent: oNode.getParent(),
                            name: oNode.getName(),
                            position: oNode.getInsertPosition(),
                            related: oNode.getRelatedNodeId()
                        },
                        dataType: 'json',
                        error: function(XMLHttpRequest) {
                            alert(XMLHttpRequest.status+': '+XMLHttpRequest.responseText);
                        }
                    };
                },
                'onDelete':function (oNode) {
                    return {
                        type: 'GET',
                        url: 'group/nodeDelete?id=' + oNode.getId(),
                        dataType: 'json',
                        error: function(XMLHttpRequest) {
                            alert(XMLHttpRequest.status+': '+XMLHttpRequest.responseText);
                        }
                    };
                },
                'onMove':function(oSource, oDestination, position) {
                    alert('on move');
                    return {
                        type: 'POST',
                        url: URI('group/nodeUpdate?id').addSearch({'id': oSource.getId()}).toString(),
                        data: {
                            related: oDestination.getId(),
                            position: position
                        },
                        dataType: 'json',
                        error: function (XMLHttpRequest) {
                            alert(XMLHttpRequest.status + ': ' + XMLHttpRequest.responseText);
                        }
                    };
                    },
                //    'sort': function (a, b) {
                //    console.table(a);
                //    console.table(b);
                //    var aName = a.name.toLowerCase();
                //    var bName = b.name.toLowerCase();
                //    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
                //},

                'types': { default: 'glyphicon glyphicon-folder-open', folder: 'glyphicon glyphicon-folder-open'},
                'inputWidth': '255px'
            });
        }
    };
}();
MasterDataGroup.init();