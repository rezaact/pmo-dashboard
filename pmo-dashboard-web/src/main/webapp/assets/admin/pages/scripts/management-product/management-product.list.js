var ManagementProductList = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementProductList.productTable();
        },

        // =========================================================================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================
        productTable: function () {
            var table = $('#product-list-table');

            table.dataTable({
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2, 3, 4 ] },
                    {
                        "aTargets": [ 0 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            if(data == undefined){
                                return '<img class="img-responsive" src="'+BASE_URL+'assets/global/img/default.png"/>';
                            }else{
                                return '<img class="img-responsive" src="'+BASE_URL+'file/'+data+'"/>';
                            }
                        }
                    },
                    {
                        "aTargets": [ 3 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<div class="table-limit-text">'+full.DESKRIPSI_PRODUK+'</div>';
                        }
                    },
                    {
                        "aTargets": [ 4 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<a href="'+BASE_URL+'admin/product/readmore?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-info tooltips" data-container="body" data-placement="top" data-original-title="Readmore" data-toggle="modal"><i class="fa fa-book"></i></a>'+
                                '<a href="'+BASE_URL+'admin/product/update?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>'+
                                '<a href="'+BASE_URL+'admin/product/detail?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                '<a href="#product-delete-'+full.NOMOR_PRODUK+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                ''+
                                '<div id="product-delete-'+full.NOMOR_PRODUK+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAMA_PRODUK+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus permanen '+full.NAMA_PRODUK+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<a href="'+BASE_URL+'admin/product/delete?id='+full.NOMOR_PRODUK+'" class="btn btn-success">Ya</a>'+
                                '</div></div></div></div>';
                        }
                    }
                ],
                "columns": [
                    {"data": "ID_FILE", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "NAMA_PRODUK", "defaultContent": ""},
                    {"data": "DESKRIPSI_PRODUK", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""}
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                }

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

            // Remove table tool
            $('.tabletools-dropdown-on-portlet').closest('.row').remove();
        }
    };
}();

ManagementProductList.init();

