var HistoryProductForm = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            HistoryProductForm.datepicker();
            HistoryProductForm.validationProduct();
        },

        // =========================================================================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================
       datepicker: function (){
           $('.date-picker').datepicker({
               rtl: Metronic.isRTL(),
               orientation: "left",
               format:"dd-mm-yyyy",
               autoclose: true
           });
       },

        validationProduct :function () {
            form = $('#form-product');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_ID_LOKASI_PEMASANGAN: {
                        required: true
                    },
                    PRM_NOMOR_PENUGASAN: {
                        required: true
                    },
                    PRM_PERIHAL: {
                        required: true
                    },
                    PRM_TGL_PENUGASAN: {
                        required: true
                    },
                    PRM_TS_PELAKSANAAN_PENUGASAN: {
                        required: true
                    },
                    PRM_REALISASI_PP: {
                        required: true
                    },
                    PRM_NILAI_PENUGASAN: {
                        required: true
                    },
                    PRM_ID_PEMBERI_PENUGASAN: {
                        required: true
                    },
                    PRM_ITEM_PEKERJAAAN: {
                        required: true,
                        number: true
                    },
                    PRM_RAB_PRODUKSI: {
                        required: true
                    },
                    PRM_HARGA_POKOK_PRODUKSI: {
                        required: true
                    },
                    PRM_REALISASI_BIAYA_PRODUKSI: {
                        required: true
                    }
                },
                messages: {
                    PRM_ID_LOKASI_PEMASANGAN: {
                        required: 'Lokasi pemasangan harus dipilih'
                    },
                    PRM_NOMOR_PENUGASAN: {
                        required: 'Nomor penugasan harus di isi'
                    },
                    PRM_PERIHAL: {
                        required: 'Perihal harus di isi'
                    },
                    PRM_TGL_PENUGASAN: {
                        required: 'Tanggal penugasan harus di isi'
                    },
                    PRM_TS_PELAKSANAAN_PENUGASAN: {
                        required : 'Time schedule pelaksanaan harus diisi',
                        number: 'Time schedule pelaksanaan harus berupa angka'
                    },
                    PRM_REALISASI_PP: {
                        required : 'Realisasi pelaksanaan harus diisi',
                        number: 'Realisasi pelaksanaan harus berupa angka'
                    },
                    PRM_NILAI_PENUGASAN: 'Nilai penugasan harus diisi',
                    PRM_ID_PEMBERI_PENUGASAN: 'Pemberi kerja harus diisi',
                    PRM_ITEM_PEKERJAAAN: {
                        required : 'Jumlah item harus diisi',
                        number: 'Jumlah item harus berupa angka'
                    },
                    PRM_RAB_PRODUKSI: 'RAB produksi harus diisi',
                    PRM_HARGA_POKOK_PRODUKSI: 'Harga pokok produksi harus diisi',
                    PRM_REALISASI_BIAYA_PRODUKSI: 'Realisasi biaya produksi harus diisi'
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : element.data('placement')});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error'); // set success class to the control group
                    icon.removeClass("fa-warning");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();
                }
            });

            $('.select2me', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

        }

    };
}();

HistoryProductForm.init();

