var ReportingHistorikal = function () {

    return {
        //main function to initiate map samples
        init: function () {
            ReportingHistorikal.historikalDatatable();
            ReportingHistorikal.datepicker();
            ReportingHistorikal.filtering();
            ReportingHistorikal.toExcel();
        },

        toExcel:function(){
          $('#excel').on('click',function(){
              var oTable = $('#historikal-list-table').dataTable();
              oTable.fnDestroy();
              //Creating of our own filtering function
              ReportingHistorikal.historikalDatatable();
              
              var workshop = $('#id_workshop').val();
              var kategori = $('#id_kategori').val();
              var tahunPenugasan = $('#tahun').val();
              var penugasan  = $('#tgl_penugasan').val();
              var produk = $('#id_jenis').val();
              var fltr=0;
              if(workshop!=''){
                  fltr=1;
              }
              if(kategori!=''){
                  fltr=1;
              }
              if(tahunPenugasan!=''){
                  fltr=1;
              }
              if (penugasan!=''){
                  fltr=1;
              }
              if(produk!=''){
                  fltr=1;
              }
              $.ajax({
                  method: "POST",
                  url: BASE_URL + "admin/reporting/historikal-produk/laporan_excel",
                  data: {
                      PRM_WORKSHOP: workshop,
                      PRM_KATEGORI: kategori,
                      PRM_TAHUN: tahunPenugasan,
                      PRM_TGL_PENUGASAN: penugasan,
                      PRM_JENIS_PRODUK: produk,
                      FIL:fltr
                  },
                  success: function (value) {
                      if (value=="true"){
                          window.location.href = BASE_URL + "assets/temp_file/historikal_produk.xls";
                      }
                  }
              });
          });
        },

        historikalDatatable: function () {
            var table = $('#historikal-list-table');
            var fltr=0;
            var workshop = $('#id_workshop').val();
            var kategori = $('#id_kategori').val();
            var tahunPenugasan = $('#tahun').val();
            var penugasan  = $('#tgl_penugasan').val();
            var produk = $('#id_jenis').val();

            if(workshop!=''){
                fltr=1;
            }
            if(kategori!=''){
                fltr=1;
            }
            if(tahunPenugasan!=''){
                fltr=1;
            }
            if (penugasan!=''){
                fltr=1;
            }
            if(produk!=''){
                fltr=1;
            }
            

            table.dataTable({
                "ajax": {
                    "url": URL,
                    "type": "GET",
                    "data": function ( d ) {
                        d.prm_workshop= workshop;
                        d.prm_kategori= kategori;
                        d.prm_tahun= tahunPenugasan;
                        d.prm_tgl_penugasan= penugasan;
                        d.prm_jenis_produk= produk;
                        d.filtering= fltr;
                        // d.custom = $('#myInput').val();
                        // etc
                    }
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,12 ] }
                ],
                "columns": [
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "NAMA_LOKASI", "defaultContent": ""},
                    {"data": "NOMOR_PENUGASAN", "defaultContent": ""},
                    {"data": "PERIHAL", "defaultContent": ""},
                    {"data": "TANGGAL_PENUGASAN", "defaultContent": ""},
                    {"data": "JADWAL_PELAKSANAAN_PENUGASAN", "defaultContent": "","sClass": "text-right"},
                    {"data": "REALISASI_PELAKSANAAN_PENUGASA", "defaultContent": "","sClass": "text-right"},
                    {"data": "NILAI_PENUGASAN", "defaultContent": "", "sClass": "text-right"},
                    {"data": "NAMA_PEMBERI_PENUGASAN", "defaultContent": ""},
                    {"data": "JUMLAH_ITEM_PEKERJAAN", "defaultContent": "","sClass": "text-right"},
                    {"data": "RAB_PRODUKSI", "defaultContent": "", "sClass": "text-right"},
                    {"data": "HARGA_POKOK_PRODUKSI", "defaultContent": "", "sClass": "text-right"},
                    {"data": "REALISASI_BIAYA_PRODUKSI", "defaultContent": "", "sClass": "text-right"}
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                }

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        },
        datepicker: function (){
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "left",
                format:"dd-mm-yyyy",
                autoclose: true
            });
        },
        filtering: function(){
            $("#filter").click(function() {
                var oTable = $('#historikal-list-table').dataTable();
                oTable.fnDestroy();
                //Creating of our own filtering function
                ReportingHistorikal.historikalDatatable();
            });

        }

    };

}();

ReportingHistorikal.init();