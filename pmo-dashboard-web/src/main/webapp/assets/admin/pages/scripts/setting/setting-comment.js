var Comment = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Comment.userTable();
            //Comment.ajaxMessage();
        },

        userTable: function () {
            var table = $('#comment-table');

            table.dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2, 3, 4 ,5 ,6] },

                    {
                        "aTargets": [6], // Column to target
                        "mRender": function (data, type, full) {
                            return '<span class="label label-sm ' + full.CLASS_INFO+ '"> ' + full.INFO+ ' </span>';
                        }
                    },
                    {
                        "aTargets": [ 7 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            console.dir(full.NOMOR_PRODUK);
                            var share=' ';
                            if(full.NOMOR_PRODUK){
                                share='<a href="'+BASE_URL+'/product/product-detail?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-primary tooltips center" data-container="body" data-placement="top" data-original-title="Lihat Produk" data-toggle="modal"><i class="fa fa-share"></i></a>';
                            }
                            if(full.STATUS == 1){
                                aksi ='<a href="#comment-delete-'+full.ID_KOMENTAR+'" class="btn btn-sm btn-danger tooltips btn-message" data-container="body" data-placement="top" data-original-title="Sembunyikan" data-toggle="modal"><i class="fa fa-trash"></i></a>';
                                info ='sembunyikan';
                            }else{
                                aksi ='<a href="#comment-delete-'+full.ID_KOMENTAR+'" class="btn btn-sm btn-danger tooltips btn-message" data-container="body" data-placement="top" data-original-title="Tampilkan" data-toggle="modal"><i class="fa fa-th-list"></i></a>';
                                info ='menampilkan';
                            }
                            return ''+ share +''+
                                '<a href="#comment-reading'+full.ID_KOMENTAR+'" class="btn btn-sm btn-success tooltips btn-message" data-container="body" data-placement="top" data-original-title="Baca" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                ''+ aksi +''+
                                '<div id="comment-reading'+full.ID_KOMENTAR+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">' +
                                '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header btn-success">' +
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                                '<h4 class="modal-title">Komentar Pelanggan Dari : <strong>'+full.CREATED_BY+'</strong></h4>' +
                                '</div>' +
                                '<div class="modal-body">' +
                                '<p style="margin: 0px">' +
                                '<h4><strong>NOMOR PRODUK : </strong></h4>' +full.NOMOR_PRODUK+'</p>'+
                                '<p style="margin: 0px">' +
                                '<h4><strong>NAMA PRODUK : </strong></h4>' +full.NAMA_PRODUK+'</p>'+
                                '<p style="margin: 0px">' +
                                '<h4><strong>Email : </strong></h4>'+full.EMAIL+'</P>' +
                                '<p style="margin: 0px">' +
                                '<h4><strong>Pesan : </strong></h4>'+full.KOMENTAR+'</P>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +

                                '<div id="comment-delete-'+full.ID_KOMENTAR+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAMA_PRODUK+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan '+ info +' komentar pada user '+full.CREATED_BY+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<a href="'+BASE_URL+'/admin/setting/comment/delete?id='+full.ID_KOMENTAR+'" class="btn btn-success">Ya</a>' +
                                '</div></div></div>';
                        }
                    }
                ],
//                "deferLoading": RECORDS_TOTAL,
                "pageLength": LENGTH,
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "columns": [
                    {"data": "ROW_NUMBER", "defaultContent": ""},
                    {"data": "CREATED_BY", "defaultContent": ""},
                    {"data": "EMAIL", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "KOMENTAR", "defaultContent": ""},
                    {"data": "RATE", "defaultContent": ""},
                    {"data": "STATUS", "defaultContent": ""}

                ],
                "lengthMenu": [
                    [5, 15, 20, RECORDS_TOTAL],
                    [5, 15, 20, "Semua"] // change per page values here
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                    //Comment.ajaxMessage();
                }

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
            $('.DTTT').closest('.col-md-12').remove();
//            $('.DTTT .btn-group').remove();
        },


        // =========================================================================
        // HANDLE GANTI STATUS MENJADI 1
        // =========================================================================
        ajaxMessage:function(){
            $('#message-table .btn-message').on('click', function (e) {
                var i = $(this);
                $.ajax({
                    method: "GET",
                    url: BASE_URL+"admin/message/update?id="+$(this).data('id'),
                    success: function() {
                        i.closest("tr").addClass("success");
                    }
                });

            });
        }
    };
}();

Comment.init();


