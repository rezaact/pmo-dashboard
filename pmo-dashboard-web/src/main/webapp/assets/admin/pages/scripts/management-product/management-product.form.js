var ManagementProductForm = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            //ManagementProductForm.inputNumber();
            ManagementProductForm.validationProduct();
            ManagementProductForm.addProduct();
            ManagementProductForm.produkUnggulan();
        },
        produkUnggulan :function(){
            $('#PRM_FLAG_PRODUK_UNGGULAN').on('change',function(){
                if (document.getElementById('PRM_FLAG_PRODUK_UNGGULAN').checked){
                    $('#VAL_PRODUK_UNGGULAN').val("1");
                }else{
                    $('#VAL_PRODUK_UNGGULAN').val("0");
                }

            });

        },

        /*inputNumber : function (){
            $('input[name="PRM_KEMAMPUAN_PRODUK_PERBULAN"]').
        },*/
        // =========================================================================
        // HANDLE ADD PRODUCT
        // =========================================================================
        addProduct :function(){
            $('#tab-product-form a[data-toggle=tab]').on('shown.bs.tab', function (e) {
                $('#tab-product-file').show();
                $('#tab-product-image').show();
            })
        },

        // =========================================================================
        // HANDLE VALIDATION FORM PRODUCT
        // =========================================================================
        validationProduct :function () {
            form = $('#form-product');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);

            $.validator.addMethod(
                "regex",
                function(value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "Please check your input."
            );

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_NOMOR_PRODUK: {
                        required: true,
                        maxlength: 20,
                        regex: "^[\\d\\w\\u002E\\u002D\\u005F]+$"
                    },
                    PRM_NAMA_PRODUK: {
                        required: true,
                        minlength: 3
                    },
                    PRM_DIMENSI: {
                        required: true,
                        minlength: 3
                    },
                    PRM_DESKRIPSI_PRODUK: {
                        required: true,
                        minlength: 3
                    },
                    PRM_TAHUN_PRODUK: {
                        required: true
                    },
                    PRM_ID_WORKSHOP: {
                        required: true
                    },
                    PRM_ID_KATEGORI: {
                        required: true
                    },
                    PRM_ID_JENIS_PRODUK: {
                        required: true
                    },
                    PRM_ID_KOMPONEN: {
                        required: true
                    },
                    PRM_ID_JENIS_PEKERJAAN: {
                        required: true
                    },
                    PRM_NOMOR_DRAWING: {
                        required: true,
                        maxlength: 20
                    }
                },
                messages: {
                    PRM_NOMOR_PRODUK: {
                        required: 'Nomor produk harus di isi',
                        maxlength: 'Karakter nomor produk terlalu banyak',
                        regex: 'Maaf karakter yang anda inputkan tidak boleh'
                    },
                    PRM_NAMA_PRODUK: {
                        required: 'Nama produk harus di isi',
                        minlength: 'Karakter nama produk masih kurang'
                    },
                    PRM_DIMENSI: {
                        required: 'Dimensi produk harus di isi',
                        minlength: 'Karakter dimensi produk masih kurang'
                    },
                    PRM_DESKRIPSI_PRODUK: {
                        required: 'Deskripsi produk harus di isi',
                        minlength: 'Karakter deskripsi produk masih kurang'
                    },
                    PRM_TAHUN_PRODUK: 'Tahun produk harus di pilih',
                    PRM_ID_WORKSHOP: 'Manufaktur produk harus di pilih',
                    PRM_ID_KATEGORI: 'Kategori produk harus di pilih',
                    PRM_ID_JENIS_PRODUK: 'Jenis produk harus di pilih',
                    PRM_ID_KOMPONEN: 'Jenis komponen produk harus di pilih',
                    PRM_ID_JENIS_PEKERJAAN: 'Jenis pekerjaan harus di pilih',
                    PRM_NOMOR_DRAWING: {
                        required: 'Nomor drawing harus di isi'
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : element.data('placement')});

//                    var textarea = $('#cke_PRM_DESKRIPSI_PRODUK').parent('.input-icon').children('i');
//                    textarea.removeClass('fa-check').addClass("fa-warning");
//                    textarea.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : $('#cke_PRM_DESKRIPSI_PRODUK').data('placement')});

//                    error.insertAfter(element);

                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
//                    $('#cke_PRM_DESKRIPSI_PRODUK').closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error'); // set success class to the control group
                    icon.removeClass("fa-warning");

//                    label.closest('.form-group').removeClass('has-error');

//                    var textarea = $('#cke_PRM_DESKRIPSI_PRODUK').parent('.input-icon').children('i');
//                    $('#cke_PRM_DESKRIPSI_PRODUK').closest('.form-group').removeClass('has-error'); // set success class to the control group
//                    textarea.removeClass("fa-warning");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();
                }
            });

            $('.select2me', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });

        }
    };
}();

ManagementProductForm.init();