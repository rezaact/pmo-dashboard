var ManagementProductList = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementProductList.productTable();
            ManagementProductList.historyTable();
        },

        // =========================================================================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================
        productTable: function () {
            if($('#product-list-table').length){
                var table = $('#product-list-table');

                table.dataTable({

                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 0, 2, 3, 4 ] },
                        {
                            "aTargets": [ 0 ], // Column to target
                            "mRender": function ( data, type, full ) {
                                return '<img class="img-responsive" src="'+BASE_URL+'file/'+data+'"/>';
                            }
                        },
                        {
                            "aTargets": [ 3 ], // Column to target
                            "mRender": function ( data, type, full ) {
                                return '<div class="table-limit-text">'+full.DESKRIPSI_PRODUK+'</div>';
                            }
                        },
                        {
                            "aTargets": [ 4 ], // Column to target
                            "mRender": function ( data, type, full ) {
                                return '<a style="margin-right: 0px" href="'+BASE_URL+'admin/history/list?id='+full.NOMOR_PRODUK+'" data-val="'+full.NOMOR_PRODUK+'" class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>'+
//                                '<a href="'+BASE_URL+'admin/history?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
//                                '<a href="#product-delete-'+full.NOMOR_PRODUK+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                    ''+
                                    '<div id="product-delete-'+full.NOMOR_PRODUK+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                    '<div class="modal-dialog">'+
                                    '<div class="modal-content">'+
                                    '<div class="modal-header bg-red">'+
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                    '<h4 class="modal-title">'+full.NAMA_PRODUK+'</h4>'+
                                    '</div>'+
                                    '<div class="modal-body">'+
                                    '<p style="margin: 0px">'+
                                    'Apakah anda yakin akan menghapus permanen '+full.NAMA_PRODUK+' ?'+
                                    '</p>'+
                                    '</div>'+
                                    '<div class="modal-footer">'+
                                    '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                    '<a href="#" class="btn btn-success">Ya</a>'+
                                    '</div></div></div></div>';
                            }
                        }
                    ],
//                "deferLoading": RECORDS_TOTAL,
                    "pageLength": LENGTH,
                    "ajax": {
                        "url": URL,
                        "type": "GET"
                    },
                    "columns": [
                        {"data": "ID_FILE", "defaultContent": ""},
                        {"data": "NOMOR_PRODUK", "defaultContent": ""},
                        {"data": "NAMA_PRODUK", "defaultContent": ""},
                        {"data": "DESKRIPSI_PRODUK", "defaultContent": ""},
                        {"data": "NOMOR_PRODUK", "defaultContent": ""}
                    ],
                    "lengthMenu": [
                        [5, 15, 20, RECORDS_TOTAL],
                        [5, 15, 20, "Semua"] // change per page values here
                    ],
                    "drawCallback": function( settings ) {
                        $('.tooltips').tooltip();
                        $(".DTTT").remove();
                    }

                });

                var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

//            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
                $(".DTTT").remove();
            }
        },

        historyTable: function () {
            if($('#history-list-table').length){
                var table = $('#history-list-table');

                table.dataTable({

                    "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [ 0, 2 ] },
                        {
                            "aTargets": [ 4 ], // Column to target
                            "mRender": function ( data, type, full ) {
                                return '<a href="'+BASE_URL+'admin/history/update?id='+full.ID_HISTORY+'" class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>'+
                                    '<a href="'+BASE_URL+'admin/history?id='+full.ID_HISTORY+'" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                    '<a href="#product-delete-'+full.NOMOR_PRODUK+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                    ''+
                                    '<div id="product-delete-'+full.NOMOR_PRODUK+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                    '<div class="modal-dialog">'+
                                    '<div class="modal-content">'+
                                    '<div class="modal-header bg-red">'+
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                    '<h4 class="modal-title">'+full.NAMA_PRODUK+'</h4>'+
                                    '</div>'+
                                    '<div class="modal-body">'+
                                    '<p style="margin: 0px">'+
                                    'Apakah anda yakin akan menghapus permanen '+full.NAMA_PRODUK+' ?'+
                                    '</p>'+
                                    '</div>'+
                                    '<div class="modal-footer">'+
                                    '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                    '<a href="'+BASE_URL+'admin/history/delete?nomor_produk='+ full.NOMOR_PRODUK + '&id='+full.ID_HISTORY+'" class="btn btn-success">Ya</a>'+
                                    '</div></div></div></div>';
                            }
                        }
                    ],
//                "deferLoading": RECORDS_TOTAL,
                    "pageLength": LENGTH,
                    "ajax": {
                        "url": URL,
                        "type": "GET",
                        "data": function ( d ) {
                            d.prm_nomor_produk= $('#nomor_produk').val();
                            // d.custom = $('#myInput').val();
                            // etc
                        }
                    },
                    "columns": [
                        {"data": "NOMOR_PRODUK", "defaultContent": ""},
                        {"data": "NAMA_PRODUK", "defaultContent": ""},
                        {"data": "NOMOR_PENUGASAN", "defaultContent": ""},
                        {"data": "NAMA_LOKASI", "defaultContent": ""},
                    ],
                    "lengthMenu": [
                        [15, 15, 20, RECORDS_TOTAL],
                        [15, 15, 20, "Semua"] // change per page values here
                    ],
                    "drawCallback": function( settings ) {
                        $('.tooltips').tooltip();
                        $(".DTTT").remove();
                    }

                });

                var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

//            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
                $(".DTTT").remove();
            }
        }
    };
}();

ManagementProductList.init();

