var SettingService = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            SettingService.formValidation();
            SettingService.selectIcon();
        },

        // =========================================================================
        // SELECT ICONS
        // =========================================================================
        selectIcon: function () {
            function formatState (state) {
                if (!state.id) { return state.text; }
                var $state = $(
                        '<span><i class="'+state.element.value.toLowerCase()+'"></i>' + state.text + '</span>'
                );
                return $state;
            };

            $(".select-icon").select2({
                templateResult: formatState,
                templateSelection: formatState
            });
        },

        // =========================================================================
        // HANDLE SERVICE VALIDATION
        // =========================================================================
        formValidation: function () {
            form = $('.service-form');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    prm_judul: {
                        required: true
                    },
                    prm_deskripsi: {
                        required: true
                    }
                },
                messages: {
                    prm_judul: "Judul harus diisi",
                    prm_deskripsi: "Deskripsi harus diisi"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();

                }
            });

        }

    };
}();

SettingService.init();


