var MasterDataMaterial = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            MasterDataMaterial.userTable();
            MasterDataMaterial.validationMaterial();
            //Comment.ajaxMessage();
        },

        validationMaterial : function(){
            form = $('#form-material');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);

            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_NAMA_MATERIAL: {
                        required: true
                    },
                    PRM_DESKRIPSI: {
                        required: true
                    }
                },
                messages: {
                    PRM_NAMA_MATERIAL: {
                        required: 'Lokasi pemasangan harus dipilih'
                    },
                    PRM_DESKRIPSI: {
                        required: 'Nomor penugasan harus di isi'
                    }
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : element.data('placement')});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element).closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight
                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error'); // set success class to the control group
                    icon.removeClass("fa-warning");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();
                }
            });
        },

        userTable: function () {
            var table = $('#material-table');

            table.dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2] },

                    {
                        "aTargets": [ 3 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return ''+
                                '<a href="'+BASE_URL+'admin/master-data/material/detail?id='+full.ID_MATERIAL+'" class="btn btn-sm btn-success tooltips btn-message" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                '<a href="'+BASE_URL+'admin/master-data/material/update?id='+full.ID_MATERIAL+'" class="btn btn-sm btn-primary tooltips btn-message" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>'+
                                '<a href="#material-delete-'+full.ID_MATERIAL+'" class="btn btn-sm btn-danger tooltips btn-message" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+

                                '<div id="material-delete-'+full.ID_MATERIAL+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAMA_MATERIAL+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus permanen '+full.NAMA_MATERIAL+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<a href="'+BASE_URL+'/admin/master-data/material/delete?id='+full.ID_MATERIAL+'" class="btn btn-success">Ya</a>' +
                                '</div></div></div>';
                        }
                    }
                ],
//                "deferLoading": RECORDS_TOTAL,
                "pageLength": LENGTH,
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "columns": [
                    {"data": "ROW_NUMBER", "defaultContent": ""},
                    {"data": "NAMA_MATERIAL", "defaultContent": ""},
                    {"data": "DESKRIPSI", "defaultContent": ""}


                ],
                "lengthMenu": [
                    [5, 15, 20, RECORDS_TOTAL],
                    [5, 15, 20, "Semua"] // change per page values here
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();

                    //Comment.ajaxMessage();
                }


            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
            $('.DTTT').closest('.col-md-12').remove();
//            $('.DTTT .btn-group').remove();
        },


        // =========================================================================
        // HANDLE GANTI STATUS MENJADI 1
        // =========================================================================
        ajaxMessage:function(){
            $('#message-table .btn-message').on('click', function (e) {
                var i = $(this);
                $.ajax({
                    method: "GET",
                    url: BASE_URL+"admin/message/update?id="+$(this).data('id'),
                    success: function() {
                        i.closest("tr").addClass("success");
                    }
                });

            });
        }
    };
}();

MasterDataMaterial.init();


