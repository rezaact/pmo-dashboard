var table;

var HistoryProductFile = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            HistoryProductFile.initSelectJenisUploload();
            HistoryProductFile.callbackTab();
            HistoryProductFile.fileTable();
        },

        fileTable: function () {
            table = $('#file-list-table').DataTable({
                "ajax": {
                    "url": BASE_URL + 'admin/history/findAllProductHistoryFileAll',
                    "data" : {
                        PRM_ID_HISTORY : PRM_ID_HISTORY //'CRYN-1M03T', //$('#form-product [name="PRM_NOMOR_PRODUK"]').val()
                    },
                    "type": "GET"
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2, 3, 4, 5 ] },
                    {
                        "aTargets": [ 5 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<a href="#product-delete-'+full.ID_PRODUCT_HISTORY_FILES+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                    //-----Modal untuk delete image ------
                                '<div id="product-delete-'+full.ID_PRODUCT_HISTORY_FILES+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAME+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus file '+full.NAME+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<button class="btn btn-success" onclick="hapusHistoryProductFile(\'' + full.ID_PRODUCT_HISTORY_FILES + '\')" data-dismiss="modal" aria-hidden="true">Ya</button>'+
                                    /*'<a href="'+BASE_URL+'admin/product/delete?id='+full.NOMOR_PRODUK+'" class="btn btn-success">Ya</a>'+*/
                                '</div></div></div></div>'
                                ;
                        }
                    }
                ],
                "columns": [
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "NOMOR_PENUGASAN", "defaultContent": ""},
                    {"data": "NAME", "defaultContent": ""},
                    {"data": "DOC_TYPE", "defaultContent": ""},
                    {"data": "CONTENT_TYPE", "defaultContent": ""}

                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                }

            });
            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
            $('.tabletools-dropdown-on-portlet').closest('.row').remove();
        },

        initSelectJenisUploload : function(){
            $('#select2-jns-upload').on("select2:select", function (e) {
                console.log('id = ' + e.params.data.id);
                $('#fileupload-history [name="PRM_JENIS_UPLOAD"]').val(e.params.data.id)
                $('#fileupload-history').show();
            });
        },


        // =========================================================================
        // HANDLE CALLBACK TAB FILE
        // =========================================================================
        callbackTab: function () {
            $('#tab-product-file a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                table.ajax.reload();
                $('#fileupload-history').hide();
                if(INIT_PLUGING_MULTI_UPLOAD==0){
                    HistoryProductFile.uploadFileHistory();
                    //ManagementProductFile.uploadFileCAD();
                }
                INIT_PLUGING_MULTI_UPLOAD ++;
            });

            $('#tab-ls-daftar-file a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                table.ajax.reload();
            });
        },

        // =========================================================================
        // HANDLE UPLOAD FILE PDF
        // =========================================================================
        uploadFileHistory : function () {
            // Initialize the jQuery File Upload widget:
            $('#fileupload-history').fileupload({
//                    disableImageResize: false
                autoUpload: false,
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 500000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|dwg|xls|xlsx|cad)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                filesContainer: $('.files-pdf'),
                uploadTemplateId: null,
                downloadTemplateId: null,
                /*add: function (e, data) {
                 console.log('on submit');
                 //data.submit();
                 },*/
                uploadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        console.log(file);
                        var row = $('<tr class="template-upload fade">' +
                            //'<td><span class="preview"></span></td>' +
                        '<td><i class="fa fa-file-text" style="font-size: 45px;line-height: 45px"></i></td>' +
                        '<td><p class="name"></p>' +
                        '<div class="error"></div>' +
                        '</td>' +
                        '<td><p class="size"></p>' +
                        '<div class="progress"></div>' +
                        '</td>' +
                        '<td>' +
                        (!index && !o.options.autoUpload ?
                            '<button class="start btn btn-primary" disabled>Start</button>' : '') +
                        (!index ? '<button class="cancel btn btn-danger">Cancel</button>' : '') +
                        '</td>' +
                        '</tr>');
                        row.find('.name').text(file.name);
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                },
                downloadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        var row = $('<tr class="template-download fade">' +
                            //'<td><span class="preview"></span></td>' +
                        '<td><i class="fa fa-file-text" style="font-size: 45px;line-height: 45px"></i></td>' +
                        '<td><p class="name"></p>' +
                        (file.error ? '<div class="error"></div>' : '') +
                        '</td>' +
                        '<td><span class="size"></span></td>' +
                        '<td><button class="delete btn btn-success">Done</button></td>' +
                        '</tr>');
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.name').text(file.name);
                            row.find('.error').text(file.error);
                        } else {
                            row.find('.name').append($('<a target="_blank"></a>').text(file.name));
                            if (file.thumbnailUrl) {
                                row.find('.preview').append(
                                    $('<a></a>').append(
                                        $('<img>').prop('src', file.thumbnailUrl)
                                    )
                                );
                            }
                            row.find('a')
                                .attr('href', BASE_URL+file.url);
                            row.find('button.delete')
                                .attr('data-type', file.delete_type)
                                .attr('data-url', file.deleteUrl);

                            console.log('delete_url = ' + file.deleteUrl);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                }
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileupload-history').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // delete files
            $('#fileupload-history')
                .bind('fileuploaddestroy', function (e, data) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + data.url,
                        success: function (response) {
                            alert('Gambar berhasil Dihapus');
                        }
                    });
                })
                .bind('fileuploaddone', function (e, data) {/* ... */
                    console.log('upload done');
                    console.log(data);
                })
            ;

        }

    };
}();

HistoryProductFile.init();

function testFunction(idFile){

    console.log('id_file = ' + idFile);

    $.ajax({
        type: "GET",
        url: BASE_URL + 'admin/product/set_primary_2D',
        data:{
            prm_nomor_produk : PRM_NOMOR_PRODUK,
            prm_id_file : idFile
        },
        success: function (response) {
            alert('Gambar berhasil Diaktifkan menjadi Gambar Utama');
            table.ajax.reload();
        }
    });

};


function hapusHistoryProductFile(idHistoryProductFile){
    //alert('hapusHistoryProductFile. id = ' + idHistoryProductFile);
    $.ajax({
        type: "POST",
        url: BASE_URL + 'admin/history/hapus_product_history_file',
        data:{
            prm_id_product_history_file : idHistoryProductFile
        },
        success: function (response) {
            alert(response.message);
            table.ajax.reload();
        }
    });
};

