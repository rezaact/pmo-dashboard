var ManagementUser = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementUser.userTable();
        },

        // ===========================================id_role==============================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================

        userTable: function () {
            var table = $('#user-table');

            table.dataTable({
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 1, 2, 3, 6 ] },
                    {
                        "aTargets": [ 1 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<a target="_blank" href="mailto:'+full.ALAMAT_EMAIL+'">'+full.ALAMAT_EMAIL+'</a>';
                        }
                    },
                    {
                        "aTargets": [ 3 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<span class="label label-sm '+full.LABEL_CLASS+'"> '+full.STATUS+' </span>';
                        }
                    },
                    {
                        "aTargets": [ 5 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            if(full.AKTIF == 1){
                                if(full.ID_ROLE == 1){
                                    return '<select class="form-control select2me id_role" name="id_role" data-id="'+full.ID_USER+'">'+
                                        '<option value="0">Pilih...</option>' +
                                        '<option value="1" selected>Administrator</option>' +
                                        '<option value="2">Secret User</option>' +
                                        '<option value="3">Private User</option>' +
                                        '<option value="4">Register User</option>';
                                }
                                if(full.ID_ROLE == 2){
                                    return '<select class="form-control select2me id_role" name="id_role" data-id="'+full.ID_USER+'">'+
                                        '<option value="0">Pilih...</option>' +
                                        '<option value="1">Administrator</option>' +
                                        '<option value="2" selected>Secret User</option>' +
                                        '<option value="3">Private User</option>' +
                                        '<option value="4">Register User</option>';
                                }
                                if(full.ID_ROLE == 3){
                                    return '<select class="form-control select2me id_role" name="id_role" data-id="'+full.ID_USER+'">'+
                                        '<option value="0">Pilih...</option>' +
                                        '<option value="1">Administrator</option>' +
                                        '<option value="2">Secret User</option>' +
                                        '<option value="3" selected>Private User</option>' +
                                        '<option value="4">Register User</option>';
                                }
                                if(full.ID_ROLE == 4){
                                    return '<select class="form-control select2me id_role" name="id_role" data-id="'+full.ID_USER+'">'+
                                        '<option value="0">Pilih...</option>' +
                                        '<option value="1">Administrator</option>' +
                                        '<option value="2">Secret User</option>' +
                                        '<option value="3">Private User</option>' +
                                        '<option value="4" selected>Register User</option>';
                                }
                            }
                        }
                    },
                    {
                        "aTargets": [ 6 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            if(full.AKTIF == 0){
                                return 'ini banned'
                            }
                            if(full.AKTIF == 1){
                                return '<a href="'+BASE_URL+'admin/user/update?id='+full.ID_USER+'" class="btn btn-sm btn-primary tooltips" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>' +
                                    '<a href="'+BASE_URL+'admin/user/detail?id='+full.ID_USER+'" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail" ><i class="fa fa-eye"></i></a>' +
                                    '<a href="#user-ban-'+full.ID_USER+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Banned" data-toggle="modal"><i class="fa fa-trash"></i></a>' +
                                    '<div id="user-ban-'+full.ID_USER+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">' +
                                    '<div class="modal-dialog">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header bg-red">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                                    '<h4 class="modal-title">Banned</h4>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<p style="margin: 0px">' +
                                    'Apakah anda yakin akan banned akun '+full.USERNAME+' ?' +
                                    '</p>' +
                                    '</div>' +
                                    '<div class="modal-footer">' +
                                    '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>' +
                                    '<a href="'+BASE_URL+'/admin/user/banned?id='+full.ID_USER+'" class="btn btn-success">Ya</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>';
                            }
                            if(full.AKTIF == 2){
                                return '<div class="actions">' +
                                    '<div class="btn-group">' +
                                    '<a class="btn btn-sm green tooltips" href="javascript:;" data-toggle="dropdown" data-container="body" data-placement="top"  data-original-title="Approve" >' +
                                    '<i class="fa fa-check"></i>' +
                                    '</a>' +
                                    '<ul class="dropdown-menu pull-right">' +

                                    '<li>' +
                                    '<a href="'+BASE_URL+'admin/user/approve?id='+full.ID_USER+'&role=1" data-val="1" data-id="'+full.ID_USER+'" class="approve">' +
                                    '<i class="fa fa-user"></i> Administrator' +
                                    '</a>' +
                                    '</li>' +

                                    '<li>' +
                                    '<a href="'+BASE_URL+'admin/user/approve?id='+full.ID_USER+'&role=2" data-val="2" data-id="'+full.ID_USER+'" class="approve">' +
                                    '<i class="fa fa-user"></i> Secret User' +
                                    '</a>' +
                                    '</li>' +

                                    '<li>' +
                                    '<a href="'+BASE_URL+'admin/user/approve?id='+full.ID_USER+'&role=3" data-val="3" data-id="'+full.ID_USER+'" class="approve">' +
                                    '<i class="fa fa-user"></i> Private User' +
                                    '</a>' +
                                    '</li>' +

                                    '<li>' +
                                    '<a href="'+BASE_URL+'admin/user/approve?id='+full.ID_USER+'&role=4" data-val="4" data-id="'+full.ID_USER+'" class="approve">' +
                                    '<i class="fa fa-user"></i> Register User' +
                                    '</a>' +
                                    '</li>' +

                                    '</ul>' +
                                    '</div>' +

                                    '<a href="'+BASE_URL+'/admin/user/detail?id='+full.ID_USER+'" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Detail"><i class="fa fa-eye"></i></a>' +
                                    '<a href="#user-reject-'+full.ID_USER+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Disapprove" data-toggle="modal"><i class="fa fa-close"></i></a>' +
                                    '<div id="user-reject-'+full.ID_USER+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">' +
                                    '<div class="modal-dialog">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header bg-red">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                                    '<h4 class="modal-title">Disapprove</h4>' +
                                    '</div>' +

                                    '<div class="modal-body">' +
                                    '<p style="margin: 0px">' +
                                    'Apakah anda yakin akan menghapus permanen akun '+full.USERNAME+' ?' +
                                    '</p>' +
                                    '</div>' +

                                    '<div class="modal-footer">' +
                                    '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>' +
                                    '<a href="'+BASE_URL+'/admin/user/disapprove?id='+full.ID_USER+'" class="btn btn-success">Ya</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>'
                            }
                            if(full.AKTIF == 0 || full.AKTIF == 3){
                                return '<a href="#user-delete-'+full.ID_USER+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>' +
                                    '<div id="user-delete-'+full.ID_USER+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">' +
                                    '<div class="modal-dialog">' +
                                    '<div class="modal-content">' +
                                    '<div class="modal-header bg-red">' +
                                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                                    '<h4 class="modal-title">Hapus Akun</h4>' +
                                    '</div>' +
                                    '<div class="modal-body">' +
                                    '<p style="margin: 0px">' +
                                    'Apakah anda yakin akan menghapus permanen '+full.USERNAME+' ?' +
                                    '</p>' +
                                    '</div>' +

                                    '<div class="modal-footer">' +
                                    '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>' +
                                    '<a href="'+BASE_URL+'/admin/user/delete?id='+full.ID_USER+'" class="btn btn-success">Ya</a>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +
                                    '</div>'
                            }
                        }
                    }
                ],
                "columns": [
                    {"data": "USERNAME", "defaultContent": ""},
                    {"data": "ALAMAT_EMAIL", "defaultContent": ""},
                    {"data": "NO_TELPON", "defaultContent": ""},
                    {"data": "AKTIF", "defaultContent": ""},
                    {"data": "TGL_REGISTER", "defaultContent": ""},
                    {"data": "ID_RULE", "defaultContent": ""}
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                    if ($().select2) {
                        $('.select2me').select2({
                            placeholder: "Select",
                            allowClear: true
                        });
                    }
                    ManagementUser.userApprove();
                }
            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

            // Remove table tool
            $('.tabletools-dropdown-on-portlet').closest('.row').remove();
        },
        userApprove: function(){
            $('.id_role').on('change', function(){
                $.ajax({
                    type: "GET",
                    url: BASE_URL + "admin/user/approve?id="+$(this).data("id")+"&role="+$(this).val(),

                    success: function (response) {
//                        console.log(response);
//                        var arrTemp = response.split(":");
//                        if (arrTemp[0] == "0") {
//                            error.show();
//                            success.hide();
//                        } else {
//                            error.hide();
//                            success.show();
//                            //form[0].reset();
                            location.reload(true);
//                        }
                    }
                });
            });

//            $('.approve').on('click', function(){
//                $.ajax({
//                    type: "POST",
//                    url: BASE_URL + "admin/user/approve?id="+$(this).data("id"),
//                    data:{
//                        prm_id_role:$(this).data('val')
//                    },
//                    success: function (response) {
////                        console.log(response);
////                        var arrTemp = response.split(":");
////                        if (arrTemp[0] == "0") {
////                            error.show();
////                            success.hide();
////                        } else {
////                            error.hide();
////                            success.show();
////                            //form[0].reset();
//                        console.log();
////                        location.reload(true);
////                        }
//                    }
//                });
//            });
        }
    };
}();

ManagementUser.init();


