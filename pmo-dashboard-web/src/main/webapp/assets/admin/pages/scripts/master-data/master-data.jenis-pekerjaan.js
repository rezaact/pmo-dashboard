var MasterDataJenisPekerjaan = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            MasterDataJenisPekerjaan.userTable();

        },

        userTable: function () {
            var table = $('#jenis-pekerjaan-table');

            table.dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0,2] },

                    {
                        "aTargets": [ 3 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return ''+
                                '<a href="'+BASE_URL+'admin/master-data/jenis-pekerjaan/detail?id='+full.ID_JENIS_PEKERJAAN+'" class="btn btn-sm btn-success tooltips btn-message" data-container="body" data-placement="top" data-original-title="Detail" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                '<a href="'+BASE_URL+'admin/master-data/jenis-pekerjaan/update?id='+full.ID_JENIS_PEKERJAAN+'" class="btn btn-sm btn-primary tooltips btn-message" data-container="body" data-placement="top" data-original-title="Edit" data-toggle="modal"><i class="fa fa-edit"></i></a>'+
                                '<a href="#jenis-pekerjaan-delete-'+full.ID_JENIS_PEKERJAAN+'" class="btn btn-sm btn-danger tooltips btn-message" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+

                                '<div id="jenis-pekerjaan-delete-'+full.ID_JENIS_PEKERJAAN+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.JENIS_PEKERJAAN+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus permanen '+full.JENIS_PEKERJAAN+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<a href="'+BASE_URL+'/admin/master-data/jenis-pekerjaan/delete?id='+full.ID_JENIS_PEKERJAAN+'" class="btn btn-success">Ya</a>' +
                                '</div></div></div>';
                        }
                    }
                ],
//                "deferLoading": RECORDS_TOTAL,
                "pageLength": LENGTH,
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "columns": [
                    {"data": "ROW_NUMBER", "defaultContent": ""},
                    {"data": "JENIS_PEKERJAAN", "defaultContent": ""},
                    {"data": "DESKRIPSI", "defaultContent": ""}


                ],
                "lengthMenu": [
                    [5, 15, 20, RECORDS_TOTAL],
                    [5, 15, 20, "Semua"] // change per page values here
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();

                    //Comment.ajaxMessage();
                }


            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
            $('.DTTT').closest('.col-md-12').remove();
//            $('.DTTT .btn-group').remove();
        }

    };
}();

MasterDataJenisPekerjaan.init();


