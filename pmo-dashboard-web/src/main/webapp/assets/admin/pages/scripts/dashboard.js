var Dashboard = function () {

    return {
        //main function to initiate map samples
        init: function () {
            //Dashboard.resizeHandle();
            //Dashboard.visitorGraph();
            //Dashboard.listWorkshop();
            //Dashboard.rabTable();
            Dashboard.testFusionChart();
            Dashboard.testFusionMap();
        },

        testFusionChart : function(){
            new FusionCharts({
                'type': 'msstackedcolumn2dlinedy',
                'renderAt': 'testFusionChart',
                'width': '100%',
                'height': '100%',
                'bgAlpha': '100%',
                'dataFormat': 'json',
                'dataSource': {
                    "chart": {
                        "caption": "Quarterly Sales vs. Profit % in Last Year",
                        "subcaption": "Product-wise Break-up - Harry's SuperMart",
                        "xAxisName": "Quarter",
                        "pYAxisName": "Sales",
                        "sYAxisName": "Profit %",
                        "numberPrefix": "$",
                        "numbersuffix": "M",
                        "sNumberSuffix": "%",
                        "sYAxisMaxValue": "25",
                        "paletteColors": "#5598c3,#2785c3,#31cc77,#1aaf5d,#f45b00",
                        "baseFontColor": "#333333",
                        "baseFont": "Helvetica Neue,Arial",
                        "captionFontSize": "14",
                        "subcaptionFontSize": "14",
                        "subcaptionFontBold": "0",
                        "showBorder": "0",
                        "bgColor": "#ffffff",
                        "showShadow": "0",
                        "canvasBgColor": "#ffffff",
                        "canvasBorderAlpha": "0",
                        "divlineAlpha": "100",
                        "divlineColor": "#999999",
                        "divlineThickness": "1",
                        "divLineDashed": "1",
                        "divLineDashLen": "1",
                        "usePlotGradientColor": "0",
                        "showplotborder": "0",
                        "valueFontColor": "#ffffff",
                        "placeValuesInside": "1",
                        "showXAxisLine": "1",
                        "xAxisLineThickness": "1",
                        "xAxisLineColor": "#999999",
                        "showAlternateHGridColor": "0",
                        "legendBgAlpha": "0",
                        "legendBorderAlpha": "0",
                        "legendShadow": "0",
                        "legendItemFontSize": "10",
                        "legendItemFontColor": "#666666"
                    },
                    "categories": [
                        {
                            "category": [
                                {
                                    "label": "Q1"
                                },
                                {
                                    "label": "Q2"
                                },
                                {
                                    "label": "Q3"
                                },
                                {
                                    "label": "Q4"
                                }
                            ]
                        }
                    ],
                    "dataset": [
                        {
                            "dataset": [
                                {
                                    "seriesname": "Processed Food",
                                    "data": [
                                        {
                                            "value": "30"
                                        },
                                        {
                                            "value": "26"
                                        },
                                        {
                                            "value": "33"
                                        },
                                        {
                                            "value": "31"
                                        }
                                    ]
                                },
                                {
                                    "seriesname": "Un-Processed Food",
                                    "data": [
                                        {
                                            "value": "21"
                                        },
                                        {
                                            "value": "28"
                                        },
                                        {
                                            "value": "39"
                                        },
                                        {
                                            "value": "41"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "dataset": [
                                {
                                    "seriesname": "Electronics",
                                    "data": [
                                        {
                                            "value": "27"
                                        },
                                        {
                                            "value": "25"
                                        },
                                        {
                                            "value": "28"
                                        },
                                        {
                                            "value": "26"
                                        }
                                    ]
                                },
                                {
                                    "seriesname": "Apparels",
                                    "data": [
                                        {
                                            "value": "17"
                                        },
                                        {
                                            "value": "15"
                                        },
                                        {
                                            "value": "18"
                                        },
                                        {
                                            "value": "16"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "dataset": [
                                {
                                    "seriesname": "Google",
                                    "data": [
                                        {
                                            "value": "22"
                                        },
                                        {
                                            "value": "17"
                                        },
                                        {
                                            "value": "23"
                                        },
                                        {
                                            "value": "21"
                                        }
                                    ]
                                },
                                {
                                    "seriesname": "Apple",
                                    "data": [
                                        {
                                            "value": "12"
                                        },
                                        {
                                            "value": "10"
                                        },
                                        {
                                            "value": "13"
                                        },
                                        {
                                            "value": "11"
                                        }
                                    ]
                                }
                            ]
                        }
                    ],
                    "lineset": [
                        {
                            "seriesname": "Profit %",
                            "showValues": "0",
                            "data": [
                                {
                                    "value": "14"
                                },
                                {
                                    "value": "16"
                                },
                                {
                                    "value": "15"
                                },
                                {
                                    "value": "17"
                                }
                            ]
                        },
                        {
                            "seriesname": "ROI %",
                            "showValues": "0",
                            "data": [
                                {
                                    "value": "10"
                                },
                                {
                                    "value": "9"
                                },
                                {
                                    "value": "7"
                                },
                                {
                                    "value": "14"
                                }
                            ]
                        }
                    ]
                }
            }).render();
        },

        testFusionMap : function(){
            new FusionCharts({
                'type': 'maps/indonesia',
                'renderAt': 'testFusionMap',
                'width': '100%',
                'height': '100%',
                'bgAlpha': '100%',
                'dataFormat': 'json',
                'dataSource': {
                    "chart": {
                        "caption": "Annual Sales by State",
                        "subcaption": "Last year",
                        "entityFillHoverColor": "#cccccc",
                        "numberScaleValue": "1,1000,1000",
                        "numberScaleUnit": "K,M,B",
                        "numberPrefix": "$",
                        "showLabels": "1",
                        "theme": "fint"
                    },
                    /*"colorrange": {
                        "minvalue": "0",
                        "startlabel": "Low",
                        "endlabel": "High",
                        "code": "#e44a00",
                        "gradient": "1",
                        "color": [
                            {
                                "maxvalue": "56580",
                                "displayvalue": "Average",
                                "code": "#f8bd19"
                            },
                            {
                                "maxvalue": "100000",
                                "code": "#6baa01"
                            }
                        ]
                    },*/
                    "data": [
                        {
                            "id": "01",
                            "value": "3189"
                        },
                        {
                            "id": "02",
                            "value": "2879"
                        },
                        {
                            "id": "03",
                            "value": "920"
                        },
                        {
                            "id": "04",
                            "value": "4607"
                        },
                        {
                            "id": "05",
                            "value": "4890"
                        },
                        {
                            "id": "06",
                            "value": "34927"
                        },
                        {
                            "id": "07",
                            "value": "65798"
                        },
                        {
                            "id": "08",
                            "value": "61861"
                        },
                        {
                            "id": "09",
                            "value": "58911"
                        },
                        {
                            "id": "10",
                            "value": "42662"
                        },
                        {
                            "id": "11",
                            "value": "78041"
                        },
                        {
                            "id": "12",
                            "value": "41558"
                        },
                        {
                            "id": "13",
                            "value": "62942"
                        },
                        {
                            "id": "14",
                            "value": "78834"
                        },
                        {
                            "id": "15",
                            "value": "50512"
                        },
                        {
                            "id": "16",
                            "value": "73026"
                        },
                        {
                            "id": "17",
                            "value": "78865"
                        },
                        {
                            "id": "18",
                            "value": "50554"
                        },
                        {
                            "id": "19",
                            "value": "35922"
                        },
                        {
                            "id": "20",
                            "value": "43736"
                        },
                        {
                            "id": "21",
                            "value": "32681"
                        },
                        {
                            "id": "22",
                            "value": "79038"
                        },
                        {
                            "id": "23",
                            "value": "75425"
                        },
                        {
                            "id": "24",
                            "value": "43485"
                        },
                        {
                            "id": "25",
                            "value": "46515"
                        },
                        {
                            "id": "26",
                            "value": "63715"
                        },
                        {
                            "id": "27",
                            "value": "34497"
                        },
                        {
                            "id": "28",
                            "value": "70706"
                        },
                        {
                            "id": "29",
                            "value": "42382"
                        },
                        {
                            "id": "30",
                            "value": "73202"
                        },
                        {
                            "id": "31",
                            "value": "79118"
                        },
                        {
                            "id": "32",
                            "value": "44657"
                        },
                        {
                            "id": "33",
                            "value": "66205"
                        },
                        {
                            "id": "SB",
                            "value": "75873"
                        },
                        {
                            "id": "YO",
                            "value": "76895"
                        }
                    ]
                }
            }).render();
        },

        showChartTooltip : function (x, y, xValue, yValue) {
        $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
            position: 'absolute',
            display: 'none',
            top: y - 40,
            left: x - 40,
            border: '0px solid #ccc',
            padding: '2px 6px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
        },

        resizeHandle : function () {
            Metronic.addResizeHandler(function () {
                jQuery('.vmaps').each(function () {
                    var map = jQuery(this);
                    map.width(map.parent().width());
                });
            });
        },

        visitorGraph: function () {
            if (!jQuery.plot) {
                return;
            }
            var data = [];
            var totalPoints = 250;

            // random data generator for plot charts

            function getRandomData() {
                if (data.length > 0) data = data.slice(1);
                // do a random walk
                while (data.length < totalPoints) {
                    var prev = data.length > 0 ? data[data.length - 1] : 50;
                    var y = prev + Math.random() * 10 - 5;
                    if (y < 0) y = 0;
                    if (y > 100) y = 100;
                    data.push(y);
                }
                // zip the generated y values with the x values
                var res = [];
                for (var i = 0; i < data.length; ++i) res.push([i, data[i]])
                return res;
            }

            function randValue() {
                return (Math.floor(Math.random() * (1 + 50 - 20))) + 10;
            }

            Dashboard.topProject();
            Dashboard.hitStatistic();

        },

        topProject: function () {
            console.log(visitors);
            /*var visitors = [
                ['02/2013', 1500],
                ['03/2013', 2500],
                ['04/2013', 1700],
                ['05/2013', 800],
                ['06/2013', 1500],
                ['07/2013', 2350],
                ['08/2013', 1500],
                ['09/2013', 1300],
                ['10/2013', 4600]
            ];*/

            if ($('#site_statistics').size() != 0) {

                $('#site_statistics_loading').hide();
                $('#site_statistics_content').show();

                var plot_statistics = $.plot($("#site_statistics"),
                    [{
                        data: visitors,
                        lines: {
                            fill: 0.6,
                            lineWidth: 0
                        },
                        color: ['#f89f9f']
                    }, {
                        data: visitors,
                        points: {
                            show: true,
                            fill: true,
                            radius: 5,
                            fillColor: "#f89f9f",
                            lineWidth: 3
                        },
                        color: '#fff',
                        shadowSize: 0
                    }],

                    {
                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                var previousPoint = null;
                $("#site_statistics").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                            Dashboard.showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Project');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

            Dashboard.topProjectFilterBy();
            Dashboard.topProjectFilterRange();
        },

        topProjectFilterBy: function () {
            $('#top-project-filter-by a').on('click', function () {
                $.ajax({
                    type: 'GET',
                    url: 'xxx',
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#site_statistics_content'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        // menghilangkan loading jika data telah sukses
                        Metronic.unblockUI($('#site_statistics_content'));
                        //console.table(response);
                        $('#fillContainer').html(response);
                    }
                });
            })
        },

        topProjectFilterRange: function () {
            $('#top-project-filter-range a').on('click', function () {
                $.ajax({
                    type: 'GET',
                    url: 'xxx',
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#site_statistics_content'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        // menghilangkan loading jika data telah sukses
                        Metronic.unblockUI($('#site_statistics_content'));
                        //console.table(response);
                        $('#fillContainer').html(response);
                    }
                });
            })
        },

        hitStatistic: function () {
            if ($('#site_activities').size() != 0) {
                //site activities
                var previousPoint2 = null;
                $('#site_activities_loading').hide();
                $('#site_activities_content').show();

                /*var data1 = [
                    ['DEC', 300],
                    ['JAN', 600],
                    ['FEB', 1100],
                    ['MAR', 1200],
                    ['APR', 860],
                    ['MAY', 1200],
                    ['JUN', 1450],
                    ['JUL', 1800],
                    ['AUG', 1200],
                    ['SEP', 600]
                ];*/


                var plot_statistics = $.plot($("#site_activities"),

                    [{
                        data: counterHitArray,
                        lines: {
                            fill: 0.2,
                            lineWidth: 0,
                        },
                        color: ['#BAD9F5']
                    }, {
                        data: counterHitArray,
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#9ACAE6",
                            lineWidth: 2
                        },
                        color: '#9ACAE6',
                        shadowSize: 1
                    }, {
                        data: counterHitArray,
                        lines: {
                            show: true,
                            fill: false,
                            lineWidth: 3
                        },
                        color: '#9ACAE6',
                        shadowSize: 0
                    }],

                    {

                        xaxis: {
                            tickLength: 0,
                            tickDecimals: 0,
                            mode: "categories",
                            min: 0,
                            font: {
                                lineHeight: 18,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        yaxis: {
                            ticks: 5,
                            tickDecimals: 0,
                            tickColor: "#eee",
                            font: {
                                lineHeight: 14,
                                style: "normal",
                                variant: "small-caps",
                                color: "#6F7B8A"
                            }
                        },
                        grid: {
                            hoverable: true,
                            clickable: true,
                            tickColor: "#eee",
                            borderColor: "#eee",
                            borderWidth: 1
                        }
                    });

                $("#site_activities").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint2 != item.dataIndex) {
                            previousPoint2 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);
                            Dashboard.showChartTooltip(item.pageX, item.pageY, item.datapoint[0],item.datapoint[1]+' Hit(s)');
                        }
                    }
                });

                $('#site_activities').bind("mouseleave", function () {
                    $("#tooltip").remove();
                });
            }

            Dashboard.hitStatisticFilterRange();
        },

        hitStatisticFilterRange: function () {
            $('#hit-statistic-filter-range a').on('click', function () {
                $.ajax({
                    type: 'GET',
                    url: 'xxx',
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                            target: $('#site_statistics_content'),
                            boxed: true
                        });
                    },
                    success: function (response) {
                        // menghilangkan loading jika data telah sukses
                        Metronic.unblockUI($('#site_statistics_content'));
                        //console.table(response);
                        $('#fillContainer').html(response);
                    }
                });
            })
        },

        listWorkshop : function () {
            var map = new GMaps({
                div: '#gmap_marker',
                lat: -7.22233,
                lng: 110.42660,
                zoom: 7
            });

            var image = BASE_URL + 'assets/admin/layout/img/green.png';
            var uwp1 = new google.maps.Marker({
                title: 'UWP I MERAK',
                position: new google.maps.LatLng(-5.904363, 106.0169227),
                icon: image
            });
            map.addMarker(uwp1);

            var uwp2 = new google.maps.Marker({
                title: 'UWP II JAKARTA',
                position: new google.maps.LatLng(-6.210972, 106.904993),
                icon: image
            });
            map.addMarker(uwp2);

            var uwp3 = new google.maps.Marker({
                title: 'UWP III BANDUNG',
                position: new google.maps.LatLng(-6.91686, 107.640743),
                icon: image
            });
            map.addMarker(uwp3);

            var uwp4 = new google.maps.Marker({
                title: 'UWP IV DAYEUHKOLOT',
                position: new google.maps.LatLng(-6.9832005, 107.6230486),
                icon: image
            });
            map.addMarker(uwp4);

            var uwp5 = new google.maps.Marker({
                title: 'UWP V SEMARANG',
                position: new google.maps.LatLng(-6.986437, 110.375079),
                icon: image
            });
            map.addMarker(uwp5);

            var uwp6 = new google.maps.Marker({
                title: 'UWP VI SURABAYA',
                position: new google.maps.LatLng(-7.289674, 112.750596),
                icon: image
            });
            map.addMarker(uwp6);

            map.setZoom(7);
        },

        rabTable: function () {
            var table = $('#product-list-table');

            table.dataTable({
                "ajax": {
                    "url": BASE_URL + "dashboard/rab-json",
                    "type": "GET"
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2, 3 ] },
                ],
                "columns": [
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "NAMA_PRODUK", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "RAB_PRODUKSI", "defaultContent": ""},
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                }

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        }

    };

}();

Dashboard.init();