var ManagementUser = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementUser.formValidation();
            ManagementUser.updateValidation();
            ManagementUser.passValidation();
        },

        // =========================================================================
        // HANDLE DATATABLE PRODUCT
        // =========================================================================


        formValidation: function () {
            form = $('.user-form');
            error = $('.alert-empty', form);
            success = $('.alert-success', form);
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_NAMA_LENGKAP: {
                        required: true
                    },
                    PRM_NO_TELP: {
                        minlength:2,
                        required: true,
                        digits:true
                    },
                    PRM_EMAIL: {
                        required: true,
                        email: true
                    },

                    PRM_USERNAME: {
                        minlength:2,
                        required: true
                    },
                    PRM_PASSWORD: {
                        minlength:2,
                        required: true
                    },
                    PRM_KONF_PASSWORD:{
                         minlength:2,
                         required:true,
                         equalTo:'#password'
                     },
                    PRM_ID_ROLE:{
                        required:true
                        }

                },
                messages: {
                    PRM_NAMA_LENGKAP: "Nama lengkap harus diisi",
                    PRM_NO_TELP: "No. Telp harus diisi",
                    PRM_EMAIL: "Email harus diisi",
                    PRM_USERNAME: "Username harus diisi",
                    PRM_PASSWORD: "Password harus diisi",
                    PRM_KONF_PASSWORD: "Password harus sesuai",
                    PRM_ID_ROLE: "Role harus sesuai"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();

                }
            });
            $('.select2me', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        },
        updateValidation: function () {
            form = $('.update-form');
            error = $('.alert-danger', form);
            success = $('.alert-success', form);
            form.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    PRM_NAMA_LENGKAP: {
                        required: true
                    },
                    PRM_NO_TELP: {
                        minlength:2,
                        required: true,
                        digits:true
                    },
                    /*PRM_UNIT : {
                        minlength: 2,
                        maxlength: 30
                    },*/
                    PRM_EMAIL: {
                        required: true,
                        email: true
                    },
                    PRM_ID_ROLE:{
                        required:true
                    }

                },
                messages: {
                    PRM_NAMA_LENGKAP: "Nama lengkap harus diisi",
                    //PRM_UNIT: "min 2, max 30 karakter",
                    PRM_NO_TELP: "No. Telp harus diisi",
                    PRM_EMAIL: "Email harus diisi",
                    PRM_ID_ROLE: "Role harus sesuai"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (data) {
                    error.hide();
                    form[0].submit();

                }
            });
            $('.select2me', form).change(function () {
                form.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        },
        passValidation: function () {
           form1 = $('.pass-form');
           error1 = $('.alert-danger', form1);
           success1 = $('.alert-success', form1);
            form1.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",  // validate all fields including form hidden input
                rules: {
                    prm_password: {
                        minlength: 2,
                        required: true
                    },
                    prm_password_again: {
                        minlength: 2,
                        required: true,
                        equalTo: '#password'

                    }
                },
                messages: {
                    prm_password: "Password harus diisi",
                    prm_password_again: "Password tidak sesuai"
                },
                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    Metronic.scrollTo(error, -200);
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    var icon = $(element).parent('.input-icon').children('i');
                    icon.removeClass('fa-check').addClass("fa-warning");
                    icon.attr("data-original-title", error.text()).tooltip({'container': 'body', 'placement' : 'left'});
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').removeClass("has-success").addClass('has-error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change done by hightlight

                },

                success: function (label, element) {
                    var icon = $(element).parent('.input-icon').children('i');
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success'); // set success class to the control group
                    icon.removeClass("fa-warning").addClass("fa-check");
                },

                submitHandler: function (data) {
                    error.hide();
                    form1[0].submit();

                }
            });
            $('.select2me', form1).change(function () {
                form1.validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });
        }
    };
}();

ManagementUser.init();


