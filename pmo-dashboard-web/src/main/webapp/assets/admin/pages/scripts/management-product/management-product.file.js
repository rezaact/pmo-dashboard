var table;

var ManagementProductFile = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {

            ManagementProductFile.initSelectJenisUploload();
            ManagementProductFile.callbackTab();
            ManagementProductFile.fileTable();
        },

        fileTable: function () {
            table = $('#file-list-table').DataTable({
                "ajax": {
                    "url": BASE_URL + 'admin/product/get2Dthumbnail',
                    "data" : {
                        prm_nomor_produk : PRM_NOMOR_PRODUK, //'CRYN-1M03T', //$('#form-product [name="PRM_NOMOR_PRODUK"]').val()
                    },
                    "type": "GET"
                },
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0, 2, 3, 4 ] },
                    {
                        "aTargets": [ 0 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            if(full.DOC_TYPE == '012'){
                                return '<img class="img-responsive" src="'+BASE_URL+'file/'+data+'"/>';
                            }else{
                                return '<i class="fa fa-file-text" style="font-size: 40px;padding: 15px"></i>';
                            }
                        }
                    },
                    /*{
                        "aTargets": [ 3 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            return '<div class="table-limit-text">'+full.DESKRIPSI_PRODUK+'</div>';
                        }
                    },*/
                    {
                        "aTargets": [ 5 ], // Column to target
                        "mRender": function ( data, type, full ) {

                            var str_render = '';
                            if(full.DOC_TYPE == '012'){
                                str_render = '<a href="#set-primary-image_' + full.ID_FILE + '" class="btn btn-sm btn-success tooltips" data-container="body" data-placement="top" data-original-title="Set Gambar Utama" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                '<a href="#product-delete-'+full.ID_PRODUCT_FILES+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                    //-----Modal untuk seting primary image ------
                                '<div id="set-primary-image_' + full.ID_FILE + '" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-blue">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAME+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan mengaktifkan '+full.NAME+' menjadi gambar utama ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<button class="btn btn-success" onclick="testFunction(\'' + full.ID_FILE + '\')" data-dismiss="modal" aria-hidden="true">Ya</button>'+
                                '</div></div></div></div>' +
                                    //-----Modal untuk delete image ------
                                '<div id="product-delete-'+full.ID_PRODUCT_FILES+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAME+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus file '+full.NAME+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<button class="btn btn-success" onclick="hapusGambar2D(\'' + full.ID_PRODUCT_FILES + '\')" data-dismiss="modal" aria-hidden="true">Ya</button>'+
                                    /*'<a href="'+BASE_URL+'admin/product/delete?id='+full.NOMOR_PRODUK+'" class="btn btn-success">Ya</a>'+*/
                                '</div></div></div></div>';
                            }else{
                                str_render =
                                '<a href="#product-delete-'+full.ID_PRODUCT_FILES+'" class="btn btn-sm btn-danger tooltips" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                 //-----Modal untuk delete image ------
                                '<div id="product-delete-'+full.ID_PRODUCT_FILES+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAME+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus file '+full.NAME+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<button class="btn btn-success" onclick="hapusGambar2D(\'' + full.ID_PRODUCT_FILES + '\')" data-dismiss="modal" aria-hidden="true">Ya</button>'+
                                    /*'<a href="'+BASE_URL+'admin/product/delete?id='+full.NOMOR_PRODUK+'" class="btn btn-success">Ya</a>'+*/
                                '</div></div></div></div>';
                            }



                            return str_render;
                        }
                    }
                ],
                "columns": [
                    {"data": "ID_FILE", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "DOC_TYPE_DESC", "defaultContent": ""},
                    {"data": "NAME", "defaultContent": ""},
                    {"data": "CONTENT_TYPE", "defaultContent": ""}
                    //{"data": "FILE_SIZE", "defaultContent": ""}
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                }

            });
            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
            $('.tabletools-dropdown-on-portlet').closest('.row').remove();
        },





        initSelectJenisUploload : function(){
            $('#select2-jns-upload').on("select2:select", function (e) {
                console.log('id = ' + e.params.data.id);
                $('#fileupload-pdf [name="PRM_JENIS_UPLOAD"]').val(e.params.data.id)
                $('#fileupload-pdf').show();
            });
        },


        // =========================================================================
        // HANDLE CALLBACK TAB FILE
        // =========================================================================
        callbackTab: function () {
            $('#tab-product-file a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                table.ajax.reload();
                $('#fileupload-pdf').hide();
                if(INIT_PLUGING_MULTI_UPLOAD==0){
                    ManagementProductFile.uploadFilePDF();
                    //ManagementProductFile.uploadFileCAD();
                }
                INIT_PLUGING_MULTI_UPLOAD ++;
            });

            $('#tab-ls-daftar-file a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                table.ajax.reload();
            });
        },

        // =========================================================================
        // HANDLE UPLOAD FILE PDF
        // =========================================================================
        uploadFilePDF : function () {
            // Initialize the jQuery File Upload widget:
            $('#fileupload-pdf').fileupload({
                    disableImageResize: false,
                imageMaxWidth: 372,
                imageMaxHeight: 372,
                imageCrop: true, // Force cropped images
                autoUpload: false,
//                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 500000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|dwg|xls|xlsx|cad)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                filesContainer: $('.files-pdf'),
                uploadTemplateId: null,
                downloadTemplateId: null,
                /*add: function (e, data) {
                    console.log('on submit');
                    //data.submit();
                },*/
                uploadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        console.log(file);
                        var row = $('<tr class="template-upload fade">' +
                            //'<td><span class="preview"></span></td>' +
                            '<td><i class="fa fa-file-text" style="font-size: 45px;line-height: 45px"></i></td>' +
                            '<td><p class="name"></p>' +
                            '<div class="error"></div>' +
                            '</td>' +
                            '<td><p class="size"></p>' +
                            '<div class="progress"></div>' +
                            '</td>' +
                            '<td>' +
                            (!index && !o.options.autoUpload ?
                                '<button class="start btn btn-primary" disabled>Start</button>' : '') +
                            (!index ? '<button class="cancel btn btn-danger">Cancel</button>' : '') +
                            '</td>' +
                            '</tr>');
                        row.find('.name').text(file.name);
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                },
                downloadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        var row = $('<tr class="template-download fade">' +
                            //'<td><span class="preview"></span></td>' +
                            '<td><i class="fa fa-file-text" style="font-size: 45px;line-height: 45px"></i></td>' +
                            '<td><p class="name"></p>' +
                            (file.error ? '<div class="error"></div>' : '') +
                            '</td>' +
                            '<td><span class="size"></span></td>' +
                            '<td><button class="delete btn btn-success">Done</button></td>' +
                            '</tr>');
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.name').text(file.name);
                            row.find('.error').text(file.error);
                        } else {
                            row.find('.name').append($('<a target="_blank"></a>').text(file.name));
                            if (file.thumbnailUrl) {
                                row.find('.preview').append(
                                    $('<a></a>').append(
                                        $('<img>').prop('src', file.thumbnailUrl)
                                    )
                                );
                            }
                            row.find('a')
                                .attr('href', BASE_URL+file.url);
                            row.find('button.delete')
                                .attr('data-type', file.delete_type)
                                .attr('data-url', file.deleteUrl);

                            console.log('delete_url = ' + file.deleteUrl);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                }
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileupload-pdf').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Upload server status check for browsers with CORS support:
//                if ($.support.cors) {
//                    $.ajax({
//                        type: 'HEAD'
//                    }).fail(function () {
//                        $('<div class="alert alert-danger"/>')
//                            .text('Upload server currently unavailable - ' +
//                                new Date())
//                            .appendTo('#fileupload-pdf');
//                    });
//                }

            // Load & display existing files:
            //$('#fileupload-pdf').addClass('fileupload-processing');
            /*$.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload-pdf').attr("action"),
                data : {
                    nomor_produk : $('#fileupload-pdf [name="PRM_NOMOR_PRODUK"]').val()
                },
                dataType: 'json',
                context: $('#fileupload-pdf')[0]
            }).always(function (e) {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
            });*/

            // delete files
            $('#fileupload-pdf')
                .bind('fileuploaddestroy', function (e, data) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + data.url,
                        success: function (response) {
                            alert('Gambar berhasil Dihapus');
                        }
                    });
                })
                .bind('fileuploaddone', function (e, data) {/* ... */
                    console.log('upload done');
                    console.log(data);
                })
            ;

        },

        // =========================================================================
        // HANDLE UPLOAD FILE 2D THUMBNAIL
        // =========================================================================
        uploadFileCAD : function () {
            // Initialize the jQuery File Upload widget:
            $('#fileupload-cad').fileupload({
//                    disableImageResize: false,
                autoUpload: false,
                disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                maxFileSize: 5000000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|cad|dwg)$/i,
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                filesContainer: $('.files-cad'),
                uploadTemplateId: null,
                downloadTemplateId: null,
                uploadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        var row = $('<tr class="template-upload fade">' +
                            '<td><span class="preview"></span></td>' +
                            //'<td><i class="fa fa-file-text" style="font-size: 45px;line-height: 45px"></i></td>' +
                            '<td><p class="name"></p>' +
                            '<div class="error"></div>' +
                            '</td>' +
                            '<td><p class="size"></p>' +
                            '<div class="progress"></div>' +
                            '</td>' +
                            '<td>' +
                            (!index && !o.options.autoUpload ?
                                '<button class="start btn btn-primary" disabled>Start</button>' : '') +
                            (!index ? '<button class="cancel btn btn-danger">Cancel</button>' : '') +
                            '</td>' +
                            '</tr>');
                        row.find('.name').text(file.name);
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.error').text(file.error);
                        }
                        rows = rows.add(row);
                    });
                    return rows;
                },
                downloadTemplate: function (o) {
                    var rows = $();
                    $.each(o.files, function (index, file) {
                        var row = $('<tr class="template-download fade">' +
                            '<td><span class="preview"></span></td>' +
                            '<td><p class="name"></p>' +
                            (file.error ? '<div class="error"></div>' : '') +
                            '</td>' +
                            '<td><span class="size"></span></td>' +
                            '<td><button class="delete btn btn-danger">Delete</button></td>' +
                            '</tr>');
                        row.find('.size').text(o.formatFileSize(file.size));
                        if (file.error) {
                            row.find('.name').text(file.name);
                            row.find('.error').text(file.error);
                        } else {
                            row.find('.name').append($('<a></a>').text(file.name));
                            if (file.thumbnailUrl) {
                                row.find('.preview').append(
                                    $('<a></a>').append(
                                        $('<img>').prop('src', file.thumbnailUrl)
                                    )
                                );
                            }
                            row.find('a')
                                .attr('href', BASE_URL+file.url);
                            row.find('button.delete')
                                .attr('data-type', file.delete_type)
                                .attr('data-url', file.deleteUrl);

                        }
                        rows = rows.add(row);
                    });
                    return rows;
                }
            });

            // Enable iframe cross-domain access via redirect option:
            $('#fileupload-cad').fileupload(
                'option',
                'redirect',
                window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                )
            );

            // Upload server status check for browsers with CORS support:
//                if ($.support.cors) {
//                    $.ajax({
//                        type: 'HEAD'
//                    }).fail(function () {
//                        $('<div class="alert alert-danger"/>')
//                            .text('Upload server currently unavailable - ' +
//                                new Date())
//                            .appendTo('#fileupload-pdf');
//                    });
//                }

            // Load & display existing files:
            $('#fileupload-cad').addClass('fileupload-processing');
            //console.log('PRM_NOMOR_PRODUK = ' + $('#fileupload-cad [name="PRM_NOMOR_PRODUK"]').val());
            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: $('#fileupload-cad').attr("action"),
                data : {
                    nomor_produk : $('#fileupload-cad [name="PRM_NOMOR_PRODUK"]').val()
                },
                dataType: 'json',
                context: $('#fileupload-cad')[0]
            }).always(function (e) {
                $(this).removeClass('fileupload-processing');
            }).done(function (result) {
                $(this).fileupload('option', 'done')
                    .call(this, $.Event('done'), {result: result});
            });

            // delete files
            $('#fileupload-cad')
                .bind('fileuploaddestroy', function (e, data) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + data.url,
                        data:{
                            nomor_produk : $('#fileupload-cad [name="PRM_NOMOR_PRODUK"]').val()
                        },
                        success: function (response) {
                            alert('Gambar berhasil Dihapus');
                        }
                    });
                }
            );
        }
    };
}();

ManagementProductFile.init();

function testFunction(idFile){

    console.log('id_file = ' + idFile);

    $.ajax({
        type: "GET",
        url: BASE_URL + 'admin/product/set_primary_2D',
        data:{
            prm_nomor_produk : PRM_NOMOR_PRODUK,
            prm_id_file : idFile
        },
        success: function (response) {
            alert('Gambar berhasil Diaktifkan menjadi Gambar Utama');
            table.ajax.reload();
        }
    });

};


function hapusGambar2D(idProductFile){
    $.ajax({
        type: "POST",
        url: BASE_URL + 'admin/product/hapus_gambar_2D',
        data:{
            prm_id_product_file : idProductFile
        },
        success: function (response) {
            alert(response.message);
            table.ajax.reload();
        }
    });
};

