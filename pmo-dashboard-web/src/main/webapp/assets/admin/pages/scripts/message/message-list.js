var Message = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            Message.userTable();
            Message.ajaxMessage();
            Message.tanggalReply();
        },
        tanggalReply: function(){
            $("#prm_tgl_reply").val(new Date());

        },
        userTable: function () {
            var table = $('#message-table');

            table.dataTable({
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [ 0,2, 3, 4,5,7 ] },
                    {
                      "aTargets":[7],
                        "mRender":function(data,type,full){
                            if(data == 'SUDAH REPLY'){
                                return'<span class="label label-success">'+full.STATUS_AKTIF+'</span>';
                            }else{
                                return'<span class="label label-danger">'+full.STATUS_AKTIF+'</span>';
                            }
                        }
                    },

                    {
                        "aTargets": [ 8 ], // Column to target
                        "mRender": function ( data, type, full ) {
                            console.dir(full.NOMOR_PRODUK);
                            var share=' ';
                                if(full.NOMOR_PRODUK){
                                    share='<a href="'+BASE_URL+'/product/product-detail?id='+full.NOMOR_PRODUK+'" class="btn btn-sm btn-primary tooltips center" data-container="body" data-placement="top" data-original-title="Lihat Produk" data-toggle="modal"><i class="fa fa-share"></i></a>';
                                }
                            return ''+ share +''+
                                '<a href="#message-reading'+full.ID_KONTAK+'" class="btn btn-sm btn-success tooltips btn-message" data-container="body" data-placement="top" data-original-title="Baca" data-toggle="modal"><i class="fa fa-eye"></i></a>'+
                                '<a href="#message-delete-'+full.ID_KONTAK+'" class="btn btn-sm btn-danger tooltips btn-message" data-container="body" data-placement="top" data-original-title="Hapus" data-toggle="modal"><i class="fa fa-trash"></i></a>'+
                                '<a href="'+BASE_URL+'admin/message/reply?id='+full.ID_KONTAK+'" class="btn btn-sm btn-warning tooltips btn-message" data-container="body" data-placement="top" data-original-title="Reply" data-toggle="modal"><i class="fa fa-mail-reply"></i></a>'+
                                '<div id="message-reading'+full.ID_KONTAK+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">' +
                                '<div class="modal-dialog">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header btn-success">' +
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>' +
                                '<h4 class="modal-title">Pemesan : <strong>'+full.NAMA+'</strong></h4>' +
                                '</div>' +
                                '<div class="modal-body">' +
                                '<p style="margin: 0px">' +
                                '<h4><strong>NAMA PRODUK : </strong></h4>' +full.NAMA_PRODUK+'</p>'+
                                '<p style="margin: 0px">' +
                                '<h4><strong>Email : </strong></h4>'+full.EMAIL+'</P>' +
                                '<p style="margin: 0px">' +
                                '<h4><strong>Pesan : </strong></h4>'+full.PESAN+'</P>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +

                                '<div id="message-delete-'+full.ID_KONTAK+'" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">'+
                                '<div class="modal-dialog">'+
                                '<div class="modal-content">'+
                                '<div class="modal-header bg-red">'+
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>'+
                                '<h4 class="modal-title">'+full.NAMA+'</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                '<p style="margin: 0px">'+
                                'Apakah anda yakin akan menghapus permanen '+full.NAMA+' ?'+
                                '</p>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                '<button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Tidak</button>'+
                                '<a href="'+BASE_URL+'/admin/message/delete?id='+full.ID_KONTAK+'" class="btn btn-success">Ya</a>' +
                                '</div></div></div>';
                        }
                    }
                ],
//                "deferLoading": RECORDS_TOTAL,
                "pageLength": LENGTH,
                "ajax": {
                    "url": URL,
                    "type": "GET"
                },
                "columns": [
                    {"data": "ROW_NUMBER", "defaultContent": ""},
                    {"data": "NOMOR_PRODUK", "defaultContent": ""},
                    {"data": "NAMA", "defaultContent": ""},
                    {"data": "EMAIL", "defaultContent": ""},
                    {"data": "NO_TELP", "defaultContent": ""},
                    {"data": "PESAN", "defaultContent": ""},
                    {"data": "CREATED_DATE", "defaultContent": ""},
                    {"data": "STATUS_AKTIF", "defaultContent": ""}


                ],
                "lengthMenu": [
                    [5, 15, 20, RECORDS_TOTAL],
                    [5, 15, 20, "Semua"] // change per page values here
                ],
                "drawCallback": function( settings ) {
                    $('.tooltips').tooltip();
                    Message.ajaxMessage();
                }

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

// Remove table tool
            $('.DTTT').closest('.col-md-12').remove();
//            $('.DTTT .btn-group').remove();
        },


        // =========================================================================
        // HANDLE GANTI STATUS MENJADI 1
        // =========================================================================
        ajaxMessage:function(){
            $('#message-table .btn-message').on('click', function (e) {
                var i = $(this);
                $.ajax({
                    method: "GET",
                    url: BASE_URL+"admin/message/update?id="+$(this).data('id'),
                    success: function() {
                        i.closest("tr").addClass("success");
                    }
                });

            });
        }
    };
}();

Message.init();


