var ManagementProductImage = function () {

    return {
        // =========================================================================
        // CONSTRUCTOR APP
        // =========================================================================
        init: function () {
            ManagementProductImage.imageTable();
            ManagementProductImage.imageUpload360();
            ManagementProductImage.hapusImage3D();
            ManagementProductImage.initSelectJenisUploload ();

        },

        initSelectJenisUploload : function(){
            $('#select2-jns-upload-3D').on("select2:select", function (e) {
                console.log('id = ' + e.params.data.id);
                $('#my-dropzone [name="PRM_JENIS_UPLOAD"]').val(e.params.data.id)
            });
        },


        hapusImage3D : function(){
            $('#btn-hapus-image-3D').click(function(){
                var prm_nomor_produk = $('#my-dropzone [name="PRM_NOMOR_PRODUK"]').val();
                var prm_jns_upload = $('#my-dropzone [name="PRM_JENIS_UPLOAD"]').val();

                console.log('prm_nomor_produk = ' + prm_nomor_produk);
                console.log('prm_jns_upload = ' + prm_jns_upload);

                $.ajax({
                    type: 'POST',
                    url: BASE_URL + 'admin/product/hapus-image-3D',
                    data : {
                        prm_nomor_produk : prm_nomor_produk,
                        prm_jns_upload : prm_jns_upload
                    },
                    beforeSend: function () {
                        // loading pada product container
                        Metronic.blockUI({
                         target: $('#tab-historical-product table'),
                         boxed: true
                         });
                    },
                    success: function (response) {
                        //console.log(response);
                        alert(response.message);
                    }
                });


            });
        },

        // =========================================================================
        // HANDLE IMAGE DATATABLE PRODUCT
        // =========================================================================
        imageTable    :function () {
            var table = $('#image-table');

            /* Table tools samples: https://www.datatables.net/release-datatables/extras/TableTools/ */

            /* Set tabletools buttons and button container */

            $.extend(true, $.fn.DataTable.TableTools.classes, {
                "container": "btn-group tabletools-dropdown-on-portlet",
                "buttons": {
                    "normal": "btn btn-sm default",
                    "disabled": "btn btn-sm default disabled"
                },
                "collection": {
                    "container": "DTTT_dropdown dropdown-menu tabletools-dropdown-menu"
                }
            });
            var oTable = table.dataTable({

                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1, 2, 3, 4,5 ] } ]

            });

            var tableWrapper = $('#product-table_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper

            tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown
        },

        // =========================================================================
        // HANDLE IMAGE UPLOAD 360
        // =========================================================================
        imageUpload360  :function () {



            Dropzone.options.myDropzone = {
                paramName : 'fileImage3D',
                thumbnailWidth: 90,
                thumbnailHeight: 90,
                srcWidth: 10,
                srcHeight: 10,
                    init: function() {
                    this.on("addedfile", function(file) {
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function(e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();

                            // Remove the file preview.
                            _this.removeFile(file);
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });

                        // Add the button to the file preview element.
                        //file.previewElement.appendChild(removeButton);
                    });
                }
            }

        }
    };
}();

ManagementProductImage.init();


